﻿using System;
using System.Diagnostics;

namespace Fortis.Business.Infrastructure
{
    public class Monitor : IDisposable
    {
        // ----- Fields
        private readonly string _stepName;
        private readonly Stopwatch _watch;

        // ----- Constructors
        public static Monitor ElapsedTime(string stepName)
        {
            return new Monitor(stepName);
        }
        private Monitor(string stepName)
        {
            _stepName = stepName;
            _watch = new Stopwatch();
            _watch.Start();
        }

        // ----- Public methods
        public void Dispose()
        {
            _watch.Stop();
        }
    }
}
