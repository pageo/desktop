﻿using Windows.UI.Popups;

namespace Fortis.Business.Services
{
    public class DialogService : IDialogService
    {
        // ----- Public methods
        public void Show(string message)
        {
            var messageDialog = new MessageDialog(message);
            var result = messageDialog.ShowAsync();
        }
    }
}
