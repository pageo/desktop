﻿namespace Fortis.Business.Services
{
    public interface IDialogService
    {
        void Show(string message);
    }
}
