﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fortis.Business.ViewModels.Base
{
    public class AsyncRelayCommand : ICommand
    {
        // ----- Fields
        private readonly Func<Task<bool>> _canExecute;
        private readonly Func<Task> _execute;

        // ----- Contructors
        public AsyncRelayCommand(Func<Task> execute) : this(execute, null)
        {
        }
        public AsyncRelayCommand(Func<Task> execute, Func<Task<bool>> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        // ----- Public methods
        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute().Result;
        }
        public void Execute(object parameter)
        {
            _execute();
        }
        public void RaiseCanExecuteChanged()
        {
            EventHandler tmpHandle = CanExecuteChanged;
            tmpHandle?.Invoke(this, new EventArgs());
        }

        // ----- Events
        public event EventHandler CanExecuteChanged;
    }

    public class AsyncRelayCommand<T> : ICommand
    {
        // ----- Fields
        private readonly Func<T, Task<bool>> _canExecute;
        private readonly Func<T, Task> _execute;

        // ----- Contructors
        public AsyncRelayCommand(Func<T, Task> execute) : this(execute, null)
        {
        }
        public AsyncRelayCommand(Func<T, Task> execute, Func<T, Task<bool>> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        // ----- Public methods
        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute((T)parameter).Result;
        }
        public void Execute(object parameter)
        {
            _execute((T)parameter);
        }
        public void RaiseCanExecuteChanged()
        {
            var tmpHandle = CanExecuteChanged;
            tmpHandle?.Invoke(this, new EventArgs());
        }

        // ----- Events
        public event EventHandler CanExecuteChanged;
    }
}
