﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Fortis.Business.ViewModels.Base
{
    public class NotifiableObject : INotifyPropertyChanged
    {
        // ----- Fields
        private readonly IDictionary<string, object> _notifiableProperties = new Dictionary<string, object>();

        // ----- Events
        public event PropertyChangedEventHandler PropertyChanged;

        // ----- Public methods
        protected T GetNotifiableProperty<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException(nameof(propertyName));

            object propertyValue;
            if (_notifiableProperties.TryGetValue(propertyName, out propertyValue))
            {
                return (T)propertyValue;
            }
            return default(T);
        }
        protected bool SetNotifiableProperty<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException(nameof(propertyName));

            var oldValue = GetNotifiableProperty<T>(propertyName);
            if (Equals(oldValue, newValue))
            {
                return false;
            }

            _notifiableProperties[propertyName] = newValue;
            OnPropertyChanged(propertyName);
            return true;
        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException(nameof(propertyName));

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
