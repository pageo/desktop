﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace Fortis.Business.ViewModels.Base
{
    public class RelayCommand : ICommand
    {
        // ----- Fields
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        // ----- Constructors
        public RelayCommand(Action execute) : this(execute, null) { }
        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        // ----- Public methods
        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute();
        }
        public void Execute(object parameter)
        {
            _execute();
        }
        public void RaiseCanExecuteChanged()
        {
            var tmpHandle = CanExecuteChanged;
            tmpHandle?.Invoke(this, new EventArgs());
        }

        // ----- Events
        public event EventHandler CanExecuteChanged;
    }

    public class RelayCommand<T> : ICommand
    {
        // ----- Fields
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        // ----- Constructors
        public RelayCommand(Action<T> execute) : this(execute, null) { }
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        // ----- Public methods
        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute((T)parameter);
        }
        public void Execute(object parameter)
        {
            _execute((T)parameter);
        }
        public void RaiseCanExecuteChanged()
        {
            var tmpHandle = CanExecuteChanged;
            tmpHandle?.Invoke(this, new EventArgs());
        }

        // ----- Events
        public event EventHandler CanExecuteChanged;
    }
}

