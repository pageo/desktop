﻿using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Fortis.Business.ViewModels.Base
{
    public abstract class ViewModelBase : ThreadSafeNotifiableObject
    {
        // ----- Properties
        public Frame AppFrame { get; set; }
        public bool IsBusy
        {
            get { return GetNotifiableProperty<bool>(); }
            private set { SetNotifiableProperty(value); }
        }

        // ----- Abstract methods
        public abstract Task OnNavigatedFrom(NavigationEventArgs args);
        public abstract Task OnNavigatedTo(NavigationEventArgs args);

        // ----- Public methods
        public void SetAppFrame(Frame viewFrame)
        {
            AppFrame = viewFrame;
        }
    }
}
