﻿using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel;
using Windows.UI.Xaml.Navigation;
using Fortis.Business.Services;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IDialogService _dialogService;

        // ----- Properties
        public string HelloWorld
        {
            get { return GetNotifiableProperty<string>(); }
            private set { SetNotifiableProperty(value); }
        }

        // ----- Commands
        public ICommand ShowMessageCommand { get; }

        // ----- Constructors
        public MainViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
            HelloWorld = DesignMode.DesignModeEnabled ? "Runs in design mode" : "Runs in running mode";

            ShowMessageCommand = new RelayCommand(ShowMessage);
        }

        // ----- Implementation commands
        private void ShowMessage()
        {
            _dialogService.Show("Hello World!");
        }

        // ----- Override methods
        public override Task OnNavigatedFrom(NavigationEventArgs args)
        {
            return null;
        }
        public override Task OnNavigatedTo(NavigationEventArgs args)
        {
            return null;
        }
    }
}
