﻿using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml.Navigation;
using Fortis.Business.Services;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.ViewModels
{
    public class TopBarViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IDialogService _dialogService;

        // ----- Commands
        public ICommand SaveCommand { get; }
        public ICommand AddCommand { get; }
        public ICommand BackCommand { get; }
        public ICommand SendCommand { get; }
        public ICommand SettingCommand { get; }

        // ----- Constructors
        public TopBarViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;

            SaveCommand = new RelayCommand(Save);
            AddCommand = new RelayCommand(Add);
            BackCommand = new RelayCommand(Back);
            SendCommand = new RelayCommand(Send);
            SettingCommand = new RelayCommand(Setting);
        }

        // ----- Override methods
        public override Task OnNavigatedFrom(NavigationEventArgs args)
        {
            return null;
        }

        public override Task OnNavigatedTo(NavigationEventArgs args)
        {
            return null;
        }

        // ----- Implementation commands
        private void Setting()
        {
            _dialogService.Show("Settings");
        }
        private void Send()
        {
            _dialogService.Show("Send");
        }
        private void Back()
        {
            _dialogService.Show("Back");
        }
        private void Add()
        {
            _dialogService.Show("Add");
        }
        private void Save()
        {
            _dialogService.Show("Save");
        }
    }
}
