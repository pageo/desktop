﻿using System;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Fortis.UWP
{
    sealed partial class App : Application
    {
        // ----- Contructors
        public App()
        {
            InitializeComponent();
            Suspending += OnSuspending;
        }

        // ----- Override methods
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
                DebugSettings.EnableFrameRateCounter = true;
#endif
            var rootFrame = Window.Current.Content as Frame;
            if (rootFrame == null) {
                rootFrame = new Frame {
                    Language =
                        Windows.Globalization.ApplicationLanguages.Languages.FirstOrDefault(x => x.Contains("en-US")),
                    CacheSize = 1
                };
                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated) {
                    //TODO: Load state from previously suspended application
                }

                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated) return;

            if (rootFrame.Content == null) {
                if (!rootFrame.Navigate(typeof(Views.MainPage), e.Arguments))
                    throw new Exception("Failed to create initial page");
            }
            Window.Current.Activate();
        }

        // ----- Callbacks
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }
        void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }
    }
}
