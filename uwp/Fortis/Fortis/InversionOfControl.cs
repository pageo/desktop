﻿using Fortis.Business.Infrastructure;
using Fortis.Business.Services;
using Fortis.Business.ViewModels;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace Fortis.UWP
{
    public class InversionOfControl
    {
        // ----- Fields
        private readonly IUnityContainer _container;

        // ----- Properties
        public static InversionOfControl Instance { get; } = new InversionOfControl();

        // ----- Constructors
        public InversionOfControl()
        {
            _container = new UnityContainer();

            // ViewModels
            _container.RegisterType<MainViewModel>();
            _container.RegisterType<TopBarViewModel>();

            // UI
            _container.RegisterType<IDialogService, DialogService>(new ContainerControlledLifetimeManager());

            // Business
            _container.RegisterType<IBus, Bus>(new ContainerControlledLifetimeManager());
        }

        // ----- Public methods
        public T GetInstance<T>()
        {
            return _container.Resolve<T>();
        }
        public IServiceLocator ServiceLocator()
        {
            return new UnityServiceLocator(_container);
        }
    }
}
