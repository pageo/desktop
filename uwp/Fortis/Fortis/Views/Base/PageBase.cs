﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Fortis.Business.ViewModels.Base;

namespace Fortis.UWP.Views.Base
{
    public class PageBase : Page
    {
        // ----- Fields
        private ViewModelBase _vm;

        // ----- Override methods
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            _vm = DataContext as ViewModelBase;
            _vm?.SetAppFrame(Frame);
            _vm?.OnNavigatedTo(e);
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            _vm.OnNavigatedFrom(e);
        }
    }
}
