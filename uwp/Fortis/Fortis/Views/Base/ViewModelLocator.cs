﻿using Fortis.Business.ViewModels;
using Microsoft.Practices.ServiceLocation;

namespace Fortis.UWP.Views.Base
{
    public class ViewModelLocator
    {
        // ----- Properties
        public MainViewModel Main => InversionOfControl.Instance.GetInstance<MainViewModel>();
        public TopBarViewModel TopBar => InversionOfControl.Instance.GetInstance<TopBarViewModel>();

        // ----- Constructors
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => InversionOfControl.Instance.ServiceLocator());
        }
    }
}
