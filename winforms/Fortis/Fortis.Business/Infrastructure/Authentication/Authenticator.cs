﻿using System.Security.Authentication;

namespace Fortis.Business.Infrastructure.Authentication
{
    public class Authenticator
    {
        // ----- Fields
        private const string GroupName = "App_Carthage";
        private readonly IAuthenticationService _authenticationService;

        // ----- Constructors
        public Authenticator(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        // ----- Public methods
        public void Authenticate()
        {
            var isConnected = _authenticationService.ConnectUser();
            if (isConnected == false)
                throw new AuthenticationException(
                    @"Vos informations personnelles n'ont pas pu être enregistrées dans Carthage. Merci de contacter votre administrateur.");
        }
    }
}
