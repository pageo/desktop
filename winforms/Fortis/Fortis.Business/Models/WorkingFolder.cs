﻿using System;

namespace Fortis.Business.Models
{
    public class WorkingFolder
    {
        // ----- Properties
        public int Id { get; }
        public string Name { get; }
        public string OwnerName { get; }
        public DateTime CreationDate { get; }

        // ----- Constructors
        public WorkingFolder(WorkingFolderDetails details)
        {
            if (details == null) throw new ArgumentNullException(nameof(details));

            Id = details.Id;
            Name = details.Name;
            OwnerName = details.OwnerName;
            CreationDate = details.CreationDate;
        }
    }
}
