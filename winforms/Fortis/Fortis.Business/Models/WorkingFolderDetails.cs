﻿using System;

namespace Fortis.Business.Models
{
    public class WorkingFolderDetails
    {
        // ----- Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string OwnerName { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
