﻿using System;

namespace Fortis.Business.Models
{
    public class WorkingFolderListItem
    {
        // ----- Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsOwner { get; set; }
    }
}
