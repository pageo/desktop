﻿using System;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.Models
{
    public class WorkingFolderSelection : NotifiableObject
    {
        // ----- Properties
        public bool Selected
        {
            get { return GetNotifiableProperty<bool>(); }
            set { SetNotifiableProperty(value); }
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Deleted { get; set; }
        public bool IsOwner { get; set; }
    }
}
