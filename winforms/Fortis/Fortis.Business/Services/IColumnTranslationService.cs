﻿namespace Fortis.Business.Services
{
    public interface IColumnTranslationService
    {
        string GetFrenchColumn<T>(string name);
    }
}
