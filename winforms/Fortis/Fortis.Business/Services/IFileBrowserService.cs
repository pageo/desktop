﻿namespace Fortis.Business.Services
{
    public interface IFileBrowserService
    {
        string Browse(string fileExtensions);
    }
}
