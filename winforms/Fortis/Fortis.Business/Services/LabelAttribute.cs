﻿using System;

namespace Fortis.Business.Services
{
    public class LabelAttribute : Attribute
    {
        // ----- Fields
        public string Name { get; set; }

        // ----- Constructors
        public LabelAttribute(string name)
        {
            Name = name;
        }
    }
}
