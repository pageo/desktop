﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Fortis.Business.Utils
{
    public class CustomBindingList<T> : BindingList<T>
    {
        // ----- Public methods
        public void AddRange(IEnumerable<T> elements)
        {
            RaiseListChangedEvents = false;
            foreach (var element in elements) {
                Add(element);
            }
            RaiseListChangedEvents = true;
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }
        public void RemoveRange(IEnumerable<T> elements)
        {
            RaiseListChangedEvents = false;
            foreach (var element in elements) {
                Remove(element);
            }
            RaiseListChangedEvents = true;
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }
        public int RemoveAll(Predicate<T> predicate)
        {
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));

            return this
                .Where(x => predicate(x))
                .ToArray()
                .Select(x => Remove(x) ? 1 : 0)
                .Sum();
        }

        // ----- Override methods
        protected override void RemoveItem(int index)
        {
            if (RaiseListChangedEvents) {
                var itemToDelete = Items[index];
                base.RemoveItem(index);
                base.OnListChanged(new ItemDeletedEventArgs(index, itemToDelete));
            }
            else {
                base.RemoveItem(index);
            }
        }
        protected override void OnListChanged(ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemDeleted)
                return;
            base.OnListChanged(e);
        }
    }
}
