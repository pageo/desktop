﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Fortis.Business.Utils
{
    public static class IEnumerableExtensions
    {
        public static bool Contains(this IEnumerable<string> collection, string value, StringComparison comparison)
        {
            return collection.Any(x => x.Equals(value, comparison));
        }
    }
}
