﻿using System.ComponentModel;

namespace Fortis.Business.Utils
{
    public class ItemDeletedEventArgs : ListChangedEventArgs
    {
        // ----- Properties
        public object DeletedItem { get; }

        // ----- Constructors
        public ItemDeletedEventArgs(int newIndex, object deletedItem) : base(ListChangedType.ItemDeleted, newIndex)
        {
            DeletedItem = deletedItem;
        }
    }
}
