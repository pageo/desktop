﻿using System;

namespace Fortis.Business.Utils
{
    public static class TimespanExtensions
    {
        // ----- Public methods
        public static string ToUserFriendlyText(this TimeSpan timespan)
        {
            if (timespan.TotalDays > 1)
                return $"{timespan.Days} jours {timespan.Hours} heures";
            if (timespan.TotalHours > 1)
                return $"{timespan.Hours} heures {timespan.Minutes} minutes";
            if (timespan.TotalMinutes > 1)
                return $"{timespan.Minutes} minutes {timespan.Seconds} secondes";
            return $"{timespan.Seconds} secondes";
        }
    }
}