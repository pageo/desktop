﻿using Fortis.Business.Base;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.ViewModels
{
    public class ActionViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;

        // ----- Constructors
        public ActionViewModel(IBus bus)
        {
            _bus = bus;
        }

        // ----- Overrides
        public override void Initialize()
        {

        }
        public override void Clean()
        {
            _bus.Unregister(this);
        }
    }
}
