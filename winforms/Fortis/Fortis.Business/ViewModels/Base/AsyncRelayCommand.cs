﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fortis.Business.ViewModels.Base
{
    public class AsyncRelayCommand : ICommand, IAsyncCommand
    {
        // ----- Fields
        private readonly Func<Task> _action;
        private readonly Func<bool> _predicate;

        // ----- Constructors
        public AsyncRelayCommand(Func<Task> action, Func<bool> predicate = null)
        {
            _action = action;
            _predicate = predicate;
        }

        // ----- Public methods
        public async Task ExecuteAsync()
        {
            if (CanExecute()) {
                await _action();
            }
        }

        public bool CanExecute()
        {
            return _predicate == null || _predicate();
        }

        // ----- Implementation commands
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        async void ICommand.Execute(object parameter)
        {
            await ExecuteAsync();
        }

        event EventHandler ICommand.CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}