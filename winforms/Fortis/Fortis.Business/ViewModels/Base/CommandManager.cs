﻿using System;
using Fortis.Business.Base;

namespace Fortis.Business.ViewModels.Base
{
    public class CommandManager
    {
        // ----- Fields
        public static event EventHandler RequerySuggested;

        // ----- Public methods
        public static void Configure(INotifyApplicationStateChanged notifyApplicationStateChanged)
        {
            if (notifyApplicationStateChanged == null)
                throw new ArgumentNullException(nameof(notifyApplicationStateChanged));

            notifyApplicationStateChanged.Idle += ApplicationOnIdle;
        }
        public static void InvalidateRequerySuggested()
        {
            RequerySuggested?.Invoke(new object(), EventArgs.Empty);
        }

        // ----- Internal logics
        private static void ApplicationOnIdle(object sender, EventArgs eventArgs)
        {
            InvalidateRequerySuggested();
        }
    }
}