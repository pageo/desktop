﻿using System.Threading.Tasks;

namespace Fortis.Business.ViewModels.Base
{
    public interface IAsyncCommand
    {
        Task ExecuteAsync();
        bool CanExecute();
    }
}