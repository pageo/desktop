﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Fortis.Business.ViewModels.Base
{
    public abstract class ValidationViewModel : ThreadSafeNotifiableObject, IDataErrorInfo
    {
        // ----- Fields
        private readonly Dictionary<string, List<IValidationRule>> _validationRules;
        private readonly Dictionary<string, string> _errors = new Dictionary<string, string>();

        // ----- Properties
        public bool HasValidationErrors
        {
            get => GetNotifiableProperty<bool>();
            private set => SetNotifiableProperty(value);
        }

        public string Error
        {
            get => GetNotifiableProperty<string>();
            private set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        protected ValidationViewModel()
        {
            var builder = new ValidationRuleBuilder();
            // ReSharper disable once VirtualMemberCallInContructor
            ConfigureValidationRules(builder);
            _validationRules = builder.Build();
        }

        // ----- IDataErrorInfo
        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                string error;
                if (_errors.TryGetValue(propertyName, out error))
                    return error;
                return null;
            }
        }

        // ----- Validation methods
        public void ShowValidationErrors()
        {
            foreach (var propertyName in _validationRules.Keys)
            {
                EvaluateRules(propertyName);
                base.OnPropertyChanged(propertyName);
            }
            UpdateValidationProperties();
        }

        public void ClearValidationErrors()
        {
            _errors.Clear();
            foreach (var propertyName in _validationRules.Keys)
                base.OnPropertyChanged(propertyName);
            UpdateValidationProperties();
        }

        protected bool AreAllValidationRulesValid()
        {
            return _validationRules.Values.SelectMany(x => x).All(x => x.IsValid());
        }

        // ----- Override / Abstract
        protected override void OnPropertyChanged(string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException(nameof(propertyName));

            if (propertyName != "Error" && propertyName != "HasValidationErrors")
                EvaluateRules(propertyName);

            base.OnPropertyChanged(propertyName);

            if (propertyName != "Error" && propertyName != "HasValidationErrors")
                UpdateValidationProperties();
        }

        protected virtual void ConfigureValidationRules(ValidationRuleBuilder builder)
        {
        }

        // ----- Internal logics
        private void EvaluateRules(string propertyName)
        {
            List<IValidationRule> rules;
            if (_validationRules.TryGetValue(propertyName, out rules)) {
                if (_errors.ContainsKey(propertyName) == false)
                    _errors.Add(propertyName, null);
                var firstOrDefault = rules.FirstOrDefault(validationRule => !validationRule.IsValid());
                if (firstOrDefault != null)
                    _errors[propertyName] = firstOrDefault.ErrorMessage;
            }
        }

        private void EvaluateAllRules()
        {
            foreach (var propertyName in _validationRules.Keys)
                EvaluateRules(propertyName);
        }

        private void UpdateValidationProperties()
        {
            Error = _errors.Values.FirstOrDefault(error => string.IsNullOrEmpty(error) == false);
            HasValidationErrors = _errors.Values.Any(error => string.IsNullOrEmpty(error) == false);
        }
    }
}