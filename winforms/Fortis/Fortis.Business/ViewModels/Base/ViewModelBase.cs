﻿namespace Fortis.Business.ViewModels.Base
{
    public abstract class ViewModelBase : ValidationViewModel
    {
        public abstract void Initialize();
        public abstract void Clean();
    }
}
