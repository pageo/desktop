﻿using Fortis.Business.Base;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.ViewModels
{
    public class BottomBarViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;

        // ----- Properties
        public int ElementCount
        {
            get => GetNotifiableProperty<int>();
            private set => SetNotifiableProperty(value);
        }
        public int EllapsedTimeSeconds
        {
            get => GetNotifiableProperty<int>();
            private set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        public BottomBarViewModel(IBus bus)
        {
            _bus = bus;
        }

        // ----- Overrides
        public override void Initialize()
        {
        }
        public override void Clean()
        {
            _bus.Unregister(this);
        }
    }
}