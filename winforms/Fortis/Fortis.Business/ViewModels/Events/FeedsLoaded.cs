﻿using System.Collections.Generic;
using Fortis.Business.Feeds;

namespace Fortis.Business.ViewModels.Events
{
    public class FeedsLoaded
    {
        // ----- Properties
        public IEnumerable<Feed> Feeds { get; set; }

        // ----- Constructors
        public FeedsLoaded(IEnumerable<Feed> feeds)
        {
            Feeds = feeds;
        }
    }
}
