﻿using Fortis.Business.Models;

namespace Fortis.Business.ViewModels.Events
{
    public class WorkingFolderLoaded
    {
        // ----- Properties
        public WorkingFolder WorkingFolder { get; }

        // ----- Constructors
        public WorkingFolderLoaded(WorkingFolder workingFolder)
        {
            WorkingFolder = workingFolder;
        }
    }
}
