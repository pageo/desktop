namespace Fortis.Business.Feeds
{
    public class Feed
    {
        // ----- Properties
        public string Name { get; set; }
        public string Image { get; set; }
        public string Status { get; set; }
        public string ProfilePic { get; set; }
        public string Url { get; set; }
    }
}