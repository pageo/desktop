﻿using System.Collections.Generic;

namespace Fortis.Business.Feeds
{
    public interface IFeedRepository
    {
        IEnumerable<Feed> GetFeeds();
        void SaveFeeds(IEnumerable<Feed> feeds);
    }
}