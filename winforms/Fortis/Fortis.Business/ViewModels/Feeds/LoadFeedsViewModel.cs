﻿using System.Collections.Generic;
using System.Linq;
using Fortis.Business.Base;
using Fortis.Business.Feeds;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;

namespace Fortis.Business.ViewModels.Feeds
{
    public class LoadFeedsViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;

        // ----- Properties
        public IEnumerable<Feed> Feeds
        {
            get => GetNotifiableProperty<IEnumerable<Feed>>();
            set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        public LoadFeedsViewModel(IBus bus)
        {
            _bus = bus;
        }

        // ----- Override methods
        public override void Initialize()
        {
            _bus.Register<FeedsLoaded>(this, OnFeedsLoaded);
        }
        public override void Clean()
        {
            _bus.Unregister(this);

            Feeds = Enumerable.Empty<Feed>();
        }

        // ----- Callbacks
        private void OnFeedsLoaded(FeedsLoaded @event)
        {
            Feeds = @event.Feeds;
        }
    }
}
