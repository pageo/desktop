﻿using System.Windows.Input;
using Fortis.Business.Base;
using Fortis.Business.Feeds;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.ViewModels
{
    public class LoadViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IFeedRepository _feedRepository;

        // ----- Commands
        public ICommand ShowCommand { get; }

        // ----- Properties
        public bool IsVisible
        {
            get => GetNotifiableProperty<bool>();
            set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        public LoadViewModel(
            IBus bus, 
            IFeedRepository feedRepository)
        {
            _bus = bus;
            _feedRepository = feedRepository;

            ShowCommand = new RelayCommand(Show);
        }

        // ----- Override methods
        public override void Initialize()
        {
            //_bus.Send(new FeedsLoaded(_feedRepository.GetFeeds()));
        }
        public override void Clean()
        {
            _bus.Unregister(this);
        }

        // ----- Implementation commands
        private void Show()
        {
            IsVisible = true;
        }
    }
}