﻿using System.Windows.Input;
using Fortis.Business.Base;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Messages;

namespace Fortis.Business.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;

        // ----- Commands
        public ICommand ExitCommand { get; }

        // ----- Constructors
        public MainViewModel(
            IBus bus)
        {
            _bus = bus;

            ExitCommand = new RelayCommand(Exit);
        }

        // ----- Override methods
        public override void Initialize()
        {
        }
        public override void Clean()
        {
            _bus.Unregister(this);
        }

        // ----- Implementation commands
        private void Exit()
        {
            _bus.Send(new ExitMessage());
        }
    }
}