﻿namespace Fortis.Business.ViewModels.Messages
{
    public class CloseWorkingFolderCommand
    {
        // ----- Properties
        public bool IsCancelled { get; set; }
    }
}
