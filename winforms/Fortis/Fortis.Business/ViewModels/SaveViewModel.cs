﻿using System.Collections.Generic;
using System.Windows.Input;
using Fortis.Business.Base;
using Fortis.Business.Feeds;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;

namespace Fortis.Business.ViewModels
{
    public class SaveViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IFeedRepository _feedRepository;
        private IEnumerable<Feed> _feeds;

        // ----- Commands
        public ICommand SaveCommand { get; set; }

        // ----- Constructors
        public SaveViewModel(IBus bus, IFeedRepository feedRepository)
        {
            _bus = bus;
            _feedRepository = feedRepository;
            SaveCommand = new RelayCommand(Save, CanSave);
        }

        // -------- Public methods
        public override void Initialize()
        {
            _bus.Register<FeedsLoaded>(this, OnFeedsLoaded);
        }

        public override void Clean()
        {
            _bus.Unregister(this);
        }

        // -------- Implementation commands
        private bool CanSave()
        {
            return true;
        }

        private void Save()
        {
            _feedRepository.SaveFeeds(_feeds);
        }

        // ----- Callbacks
        private void OnFeedsLoaded(FeedsLoaded @event)
        {
            _feeds = @event.Feeds;
        }
    }
}