﻿using System;
using System.Threading.Tasks;
using Fortis.Business.Base;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Services;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;
using Fortis.Business.ViewModels.Messages;
using Fortis.Business.ViewModels.WorkingFolders.Exceptions;

namespace Fortis.Business.ViewModels.WorkingFolders
{
    public class CreateAndLoadNewWorkingFolderViewModel : AsyncViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IWorkingFolderRepository _workingFolderRepository;
        private readonly IMessageBoxService _messageBoxService;
        private readonly ICollectionFactory _collectionFactory;

        // ----- Properties
        public string Name
        {
            get => GetNotifiableProperty<string>();
            set => SetNotifiableProperty(value);
        }

        // ----- Commands
        public AsyncRelayCommand CreateAndLoadWorkingFolderCommand { get; }

        // ----- Constructor
        public CreateAndLoadNewWorkingFolderViewModel(
            IBus bus,
            IWorkingFolderRepository workingFolderRepository,
            IMessageBoxService messageBoxService,
            ICollectionFactory collectionFactory)
        {
            _bus = bus;
            _workingFolderRepository = workingFolderRepository;
            _messageBoxService = messageBoxService;
            _collectionFactory = collectionFactory;

            CreateAndLoadWorkingFolderCommand = new AsyncRelayCommand(CreateAndLoadWorkingFolderAsync, CanCreateAndLoadWorkingFolder);
        }

        // ----- Public methods
        public override Task Initialize()
        {
            return Task.FromResult(0);
        }
        protected override void ConfigureValidationRules(ValidationRuleBuilder builder)
        {
            builder.Add(() => string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name), "Veuillez choisir un nom pour le dossier.");
        }
        public override void Clean()
        {
            Name = null;

            ClearValidationErrors();
        }

        // ----- Internal logics
        private bool CanCreateAndLoadWorkingFolder()
        {
            return AreAllValidationRulesValid();
        }
        private async Task CreateAndLoadWorkingFolderAsync()
        {
            try
            {
                _bus.Send(new CloseWorkingFolderCommand());
                _bus.Send(new WorkingFolderLoading());

                var @event = await RunAsync(() => {
                    var workingFolderId = _workingFolderRepository.Create(Name);
                    var workingFolder = _workingFolderRepository.Load(workingFolderId);
                    return new WorkingFolderLoaded(workingFolder);
                });

                _bus.Send(@event);
            }
            catch (WorkingFolderWithSameNameAlreadyExists) {
                _messageBoxService.ShowInformation($"Un dossier de travail avec le nom \"{Name}\" existe déjà. {Environment.NewLine}Veuillez saisir un autre nom.");
            }
        }
    }
}
