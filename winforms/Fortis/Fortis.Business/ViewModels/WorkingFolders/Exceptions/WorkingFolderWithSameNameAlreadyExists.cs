﻿using System;

namespace Fortis.Business.ViewModels.WorkingFolders.Exceptions
{
    public class WorkingFolderWithSameNameAlreadyExists : Exception { }
}
