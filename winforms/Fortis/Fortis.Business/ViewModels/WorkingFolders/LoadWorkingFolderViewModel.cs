﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fortis.Business.Base;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Models;
using Fortis.Business.Services;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;
using Fortis.Business.ViewModels.Messages;

namespace Fortis.Business.ViewModels.WorkingFolders
{
    public class LoadWorkingFolderViewModel : AsyncViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IWorkingFolderRepository _workingFolderRepository;

        // ----- Properties
        public List<WorkingFolderListItem> ExistingWorkingFolders
        {
            get => GetNotifiableProperty<List<WorkingFolderListItem>>();
            private set => SetNotifiableProperty(value);
        }
        public WorkingFolderListItem SelectedExistingWorkingFolder
        {
            get => GetNotifiableProperty<WorkingFolderListItem>();
            set => SetNotifiableProperty(value);
        }
        public IAsyncCommand LoadWorkingFolderCommand { get; }

        // ----- Constructors
        public LoadWorkingFolderViewModel(
            IBus bus,
            IWorkingFolderRepository workingFolderRepository,
            ICollectionFactory collectionFactory)
        {
            _bus = bus;
            _workingFolderRepository = workingFolderRepository;

            LoadWorkingFolderCommand = new AsyncRelayCommand(LoadWorkingFolder, () => SelectedExistingWorkingFolder != null);
            ExistingWorkingFolders = new List<WorkingFolderListItem>();
        }

        // ----- Public methods
        public override async Task Initialize()
        {
            ExistingWorkingFolders = await RunAsync(() => _workingFolderRepository.GetList().OrderBy(x => x.Id).ToList());

            _bus.Register<WorkingFolderDeleted>(this, OnWorkingFolderListItemUpdated);
        }
        public override void Clean()
        {
            ExistingWorkingFolders = new List<WorkingFolderListItem>();
            SelectedExistingWorkingFolder = null;
        }

        // ----- Command implementations
        private async Task LoadWorkingFolder()
        {
            _bus.Send(new CloseWorkingFolderCommand());
            _bus.Send(new WorkingFolderLoading());

            var @event = await RunAsync(() => {
                var selectedExistingWorkingFolder = SelectedExistingWorkingFolder;
                var workingFolder = _workingFolderRepository.Load(selectedExistingWorkingFolder.Id);
                return new WorkingFolderLoaded(workingFolder);
            });

            _bus.Send(@event);
        }

        // ----- Callbacks
        private void OnWorkingFolderListItemUpdated(WorkingFolderDeleted @event)
        {
            ExistingWorkingFolders.RemoveAll(x => @event.WorkingFoldersIds.Contains(x.Id));
        }
    }
}
