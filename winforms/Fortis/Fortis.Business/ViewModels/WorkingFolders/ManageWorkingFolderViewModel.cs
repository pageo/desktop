﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Fortis.Business.Base;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Models;
using Fortis.Business.Utils;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;

namespace Fortis.Business.ViewModels.WorkingFolders
{
    public class ManageWorkingFolderViewModel : AsyncViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IMessageBoxService _messageBoxService;
        private readonly IWorkingFolderRepository _workingFolderRepository;
        private List<WorkingFolderListItem> _existingWorkingFolders;

        // ----- Properties
        public bool IsVisible
        {
            get => GetNotifiableProperty<bool>();
            set => SetNotifiableProperty(value);
        }
        public CustomBindingList<WorkingFolderSelection> WorkingFolders
        {
            get => GetNotifiableProperty<CustomBindingList<WorkingFolderSelection>>();
            set => SetNotifiableProperty(value);
        }
        public WorkingFolder WorkingFolderLoaded { get; set; }

        // ----- Commands
        public ICommand DeleteCommand { get; }
        public ICommand CancelCommand { get; }
        public ICommand SelectAllCommand { get; }
        public ICommand UnSelectAllCommand { get; }
        public ICommand ShowCommand { get; }

        // ----- Constructors
        public ManageWorkingFolderViewModel(
            IBus bus,
            IMessageBoxService messageBoxService,
            IWorkingFolderRepository workingFolderRepository)
        {
            _bus = bus;
            _messageBoxService = messageBoxService;
            _workingFolderRepository = workingFolderRepository;

            DeleteCommand = new RelayCommand(DeleteSelection, () => WorkingFolders != null && WorkingFolders.Any(x => x.Selected));
            CancelCommand = new RelayCommand(Cancel);

            SelectAllCommand = new RelayCommand(SelectAll);
            UnSelectAllCommand = new RelayCommand(UnSelectAll);

            ShowCommand = new RelayCommand(Show, () => _existingWorkingFolders != null);

            _bus.Register<WorkingFolderLoaded>(this, OnWorkingFolderLoaded);
            _bus.Register<WorkingFolderUnLoaded>(this, OnWorkingFolderUnLoaded);

            WorkingFolders = new CustomBindingList<WorkingFolderSelection>();
        }

        // ----- Overrides methods
        public override async Task Initialize()
        {
            await InitWorkingFoldersList();
        }
        public override void Clean()
        {
            _bus.Unregister(this);
            WorkingFolders.Clear();
        }

        // ----- Commands implementations
        private void DeleteSelection()
        {
            DeleteWorkingFolders();
            UpdateListsWorkingFolders();
        }
        private void Show()
        {
            IsVisible = true;
        }
        private void Cancel()
        {
            IsVisible = false;
        }
        private void SelectAll()
        {
            foreach (var displayedWorkingFolder in WorkingFolders)
                displayedWorkingFolder.Selected = true;
        }
        private void UnSelectAll()
        {
            foreach (var displayedWorkingFolder in WorkingFolders)
                displayedWorkingFolder.Selected = false;
        }

        // ----- Callbacks
        private void OnWorkingFolderLoaded(WorkingFolderLoaded @event)
        {
            WorkingFolderLoaded = @event.WorkingFolder;
        }
        private void OnWorkingFolderUnLoaded(WorkingFolderUnLoaded @event)
        {
            WorkingFolderLoaded = null;
        }

        // ----- Private methods
        private async Task InitWorkingFoldersList()
        {
            WorkingFolders.Clear();

            _existingWorkingFolders =
                await RunAsync(() => _workingFolderRepository.GetList().OrderBy(x => x.Id).ToList());
            var workingFolders = _existingWorkingFolders
                .Where(x => x.IsOwner)
                .Select(x => new WorkingFolderSelection {
                    Id = x.Id,
                    Name = x.Name,
                    CreationDate = x.CreationDate,
                    IsOwner = x.IsOwner,
                    Selected = false,
                    Deleted = false
                })
                .ToList();

            WorkingFolders.AddRange(workingFolders);
        }
        private void UpdateListsWorkingFolders()
        {
            var workingFoldersToDelete = WorkingFolders.Where(x => x.Deleted).ToList();
            _bus.Send(new WorkingFolderDeleted(workingFoldersToDelete.Select(x => x.Id)));

            foreach (var workingFolderToDelete in workingFoldersToDelete) {
                WorkingFolders.Remove(workingFolderToDelete);
            }
        }
        private void DeleteWorkingFolders()
        {
            var answerToDelete =
                _messageBoxService.AskQuestion("Êtes-vous sûr de vouloir supprimer le(s) dossier(s) sélectionné(s) ?");
            if (answerToDelete != UserAnswer.Yes) return;

            var workingFoldersSelected = WorkingFolders.Where(x => x.Selected);
            foreach (var workingfolderToDelete in workingFoldersSelected) {
                if (WorkingFolderLoaded?.Id == workingfolderToDelete.Id) {
                    _messageBoxService.ShowInformation(
                        $"Le dossier chargé '{workingfolderToDelete.Name}' ne peut être supprimé !");
                    workingfolderToDelete.Selected = false;
                    continue;
                }

                try
                {
                    _workingFolderRepository.Delete(workingfolderToDelete.Id);
                    workingfolderToDelete.Deleted = true;
                    workingfolderToDelete.Selected = false;
                }
                catch (Exception)
                {
                    _messageBoxService.ShowInformation(
                        $"Erreur lors de la suppression du dossier de travail de nom \"{workingfolderToDelete.Name}\".");
                }
            }
        }
    }
}