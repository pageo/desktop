﻿using System;

namespace Fortis.Common.Collections
{
    public class CollectionChangeEventArgs : EventArgs
    {
        // ----- Enums
        public enum CollectionChangeAction
        {
            None,
            RecordModified,
            RecordsModified,
            RecordAdded,
            RecordDeleted,
            CurrentRecordChanged,
            CollectionCleared,
            CollectionCreated,
            CollectionDeleted,
            CollectionAdded,
            CollectionDeleting,
        }

        // ----- Properties
        public ViewCollection Collection { get; }
        public Record Record { get; }
        public string FieldName { get; }
        public CollectionChangeAction Action { get; } = CollectionChangeAction.None;

        // ----- Constructors
        public CollectionChangeEventArgs()
        {
        }
        public CollectionChangeEventArgs(CollectionChangeAction action)
        {
            Action = action;
        }
        public CollectionChangeEventArgs(ViewCollection collection, CollectionChangeAction action)
        {
            Collection = collection;
            Action = action;
        }
        public CollectionChangeEventArgs(Record record, string fieldname, CollectionChangeAction action)
        {
            Record = record;
            if (record != null)
                Collection = record.Collection;
            FieldName = fieldname;
            Action = action;
        }
    }
}