﻿using System;
using System.Collections.Generic;

namespace Fortis.Common.Collections
{
    public class CollectionManager
    {
        // ----- Fields
        private static CollectionManager _instance;
        private static readonly object Lock = new object();
        private readonly List<ViewCollection> _collections;

        // ----- Properties
        public static CollectionManager Instance
        {
            get
            {
                lock (Lock)
                    return _instance ?? (_instance = new CollectionManager());
            }
        }
        public int CollectionCount => _collections.Count;
        public delegate void CollectionChangeEventHandler(object sender, CollectionChangeEventArgs e);
        public event CollectionChangeEventHandler CollectionChange;

        // ----- Constructors
        private CollectionManager()
        {
            _collections = new List<ViewCollection>();
        }

        // ----- Public methods
        public void CheckCollectionName(ref string name)
        {
            if (string.IsNullOrEmpty(name))
                name = "ViewCollection";
            var colName = name;
            var n = 1;
            while (GetCollection(colName) != null)
                colName = name + (++n);
            name = colName;
        }
        public void AddCollection(ViewCollection collection)
        {
            if (collection == null)
                return;
            if (GetCollection(collection.Name) != null)
                throw new Exception($"A collection named  {collection.Name} already exists!");
            _collections.Add(collection);
            collection.Change += OnCollectionChange;
            RaiseCollectionChange(collection,
                new CollectionChangeEventArgs(CollectionChangeEventArgs.CollectionChangeAction.CollectionAdded));
        }
        public void DeleteCollection(ViewCollection collection)
        {
            RaiseCollectionChange(collection,
                new CollectionChangeEventArgs(CollectionChangeEventArgs.CollectionChangeAction.CollectionDeleting));
            _collections.Remove(collection);
            RaiseCollectionChange(collection,
                new CollectionChangeEventArgs(CollectionChangeEventArgs.CollectionChangeAction.CollectionDeleted));
            collection.Change -= OnCollectionChange;
        }
        public void DeleteAllCollections()
        {
            var collections = _collections.ToArray();
            foreach (var collection in collections)
                DeleteCollection(collection);
        }
        public ViewCollection GetCollection(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;
            foreach (var collection in _collections)
                if (name.Equals(collection.Name, StringComparison.InvariantCultureIgnoreCase))
                    return collection;
            return null;
        }
        public ViewCollection GetCollection(int index)
        {
            return _collections[index];
        }
        public List<ViewCollection> GetCollections(string type = null)
        {
            var collections = new List<ViewCollection>();
            foreach (var collection in _collections)
                if (string.IsNullOrEmpty(type) || collection.Type.Equals(type))
                    collections.Add(collection);
            return collections;
        }

        // ----- Protected methods
        protected virtual void RaiseCollectionChange(object sender, CollectionChangeEventArgs e)
        {
            CollectionChange?.Invoke(sender, e);
        }

        // ----- Internal logics
        private void OnCollectionChange(object sender, CollectionChangeEventArgs e)
        {
            RaiseCollectionChange(sender, e);
        }
    }
}