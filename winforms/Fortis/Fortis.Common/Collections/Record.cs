﻿using System;
using System.Data;

namespace Fortis.Common.Collections
{
    public class Record : DataRow, IEquatable<Record>
    {
        // ----- Properties
        public ViewCollection Collection { get; }
        public Record MyRecord { get; }
        public string Code { get; }

        // ----- Constructors
        public Record(ViewCollection collection, Record record, DataRowBuilder builder)
            : this(builder)
        {
            Collection = collection;
            MyRecord = record;
        }
        public Record(DataRowBuilder builder) : base(builder)
        {
        }

        // ----- Public methods
        public bool Equals(Record other)
        {
            if (other == null)
                return false;
            return other.Code == Code;
        }
    }
}