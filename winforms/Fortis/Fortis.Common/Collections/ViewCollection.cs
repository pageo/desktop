﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Fortis.Common.Collections
{
    public class ViewCollection : ICollection<Record>
    {
        // ----- Properties
        public int Count { get; }
        public bool IsReadOnly { get; }
        public string Name { get; set; }
        public string Type { get; set; }

        public delegate void ChangeEventHandler(object sender, CollectionChangeEventArgs e);

        public event ChangeEventHandler Change;

        public DataTable Table { get; set; }

        // ----- Constructors
        public ViewCollection(string name, string type, int count, bool isReadOnly)
        {
            Name = name;
            Type = type;
            Count = count;
            IsReadOnly = isReadOnly;
        }

        // ----- Public methods
        public IEnumerator<Record> GetEnumerator()
        {
            return Table.AsEnumerable().OfType<Record>().GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public void Add(Record item)
        {
            Table.Rows?.Add(item);
        }
        public void Clear()
        {
            Table.Clear();
        }
        public bool Contains(Record item)
        {
            return Table.AsEnumerable().Any(r => r.Equals(item));
        }
        public void CopyTo(Record[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }
        public bool Remove(Record item)
        {
            Table.Rows?.Remove(item);
            return true;
        }
        public override string ToString()
        {
            return Name ?? base.ToString();
        }

        // ----- Protected methods
        protected virtual void OnChange(CollectionChangeEventArgs e)
        {
            var handler = Change;
            handler?.Invoke(this, e);
        }
    }
}