﻿using System.Linq;
using System.Reflection;

namespace Fortis.Common.Utils
{
    public class AssemblyInfo
    {
        // ----- Public methods
        public static string GetCompanyName()
        {
            var attribute = GetAssemblyAttributeOfExecutingAssembly<AssemblyCompanyAttribute>();
            return attribute != null ? attribute.Company ?? "Fortis" : null;
        }

        public static string GetApplicationName()
        {
            var attribute = GetAssemblyAttributeOfExecutingAssembly<AssemblyProductAttribute>();
            return attribute?.Product;
        }

        // ----- Internal logics
        private static T GetAssemblyAttributeOfExecutingAssembly<T>() where T : class
        {
            var assembly = Assembly.GetEntryAssembly();
            return (assembly != null)
                ? assembly.GetCustomAttributes(typeof(T), true).Select(x => (T) x).FirstOrDefault()
                : null;
        }
    }
}