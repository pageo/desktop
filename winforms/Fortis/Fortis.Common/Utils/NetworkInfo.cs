﻿using System;
using System.Net.NetworkInformation;
using System.Text;

namespace Fortis.Common.Utils
{
    public static class NetworkInfo
    {
        // ----- Public methods
        public static bool PingNetwork(string hostNameOrAddress)
        {
            var pingStatus = false;

            using (var p = new Ping())
            {
                var buffer = Encoding.ASCII.GetBytes("hellllllllllllllllllllllllllo");
                var timeout = 4444; // 4s

                try
                {
                    PingReply reply = p.Send(hostNameOrAddress, timeout, buffer);
                    if (reply != null) pingStatus = reply.Status == IPStatus.Success;
                }
                catch (Exception) {
                    pingStatus = false;
                }
            }

            return pingStatus;
        }
    }
}
