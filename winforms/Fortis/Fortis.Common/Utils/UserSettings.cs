﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Fortis.Common.Utils
{
    public class UserSettings
    {
        // ----- Properties
        public static UserSettings LocalApplicationData { get; set; } = Create();
        public string FilePath { get; }

        // ----- Constructors
        public UserSettings(string filePath)
        {
            FilePath = filePath;
        }

        // ----- Public methods
        public void SetValue<T>(string key, T value)
        {
            var document = XDocument.Load(FilePath);
            var userSettings = document.Element("configuration")?.Element("userSettings");
            var xml = XmlUtils.Serialize(value);
            var valueElement = GetOrCreateValueElement(key, userSettings);
            valueElement.RemoveAll();
            valueElement.Add(XElement.Parse(xml));

            document.Save(FilePath);
        }
        public void UpdateValue<T>(string key, Action<T> action) where T : class, new()
        {
            var currentValue = GetValue<T>(key) ?? new T();
            action(currentValue);
            SetValue(key, currentValue);
        }
        public T GetValue<T>(string key)
        {
            var valueElement = XDocument.Load(FilePath)
                .Element("configuration")
                ?.Element("userSettings")
                ?.Elements("add")
                ?.FirstOrDefault(x => x.Attribute("key")?.Value == key)
                ?.Element("value");

            if (valueElement == null)
                return default(T);

            try
            {
                var xml = valueElement.FirstNode.ToString();
                return XmlUtils.Deserialize<T>(xml);
            }
            catch (InvalidOperationException) {
                return default(T);
            }
        }

        // ----- Internal logics
        private static UserSettings Create()
        {
            var path = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                AssemblyInfo.GetCompanyName(),
                AssemblyInfo.GetApplicationName(),
                "user.config");

            if (File.Exists(path) == false){
                Directory.CreateDirectory(Path.GetDirectoryName(path));
                CreateEmptyUserSettings(path);
            }

            return new UserSettings(path);
        }
        private static void CreateEmptyUserSettings(string path)
        {
            var document = new XDocument();
            document.Add(
                new XElement("configuration",
                    new XElement("userSettings")));
            document.Save(path);
        }
        private static XElement GetOrCreateValueElement(string key, XContainer parent)
        {
            var element = parent
                .Elements("add")
                .FirstOrDefault(x => x.Attribute("key")?.Value == key);

            if (element == null) {
                element = XElement.Parse($"<add key=\"{key}\"><value></value></add>");
                parent.Add(element);
            }
            else if (element.Element("value") == null) {
                element.RemoveNodes();
                element.Add(new XElement("value"));
            }
            return element.Element("value");
        }
    }
}
