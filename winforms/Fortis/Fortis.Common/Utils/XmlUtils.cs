﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Fortis.Common.Utils
{
    public static class XmlUtils
    {
        // ----- Fields
        private static readonly XmlWriterSettings WriterSettings = new XmlWriterSettings { OmitXmlDeclaration = true, Indent = false, NewLineChars = Environment.NewLine, Encoding = Encoding.UTF8 };
        private static readonly XmlSerializerNamespaces Namespaces = new XmlSerializerNamespaces(new[] { new XmlQualifiedName(null, null) });

        // ----- Public methods
        public static string Serialize<T>(T obj)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));

            using (var ms = new MemoryStream())
            using (var writer = XmlWriter.Create(ms, WriterSettings)) {
                var serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(writer, obj, Namespaces);
                var xml = Encoding.UTF8.GetString(ms.ToArray());
                xml = RemoveInvalidCharacters<T>(xml);
                return xml;
            }
        }
        public static T Deserialize<T>(string data)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));

            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(data))) {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(ms);
            }
        }

        // ----- Internal logics
        private static string RemoveInvalidCharacters<T>(string xml)
        {
            if (xml.Length > 0 && xml[0] == (char)65279)
                xml = xml.Substring(1);
            return xml;
        }

    }
}
