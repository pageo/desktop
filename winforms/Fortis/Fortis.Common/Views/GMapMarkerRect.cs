﻿using System;
using System.Drawing;
using GMap.NET;
using GMap.NET.WindowsForms;

namespace Fortis.Common.Views
{
    public class GMapMarkerRect : GMapMarker
    {
        // ----- Fields
        private readonly GMapControl _mainMap;
        private readonly int _wprad = 0;
        private static readonly Pen Pen = new Pen(Brushes.White, 2);

        // ----- Properties
        public Color Color { get; set; } = Pen.Color;
        public GMapMarker InnerMarker;

        // ----- Conctructors
        public GMapMarkerRect(GMapControl mainMap, PointLatLng p)
            : base(p)
        {
            _mainMap = mainMap;
            Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

            Size = new Size(50, 50);
            Offset = new Point(-Size.Width / 2, -Size.Height / 2 - 20);
        }

        // ----- Callbacks
        public override void OnRender(Graphics graphics) {
            base.OnRender(graphics);

            if (_wprad == 0 || _mainMap == null) return;
            if (Pen.Color == Color.Blue) Pen.Color = Color.White;

            var width = _mainMap.MapProvider.Projection.GetDistance(_mainMap.FromLocalToLatLng(0, 0), _mainMap.FromLocalToLatLng(_mainMap.Width, 0)) * 1000.0;
            var m2Pixelwidth = _mainMap.Width / width;

            var loc = new GPoint((int)(LocalPosition.X - m2Pixelwidth * _wprad * 2), LocalPosition.Y);
            graphics.DrawArc(
                Pen,
                new Rectangle((int) (LocalPosition.X - Offset.X - Math.Abs(loc.X - LocalPosition.X)/2),
                    (int) (LocalPosition.Y - Offset.Y - Math.Abs(loc.X - LocalPosition.X)/2),
                    (int) Math.Abs(loc.X - LocalPosition.X),
                    (int) Math.Abs(loc.X - LocalPosition.X)),
                0,
                360);
        }
    }
}
