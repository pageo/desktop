﻿using System.Windows.Forms;
using Fortis.Common.Utils;

namespace Fortis.Common.Views.UserControls
{
    public partial class CardViewCtrl : UserControl
    {
        // ----- Constructors
        public CardViewCtrl()
        {
            InitializeComponent();
        }

        // ----- Public methods
        public void SaveSettings()
        {
            UserSettings.LocalApplicationData.SetValue("CardView.ColumnNames", "card");
        }
    }
}
