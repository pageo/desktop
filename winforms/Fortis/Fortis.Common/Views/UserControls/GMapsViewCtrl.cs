﻿using System.IO;
using System.Windows.Forms;
using Fortis.Common.Utils;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace Fortis.Common.Views.UserControls
{
    public partial class GMapsViewCtrl : GMapControl
    {
        // ----- Fields
        private readonly GMapMarker _center;
        private readonly GMarkerGoogle _currentMarker;
        private readonly GMapMarkerRect _curentRectMarker = null;

        private PointLatLng _gpsPos;
        private PointLatLng _start;

        private static bool _isMouseDown;
        private static bool _isMouseDraging;

        // ----- Constructors
        public GMapsViewCtrl()
        {
            InitializeComponent();

            if (IsDesignerHosted) return;

            // config map  
            Manager.Mode = AccessMode.ServerOnly;
            CacheLocation = Path.GetDirectoryName(Application.ExecutablePath) + Path.DirectorySeparatorChar + "gmapcache" + Path.DirectorySeparatorChar;
            MapProvider = GoogleMapProvider.Instance;

            // Zoom  
            MinZoom = 0;
            MaxZoom = 24;
            Zoom = 12;
            SetPositionByKeywords("77 rue du révérend père christian gilbert, Asnières-sur-Seine, France");

            trackBar.Minimum = MinZoom * 100;
            trackBar.Maximum = MaxZoom * 100;
            trackBar.TickFrequency = 100;
            trackBar.Value = 12 * 100;
            trackBar.ValueChanged += TrackBar_ValueChanged;

            // set center
            _center = new GMarkerGoogle(Position, GMarkerGoogleType.blue_dot);

            // set current marker
            var markersOverlay = new GMapOverlay("markers");
            _currentMarker = new GMarkerGoogle(Position, GMarkerGoogleType.green_dot) {
                ToolTipMode = MarkerTooltipMode.Always,
                ToolTipText = "Welcome to my Home ! ;)",
                IsHitTestVisible = true
            };
            markersOverlay.Markers.Add(_currentMarker);
            Overlays.Add(markersOverlay);

            // Infos
            MapScaleInfoEnabled = false;

            cbMapType.DataSource = GMapProviders.List.ToArray();
            cbMapType.SelectedItem = MapProvider;

            // Events
            OnMapZoomChanged += GMapsViewCtrl_OnMapZoomChanged;
        }

        // ----- Public methods
        public void SaveSettings()
        {
            UserSettings.LocalApplicationData.SetValue("GMapsView.ColumnNames", "card");
        }

        // ----- Callbacks
        private void TrackBar_ValueChanged(object sender, System.EventArgs e)
        {
            Zoom = trackBar.Value/100.0;
        }
        private void cbMapType_DropDownClosed(object sender, System.EventArgs e)
        {
            MapProvider = cbMapType.SelectedItem as GMapProvider;
        }
        private void GMapsViewCtrl_OnMapZoomChanged()
        {
            if (Zoom > 0)
                _center.Position = Position;
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left || ModifierKeys == Keys.Alt) return;

            _start = FromLocalToLatLng(e.X, e.Y);

            _isMouseDown = true;
            _isMouseDraging = false;

            if (_currentMarker.IsVisible)
                _currentMarker.Position = FromLocalToLatLng(e.X, e.Y);
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left || !_isMouseDown) return;

            _isMouseDraging = true;
            if (_curentRectMarker == null) {
                var point = FromLocalToLatLng(e.X, e.Y);
                var latdif = _start.Lat - point.Lat;
                var lngdif = _start.Lng - point.Lng;
                Position = new PointLatLng(_center.Position.Lat + latdif, _center.Position.Lng + lngdif);
            }
            else {
                var pnew = FromLocalToLatLng(e.X, e.Y);
                if (_currentMarker.IsVisible)
                    _currentMarker.Position = pnew;
                _curentRectMarker.Position = pnew;

                if (_curentRectMarker.InnerMarker != null) {
                    _curentRectMarker.InnerMarker.Position = pnew;
                    _gpsPos = pnew;
                }
            }
        }
    }
}
