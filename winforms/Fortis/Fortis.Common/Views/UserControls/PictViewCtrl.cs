﻿using System.Windows.Forms;
using Fortis.Common.Properties;
using Fortis.Common.Utils;

namespace Fortis.Common.Views.UserControls
{
    public partial class PictViewCtrl : UserControl
    {
        // ----- Constructors
        public PictViewCtrl()
        {
            InitializeComponent();

            var documentHtml = Resources.streetview
                .Replace("API_KEY", "AIzaSyDKLbMOz6Bc3yFUfIoEtGwdNPtvlrLRxvI")
                .Replace("lat: -33.867386, lng: 151.195767", "lat: 48.9174225, lng: 2.2832445");
            webBrowser.DocumentText = documentHtml;
            webBrowser.ScriptErrorsSuppressed = true;
        }

        public void SaveSettings()
        {
            UserSettings.LocalApplicationData.SetValue("PictView.ColumnNames", "card");
        }
    }
}