﻿using System;

namespace Fortis.DataAccess.Exceptions
{
    public class InternalServerError : Exception
    {
        // ----- Constantes
        private const string BaseMessage = "Echec lors de la récupération des informations du serveur fortis. ";

        // ----- Constructors
        public InternalServerError(string message, Exception innerException)
            : base(BaseMessage + message, innerException)
        {
        }
    }
}