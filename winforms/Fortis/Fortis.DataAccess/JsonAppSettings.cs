﻿using System.IO;
using Newtonsoft.Json;

namespace Fortis.DataAccess
{
    public class JsonAppSettings<TSetting> where TSetting : new()
    {
        // ----- Fields
        private const string DefaultFilename = "settings.json";

        // ----- Public methods
        public void Save(string fileName = DefaultFilename)
        {
            File.WriteAllText(fileName, JsonConvert.SerializeObject(this));
        }

        public static TSetting Load(string fileName = DefaultFilename)
        {
            if (File.Exists(fileName) == false) 
                throw new FileNotFoundException($"Json settings file was not found at '{fileName}'.");
            return JsonConvert.DeserializeObject<TSetting>(File.ReadAllText(fileName));
        }
    }
}
