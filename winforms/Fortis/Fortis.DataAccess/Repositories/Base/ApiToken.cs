﻿namespace Fortis.DataAccess.Repositories.Base
{
    public class ApiToken
    {
        // ----- Properties
        public string Value { get; set; }
    }
}
