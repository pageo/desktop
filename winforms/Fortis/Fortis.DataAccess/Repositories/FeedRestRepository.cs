﻿using System.Collections.Generic;
using Fortis.Business.Feeds;
using Fortis.DataAccess.Repositories.Base;
using RestSharp;

namespace Fortis.DataAccess.Repositories
{
    public class FeedRestRepository : RestRepository, IFeedRepository
    {
        // ----- Constructors
        public FeedRestRepository(EnvironmentSettings settings, ApiToken apiToken) : base(settings, apiToken) { }

        // ----- Public methods
        public IEnumerable<Feed> GetFeeds()
        {
            var request = new RestRequest("feeds", Method.GET);
            return Get<List<Feed>>(request);
        }

        public void SaveFeeds(IEnumerable<Feed> feeds)
        {
            var request = new RestRequest("feeds", Method.PUT);
            Put(request);
        }
    }
}