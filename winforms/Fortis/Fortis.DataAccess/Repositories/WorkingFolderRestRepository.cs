﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Models;
using Fortis.Business.ViewModels.WorkingFolders.Exceptions;
using Fortis.DataAccess.Exceptions;
using Fortis.DataAccess.Repositories.Base;
using RestSharp;

namespace Fortis.DataAccess.Repositories
{
    public class WorkingFolderRestRepository : RestRepository, IWorkingFolderRepository
    {
        // ----- Constructors
        public WorkingFolderRestRepository(EnvironmentSettings settings, ApiToken apiToken) : base(settings, apiToken) { }

        // ----- Public methods
        public IEnumerable<WorkingFolderListItem> GetList()
        {
            var request = new RestRequest("workingfolders");
            return Get<IEnumerable<WorkingFolderListItem>>(request);
        }
        public int Create(string name)
        {
            try
            {
                var request = new RestRequest("workingfolders");
                request.RequestFormat = DataFormat.Json;
                request.AddBody(new {
                    name
                });
                return Post(request).Id;
            }
            catch (UnknownErrorCode ex) {
                if (ex.Code == HttpStatusCode.Conflict)
                    throw new WorkingFolderWithSameNameAlreadyExists();
                throw;
            }
        }
        public WorkingFolder Load(int workingFolderId)
        {
            try
            {
                var loadDetailsTask = Task.Factory.StartNew(() => GetWorkingFolderDetails(workingFolderId));
                Task.WaitAll(loadDetailsTask);
                return new WorkingFolder(loadDetailsTask.Result);
            }
            catch (AggregateException ex) {
                throw ex.InnerExceptions.First();
            }
        }
        public void Delete(int workingFolderId)
        {
            var request = new RestRequest($"workingFolders/{workingFolderId}");
            request.RequestFormat = DataFormat.Json;
            Delete(request);
        }
        public void Save(int workingFolderId)
        {
            var request = new RestRequest($"workingFolders/{workingFolderId}");
            request.RequestFormat = DataFormat.Json;
            Put(request);
        }

        // ----- Internal logics
        private WorkingFolderDetails GetWorkingFolderDetails(int workingFolderId)
        {
            var request = new RestRequest($"workingfolders/{workingFolderId}");
            return Get<WorkingFolderDetails>(request);
        }
    }
}
