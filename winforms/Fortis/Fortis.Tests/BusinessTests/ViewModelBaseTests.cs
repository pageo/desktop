﻿using Fortis.Business.ViewModels.Base;
using NFluent;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class ViewModelBaseTests
    {
        // ----- Fields
        private const string SOME_VALUE = "hello world";
        private readonly TestedViewModel _viewModel;

        // ----- Constructors
        public ViewModelBaseTests()
        {
            _viewModel = new TestedViewModel();
        }

        // ----- Tests
        [Fact(DisplayName = "notifiable property raises property changed event when value changed")]
        public void notifiable_property_raises_property_changed_event_when_value_changed()
        {
            // Arranges
            var changedPropertyName = string.Empty;
            _viewModel.PropertyChanged += (sender, args) => { changedPropertyName = args.PropertyName; };

            // Acts
            _viewModel.Name = SOME_VALUE;

            // Asserts
            Check.That(changedPropertyName).IsEqualTo("Name");
        }

        [Fact(DisplayName = "notifiable property does not raise property changed event when value is the same")]
        public void does_not_raise_propert_changed_event_when_value_is_the_same()
        {
            // Arranges
            string changedPropertyName = null;

            // Acts
            _viewModel.Name = SOME_VALUE;
            _viewModel.PropertyChanged += (sender, args) => { changedPropertyName = args.PropertyName; };
            _viewModel.Name = SOME_VALUE;

            // Asserts
            Check.That(changedPropertyName).IsNull();
        }

        [Fact(DisplayName = "notifiable property returns correct value")]
        public void notifiable_property_returns_correct_value()
        {
            // Acts
            _viewModel.Name = SOME_VALUE;

            // Asserts
            Check.That(_viewModel.Name).IsEqualTo(SOME_VALUE);
        }

        // ----- Internal class for testing

        private class TestedViewModel : ViewModelBase
        {
            // ----- Properties
            public string Name
            {
                get { return GetNotifiableProperty<string>(); }
                set { SetNotifiableProperty(value); }
            }

            // ----- Override methods
            public override void Initialize()
            {
            }

            public override void Clean()
            {
            }
        }
    }
}