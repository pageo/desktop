﻿using System;
using Fortis.Business.Base;
using NFluent;
using Xunit;

namespace Fortis.Tests.InfrastructureTests
{
    public class BusTests
    {
        // ----- Fields
        private readonly Bus _bus;

        // ----- Constructors
        public BusTests()
        {
            _bus = new Bus();
        }

        // ----- Tests
        [Fact(DisplayName = "notifies registered delegates")]
        public void notifies_registered_delegates()
        {
            // Arranges
            var subscriber1 = new object();
            var valueReceived1 = default(int);

            var subscriber2 = new object();
            var valueReceived2 = default(int);

            _bus.Register<int>(subscriber1, message => valueReceived1 = message);
            _bus.Register<int>(subscriber2, message => valueReceived2 = message);

            // Acts
            _bus.Send(88);

            // Assets
            Check.That(valueReceived1).IsEqualTo(88);
            Check.That(valueReceived2).IsEqualTo(88);
        }

        [Fact(DisplayName = "does not notify unregistered delegates")]
        public void does_not_notify_unregistered_delegates()
        {
            // Arranges
            var subscriber = new object();
            var valueReceived1 = default(int);
            var valueReceived2 = default(string);

            // Acts
            _bus.Register<int>(subscriber, message => valueReceived1 = message);
            _bus.Register<string>(subscriber, message => valueReceived2 = message);
            _bus.Unregister(subscriber);

            _bus.Send(88);
            _bus.Send("hello");

            // Assets
            Check.That(valueReceived1).IsEqualTo(default(int));
            Check.That(valueReceived2).IsEqualTo(default(string));
        }

        [Fact(DisplayName = "raises original exception when registerer throws error")]
        public void raises_original_exception_when_registerer_throws_error()
        {
            // Arranges
            var subscriber1 = new object();
            _bus.Register<int>(subscriber1, message => { throw new Exception("Boom"); });

            // Acts
            Action code = () => _bus.Send(88);

            // Assets
            Check.ThatCode(code).Throws<Exception>().WithMessage("Boom");
        }
    }
}