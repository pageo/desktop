﻿using System.Windows.Input;
using NFluent;
using NFluent.Extensibility;

namespace Fortis.Tests.Utils
{
    public static class CheckCommandExtensions
    {
        // ----- Public methods
        public static void IsDisabled(this ICheck<ICommand> check)
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);
            var command = checker.Value;
            Check.That(command.CanExecute(null)).IsFalse();
        }

        public static void IsEnabled(this ICheck<ICommand> check)
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);
            var command = checker.Value;
            Check.That(command.CanExecute(null)).IsTrue();
        }
    }
}