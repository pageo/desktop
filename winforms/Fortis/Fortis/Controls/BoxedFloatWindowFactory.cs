using System.Drawing;
using WeifenLuo.WinFormsUI.Docking;

namespace Fortis.Winforms.Controls
{
    public class BoxedFloatWindowFactory : DockPanelExtender.IFloatWindowFactory
    {
        // ----- Public methods
        public FloatWindow CreateFloatWindow(DockPanel dockPanel, DockPane pane, Rectangle bounds)
        {
            return new BoxedFloatWindow(dockPanel, pane, bounds);
        }

        public FloatWindow CreateFloatWindow(DockPanel dockPanel, DockPane pane)
        {
            return new BoxedFloatWindow(dockPanel, pane);
        }
    }
}