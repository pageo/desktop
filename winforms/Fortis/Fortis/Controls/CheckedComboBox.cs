﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Fortis.Winforms.Controls
{
    public class CheckedComboBox : ComboBox
    {
        /// <summary>
        /// Internal class to represent the dropdown list of the CheckedComboBox
        /// </summary>
        internal class Dropdown : Form
        {
            // ---------------------------------- internal class CCBoxEventArgs --------------------------------------------
            /// <summary>
            /// Custom EventArgs encapsulating value as to whether the combo box value(s) should be assignd to or not.
            /// </summary>
            internal class CCBoxEventArgs : EventArgs
            {
                private bool assignValues;

                public bool AssignValues
                {
                    get { return assignValues; }
                    set { assignValues = value; }
                }

                private EventArgs e;

                public EventArgs EventArgs
                {
                    get { return e; }
                    set { e = value; }
                }

                public CCBoxEventArgs(EventArgs e, bool assignValues)
                    : base()
                {
                    this.e = e;
                    this.assignValues = assignValues;
                }
            }

            // ---------------------------------- internal class CustomCheckedListBox --------------------------------------------

            /// <summary>
            /// A custom CheckedListBox being shown within the dropdown form representing the dropdown list of the CheckedComboBox.
            /// </summary>
            internal class CustomCheckedListBox : CheckedListBox
            {
                private int curSelIndex = -1;

                public CustomCheckedListBox()
                    : base()
                {
                    this.SelectionMode = SelectionMode.One;
                    this.HorizontalScrollbar = true;
                }

                /// <summary>
                /// Intercepts the keyboard input, [Enter] confirms a selection and [Esc] cancels it.
                /// </summary>
                /// <param name="e">The Key event arguments</param>
                protected override void OnKeyDown(KeyEventArgs e)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        // Enact selection.
                        ((CheckedComboBox.Dropdown) Parent).OnDeactivate(new CCBoxEventArgs(null, true));
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Escape)
                    {
                        // Cancel selection.
                        ((CheckedComboBox.Dropdown) Parent).OnDeactivate(new CCBoxEventArgs(null, false));
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Delete)
                    {
                        // Delete unckecks all, [Shift + Delete] checks all.
                        for (int i = 0; i < Items.Count; i++)
                        {
                            SetItemChecked(i, e.Shift);
                        }
                        e.Handled = true;
                    }
                    // If no Enter or Esc keys presses, let the base class handle it.
                    base.OnKeyDown(e);
                }

                protected override void OnMouseMove(MouseEventArgs e)
                {
                    base.OnMouseMove(e);
                    int index = IndexFromPoint(e.Location);
                    Debug.WriteLine("Mouse over item: " + (index >= 0 ? GetItemText(Items[index]) : "None"));
                    if ((index >= 0) && (index != curSelIndex))
                    {
                        curSelIndex = index;
                        SetSelected(index, true);
                    }
                }

                protected override void OnDataSourceChanged(EventArgs e)
                {
                    var oldCheckedItems = new object[CheckedItems.Count];
                    CheckedItems.CopyTo(oldCheckedItems, 0);

                    try
                    {
                        base.OnDataSourceChanged(e);
                    }
                    catch (Exception)
                    {
                    }

                    for (var index = 0; index < Items.Count; index++)
                    {
                        var item = Items[index];
                        var exists = oldCheckedItems.Contains(item);
                        SetItemChecked(Items.IndexOf(item), exists);
                    }
                }
            } // end internal class CustomCheckedListBox

            // --------------------------------------------------------------------------------------------------------

            // ********************************************* Data *********************************************

            private readonly CheckedComboBox _checkedComboBoxParent;

            // Keeps track of whether checked item(s) changed, hence the value of the CheckedComboBox as a whole changed.
            // This is simply done via maintaining the old string-representation of the value(s) and the new one and comparing them!
            private string oldStrValue = "";

            public bool ValueChanged
            {
                get
                {
                    string newStrValue = _checkedComboBoxParent.Text;
                    if ((oldStrValue.Length > 0) && (newStrValue.Length > 0))
                    {
                        return (oldStrValue.CompareTo(newStrValue) != 0);
                    }
                    else
                    {
                        return (oldStrValue.Length != newStrValue.Length);
                    }
                }
            }

            // Array holding the checked states of the items. This will be used to reverse any changes if user cancels selection.
            private bool[] _checkedStateArr;

            // Whether the dropdown is closed.
            private bool dropdownClosed = true;

            private CustomCheckedListBox _checkedListBox;

            public CustomCheckedListBox List
            {
                get { return _checkedListBox; }
                set { _checkedListBox = value; }
            }

            // ********************************************* Construction *********************************************

            public Dropdown(CheckedComboBox checkedComboBoxParent)
            {
                this._checkedComboBoxParent = checkedComboBoxParent;
                InitializeComponent();
                this.ShowInTaskbar = false;
                // Add a handler to notify our parent of ItemCheck events.
                this._checkedListBox.ItemCheck += this._checkedListBox_ItemCheck;
            }

            // ********************************************* Methods *********************************************

            // Create a CustomCheckedListBox which fills up the entire form area.
            private void InitializeComponent()
            {
                this._checkedListBox = new CustomCheckedListBox();
                this.SuspendLayout();
                // 
                // checkedListBox
                // 
                this._checkedListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this._checkedListBox.Dock = System.Windows.Forms.DockStyle.Fill;
                this._checkedListBox.FormattingEnabled = true;
                this._checkedListBox.Location = new System.Drawing.Point(0, 0);
                this._checkedListBox.Name = "_checkedListBox";
                this._checkedListBox.Size = new System.Drawing.Size(47, 15);
                this._checkedListBox.TabIndex = 0;
                // 
                // Dropdown
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.BackColor = System.Drawing.SystemColors.Menu;
                this.ClientSize = new System.Drawing.Size(47, 16);
                this.ControlBox = false;
                this.Controls.Add(this._checkedListBox);
                this.ForeColor = System.Drawing.SystemColors.ControlText;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
                this.MinimizeBox = false;
                this.Name = "_checkedComboBoxParent";
                this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
                this.ResumeLayout(false);
            }

            public string GetCheckedItemsStringValue()
            {
                var values = new List<string>();
                for (var i = 0; i < _checkedListBox.CheckedItems.Count; i++)
                {
                    values.Add(_checkedComboBoxParent.GetItemText(_checkedListBox.CheckedItems[i]));
                }
                return string.Join(_checkedComboBoxParent.ValueSeparator, values.ToArray());
            }

            /// <summary>
            /// Closes the dropdown portion and enacts any changes according to the specified boolean parameter.
            /// NOTE: even though the caller might ask for changes to be enacted, this doesn't necessarily mean
            ///       that any changes have occurred as such. Caller should check the ValueChanged property of the
            ///       CheckedComboBox (after the dropdown has closed) to determine any actual value changes.
            /// </summary>
            /// <param name="enactChanges"></param>
            public void CloseDropdown(bool enactChanges)
            {
                if (dropdownClosed)
                {
                    return;
                }
                Debug.WriteLine("CloseDropdown");
                // Perform the actual selection and display of checked items.
                if (enactChanges)
                {
                    _checkedComboBoxParent.SelectedIndex = -1;
                    // Set the text portion equal to the string comprising all checked items (if any, otherwise empty!).
                    _checkedComboBoxParent.Text = GetCheckedItemsStringValue();
                }
                else
                {
                    // Caller cancelled selection - need to restore the checked items to their original state.
                    for (int i = 0; i < _checkedListBox.Items.Count; i++)
                    {
                        _checkedListBox.SetItemChecked(i, _checkedStateArr[i]);
                    }
                }
                // From now on the dropdown is considered closed. We set the flag here to prevent OnDeactivate() calling
                // this method once again after hiding this window.
                dropdownClosed = true;
                // Set the focus to our parent CheckedComboBox and hide the dropdown check list.
                _checkedComboBoxParent.Focus();
                this.Hide();
                // Notify CheckedComboBox that its dropdown is closed. (NOTE: it does not matter which parameters we pass to
                // OnDropDownClosed() as long as the argument is CCBoxEventArgs so that the method knows the notification has
                // come from our code and not from the framework).
                _checkedComboBoxParent.OnDropDownClosed(new CCBoxEventArgs(null, false));
            }

            protected override void OnActivated(EventArgs e)
            {
                Debug.WriteLine("OnActivated");
                //base.OnActivated(e);
                dropdownClosed = false;
                // Assign the old string value to compare with the new value for any changes.
                oldStrValue = _checkedComboBoxParent.Text;

                var ids = oldStrValue.Split(new[] {_checkedComboBoxParent.ValueSeparator, " "},
                    StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < _checkedComboBoxParent.Items.Count; i++)
                {
                    var item = _checkedComboBoxParent.Items[i];
                    var propertyInfo = item.GetType().GetProperty(_checkedComboBoxParent.DisplayMember);
                    if (propertyInfo != null)
                    {
                        _checkedListBox.SetItemChecked(i,
                            ids.Contains(propertyInfo.GetValue(item, new object[0]).ToString()));
                    }
                }

                // Make a copy of the checked state of each item, in cace caller cancels selection.
                _checkedStateArr = new bool[_checkedListBox.Items.Count];
                for (int i = 0; i < _checkedListBox.Items.Count; i++)
                {
                    _checkedStateArr[i] = _checkedListBox.GetItemChecked(i);
                }
            }

            protected override void OnDeactivate(EventArgs e)
            {
                Debug.WriteLine("OnDeactivate");
                base.OnDeactivate(e);
                CCBoxEventArgs ce = e as CCBoxEventArgs;
                if (ce != null)
                {
                    CloseDropdown(ce.AssignValues);
                }
                else
                {
                    // If not custom event arguments passed, means that this method was called from the
                    // framework. We assume that the checked values should be registered regardless.
                    CloseDropdown(true);
                }
            }

            private void _checkedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
            {
                if (_checkedComboBoxParent.ItemCheck != null) _checkedComboBoxParent.ItemCheck.Invoke(sender, e);
            }
        } // end internal class Dropdown

        // ******************************** Data ********************************
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // A form-derived object representing the drop-down list of the checked combo box.
        private readonly Dropdown _dropdown;

        // The valueSeparator character(s) between the ticked elements as they appear in the 
        // text portion of the CheckedComboBox.
        public string ValueSeparator { get; set; }

        public bool CheckOnClick
        {
            get { return _dropdown.List.CheckOnClick; }
            set { _dropdown.List.CheckOnClick = value; }
        }

        public string CheckDisplayMember
        {
            get { return _dropdown.List.DisplayMember; }
            set { _dropdown.List.DisplayMember = value; }
        }

        public new object DataSource
        {
            get { return _dropdown.List.DataSource; }
            set
            {
                if (value != DBNull.Value)
                {
                    _dropdown.List.DataSource = value;
                    OnDataSourceChanged(EventArgs.Empty);
                    Text = _dropdown.GetCheckedItemsStringValue();
                }
            }
        }

        public new CheckedListBox.ObjectCollection Items
        {
            get { return _dropdown.List.Items; }
        }

        public CheckedListBox.CheckedItemCollection CheckedItems
        {
            get { return _dropdown.List.CheckedItems; }
        }

        public CheckedListBox.CheckedIndexCollection CheckedIndices
        {
            get { return _dropdown.List.CheckedIndices; }
        }

        public bool ValueChanged
        {
            get { return _dropdown.ValueChanged; }
        }

        public event ItemCheckEventHandler ItemCheck;

        // ******************************** Construction ********************************

        public CheckedComboBox()
            : base()
        {
            // We want to do the drawing of the dropdown.
            this.DrawMode = DrawMode.OwnerDrawVariable;
            // Default value separator.
            this.ValueSeparator = ", ";
            // This prevents the actual ComboBox dropdown to show, although it's not strickly-speaking necessary.
            // But including this remove a slight flickering just before our dropdown appears (which is caused by
            // the empty-dropdown list of the ComboBox which is displayed for fractions of a second).
            this.DropDownHeight = 1;
            // This is the default setting - text portion is editable and user must click the arrow button
            // to see the list portion. Although we don't want to allow the user to edit the text portion
            // the DropDownList style is not being used because for some reason it wouldn't allow the text
            // portion to be programmatically set. Hence we set it as editable but disable keyboard input (see below).
            this.DropDownStyle = ComboBoxStyle.DropDown;
            this._dropdown = new Dropdown(this);
            // CheckOnClick style for the dropdown (NOTE: must be set after dropdown is created).
            this.CheckOnClick = true;
        }

        // ******************************** Operations ********************************

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnDropDown(EventArgs e)
        {
            base.OnDropDown(e);
            DoDropDown();
        }

        protected override void OnDropDownClosed(EventArgs e)
        {
            // Call the handlers for this event only if the call comes from our code - NOT the framework's!
            // NOTE: that is because the events were being fired in a wrong order, due to the actual dropdown list
            //       of the ComboBox which lies underneath our dropdown and gets involved every time.
            if (e is Dropdown.CCBoxEventArgs)
            {
                base.OnDropDownClosed(e);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                // Signal that the dropdown is "down". This is required so that the behaviour of the dropdown is the same
                // when it is a result of user pressing the Down_Arrow (which we handle and the framework wouldn't know that
                // the list portion is down unless we tell it so).
                // NOTE: all that so the DropDownClosed event fires correctly!                
                OnDropDown(null);
            }
            // Make sure that certain keys or combinations are not blocked.
            e.Handled = !e.Alt && !(e.KeyCode == Keys.Tab) &&
                        !((e.KeyCode == Keys.Left) || (e.KeyCode == Keys.Right) || (e.KeyCode == Keys.Home) ||
                          (e.KeyCode == Keys.End));

            base.OnKeyDown(e);
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x201:
                case 0x203:
                    DoDropDown();
                    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        public bool GetItemChecked(int index)
        {
            if (index < 0 || index > Items.Count)
            {
                throw new ArgumentOutOfRangeException("index", "value out of range");
            }
            else
            {
                return _dropdown.List.GetItemChecked(index);
            }
        }

        public void SetItemChecked(int index, bool isChecked)
        {
            if (index < 0 || index > Items.Count)
            {
                throw new ArgumentOutOfRangeException("index", "value out of range");
            }
            else
            {
                _dropdown.List.SetItemChecked(index, isChecked);
                // Need to update the Text.
                this.Text = _dropdown.GetCheckedItemsStringValue();
            }
        }

        public CheckState GetItemCheckState(int index)
        {
            if (index < 0 || index > Items.Count)
            {
                throw new ArgumentOutOfRangeException("index", "value out of range");
            }
            else
            {
                return _dropdown.List.GetItemCheckState(index);
            }
        }

        public void SetItemCheckState(int index, CheckState state)
        {
            if (index < 0 || index > Items.Count)
            {
                throw new ArgumentOutOfRangeException("index", "value out of range");
            }
            else
            {
                _dropdown.List.SetItemCheckState(index, state);
                // Need to update the Text.
                this.Text = _dropdown.GetCheckedItemsStringValue();
            }
        }

        private void DoDropDown()
        {
            if (!_dropdown.Visible)
            {
                Rectangle rect = RectangleToScreen(this.ClientRectangle);
                _dropdown.Location = new Point(rect.X, rect.Y + this.Size.Height);
                int count = _dropdown.List.Items.Count;
                if (count > this.MaxDropDownItems)
                {
                    count = this.MaxDropDownItems;
                }
                else if (count == 0)
                {
                    count = 1;
                }
                _dropdown.Size = new Size(this.DropDownWidth, (_dropdown.List.ItemHeight)*count + 2);
                _dropdown.Show(this);
            }
        }
    }
}