﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Fortis.Winforms.Controls
{
    public class CheckedListBoxExtended : CheckedListBox
    {
        private string _checkedMember;
        public string CheckedMember
        {
            get { return _checkedMember; }
            set
            {
                _checkedMember = value;
                UpdateCheckStates();
            }
        }

        protected override void OnItemCheck(ItemCheckEventArgs e)
        {
            if (string.IsNullOrEmpty(CheckedMember) == false)
            {
                var item = Items[e.Index];
                SetBooleanValue(item, e.NewValue);
            }

            base.OnItemCheck(e);
        }
        protected override void OnDataSourceChanged(EventArgs e)
        {
            foreach (var item in Items.OfType<INotifyPropertyChanged>())
            {
                item.PropertyChanged -= ItemOnPropertyChanged;
            }

            try
            {
                base.OnDataSourceChanged(e);
            }
            catch (Exception) { }

            UpdateCheckStates();

            foreach (var item in Items.OfType<INotifyPropertyChanged>())
            {
                item.PropertyChanged += ItemOnPropertyChanged;
            }
        }

        private void UpdateCheckStates()
        {
            if (string.IsNullOrEmpty(CheckedMember) == false)
            {
                for (var i = 0; i < Items.Count; ++i)
                {
                    var item = Items[i];
                    UpdateCheckState(item);
                }
            }

            this.Invalidate();
        }
        private void UpdateCheckState(object item)
        {
            var propertyInfo = item.GetType().GetProperty(CheckedMember);
            if (propertyInfo != null)
            {
                SetItemChecked(Items.IndexOf(item), (bool)propertyInfo.GetValue(item));
            }
        }
        private void SetBooleanValue(object item, CheckState value)
        {
            var propertyInfo = item.GetType().GetProperty(CheckedMember);
            if (propertyInfo != null)
            {
                if (propertyInfo.PropertyType == typeof(bool?))
                {
                    switch (value)
                    {
                        case CheckState.Unchecked:
                            propertyInfo.SetValue(item, false);
                            break;
                        case CheckState.Checked:
                            propertyInfo.SetValue(item, true);
                            break;
                        case CheckState.Indeterminate:
                            propertyInfo.SetValue(item, null);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                else if (propertyInfo.PropertyType == typeof(bool))
                {
                    propertyInfo.SetValue(item, value == CheckState.Checked);
                }
            }
        }

        private void ItemOnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == CheckedMember)
            {
                UpdateCheckState(sender);
            }
        }
    }
}
