﻿using System;
using System.Windows.Forms;

namespace Fortis.Winforms.Controls
{
    public static class ComboBoxExtensions
    {
        public static void SynchronizeSelectedItemBindingToSelectedIndexChanged(this ComboBox @this)
        {
            var binding = @this.DataBindings["SelectedItem"];
            if (binding == null)
                throw new Exception("Cannot synchronize combobox if no binding defined on SelectedItem property.");
            @this.SelectedIndexChanged += (sender, args) => { binding.WriteValue(); };
        }
    }
}
