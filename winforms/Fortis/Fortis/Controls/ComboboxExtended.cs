﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace Fortis.Winforms.Controls
{
    public class ComboboxExtended : ComboBox
    {
        private bool _updatingDataSource = false;

        protected override void OnDataSourceChanged(EventArgs e)
        {
            _updatingDataSource = true;

            var currentItem = this.SelectedItem;
            base.OnDataSourceChanged(e);
            SelectedIndex = GetDatasourceIndexOf(currentItem);

            _updatingDataSource = false;
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);

            if (_updatingDataSource == false)
            {
                var dataBinding = DataBindings["SelectedItem"];
                if (dataBinding != null) dataBinding.WriteValue();
            }
        }

        private int GetDatasourceIndexOf(object item)
        {
            var enumerable = DataSource as IEnumerable;
            if (enumerable != null)
            {
                return IndexOf(enumerable, item);
            }
            else
            {
                return -1;
            }
        }

        private static int IndexOf(IEnumerable enumerable, object item)
        {
            var enumerator = enumerable.GetEnumerator();
            var index = 0;
            while (enumerator.MoveNext())
            {
                if (Equals(item, enumerator.Current))
                {
                    return index;
                }
                index++;
            }
            return -1;
        }
    }
}