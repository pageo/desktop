﻿using System.Windows.Forms;
using Fortis.Common.Views.UserControls;

namespace Fortis.Winforms.Controls.Dock
{
    public partial class CardViewDockContent : DockContentExtended
    {
        // ----- Properties
        public CardViewCtrl Control => cardViewCtrl;

        // ----- Constructors
        public CardViewDockContent(ToolStripItem menuItem) : base(menuItem)
        {
            InitializeComponent();
        }

    }
}
