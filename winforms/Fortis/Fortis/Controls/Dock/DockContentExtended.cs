﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Fortis.Winforms.Controls.Dock
{
    public class DockContentExtended : DockContent
    {
        // ----- Fields
        public DockContentExtended()
        {
            HideOnClose = true;
        }

        // ----- Constructors
        public DockContentExtended(ToolStripItem menuItem) : this()
        {
            menuItem.Click += (sender, args) => Show();
        }

        // ----- Public methods
        public new void Show()
        {
            if (DockHelper.IsDockStateAutoHide(DockState)) {
                DockPanel.ActiveAutoHideContent = this;
                Focus();
            }
            else {
                base.Show();
            }
        }

        public void BindVisibility<T>(T viewModel, Expression<Func<T, bool>> property, bool reverse = false)
            where T : INotifyPropertyChanged
        {
            var memberExpression = (MemberExpression) property.Body;
            var propertyInfo = (PropertyInfo) memberExpression.Member;
            viewModel.PropertyChanged += (sender, args) => {
                if (args.PropertyName == memberExpression.Member.Name) {
                    var isVisible = (bool) propertyInfo.GetValue(viewModel);
                    if (isVisible)
                        Show();
                    else
                        Hide();
                }
            };

            DockStateChanged += (sender, args) => {
                var isVisible = (bool) propertyInfo.GetValue(viewModel);
                if (isVisible && DockState == DockState.Hidden)
                    propertyInfo.SetValue(viewModel, false);
                else if (!isVisible && DockState != DockState.Hidden)
                    propertyInfo.SetValue(viewModel, true);
            };
        }
    }
}