﻿using Fortis.Common.Views.UserControls;

namespace Fortis.Winforms.Controls.Dock
{
    partial class GoogleMapDockContent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gMapsViewCtrl = new Fortis.Common.Views.UserControls.GMapsViewCtrl();
            this.SuspendLayout();
            // 
            // gMapsViewCtrl
            // 
            this.gMapsViewCtrl.Bearing = 0F;
            this.gMapsViewCtrl.CanDragMap = true;
            this.gMapsViewCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gMapsViewCtrl.EmptyTileColor = System.Drawing.Color.Navy;
            this.gMapsViewCtrl.GrayScaleMode = false;
            this.gMapsViewCtrl.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gMapsViewCtrl.LevelsKeepInMemmory = 5;
            this.gMapsViewCtrl.Location = new System.Drawing.Point(0, 0);
            this.gMapsViewCtrl.MarkersEnabled = true;
            this.gMapsViewCtrl.MaxZoom = 20;
            this.gMapsViewCtrl.MinZoom = 2;
            this.gMapsViewCtrl.MouseWheelZoomEnabled = true;
            this.gMapsViewCtrl.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gMapsViewCtrl.Name = "gMapsViewCtrl";
            this.gMapsViewCtrl.NegativeMode = false;
            this.gMapsViewCtrl.PolygonsEnabled = true;
            this.gMapsViewCtrl.RetryLoadTile = 0;
            this.gMapsViewCtrl.RoutesEnabled = true;
            this.gMapsViewCtrl.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gMapsViewCtrl.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gMapsViewCtrl.ShowTileGridLines = false;
            this.gMapsViewCtrl.Size = new System.Drawing.Size(408, 377);
            this.gMapsViewCtrl.TabIndex = 0;
            this.gMapsViewCtrl.Zoom = 20D;
            // 
            // GoogleMapDockContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 377);
            this.Controls.Add(this.gMapsViewCtrl);
            this.Name = "GoogleMapDockContent";
            this.Text = "GoogleMapDockContent";
            this.ResumeLayout(false);

        }

        #endregion

        private GMapsViewCtrl gMapsViewCtrl;
    }
}