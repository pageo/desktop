﻿using System.Windows.Forms;
using Fortis.Common.Views.UserControls;
using GMap.NET.WindowsForms;

namespace Fortis.Winforms.Controls.Dock
{
    public partial class GoogleMapDockContent : DockContentExtended
    {
        // ----- Fields
        private readonly GMapMarker _marker;

        // ----- Properties
        public GMapsViewCtrl Control => gMapsViewCtrl;

        // ----- Constructors
        public GoogleMapDockContent(ToolStripItem menuItem) : base(menuItem)
        {
            InitializeComponent();
        }
    }
}
