﻿using System.Windows.Forms;

namespace Fortis.Winforms.Controls.Dock
{
    public partial class ListViewDockContent : DockContentExtended
    {
        // ----- Constructors
        public ListViewDockContent(ToolStripItem menuItem) : base(menuItem)
        {
            InitializeComponent();
        }
    }
}