﻿using Fortis.Common.Views;
using Fortis.Common.Views.UserControls;

namespace Fortis.Winforms.Controls.Dock
{
    partial class PictureDockContent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PictureDockContent));
            this.pictViewCtrl = new PictViewCtrl();
            this.SuspendLayout();
            // 
            // pictViewCtrl
            // 
            this.pictViewCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictViewCtrl.Location = new System.Drawing.Point(0, 0);
            this.pictViewCtrl.Name = "pictViewCtrl";
            this.pictViewCtrl.Size = new System.Drawing.Size(284, 261);
            this.pictViewCtrl.TabIndex = 0;
            // 
            // PictureDockContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.pictViewCtrl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "PictureDockContent";
            this.Text = "Photos";
            this.ResumeLayout(false);

        }

        #endregion

        private PictViewCtrl pictViewCtrl;
    }
}