﻿using System.Windows.Forms;
using Fortis.Common.Views.UserControls;

namespace Fortis.Winforms.Controls.Dock
{
    public partial class PictureDockContent : DockContentExtended
    {
        // ----- Properties
        public PictViewCtrl Control => pictViewCtrl;

        // ----- Constructors
        public PictureDockContent(ToolStripItem menuItem) : base(menuItem)
        {
            InitializeComponent();
        }
    }
}