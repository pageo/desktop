﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Fortis.Winforms.Controls
{
    public class MultiColumnCombobox : ComboboxExtended
    {
        private readonly ToolStripControlHost _host;
        private ToolStripDropDown _dropDown;

        private readonly MultiColumnComboboxPopup _popup = new MultiColumnComboboxPopup();

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), MergableProperty(false)]
        public DataGridViewColumnCollection PopupColumns
        {
            get { return _popup.GridColumns; }
        }

        // ----- Constructors
        public MultiColumnCombobox()
        {
            _popup.ElementSelected += PopupOnElementSelected;

            _host = new ToolStripControlHost(_popup)
            {
                Margin = Padding.Empty,
                AutoSize = false,
                Size = _popup.Size
            };

            _dropDown = new ToolStripDropDown
            {
                Padding = Padding.Empty,
                CanOverflow = true,
                AutoClose = true,
                DropShadowEnabled = true
            };

            _dropDown.Items.Add(_host);
        }

        // ----- Internal logics
        private void ToggleDropDownVisibility()
        {
            if (_dropDown.Visible)
            {
                _dropDown.Close();
            }
            else
            {
                ShowDropDown();
            }
        }

        private void ShowDropDown()
        {
            if (_dropDown != null)
            {
                _popup.SetDataSource(DataSource);
                _popup.SetSelectedItem(SelectedItem);

                _host.Width = DropDownWidth;
                _host.Height = DropDownHeight;

                _dropDown.Show(this, 0, Height);
                _dropDown.Select();
            }
        }

        // ----- Callbacks
        private void PopupOnElementSelected(object sender, ElementSelectedEventArgs args)
        {
            if (_dropDown.Visible)
            {
                _dropDown.Close();
            }

            SelectedItem = args.SelectedElement;
        }

        // ----- Overrides 
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x201:
                case 0x203:
                    ToggleDropDownVisibility();
                    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_dropDown != null)
                {
                    _dropDown.Dispose();
                    _dropDown = null;
                }
            }
            base.Dispose(disposing);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                ShowDropDown();
                e.Handled = true;
            }
            base.OnKeyDown(e);
        }
    }
}