﻿using System;
using System.Windows.Forms;

namespace Fortis.Winforms.Controls
{
    public partial class MultiColumnComboboxPopup : Panel
    {
        public event EventHandler<ElementSelectedEventArgs> ElementSelected;

        public DataGridViewColumnCollection GridColumns
        {
            get { return dataGridView.Columns; }
        }

        public MultiColumnComboboxPopup()
        {
            InitializeComponent();
        }

        public void SetDataSource(object dataSource)
        {
            dataGridView.AutoGenerateColumns = GridColumns.Count == 0;
            dataGridView.DataSource = dataSource;
            dataGridView.BindingContext = new BindingContext();
        }

        public void SetSelectedItem(object selectedItem)
        {
            foreach (DataGridViewRow row in this.dataGridView.Rows)
            {
                if (row.DataBoundItem == selectedItem)
                {
                    row.Selected = true;
                    dataGridView.FirstDisplayedScrollingRowIndex = row.Index;
                    break;
                }
            }
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (ElementSelected != null)
                    ElementSelected.Invoke(this,
                        new ElementSelectedEventArgs(this.dataGridView.Rows[e.RowIndex].DataBoundItem));
            }
        }

        private void MultiColumnComboboxPopup_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.BeginInvoke(new Action(() => { dataGridView.Focus(); }));
            }
        }

        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.dataGridView.SelectedRows.Count > 0)
                {
                    if (ElementSelected != null)
                        ElementSelected.Invoke(this,
                            new ElementSelectedEventArgs(this.dataGridView.SelectedRows[0].DataBoundItem));
                    e.Handled = true;
                }
            }
        }

        private void dataGridView_MouseMove(object sender, MouseEventArgs e)
        {
            base.OnMouseMove(e);

            var info = dataGridView.HitTest(e.X, e.Y);
            if (info.RowIndex >= 0)
            {
                dataGridView.Rows[info.RowIndex].Selected = true;
            }
        }
    }

    public class ElementSelectedEventArgs : EventArgs
    {
        public object SelectedElement { get; set; }

        public ElementSelectedEventArgs(object selectedElement)
        {
            SelectedElement = selectedElement;
        }
    }
}