using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;

namespace Fortis.Winforms.Controls
{
    public class PopupForm : Form
    {
        public PopupForm()
        {
            InitializeComponent();
        }

        public void BindPopupVisibility<T>(T viewModel, Expression<Func<T, bool>> property)
            where T : INotifyPropertyChanged
        {
            var memberExpression = (MemberExpression) property.Body;
            var propertyInfo = (PropertyInfo) memberExpression.Member;
            viewModel.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == memberExpression.Member.Name)
                {
                    var isVisible = (bool) propertyInfo.GetValue(viewModel);
                    if (isVisible)
                    {
                        ShowDialog();

                        // Fin d'ex�cution de la pompe � message ShowDialog(), 
                        // la fen�tre est donc ferm�e, on remet la propri�t� � false.
                        propertyInfo.SetValue(viewModel, false);
                    }
                    else
                    {
                        if (Visible)
                        {
                            Visible = false;
                        }
                    }
                }
            };
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // PopupForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.KeyPreview = true;
            this.Name = "PopupForm";
            this.KeyPress += PopupForm_KeyPress;
            this.ResumeLayout(false);
        }

        private void PopupForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Echap
            if (e.KeyChar == 27)
            {
                Visible = false;
            }
        }
    }
}