﻿using Fortis.Winforms.Utils;

namespace Fortis.Winforms.Converters
{
    public class ReverseBooleanConverter : IValueConverter<bool, bool>
    {
        // ----- Public methods
        public object ConvertViewModelToControl(bool viewModelValue)
        {
            return !viewModelValue;
        }
        public object ConvertControlToViewModel(bool source)
        {
            return !source;
        }
    }
}
