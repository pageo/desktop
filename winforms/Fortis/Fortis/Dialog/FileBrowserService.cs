﻿using System.Windows.Forms;
using Fortis.Business.Services;

namespace Fortis.Winforms.Dialog
{
    public class FileBrowserService : IFileBrowserService
    {
        // ----- Public methods
        public string Browse(string fileExtensions)
        {
            var dialog = new OpenFileDialog {Filter = fileExtensions};
            if (dialog.ShowDialog() == DialogResult.OK)
                return dialog.FileName;
            return null;
        }
    }
}