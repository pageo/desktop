﻿using System.Windows.Forms;
using Fortis.Business;


namespace Fortis.Winforms.Dialog
{
    public class WinformMessageBoxService : IMessageBoxService
    {
        // ----- Public methods
        public void ShowInformation(string message)
        {
            MessageBox.Show(message, @"Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public UserAnswer AskQuestion(string question)
        {
            var dialogResult = MessageBox.Show(question, @"Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            return dialogResult == DialogResult.Yes ? UserAnswer.Yes : UserAnswer.No;
        }
    }
}
