﻿namespace Fortis.Winforms
{
    partial class FormLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageWorkingFolder = new System.Windows.Forms.TabPage();
            this.groupBoxCreateWorkingFolder = new System.Windows.Forms.GroupBox();
            this.createNewWorkingFolderView = new Fortis.Winforms.UserControls.CreateNewWorkingFolderView();
            this.groupBoxLoadWorkingFolder = new System.Windows.Forms.GroupBox();
            this.loadWorkingFolderView = new Fortis.Winforms.UserControls.LoadWorkingFolderView();
            this.tabControl.SuspendLayout();
            this.tabPageWorkingFolder.SuspendLayout();
            this.groupBoxCreateWorkingFolder.SuspendLayout();
            this.groupBoxLoadWorkingFolder.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageWorkingFolder);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(659, 303);
            this.tabControl.TabIndex = 1;
            // 
            // tabPageWorkingFolder
            // 
            this.tabPageWorkingFolder.Controls.Add(this.groupBoxCreateWorkingFolder);
            this.tabPageWorkingFolder.Controls.Add(this.groupBoxLoadWorkingFolder);
            this.tabPageWorkingFolder.ImageIndex = 0;
            this.tabPageWorkingFolder.Location = new System.Drawing.Point(4, 29);
            this.tabPageWorkingFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPageWorkingFolder.Name = "tabPageWorkingFolder";
            this.tabPageWorkingFolder.Padding = new System.Windows.Forms.Padding(8);
            this.tabPageWorkingFolder.Size = new System.Drawing.Size(651, 270);
            this.tabPageWorkingFolder.TabIndex = 0;
            this.tabPageWorkingFolder.Text = "Dossier";
            this.tabPageWorkingFolder.ToolTipText = "Dossiers";
            this.tabPageWorkingFolder.UseVisualStyleBackColor = true;
            // 
            // groupBoxCreateWorkingFolder
            // 
            this.groupBoxCreateWorkingFolder.Controls.Add(this.createNewWorkingFolderView);
            this.groupBoxCreateWorkingFolder.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxCreateWorkingFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxCreateWorkingFolder.Location = new System.Drawing.Point(8, 125);
            this.groupBoxCreateWorkingFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxCreateWorkingFolder.Name = "groupBoxCreateWorkingFolder";
            this.groupBoxCreateWorkingFolder.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxCreateWorkingFolder.Size = new System.Drawing.Size(635, 114);
            this.groupBoxCreateWorkingFolder.TabIndex = 1;
            this.groupBoxCreateWorkingFolder.TabStop = false;
            this.groupBoxCreateWorkingFolder.Text = "Création d\'un nouveau dossier";
            // 
            // createNewWorkingFolderView
            // 
            this.createNewWorkingFolderView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(249)))), ((int)(((byte)(240)))));
            this.createNewWorkingFolderView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createNewWorkingFolderView.Location = new System.Drawing.Point(4, 24);
            this.createNewWorkingFolderView.Margin = new System.Windows.Forms.Padding(0);
            this.createNewWorkingFolderView.Name = "createNewWorkingFolderView";
            this.createNewWorkingFolderView.Size = new System.Drawing.Size(627, 85);
            this.createNewWorkingFolderView.TabIndex = 0;
            // 
            // groupBoxLoadWorkingFolder
            // 
            this.groupBoxLoadWorkingFolder.Controls.Add(this.loadWorkingFolderView);
            this.groupBoxLoadWorkingFolder.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxLoadWorkingFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxLoadWorkingFolder.Location = new System.Drawing.Point(8, 8);
            this.groupBoxLoadWorkingFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxLoadWorkingFolder.Name = "groupBoxLoadWorkingFolder";
            this.groupBoxLoadWorkingFolder.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxLoadWorkingFolder.Size = new System.Drawing.Size(635, 117);
            this.groupBoxLoadWorkingFolder.TabIndex = 0;
            this.groupBoxLoadWorkingFolder.TabStop = false;
            this.groupBoxLoadWorkingFolder.Text = "Chargement d\'un dossier existant";
            // 
            // loadWorkingFolderView
            // 
            this.loadWorkingFolderView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(249)))), ((int)(((byte)(240)))));
            this.loadWorkingFolderView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadWorkingFolderView.Location = new System.Drawing.Point(4, 24);
            this.loadWorkingFolderView.Margin = new System.Windows.Forms.Padding(0);
            this.loadWorkingFolderView.Name = "loadWorkingFolderView";
            this.loadWorkingFolderView.Size = new System.Drawing.Size(627, 88);
            this.loadWorkingFolderView.TabIndex = 0;
            // 
            // FormLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 303);
            this.Controls.Add(this.tabControl);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormLoad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chargement des données";
            this.tabControl.ResumeLayout(false);
            this.tabPageWorkingFolder.ResumeLayout(false);
            this.groupBoxCreateWorkingFolder.ResumeLayout(false);
            this.groupBoxLoadWorkingFolder.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageWorkingFolder;
        private System.Windows.Forms.GroupBox groupBoxCreateWorkingFolder;
        private System.Windows.Forms.GroupBox groupBoxLoadWorkingFolder;
        private UserControls.LoadWorkingFolderView loadWorkingFolderView;
        private UserControls.CreateNewWorkingFolderView createNewWorkingFolderView;
    }
}