﻿using System;
using Fortis.Business.ViewModels;
using Fortis.Winforms.Controls;

namespace Fortis.Winforms
{
    public partial class FormLoad : PopupForm
    {
        // ----- Fields
        private readonly LoadViewModel _viewModel = InversionOfControl.Instance.GetInstance<LoadViewModel>();

        // ----- Constructors
        public FormLoad()
        {
            InitializeComponent();

            BindPopupVisibility(_viewModel, viewModel => viewModel.IsVisible);

            _viewModel.Initialize();

            Disposed += OnDispose;
        }

        // ----- Internal logics
        private void OnDispose(object sender, EventArgs e)
        {
            _viewModel.Clean();
        }
    }
}