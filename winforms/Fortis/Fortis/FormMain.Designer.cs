﻿using Fortis.Winforms.Controls;

namespace Fortis.Winforms
{
    partial class FormMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.panel = new System.Windows.Forms.Panel();
            this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.bottomBar = new Fortis.Winforms.UserControls.BottomBar();
            this.topBar = new Fortis.Winforms.UserControls.TopBar();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.workingFoldersToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.loadToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.saveToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.closeToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.actionsToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.showStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.agencementToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.emptyLayoutToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.browseLayoutToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.viewsToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.listViewToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.mapViewToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.picViewToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.dToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.aToolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.detailToolStripMenuItem = new Fortis.Winforms.Controls.CommandToolStripMenuItem();
            this.panel.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.dockPanel);
            this.panel.Controls.Add(this.bottomBar);
            this.panel.Controls.Add(this.topBar);
            this.panel.Controls.Add(this.menuStrip);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1431, 1066);
            this.panel.TabIndex = 1;
            // 
            // dockPanel
            // 
            this.dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
            this.dockPanel.Location = new System.Drawing.Point(0, 84);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.Size = new System.Drawing.Size(1431, 953);
            this.dockPanel.TabIndex = 1;
            // 
            // bottomBar
            // 
            this.bottomBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomBar.Location = new System.Drawing.Point(0, 1037);
            this.bottomBar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bottomBar.Name = "bottomBar";
            this.bottomBar.Size = new System.Drawing.Size(1431, 29);
            this.bottomBar.TabIndex = 6;
            // 
            // topBar
            // 
            this.topBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.topBar.Location = new System.Drawing.Point(0, 33);
            this.topBar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.topBar.Name = "topBar";
            this.topBar.Size = new System.Drawing.Size(1431, 51);
            this.topBar.TabIndex = 3;
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.workingFoldersToolStripMenuItem,
            this.actionsToolStripMenuItem,
            this.showStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1431, 33);
            this.menuStrip.TabIndex = 5;
            this.menuStrip.Text = "menuStrip1";
            // 
            // workingFoldersToolStripMenuItem
            // 
            this.workingFoldersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.workingFoldersToolStripMenuItem.Name = "workingFoldersToolStripMenuItem";
            this.workingFoldersToolStripMenuItem.Size = new System.Drawing.Size(91, 29);
            this.workingFoldersToolStripMenuItem.Text = "Dossiers";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("loadToolStripMenuItem.Image")));
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(297, 30);
            this.loadToolStripMenuItem.Text = "Charger ...";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(297, 30);
            this.saveToolStripMenuItem.Text = "Sauvegarder";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("closeToolStripMenuItem.Image")));
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(297, 30);
            this.closeToolStripMenuItem.Text = "Fermer le dossier";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(294, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(297, 30);
            this.exitToolStripMenuItem.Text = "Quitter";
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(83, 29);
            this.actionsToolStripMenuItem.Text = "Actions";
            // 
            // showStripMenuItem
            // 
            this.showStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agencementToolStripMenuItem,
            this.viewsToolStripMenuItem});
            this.showStripMenuItem.Name = "showStripMenuItem";
            this.showStripMenuItem.Size = new System.Drawing.Size(99, 29);
            this.showStripMenuItem.Text = "Affichage";
            // 
            // agencementToolStripMenuItem
            // 
            this.agencementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emptyLayoutToolStripMenuItem,
            this.toolStripSeparator2,
            this.browseLayoutToolStripMenuItem});
            this.agencementToolStripMenuItem.Name = "agencementToolStripMenuItem";
            this.agencementToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.agencementToolStripMenuItem.Text = "Agencement";
            // 
            // emptyLayoutToolStripMenuItem
            // 
            this.emptyLayoutToolStripMenuItem.Name = "emptyLayoutToolStripMenuItem";
            this.emptyLayoutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.emptyLayoutToolStripMenuItem.Size = new System.Drawing.Size(238, 30);
            this.emptyLayoutToolStripMenuItem.Text = "Vide";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(235, 6);
            // 
            // browseLayoutToolStripMenuItem
            // 
            this.browseLayoutToolStripMenuItem.Name = "browseLayoutToolStripMenuItem";
            this.browseLayoutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.browseLayoutToolStripMenuItem.Size = new System.Drawing.Size(238, 30);
            this.browseLayoutToolStripMenuItem.Text = "Consultation";
            // 
            // viewsToolStripMenuItem
            // 
            this.viewsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listViewToolStripMenuItem,
            this.mapViewToolStripMenuItem,
            this.picViewToolStripMenuItem,
            this.detailToolStripMenuItem});
            this.viewsToolStripMenuItem.Name = "viewsToolStripMenuItem";
            this.viewsToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.viewsToolStripMenuItem.Text = "Vues";
            // 
            // listViewToolStripMenuItem
            // 
            this.listViewToolStripMenuItem.Name = "listViewToolStripMenuItem";
            this.listViewToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.listViewToolStripMenuItem.Text = "Liste";
            // 
            // mapViewToolStripMenuItem
            // 
            this.mapViewToolStripMenuItem.Name = "mapViewToolStripMenuItem";
            this.mapViewToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.mapViewToolStripMenuItem.Text = "Carte";
            // 
            // picViewToolStripMenuItem
            // 
            this.picViewToolStripMenuItem.Name = "picViewToolStripMenuItem";
            this.picViewToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.picViewToolStripMenuItem.Text = "Photo";
            // 
            // dToolStripMenuItem
            // 
            this.dToolStripMenuItem.Name = "dToolStripMenuItem";
            this.dToolStripMenuItem.Size = new System.Drawing.Size(235, 6);
            // 
            // aToolStripMenuItem1
            // 
            this.aToolStripMenuItem1.Name = "aToolStripMenuItem1";
            this.aToolStripMenuItem1.Size = new System.Drawing.Size(235, 6);
            // 
            // detailToolStripMenuItem
            // 
            this.detailToolStripMenuItem.Name = "detailToolStripMenuItem";
            this.detailToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.detailToolStripMenuItem.Text = "Détail";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1431, 1066);
            this.Controls.Add(this.panel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fortis";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTipMain;
        private System.Windows.Forms.Panel panel;
        private UserControls.TopBar topBar;
        private UserControls.BottomBar bottomBar;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripSeparator dToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator aToolStripMenuItem1;
        private CommandToolStripMenuItem workingFoldersToolStripMenuItem;
        private CommandToolStripMenuItem actionsToolStripMenuItem;
        private CommandToolStripMenuItem showStripMenuItem;
        private CommandToolStripMenuItem loadToolStripMenuItem;
        private CommandToolStripMenuItem saveToolStripMenuItem;
        private CommandToolStripMenuItem closeToolStripMenuItem;
        private CommandToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private CommandToolStripMenuItem agencementToolStripMenuItem;
        private CommandToolStripMenuItem viewsToolStripMenuItem;
        private CommandToolStripMenuItem listViewToolStripMenuItem;
        private CommandToolStripMenuItem mapViewToolStripMenuItem;
        private CommandToolStripMenuItem picViewToolStripMenuItem;
        private CommandToolStripMenuItem emptyLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private CommandToolStripMenuItem browseLayoutToolStripMenuItem;
        private CommandToolStripMenuItem detailToolStripMenuItem;
    }
}

