﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fortis.Business.Base;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Messages;
using Fortis.Common.Utils;
using Fortis.Winforms.Controls;
using Fortis.Winforms.Controls.Dock;
using Fortis.Winforms.Utils;
using WeifenLuo.WinFormsUI.Docking;

namespace Fortis.Winforms
{
    public partial class FormMain : Form
    {
        // ----- Fields
        private FormLoad _formLoad = new FormLoad();

        private readonly MainViewModel _mainViewModel = InversionOfControl.Instance.GetInstance<MainViewModel>();
        private readonly SaveViewModel _saveViewModel = InversionOfControl.Instance.GetInstance<SaveViewModel>();

        private readonly IBus _bus = InversionOfControl.Instance.GetInstance<IBus>();
        private readonly IEnumerable<DockContentExtended> _dockContents;

        // ----- DockContents
        public ListViewDockContent ListViewDockContent => _dockContents.OfType<ListViewDockContent>().Single();
        public GoogleMapDockContent GoogleMapDockContent => _dockContents.OfType<GoogleMapDockContent>().Single();
        public CardViewDockContent CardViewDockContent => _dockContents.OfType<CardViewDockContent>().Single();
        public PictureDockContent PictureDockContent => _dockContents.OfType<PictureDockContent>().Single();

        // ----- Constructor
        public FormMain()
        {
            InitializeComponent();

            dockPanel.Theme = new VS2015LightTheme();
            dockPanel.Theme.Extender.FloatWindowFactory = new BoxedFloatWindowFactory();

            _dockContents = new List<DockContentExtended> {
                new ListViewDockContent(listViewToolStripMenuItem),
                new GoogleMapDockContent(mapViewToolStripMenuItem),
                new CardViewDockContent(detailToolStripMenuItem),
                new PictureDockContent(picViewToolStripMenuItem)
            };

            if (DesignTimeHelper.IsInDesignMode) {
                SetBrowseLayout();
                return;
            }

            InitializeLayout();

            exitToolStripMenuItem.Command = _mainViewModel.ExitCommand;
            saveToolStripMenuItem.Command = _saveViewModel.SaveCommand;

            ConfigureLayouts();

            _bus.Register<ExitMessage>(this, OnExit);
        }

        // ----- Internal logics
        private void InitializeLayout()
        {
            var filePath = GetLastLayoutPath();
            if (File.Exists(filePath))
                InitializeLayout(filePath);
            else
                SetEmptyLayout();
        }
        private void InitializeLayout(string filePath)
        {
            dockPanel.LoadFromXml(filePath, dockContentIdentifier => {
                return _dockContents.FirstOrDefault(dockContent => dockContent.GetType().Name == dockContentIdentifier?.Split('.').Last());
            });
            foreach (var contentNotPositioned in _dockContents.Where(x => x.Pane == null)) {
                contentNotPositioned.Show(dockPanel, DockState.Float);
                contentNotPositioned.Hide();
            }
            SetFloatWindowsOwner(this);
        }
        private void ConfigureLayouts()
        {
            emptyLayoutToolStripMenuItem.Click += (sender, args) => SetEmptyLayout();
            browseLayoutToolStripMenuItem.Click += (sender, args) => SetBrowseLayout();
        }
        public void SetEmptyLayout()
        {
            dockPanel.DockTopPortion = 0.5;
            dockPanel.DockBottomPortion = 0.5;
            dockPanel.DockRightPortion = 0.5;
            dockPanel.DockLeftPortion = 0.5;

            ListViewDockContent.Show(dockPanel, DockState.Document);
            ListViewDockContent.Hide();

            foreach (var dockContent in _dockContents) {
                if (dockContent != ListViewDockContent) {
                    dockContent.Show(ListViewDockContent.Pane, null);
                    dockContent.Hide();
                }
            }
        }
        public void SetBrowseLayout()
        {
            dockPanel.DockTopPortion = 0.5;
            dockPanel.DockBottomPortion = 0.5;
            dockPanel.DockRightPortion = 0.5;
            dockPanel.DockLeftPortion = 0.5;

            ListViewDockContent.Show(dockPanel, DockState.Document);

            CardViewDockContent.Show(dockPanel, DockState.DockRightAutoHide);
            PictureDockContent.Show(dockPanel, DockState.DockRightAutoHide);
            GoogleMapDockContent.Show(dockPanel, DockState.DockRightAutoHide);
            CardViewDockContent.Show(dockPanel, DockState.DockRightAutoHide);

            ListViewDockContent.Pane.ActiveContent = ListViewDockContent;
        }
        private void SaveUserSettings()
        {
            CardViewDockContent.Control.SaveSettings();
            PictureDockContent.Control.SaveSettings();
            GoogleMapDockContent.Control.SaveSettings();

            dockPanel.SaveAsXml(GetLastLayoutPath(), Encoding.UTF8);
        }
        private void SetFloatWindowsOwner(Form form)
        {
            foreach (var floatWindow in dockPanel.FloatWindows.ToArray()) {
                floatWindow.Owner = form;
            }
        }
        private static string GetLastLayoutPath()
        {
            return Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                AssemblyInfo.GetCompanyName(),
                AssemblyInfo.GetApplicationName(),
                "last-layout.xml");
        }

        // ----- Business callbacks
        private void OnExit(ExitMessage message)
        {
            Close();
        }

        // ----- UI callbacks
        private void FormMain_Shown(object sender, EventArgs e)
        {
            _mainViewModel.Initialize();
            _saveViewModel.Initialize();
        }
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveUserSettings();

            _mainViewModel.Clean();
            _saveViewModel.Clean();
        }

        // ----- Overrides
        protected override void OnClosing(CancelEventArgs e)
        {
            var answer = MessageBox.Show(@"Êtes-vous sûr de vouloir quitter Fortis ?", @"Quitter Fortis",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (answer == DialogResult.No)
                e.Cancel = true;
            SaveUserSettings();
            base.OnClosing(e);
        }
    }
}