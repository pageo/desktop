﻿using System;
using System.IO;
using Fortis.Business;
using Fortis.Business.Base;
using Fortis.Business.Feeds;
using Fortis.Business.Infrastructure.Authentication;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Services;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.WorkingFolders;
using Fortis.DataAccess;
using Fortis.DataAccess.Authentication;
using Fortis.DataAccess.Repositories;
using Fortis.DataAccess.Repositories.Base;
using Fortis.Winforms.Dialog;
using Fortis.Winforms.Utils;
using StructureMap;

namespace Fortis.Winforms
{
    public class InversionOfControl
    {
        // ----- Fields
        private readonly Container _container;

        // ----- Properties
        public static InversionOfControl Instance { get; } = new InversionOfControl();

        // ----- Constructors
        public InversionOfControl()
        {
            _container = new Container(configuration => {
                // UI
                configuration.For<IFileBrowserService>().Use<FileBrowserService>().Singleton();
                configuration.For<IMessageBoxService>().Use<WinformMessageBoxService>().Singleton();

                // ViewModels
                configuration.For<ActionViewModel>().Singleton();
                configuration.For<BottomBarViewModel>().Singleton();
                configuration.For<ActionViewModel>().Singleton();
                configuration.For<LoadViewModel>().Singleton();
                configuration.For<MainViewModel>().Singleton();
                configuration.For<SaveViewModel>().Singleton();
                configuration.For<LoadWorkingFolderViewModel>().Singleton();
                configuration.For<CreateAndLoadNewWorkingFolderViewModel>().Singleton();
                configuration.For<ManageWorkingFolderViewModel>().Singleton();

                // Business
                configuration.For<IColumnTranslationService>().Use<ColumnAttributeTranslationService>();
                configuration.For<ICollectionFactory>().Use<CollectionFactory>();
                configuration.For<IBus>().Use<Bus>().Singleton();
                configuration.For<IClock>().Use<Clock>().Singleton();
                configuration.For<Authenticator>().Singleton();
                configuration.For<ApiToken>().Singleton();

                // Services
                if (DesignTimeHelper.IsInDesignMode)
                    configuration.For<EnvironmentSettings>().Use(new EnvironmentSettings { FortisApiServiceUrl = "http://localhost" }).Singleton();
                else
                    configuration.For<EnvironmentSettings>().Use(info => EnvironmentSettings.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Fortis.Winforms.exe.json"))).Singleton();
                configuration.For<IAuthenticationService>().Use<OnlineAuthenticationService>().Singleton();

                // Repositories
                configuration.For<IUserRepository>().Use<UserRestRepository>();
                configuration.For<IFeedRepository>().Use<FeedRestRepository>();
                configuration.For<IWorkingFolderRepository>().Use<WorkingFolderRestRepository>();
            });
        }

        // ----- Public methods
        public T GetInstance<T>()
        {
            return _container.GetInstance<T>();
        }
    }
}