﻿using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Threading;
using System.Windows.Forms;
using Fortis.Business.Infrastructure.Authentication;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Winforms
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SetProcessDPIAware();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += ApplicationOnThreadException; ;

            try
            {
                InversionOfControl.Instance.GetInstance<Authenticator>().Authenticate();
                CommandManager.Configure(new WinformNotifyApplicationStateChanged());
                Application.Run(new FormMain());
            }
            catch (AuthenticationException ex) {
                MessageBox.Show(ex.Message, @"Erreur d'accès", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex) {
                ApplicationOnThreadException(new object(), new ThreadExceptionEventArgs(ex));
            }
        }

        // ----- Internal logics
        private static void ApplicationOnThreadException(object sender, ThreadExceptionEventArgs args)
        {
            var messageDetail = GetMessageDetail(args.Exception);

            MessageBox.Show($@"Une erreur non gérée est survenue. {Environment.NewLine} {messageDetail}", @"Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static string GetMessageDetail(Exception exception)
        {
            var messages = new List<string>();
            while (exception != null) {
                messages.Add($" -  {exception.Message}");
                exception = exception.InnerException;
            }
            return string.Join(Environment.NewLine, messages);
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();
    }
}