﻿namespace Fortis.Winforms.UserControls
{
    partial class BottomBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblSeconds = new System.Windows.Forms.Label();
            this.lblEllapsed = new System.Windows.Forms.Label();
            this.lblElement = new System.Windows.Forms.Label();
            this.lblElementCount = new System.Windows.Forms.Label();
            this.lblDe = new System.Windows.Forms.Label();
            this.lblWorkingFolderEffectiveDate = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblWorkingFolderName = new System.Windows.Forms.Label();
            this.lblFolder = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panelAudienceLoading = new System.Windows.Forms.Panel();
            this.labelAudience = new System.Windows.Forms.Label();
            this.mainTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.panelAudienceLoading.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.lblSeconds);
            this.flowLayoutPanel1.Controls.Add(this.lblEllapsed);
            this.flowLayoutPanel1.Controls.Add(this.lblElement);
            this.flowLayoutPanel1.Controls.Add(this.lblElementCount);
            this.flowLayoutPanel1.Controls.Add(this.lblDe);
            this.flowLayoutPanel1.Controls.Add(this.lblWorkingFolderEffectiveDate);
            this.flowLayoutPanel1.Controls.Add(this.lblDate);
            this.flowLayoutPanel1.Controls.Add(this.lblWorkingFolderName);
            this.flowLayoutPanel1.Controls.Add(this.lblFolder);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(335, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(465, 18);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // lblSeconds
            // 
            this.lblSeconds.AutoSize = true;
            this.lblSeconds.Location = new System.Drawing.Point(412, 0);
            this.lblSeconds.Margin = new System.Windows.Forms.Padding(0);
            this.lblSeconds.Name = "lblSeconds";
            this.lblSeconds.Size = new System.Drawing.Size(53, 13);
            this.lblSeconds.TabIndex = 4;
            this.lblSeconds.Text = "secondes";
            // 
            // lblEllapsed
            // 
            this.lblEllapsed.AutoSize = true;
            this.lblEllapsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEllapsed.Location = new System.Drawing.Point(398, 0);
            this.lblEllapsed.Margin = new System.Windows.Forms.Padding(0);
            this.lblEllapsed.Name = "lblEllapsed";
            this.lblEllapsed.Size = new System.Drawing.Size(14, 13);
            this.lblEllapsed.TabIndex = 3;
            this.lblEllapsed.Text = "0";
            // 
            // lblElement
            // 
            this.lblElement.AutoSize = true;
            this.lblElement.Location = new System.Drawing.Point(293, 0);
            this.lblElement.Margin = new System.Windows.Forms.Padding(0);
            this.lblElement.Name = "lblElement";
            this.lblElement.Size = new System.Drawing.Size(105, 13);
            this.lblElement.TabIndex = 2;
            this.lblElement.Text = "éléments chargés en";
            // 
            // lblElementCount
            // 
            this.lblElementCount.AutoSize = true;
            this.lblElementCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElementCount.Location = new System.Drawing.Point(279, 0);
            this.lblElementCount.Margin = new System.Windows.Forms.Padding(0);
            this.lblElementCount.Name = "lblElementCount";
            this.lblElementCount.Size = new System.Drawing.Size(14, 13);
            this.lblElementCount.TabIndex = 1;
            this.lblElementCount.Text = "0";
            // 
            // lblDe
            // 
            this.lblDe.AutoSize = true;
            this.lblDe.Location = new System.Drawing.Point(260, 0);
            this.lblDe.Margin = new System.Windows.Forms.Padding(0);
            this.lblDe.Name = "lblDe";
            this.lblDe.Size = new System.Drawing.Size(19, 13);
            this.lblDe.TabIndex = 8;
            this.lblDe.Text = "de";
            this.lblDe.Visible = false;
            // 
            // lblWorkingFolderEffectiveDate
            // 
            this.lblWorkingFolderEffectiveDate.AutoSize = true;
            this.lblWorkingFolderEffectiveDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblWorkingFolderEffectiveDate.Location = new System.Drawing.Point(185, 0);
            this.lblWorkingFolderEffectiveDate.Margin = new System.Windows.Forms.Padding(0);
            this.lblWorkingFolderEffectiveDate.Name = "lblWorkingFolderEffectiveDate";
            this.lblWorkingFolderEffectiveDate.Size = new System.Drawing.Size(75, 13);
            this.lblWorkingFolderEffectiveDate.TabIndex = 0;
            this.lblWorkingFolderEffectiveDate.Text = "21/03/2017";
            this.lblWorkingFolderEffectiveDate.Visible = false;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(142, 0);
            this.lblDate.Margin = new System.Windows.Forms.Padding(0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(43, 13);
            this.lblDate.TabIndex = 5;
            this.lblDate.Text = "daté du";
            this.lblDate.Visible = false;
            // 
            // lblWorkingFolderName
            // 
            this.lblWorkingFolderName.AutoSize = true;
            this.lblWorkingFolderName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblWorkingFolderName.Location = new System.Drawing.Point(50, 0);
            this.lblWorkingFolderName.Name = "lblWorkingFolderName";
            this.lblWorkingFolderName.Size = new System.Drawing.Size(89, 13);
            this.lblWorkingFolderName.TabIndex = 6;
            this.lblWorkingFolderName.Text = "France entière";
            this.lblWorkingFolderName.Visible = false;
            // 
            // lblFolder
            // 
            this.lblFolder.AutoSize = true;
            this.lblFolder.Location = new System.Drawing.Point(5, 0);
            this.lblFolder.Margin = new System.Windows.Forms.Padding(0);
            this.lblFolder.Name = "lblFolder";
            this.lblFolder.Size = new System.Drawing.Size(42, 13);
            this.lblFolder.TabIndex = 7;
            this.lblFolder.Text = "Dossier";
            this.lblFolder.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 0);
            this.progressBar1.MarqueeAnimationSpeed = 30;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(145, 18);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 2;
            // 
            // panelAudienceLoading
            // 
            this.panelAudienceLoading.Controls.Add(this.labelAudience);
            this.panelAudienceLoading.Controls.Add(this.progressBar1);
            this.panelAudienceLoading.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelAudienceLoading.Location = new System.Drawing.Point(0, 0);
            this.panelAudienceLoading.Name = "panelAudienceLoading";
            this.panelAudienceLoading.Size = new System.Drawing.Size(151, 19);
            this.panelAudienceLoading.TabIndex = 3;
            this.panelAudienceLoading.Visible = false;
            // 
            // labelAudience
            // 
            this.labelAudience.AutoSize = true;
            this.labelAudience.BackColor = System.Drawing.Color.Transparent;
            this.labelAudience.Location = new System.Drawing.Point(25, 2);
            this.labelAudience.Name = "labelAudience";
            this.labelAudience.Size = new System.Drawing.Size(103, 13);
            this.labelAudience.TabIndex = 3;
            this.labelAudience.Text = "Calcul d\'audience ...";
            // 
            // BottomBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelAudienceLoading);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "BottomBar";
            this.Size = new System.Drawing.Size(800, 19);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panelAudienceLoading.ResumeLayout(false);
            this.panelAudienceLoading.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblSeconds;
        private System.Windows.Forms.Label lblEllapsed;
        private System.Windows.Forms.Label lblElement;
        private System.Windows.Forms.Label lblElementCount;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panelAudienceLoading;
        private System.Windows.Forms.Label labelAudience;
        private System.Windows.Forms.ToolTip mainTooltip;
        private System.Windows.Forms.Label lblWorkingFolderEffectiveDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblWorkingFolderName;
        private System.Windows.Forms.Label lblFolder;
        private System.Windows.Forms.Label lblDe;
    }
}
