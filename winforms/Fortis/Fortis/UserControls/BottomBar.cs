﻿using System.Windows.Forms;
using Fortis.Business.ViewModels;
using Fortis.Winforms.Utils;

namespace Fortis.Winforms.UserControls
{
    public partial class BottomBar : UserControl
    {
        // ----- Fields
        private readonly BottomBarViewModel _viewModel = InversionOfControl.Instance.GetInstance<BottomBarViewModel>();

        // ----- Constructors
        public BottomBar()
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode) {
                return;
            }

            Load += (sender, args) => { _viewModel.Initialize(); };
        }
    }
}