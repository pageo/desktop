﻿using Fortis.Winforms.Controls;

namespace Fortis.Winforms.UserControls
{
    partial class CreateNewWorkingFolderView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateNewWorkingFolderView));
            this.txtName = new System.Windows.Forms.TextBox();
            this.panel = new System.Windows.Forms.Panel();
            this.label = new System.Windows.Forms.Label();
            this.btnCreateAndLoadWorkingFolder = new Fortis.Winforms.Controls.CommandButton();
            this.progressBarBusy = new System.Windows.Forms.ProgressBar();
            this.lblValidation = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblName = new System.Windows.Forms.Label();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // txtName
            // 
            this.errorProvider.SetIconPadding(this.txtName, 5);
            this.txtName.Location = new System.Drawing.Point(120, 6);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(314, 26);
            this.txtName.TabIndex = 0;
            // 
            // panel
            // 
            this.panel.Controls.Add(this.label);
            this.panel.Controls.Add(this.btnCreateAndLoadWorkingFolder);
            this.panel.Controls.Add(this.progressBarBusy);
            this.panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel.Location = new System.Drawing.Point(0, 37);
            this.panel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(449, 46);
            this.panel.TabIndex = 14;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.ForeColor = System.Drawing.Color.Red;
            this.label.Location = new System.Drawing.Point(4, 12);
            this.label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(161, 20);
            this.label.TabIndex = 16;
            this.label.Text = "Erreur de validation ...";
            this.label.Visible = false;
            // 
            // btnCreateAndLoadWorkingFolder
            // 
            this.btnCreateAndLoadWorkingFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateAndLoadWorkingFolder.Location = new System.Drawing.Point(263, 5);
            this.btnCreateAndLoadWorkingFolder.Name = "btnCreateAndLoadWorkingFolder";
            this.btnCreateAndLoadWorkingFolder.Size = new System.Drawing.Size(150, 35);
            this.btnCreateAndLoadWorkingFolder.TabIndex = 17;
            this.btnCreateAndLoadWorkingFolder.Text = "Créer";
            this.btnCreateAndLoadWorkingFolder.UseVisualStyleBackColor = true;
            // 
            // progressBarBusy
            // 
            this.progressBarBusy.Location = new System.Drawing.Point(9, 5);
            this.progressBarBusy.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.progressBarBusy.MarqueeAnimationSpeed = 30;
            this.progressBarBusy.Name = "progressBarBusy";
            this.progressBarBusy.Size = new System.Drawing.Size(150, 35);
            this.progressBarBusy.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarBusy.TabIndex = 15;
            this.progressBarBusy.Visible = false;
            // 
            // lblValidation
            // 
            this.lblValidation.AutoSize = true;
            this.lblValidation.ForeColor = System.Drawing.Color.Red;
            this.lblValidation.Location = new System.Drawing.Point(4, 14);
            this.lblValidation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblValidation.Name = "lblValidation";
            this.lblValidation.Size = new System.Drawing.Size(161, 20);
            this.lblValidation.TabIndex = 14;
            this.lblValidation.Text = "Erreur de validation ...";
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            this.errorProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider.Icon")));
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(4, 9);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(50, 20);
            this.lblName.TabIndex = 15;
            this.lblName.Text = "Nom :";
            // 
            // CreateNewWorkingFolderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(249)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.txtName);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CreateNewWorkingFolderView";
            this.Size = new System.Drawing.Size(449, 83);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtName;
        private CommandButton btnCreateAndLoadWorkingFolder;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Label lblValidation;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ProgressBar progressBarBusy;
        private System.Windows.Forms.Label lblName;
    }
}
