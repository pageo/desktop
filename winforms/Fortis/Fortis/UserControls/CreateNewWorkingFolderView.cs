﻿using System;
using System.Windows.Forms;
using Fortis.Business.ViewModels.WorkingFolders;
using Fortis.Winforms.Controls;
using Fortis.Winforms.Converters;
using Fortis.Winforms.Utils;

namespace Fortis.Winforms.UserControls
{
    public partial class CreateNewWorkingFolderView : UserControl
    {
        // ----- Fields
        public readonly CreateAndLoadNewWorkingFolderViewModel ViewModel = InversionOfControl.Instance.GetInstance<CreateAndLoadNewWorkingFolderViewModel>();

        // ----- Constructors
        public CreateNewWorkingFolderView()
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode) {
                return;
            }

            txtName.Bind(x => x.Text).To(ViewModel).On(viewModel => viewModel.Name);
            txtName.Enter += TxtNameOnEnter;

            progressBarBusy.BindVisibility(ViewModel, x => x.IsLoading);

            lblValidation.Bind(x => x.Text).To(ViewModel).On(x => x.Error);
            lblValidation.BindVisibility(ViewModel, x => x.HasValidationErrors);
            errorProvider.DataSource = ViewModel;

            btnCreateAndLoadWorkingFolder.Command = ViewModel.CreateAndLoadWorkingFolderCommand;

            this.Bind(x => x.Enabled).To(ViewModel).On(x => x.IsLoading).UseConverter(new ReverseBooleanConverter());

            Load += CreateNewWorkingFolderView_Load;
            HandleDestroyed += CreateNewWorkingFolderView_HandleDestroyed;
        }

        // ----- Callbacks
        private void TxtNameOnEnter(object sender, EventArgs eventArgs)
        {
            ViewModel.ShowValidationErrors();
        }
        private async void CreateNewWorkingFolderView_Load(object sender, EventArgs e)
        {
            await ViewModel.Initialize();
        }
        private void CreateNewWorkingFolderView_HandleDestroyed(object sender, EventArgs eventArgs)
        {
            ViewModel.Clean();
        }
    }
}