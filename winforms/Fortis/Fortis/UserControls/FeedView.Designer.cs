﻿namespace Fortis.Winforms.UserControls
{
    partial class FeedView
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbbFeeds = new Fortis.Winforms.Controls.ComboboxExtended();
            this.lblFeeds = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbbFeeds
            // 
            this.cbbFeeds.FormattingEnabled = true;
            this.cbbFeeds.Location = new System.Drawing.Point(54, 16);
            this.cbbFeeds.Name = "cbbFeeds";
            this.cbbFeeds.Size = new System.Drawing.Size(159, 21);
            this.cbbFeeds.TabIndex = 0;
            // 
            // lblFeeds
            // 
            this.lblFeeds.AutoSize = true;
            this.lblFeeds.Location = new System.Drawing.Point(3, 19);
            this.lblFeeds.Name = "lblFeeds";
            this.lblFeeds.Size = new System.Drawing.Size(42, 13);
            this.lblFeeds.TabIndex = 1;
            this.lblFeeds.Text = "Feeds :";
            // 
            // LoadFeedView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblFeeds);
            this.Controls.Add(this.cbbFeeds);
            this.Name = "LoadFeedView";
            this.Size = new System.Drawing.Size(224, 53);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.ComboboxExtended cbbFeeds;
        private System.Windows.Forms.Label lblFeeds;
    }
}
