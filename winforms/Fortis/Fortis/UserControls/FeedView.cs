﻿using System.Windows.Forms;
using Fortis.Business.ViewModels.Feeds;
using Fortis.Winforms.Utils;

namespace Fortis.Winforms.UserControls
{
    public partial class FeedView : UserControl
    {
        // ----- Fields
        private readonly LoadFeedsViewModel _viewModel = InversionOfControl.Instance.GetInstance<LoadFeedsViewModel>();

        // ----- Constructors
        public FeedView()
        {
            InitializeComponent();

            cbbFeeds.Bind(x => x.DataSource).To(_viewModel).On(x => x.Feeds).TwoWay();
            cbbFeeds.DisplayMember = "Name";

            _viewModel.Initialize();
        }
    }
}