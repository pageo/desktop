﻿using Fortis.Winforms.Controls;

namespace Fortis.Winforms.UserControls
{
    partial class LoadWorkingFolderView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelFolder = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.buttonManageWorkingFolder = new Fortis.Winforms.Controls.CommandButton();
            this.progressBarBusy = new System.Windows.Forms.ProgressBar();
            this.buttonLoadWorkingFolder = new Fortis.Winforms.Controls.CommandButton();
            this.multiColumnComboBox = new Fortis.Winforms.Controls.MultiColumnCombobox();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelFolder
            // 
            this.labelFolder.AutoSize = true;
            this.labelFolder.Location = new System.Drawing.Point(4, 9);
            this.labelFolder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelFolder.Name = "labelFolder";
            this.labelFolder.Size = new System.Drawing.Size(71, 20);
            this.labelFolder.TabIndex = 2;
            this.labelFolder.Text = "Dossier :";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.buttonManageWorkingFolder);
            this.panel.Controls.Add(this.progressBarBusy);
            this.panel.Controls.Add(this.buttonLoadWorkingFolder);
            this.panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel.Location = new System.Drawing.Point(0, 43);
            this.panel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(544, 51);
            this.panel.TabIndex = 4;
            // 
            // buttonManageWorkingFolder
            // 
            this.buttonManageWorkingFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonManageWorkingFolder.Location = new System.Drawing.Point(202, 6);
            this.buttonManageWorkingFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonManageWorkingFolder.Name = "buttonManageWorkingFolder";
            this.buttonManageWorkingFolder.Size = new System.Drawing.Size(150, 35);
            this.buttonManageWorkingFolder.TabIndex = 3;
            this.buttonManageWorkingFolder.Text = "Gérer...";
            this.buttonManageWorkingFolder.UseVisualStyleBackColor = true;
            // 
            // progressBarBusy
            // 
            this.progressBarBusy.Location = new System.Drawing.Point(9, 6);
            this.progressBarBusy.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.progressBarBusy.MarqueeAnimationSpeed = 30;
            this.progressBarBusy.Name = "progressBarBusy";
            this.progressBarBusy.Size = new System.Drawing.Size(150, 35);
            this.progressBarBusy.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarBusy.TabIndex = 1;
            this.progressBarBusy.Visible = false;
            // 
            // buttonLoadWorkingFolder
            // 
            this.buttonLoadWorkingFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoadWorkingFolder.Location = new System.Drawing.Point(358, 6);
            this.buttonLoadWorkingFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonLoadWorkingFolder.Name = "buttonLoadWorkingFolder";
            this.buttonLoadWorkingFolder.Size = new System.Drawing.Size(150, 35);
            this.buttonLoadWorkingFolder.TabIndex = 2;
            this.buttonLoadWorkingFolder.Text = "Charger";
            this.buttonLoadWorkingFolder.UseVisualStyleBackColor = true;
            // 
            // multiColumnComboBox
            // 
            this.multiColumnComboBox.DisplayMember = "Name";
            this.multiColumnComboBox.DropDownHeight = 200;
            this.multiColumnComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.multiColumnComboBox.DropDownWidth = 450;
            this.multiColumnComboBox.FormattingEnabled = true;
            this.multiColumnComboBox.IntegralHeight = false;
            this.multiColumnComboBox.Location = new System.Drawing.Point(102, 6);
            this.multiColumnComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.multiColumnComboBox.Name = "multiColumnComboBox";
            this.multiColumnComboBox.Size = new System.Drawing.Size(406, 28);
            this.multiColumnComboBox.TabIndex = 1;
            // 
            // LoadWorkingFolderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(249)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.multiColumnComboBox);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.labelFolder);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "LoadWorkingFolderView";
            this.Size = new System.Drawing.Size(544, 94);
            this.panel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelFolder;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.ProgressBar progressBarBusy;
        private CommandButton buttonLoadWorkingFolder;
        private MultiColumnCombobox multiColumnComboBox;
        private CommandButton buttonManageWorkingFolder;
    }
}
