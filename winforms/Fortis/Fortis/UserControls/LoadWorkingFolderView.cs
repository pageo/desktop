﻿using System;
using System.Windows.Forms;
using System.Windows.Input;
using Fortis.Business.ViewModels.WorkingFolders;
using Fortis.Winforms.Controls;
using Fortis.Winforms.Converters;
using Fortis.Winforms.Utils;

namespace Fortis.Winforms.UserControls
{
    public partial class LoadWorkingFolderView : UserControl
    {
        // ----- Fields
        public readonly LoadWorkingFolderViewModel ViewModel = InversionOfControl.Instance.GetInstance<LoadWorkingFolderViewModel>();
        private readonly ManageWorkingFolderViewModel _manageWorkingFolderViewModel = InversionOfControl.Instance.GetInstance<ManageWorkingFolderViewModel>();

        // ----- Constructors
        public LoadWorkingFolderView()
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode) {
                return;
            }

            multiColumnComboBox.Bind(x => x.SelectedItem).To(ViewModel).On(x => x.SelectedExistingWorkingFolder);
            multiColumnComboBox.Bind(x => x.DataSource).To(ViewModel).On(x => x.ExistingWorkingFolders);
            multiColumnComboBox.PopupColumns.AddRange(
                new DataGridViewTextBoxColumn { DataPropertyName = "Name", HeaderText = @"Nom"}
            );

            buttonLoadWorkingFolder.Command = (ICommand) ViewModel.LoadWorkingFolderCommand;
            buttonManageWorkingFolder.Command = _manageWorkingFolderViewModel.ShowCommand;

            progressBarBusy.BindVisibility(ViewModel, x => x.IsLoading);

            this.Bind(x => x.Enabled).To(ViewModel).On(x => x.IsLoading).UseConverter(new ReverseBooleanConverter());
            Load += LoadWorkingFolderView_Load1;
            HandleDestroyed += OnHandleDestroyed;
        }

        // ----- LoadWorkingFolderView
        public async void LoadWorkingFolders()
        {
            await ViewModel.LoadWorkingFolderCommand.ExecuteAsync();
        }
        private async void LoadWorkingFolderView_Load1(object sender, EventArgs e)
        {
            await ViewModel.Initialize();
        }
        private void OnHandleDestroyed(object sender, EventArgs eventArgs)
        {
            ViewModel.Clean();
        }
    }
}