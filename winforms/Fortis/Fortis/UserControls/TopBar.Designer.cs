﻿namespace Fortis.Winforms.UserControls
{
    partial class TopBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TopBar));
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.btnManageWorkingFolders = new Fortis.Winforms.Controls.CommandButton();
            this.btnLoad = new Fortis.Winforms.Controls.CommandButton();
            this.btnSave = new Fortis.Winforms.Controls.CommandButton();
            this.SuspendLayout();
            // 
            // btnManageWorkingFolders
            // 
            this.btnManageWorkingFolders.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnManageWorkingFolders.Enabled = false;
            this.btnManageWorkingFolders.Image = ((System.Drawing.Image)(resources.GetObject("btnManageWorkingFolders.Image")));
            this.btnManageWorkingFolders.Location = new System.Drawing.Point(116, 0);
            this.btnManageWorkingFolders.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnManageWorkingFolders.Name = "btnManageWorkingFolders";
            this.btnManageWorkingFolders.Size = new System.Drawing.Size(48, 49);
            this.btnManageWorkingFolders.TabIndex = 15;
            this.toolTipMain.SetToolTip(this.btnManageWorkingFolders, "Gérer mes dossiers");
            this.btnManageWorkingFolders.UseVisualStyleBackColor = true;
            // 
            // btnLoad
            // 
            this.btnLoad.Enabled = false;
            this.btnLoad.Image = ((System.Drawing.Image)(resources.GetObject("btnLoad.Image")));
            this.btnLoad.Location = new System.Drawing.Point(4, 2);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(48, 49);
            this.btnLoad.TabIndex = 0;
            this.toolTipMain.SetToolTip(this.btnLoad, "Chargement de données...");
            this.btnLoad.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Enabled = false;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(60, 0);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(48, 49);
            this.btnSave.TabIndex = 3;
            this.toolTipMain.SetToolTip(this.btnSave, "Enregistrer mes modifications");
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // TopBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnManageWorkingFolders);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSave);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "TopBar";
            this.Size = new System.Drawing.Size(998, 51);
            this.Load += new System.EventHandler(this.TopBar_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private Controls.CommandButton btnManageWorkingFolders;
        private Controls.CommandButton btnSave;
        private Controls.CommandButton btnLoad;
        private System.Windows.Forms.ToolTip toolTipMain;
    }
}
