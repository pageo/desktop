﻿using System;
using System.Windows.Forms;
using Fortis.Business.ViewModels;
using Fortis.Winforms.Utils;

namespace Fortis.Winforms.UserControls
{
    public partial class TopBar : UserControl
    {
        // ----- Fields
        private readonly ActionViewModel _actionViewModel = InversionOfControl.Instance.GetInstance<ActionViewModel>();
        private readonly LoadViewModel _loadViewModel = InversionOfControl.Instance.GetInstance<LoadViewModel>();
        private readonly SaveViewModel _saveViewModel = InversionOfControl.Instance.GetInstance<SaveViewModel>();

        // ----- Constructors
        public TopBar()
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode) {
                return;
            }

            btnLoad.Command = _loadViewModel.ShowCommand;
            btnSave.Command = _saveViewModel.SaveCommand;

            Disposed += OnDisposed;
        }

        // ----- ActionViewModel
        private void TopBar_Load(object sender, EventArgs e)
        {
            _actionViewModel.Initialize();
        }

        private void OnDisposed(object sender, EventArgs eventArgs)
        {
            _actionViewModel.Clean();
        }
    }
}