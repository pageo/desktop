﻿using System;

namespace Fortis.Winforms.Utils
{
    public class BindingException : Exception
    {
        // ----- Constructors
        public BindingException(string viewModelPropertyName, string controlPropertyName, string controlName,
            string innerMessage)
            : base(BuildMessage(viewModelPropertyName, controlPropertyName, controlName, innerMessage))
        {
        }

        // ----- Internal logics
        private static string BuildMessage(string viewModelPropertyName, string controlPropertyName, string controlName,
            string innerMessage)
        {
            return
                $"Erreur de binding sur le contrôle '{controlName}' sur la propriété '{controlPropertyName}', bindé à '{viewModelPropertyName}' du viewModel. {innerMessage}";
        }
    }
}