﻿using System.Windows.Forms;

namespace Fortis.Winforms.Utils
{
    public class ControlBinding<TControlValue>
    {
        // ----- Fields
        private readonly Control _control;
        private readonly string _controlPropertyName;

        // ----- Constructors
        public ControlBinding(Control control, string controlPropertyName)
        {
            _control = control;
            _controlPropertyName = controlPropertyName;
        }

        // ----- Public methods
        public ViewModelBinding<TViewModel, TControlValue> To<TViewModel>(TViewModel viewModel)
        {
            return new ViewModelBinding<TViewModel, TControlValue>(_control, _controlPropertyName, viewModel);
        }
    }
}