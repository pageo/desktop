﻿using System.Reflection;

namespace Fortis.Winforms.Utils
{
    public static class DesignTimeHelper
    {
        // ----- Properties
        public static bool IsInDesignMode => Assembly.GetEntryAssembly() == null;
    }
}