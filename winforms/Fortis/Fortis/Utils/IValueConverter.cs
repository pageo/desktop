﻿namespace Fortis.Winforms.Utils
{
    public interface IValueConverter<TViewModelValue, TControlValue>
    {
        object ConvertViewModelToControl(TViewModelValue viewModelValue);
        object ConvertControlToViewModel(TControlValue source);
    }
}
