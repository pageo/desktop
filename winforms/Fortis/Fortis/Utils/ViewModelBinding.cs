﻿using System;
using System.Linq.Expressions;
using System.Windows.Forms;

namespace Fortis.Winforms.Utils
{
    public class ViewModelBinding<TViewModel, TControlValue>
    {
        // ----- Fields
        private readonly Control _control;
        private readonly string _controlPropertyName;
        private readonly TViewModel _viewModel;

        // ----- Constructors
        public ViewModelBinding(Control control, string controlPropertyName, TViewModel viewModel)
        {
            _control = control;
            _controlPropertyName = controlPropertyName;
            _viewModel = viewModel;
        }

        // ----- Public methods
        public BindingBuilder<TViewModelValue, TControlValue> On<TViewModelValue>(
            Expression<Func<TViewModel, TViewModelValue>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
                throw new Exception(
                    $"Unable to create binding from expression that does not represent a Member : {expression}");

            var binding = new Binding(_controlPropertyName, _viewModel, memberExpression.Member.Name, true,
                DataSourceUpdateMode.OnPropertyChanged);
            _control.DataBindings.Add(binding);

            binding.BindingComplete += (sender, args) =>
            {
                if (args.Exception == null) return;

                var form = _control.FindForm();
                if (form != null && form.IsHandleCreated)
                    form.BeginInvoke(
                        new Action(() => {
                            if (binding.Control != null)
                                throw new BindingException(binding.BindingMemberInfo.BindingField,
                                    binding.PropertyName, binding.Control.Name, args.ErrorText);
                        }));
            };

            binding.Format += (sender, args) => {
                // HACK : bug winform, lors de la mise à NULL de l'élément bindé à un SelectedItem d'une ComboBox, cet élément n'est pas mis à jour.
                if (binding.Control is ComboBox && binding.PropertyName == "SelectedItem") {
                    var combobox = (ComboBox) binding.Control;
                    if (args.Value == null || args.Value == DBNull.Value)
                        combobox.SelectedItem = null;
                }
            };
            return new BindingBuilder<TViewModelValue, TControlValue>(binding);
        }
    }
}