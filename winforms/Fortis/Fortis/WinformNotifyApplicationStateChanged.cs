﻿using System;
using System.Windows.Forms;
using Fortis.Business.Base;

namespace Fortis.Winforms
{
    public class WinformNotifyApplicationStateChanged : INotifyApplicationStateChanged
    {
        // ----- Events
        public event EventHandler Idle
        {
            add { Application.Idle += value; }
            remove { Application.Idle -= value; }
        }
    }
}