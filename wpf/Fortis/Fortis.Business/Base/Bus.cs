﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Fortis.Business.Base
{
    public class Bus : IBus
    {
        // ----- Fields
        private readonly IDictionary<Type, IList<Delegate>> _callbacks = new Dictionary<Type, IList<Delegate>>();
        private readonly IDictionary<object, IList<Delegate>> _ownedCallbacks = new Dictionary<object, IList<Delegate>>();

        // ----- Public methods
        public void Send<T>(T message)
        {
            try
            {
                using (Monitor.ElapsedTime($"{typeof(T).Name} processed in {{0}}")) {
                    if (!_callbacks.ContainsKey(typeof(T))) return;
                    foreach (var callback in _callbacks[typeof(T)].ToArray()) {
                        using (
                            Monitor.ElapsedTime(
                                $"{callback.Target?.GetType()?.Name} processed {typeof(T)?.Name} message in {{0}}"))
                            callback.DynamicInvoke(message);
                    }
                }
            }
            catch (TargetInvocationException ex) {
                if (ex.InnerException != null)
                    throw ex.InnerException;
            }
            catch (Exception ex) {
                throw ex;
            }
        }
        public async Task SendAsync<T>(T message)
        {
            try
            {
                if (_callbacks.ContainsKey(typeof(T))) {
                    foreach (var callback in _callbacks[typeof(T)].ToArray()) {
                        if (callback.GetMethodInfo().ReturnType == typeof(Task))
                            await (Task)callback.DynamicInvoke(message);
                        else
                            callback.DynamicInvoke(message);
                    }
                }
            }
            catch (TargetInvocationException ex) {
                if (ex.InnerException != null)
                    throw ex.InnerException;
            }
        }
        public void Register<T>(object subscriber, Action<T> callback)
        {
            RegisterDelegate<T>(subscriber, callback);
        }
        public void RegisterAsync<T>(object subscriber, Func<T, Task> callback)
        {
            RegisterDelegate<T>(subscriber, callback);
        }
        public void Unregister(object subscriber)
        {
            if (!_ownedCallbacks.ContainsKey(subscriber)) return;

            foreach (var @delegate in _ownedCallbacks[subscriber]) {
                var messageType = @delegate.GetType().GetGenericArguments()[0];
                if (_callbacks.ContainsKey(messageType))
                    _callbacks[messageType].Remove(@delegate);
            }
            _ownedCallbacks.Remove(subscriber);
        }

        // ----- Internal logics
        private void RegisterDelegate<T>(object subscriber, Delegate callback)
        {
            if (_callbacks.ContainsKey(typeof(T)) == false)
                _callbacks[typeof(T)] = new List<Delegate>();
            _callbacks[typeof(T)].Add(callback);

            if (_ownedCallbacks.ContainsKey(subscriber) == false)
                _ownedCallbacks[subscriber] = new List<Delegate>();
            _ownedCallbacks[subscriber].Add(callback);
        }
    }
}