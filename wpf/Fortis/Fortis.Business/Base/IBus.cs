﻿using System;
using System.Threading.Tasks;

namespace Fortis.Business.Base
{
    public interface IBus
    {
        void Send<T>(T message);
        Task SendAsync<T>(T message);
        void Register<T>(object subscriber, Action<T> callback);
        void RegisterAsync<T>(object subscriber, Func<T, Task> callback);
        void Unregister(object subscriber);
    }
}
