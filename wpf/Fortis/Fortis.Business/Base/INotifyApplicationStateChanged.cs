﻿using System;

namespace Fortis.Business.Base
{
    public interface INotifyApplicationStateChanged
    {
        event EventHandler Idle;
    }
}