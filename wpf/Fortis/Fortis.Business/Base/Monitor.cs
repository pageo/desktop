﻿using System;
using System.Diagnostics;

namespace Fortis.Business.Base
{
    public class Monitor : IDisposable
    {
        // ----- Fields
        private readonly Stopwatch _watch;
        private string _stepName;

        // ----- Constructors
        public static Monitor ElapsedTime(string stepName)
        {
            return new Monitor(stepName);
        }
        private Monitor(string stepName)
        {
            _stepName = stepName;
            _watch = new Stopwatch();
            _watch.Start();
        }

        // ----- Public methods
        public void Dispose()
        {
            _watch.Stop();
        }
    }
}
