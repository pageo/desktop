﻿namespace Fortis.Business.Infrastructure.Authentication
{
    public interface IAuthenticationService
    {
        bool ConnectUser();
    }
}
