﻿namespace Fortis.Business.Infrastructure.Repositories
{
    public interface IUserRepository
    {
        string Authenticate(string login, string password);
        string Register(string fullName, string login, string password);
    }
}