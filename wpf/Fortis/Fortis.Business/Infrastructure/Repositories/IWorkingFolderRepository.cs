﻿using System.Collections.Generic;
using Fortis.Business.Models;

namespace Fortis.Business.Infrastructure.Repositories
{
    public interface IWorkingFolderRepository
    {
        IEnumerable<WorkingFolderListItem> GetList();
        int Create(string name);
        WorkingFolder Load(int workingFolderId);
        void Delete(int workingFolderId);
        void Save(int workingFolderId);
    }
}
