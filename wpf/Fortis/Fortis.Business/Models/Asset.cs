﻿using Fortis.Business.Services;

namespace Fortis.Business.Models
{
    public class Asset
    {
        // ----- Properties
        [Label("Code")]
        public string Code { get; set; }
        [Label("Nom de commune")]
        public string TownName { get; set; }
        [Label("Nom de région")]
        public string RegionName { get; set; }
        [Label("X")]
        public double? X { get; set; }
        [Label("Y")]
        public double? Y { get; set; }
    }
}