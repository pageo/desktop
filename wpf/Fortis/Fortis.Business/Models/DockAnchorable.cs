﻿using System.Windows.Input;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.Models
{
    public class DockAnchorable : DockItem
    {
        // ----- Properties
        public bool CanHide
        {
            get => GetNotifiableProperty<bool>();
            set => SetNotifiableProperty(value);
        }    

        // ----- Commands
        public ICommand HideCommand { get; }

        // -----Constructors
        public DockAnchorable(DockManager dockManager)
        {
            CanHide = true;

            HideCommand = new RelayCommand(Hide, () => CanHide);

            dockManager.Add(this);
        }

        // ----- Implementation commands
        public void Hide()
        {
            IsVisible = false;
        }

        // ----- Override methods
        public override void Close()
        {
            Hide();
        }
        public override void Show()
        {
            IsVisible = true;
            IsActive = true;
        }
    }
}