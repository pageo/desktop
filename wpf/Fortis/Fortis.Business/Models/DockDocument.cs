﻿namespace Fortis.Business.Models
{
    public abstract class DockDocument : DockItem
    {
        // ----- Fields
        private readonly DockManager _dockManager;

        // ----- Constructors
        protected DockDocument(DockManager dockManager)
        {
            _dockManager = dockManager;
            _dockManager.Add(this);
        }

        // ---- Override methods
        public override void Close()
        {
            _dockManager.Remove(this);
        }
        public override void Show()
        {
            IsActive = true;
            _dockManager.Add(this);
        }
    }
}