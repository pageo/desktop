﻿using System;
using System.Windows.Input;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.Models
{
    public abstract class DockItem : NotifiableObject
    {
        // ----- Properties
        public string ContentId
        {
            get => GetNotifiableProperty<string>();
            set => SetNotifiableProperty(value);
        }
        public bool IsVisible
        {
            get => GetNotifiableProperty<bool>();
            set => SetNotifiableProperty(value);
        }
        public bool IsActive
        {
            get => GetNotifiableProperty<bool>();
            set => SetNotifiableProperty(value);
        }
        public bool CanClose
        {
            get => GetNotifiableProperty<bool>();
            set => SetNotifiableProperty(value);
        }
        public string Title
        {
            get => GetNotifiableProperty<string>();
            set => SetNotifiableProperty(value);
        }
        public Uri IconSource
        {
            get => GetNotifiableProperty<Uri>();
            set => SetNotifiableProperty(value);
        }
        public string ToolTip
        {
            get => GetNotifiableProperty<string>();
            set => SetNotifiableProperty(value);
        }

        // ----- View
        public object View { get; set; }

        // ----- Commands
        public ICommand CloseCommand { get; }
        public ICommand ShowCommand { get; }

        // -----Constructors
        protected DockItem()
        {
            CanClose = true;

            CloseCommand = new RelayCommand(Close, () => CanClose);
            ShowCommand = new RelayCommand(Show);
        }

        // ----- Abstract methods
        public abstract void Close();
        public abstract void Show();
    }
}
