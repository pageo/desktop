﻿using System.Collections.ObjectModel;
using System.Linq;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.Models
{
    public class DockManager : NotifiableObject
    {
        // ----- Properties
        public ObservableCollection<DockDocument> Documents
        {
            get => GetNotifiableProperty<ObservableCollection<DockDocument>>();
            private set => SetNotifiableProperty(value);
        }
        public ObservableCollection<DockAnchorable> Anchorables
        {
            get => GetNotifiableProperty<ObservableCollection<DockAnchorable>>();
            private set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        public DockManager()
        {
            Documents = new ObservableCollection<DockDocument>();
            Anchorables = new ObservableCollection<DockAnchorable>();
        }

        // ----- Public methods
        public void Add(DockItem documentOrAnchorable)
        {
            if (documentOrAnchorable is DockDocument && Contains(documentOrAnchorable) == false)
                Documents.Add((DockDocument)documentOrAnchorable);
            else if (documentOrAnchorable is DockAnchorable && Contains(documentOrAnchorable) == false)
                Anchorables.Add((DockAnchorable)documentOrAnchorable);
        }
        public void Remove(DockItem documentOrAnchorable)
        {
            if (documentOrAnchorable is DockDocument && Contains(documentOrAnchorable))
                Documents.Remove((DockDocument)documentOrAnchorable);
            else if (documentOrAnchorable is DockAnchorable && Contains(documentOrAnchorable))
                Anchorables.Remove((DockAnchorable)documentOrAnchorable);
        }

        // ----- Internal logics
        private bool Contains(DockItem item)
        {
            if (item is DockDocument) return Documents.Any(x => x.ContentId == item.ContentId);
            if (item is DockAnchorable) return Anchorables.Any(x => x.ContentId == item.ContentId);
            return false;
        }
    }
}