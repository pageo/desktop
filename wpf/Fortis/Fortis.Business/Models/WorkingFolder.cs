﻿using System;
using System.Collections.Generic;

namespace Fortis.Business.Models
{
    public class WorkingFolder
    {
        // ----- Properties
        public int Id { get; }
        public string Name { get; }
        public string OwnerName { get; }
        public DateTime CreationDate { get; }
        public IEnumerable<Asset> Assets { get; }

        // ----- Constructors
        public WorkingFolder(WorkingFolderDetails details, IEnumerable<Asset> assets)
        {
            if (details == null) throw new ArgumentNullException(nameof(details));
            if (assets == null) throw new ArgumentNullException(nameof(assets));

            Id = details.Id;
            Name = details.Name;
            OwnerName = details.OwnerName;
            CreationDate = details.CreationDate;
            Assets = assets;
        }
    }
}
