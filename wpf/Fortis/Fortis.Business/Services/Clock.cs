﻿using System;

namespace Fortis.Business.Services
{
    public class Clock : IClock
    {
        // ----- Public methods
        public DateTime Now()
        {
            return DateTime.Now;
        }
    }
}
