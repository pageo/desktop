﻿using System.Collections.Generic;
using Fortis.Business.Models;
using Fortis.Common.Records;

namespace Fortis.Business.Services
{
    public class CollectionFactory : ICollectionFactory
    {
        // ----- Constantes
        private const string Pat = "PAT";

        // ----- Fields
        private readonly IColumnTranslationService _columnTranslationService;
        private readonly IRecordFeeder _recordFeeder;

        // ----- Constructors
        public CollectionFactory(
            IColumnTranslationService columnTranslationService,
            IRecordFeeder recordFeeder)
        {
            _columnTranslationService = columnTranslationService;
            _recordFeeder = recordFeeder;
        }

        // ----- Public methods
        public Collection CreateAssetCollection(
            string name,
            IEnumerable<Asset> assets)
        {
            var collection = new Collection(name, Pat);
            CreateFieldsToCollection<Asset>(collection);
            AddRecordsToCollection(collection, assets);
            return collection;
        }

        // ----- Internal logics
        private void CreateFieldsToCollection<T>(Collection collection)
        {
            var propertyInfos = typeof(T).GetProperties();
            foreach (var propInfo in propertyInfos) {
                var label = _columnTranslationService.GetFrenchColumn<T>(propInfo.Name);
                var columnName = label ?? propInfo.Name;
                collection.AddField(columnName, propInfo.PropertyType);
            }
        }
        private void AddRecordsToCollection<T>(Collection collection, IEnumerable<T> assets)
        {
            foreach (var asset in assets) {
                var record = collection.AddRecord();
                _recordFeeder.FeedRecordFromData(record, asset);
            }
        }
    }
}
