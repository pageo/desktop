﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Fortis.Business.Services
{
    public class ColumnAttributeTranslationService : IColumnTranslationService
    {
        // ----- FIelds
        private readonly IDictionary<Type, IDictionary<string, string>> _dictionary = new Dictionary<Type, IDictionary<string, string>>();

        // ----- Public methods
        public string GetFrenchColumn<T>(string name)
        {
            IDictionary<string, string> columnDictionary;
            if (_dictionary.TryGetValue(typeof(T), out columnDictionary) == false) {
                columnDictionary = CreateNewDictionary<T>();
                _dictionary.Add(typeof(T), columnDictionary);
            }
            return columnDictionary[name];
        }
        private static IDictionary<string, string> CreateNewDictionary<T>()
        {
            var dictionary = new Dictionary<string, string>();
            var properties = typeof(T).GetProperties();
            foreach (var propertyInfo in properties) {
                var labelAttribute = propertyInfo.GetCustomAttributes<LabelAttribute>().FirstOrDefault();
                dictionary.Add(propertyInfo.Name, labelAttribute?.Name);
            }
            return dictionary;
        }
    }
}
