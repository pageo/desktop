﻿using System;

namespace Fortis.Business.Services
{
    public interface IClock
    {
        DateTime Now();
    }
}
