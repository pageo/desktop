﻿using System.Collections.Generic;
using Fortis.Business.Models;
using Fortis.Common;
using Fortis.Common.Records;

namespace Fortis.Business.Services
{
    public interface ICollectionFactory
    {
        Collection CreateAssetCollection(string name, IEnumerable<Asset> assets);
    }
}
