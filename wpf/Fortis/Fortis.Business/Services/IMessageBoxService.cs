﻿namespace Fortis.Business.Services
{
    public interface IMessageBoxService
    {
        void ShowInformation(string message);
        UserAnswer AskQuestion(string question);
    }

    public enum UserAnswer
    {
        Yes, No
    }
}
