﻿using Fortis.Common.Records;

namespace Fortis.Business.Services
{
    public interface IRecordFeeder
    {
        void FeedRecordFromData<T>(Record record, T data);
        void FeedDataFromRecord<T>(T data, Record record);
    }
}
