﻿using System;

namespace Fortis.Business.Services
{
    public class LabelAttribute : Attribute
    {
        // ----- Properties
        public string Name { get; set; }

        // ----- Constructors
        public LabelAttribute(string name)
        {
            Name = name;
        }
    }
}
