﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Fortis.Common.Records;

namespace Fortis.Business.Services
{
    public class ReflectionRecordFeeder : IRecordFeeder
    {
        // ----- Fields
        private readonly IColumnTranslationService _columnTranslationService;
        private readonly IDictionary<Type, IEnumerable<CopyInformation>> _propertyInfos = new Dictionary<Type, IEnumerable<CopyInformation>>();

        // ----- Constructors
        public ReflectionRecordFeeder(IColumnTranslationService columnTranslationService)
        {
            _columnTranslationService = columnTranslationService;
        }

        // ----- Public methods
        public void FeedRecordFromData<T>(Record record, T data)
        {
            var propertyInfos = GetPropertyInformation<T>();
            var values =
                from info in propertyInfos
                select new RecordValue(info.ColumnName, info.PropertyInfo.GetValue(data));
            record.SetValues(values);
        }
        public void FeedDataFromRecord<T>(T data, Record record)
        {
            var informations = GetPropertyInformation<T>();
            foreach (var value in informations)
                value.PropertyInfo.SetValue(data, record.GetValue(value.ColumnName));
        }
        private IEnumerable<CopyInformation> GetPropertyInformation<T>()
        {
            IEnumerable<CopyInformation> propertyInfos;
            if (_propertyInfos.TryGetValue(typeof(T), out propertyInfos) == false) {
                propertyInfos = (from propInfo in typeof(T).GetProperties()
                                 let label = _columnTranslationService.GetFrenchColumn<T>(propInfo.Name)
                                 let columnName = label ?? propInfo.Name
                                 select new CopyInformation(columnName, propInfo)).ToArray();

                _propertyInfos.Add(typeof(T), propertyInfos);
            }
            return propertyInfos;
        }

        // ----- Internal logics

        private class CopyInformation
        {
            // ----- Properties
            public string ColumnName { get; }
            public PropertyInfo PropertyInfo { get; }

            // ----- 
            public CopyInformation(string columnName, PropertyInfo propInfo)
            {
                ColumnName = columnName;
                PropertyInfo = propInfo;
            }
        }
    }
}
