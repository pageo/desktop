﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fortis.Business.ViewModels.Base
{
    public class AsyncCancelableRelayCommand : ICommand, IAsyncCommand
    {
        // ----- Fields
        private readonly Func<CancellationToken, Task> _action;
        private readonly Func<bool> _predicate;
        private CancellationTokenSource _tokenSource;

        // ----- Properties
        public bool HasBeenCanceled { get; private set; }

        // ----- Construtors
        public AsyncCancelableRelayCommand(Func<CancellationToken, Task> action, Func<bool> predicate = null)
        {
            _action = action;
            _predicate = predicate;
        }

        // ----- Public methods
        public async Task ExecuteAsync()
        {
            if (CanExecute()) {
                HasBeenCanceled = false;
                _tokenSource = new CancellationTokenSource();
                await _action(_tokenSource.Token).ContinueWith(task => { HasBeenCanceled = task.IsCanceled; });
                _tokenSource.Dispose();
                _tokenSource = null;
            }
        }

        public bool CanExecute()
        {
            return _predicate == null || _predicate();
        }

        public void Cancel()
        {
            _tokenSource?.Cancel();
        }

        // ----- Implementation commands
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        // ----- Events
        async void ICommand.Execute(object parameter)
        {
            await ExecuteAsync();
        }
        event EventHandler ICommand.CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}
