﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fortis.Business.ViewModels.Base
{
    public class AsyncRelayCommand : ICommand, IAsyncCommand
    {
        // ----- Fields
        private readonly Func<Task> _action;
        private readonly Func<bool> _predicate;

        // ----- Constructors
        public AsyncRelayCommand(Func<Task> action, Func<bool> predicate = null)
        {
            _action = action;
            _predicate = predicate;
        }

        // ----- Public methods
        public async Task ExecuteAsync()
        {
            if (CanExecute())
                await _action();
        }

        public bool CanExecute()
        {
            return _predicate == null || _predicate();
        }

        // ----- Implementation commands
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }
        async void ICommand.Execute(object parameter)
        {
            await ExecuteAsync();
        }

        // ----- Events
        event EventHandler ICommand.CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }

    public class AsyncRelayCommand<T> : ICommand, IAsyncCommand<T>
    {
        // ----- Fields
        private readonly Func<T, Task> _action;
        private readonly Func<T, bool> _predicate;

        // ----- Constructors
        public AsyncRelayCommand(Func<T, Task> action, Func<T, bool> predicate = null)
        {
            _action = action;
            _predicate = predicate;
        }

        // ----- Public methods
        public async Task ExecuteAsync(T parameter)
        {
            if (CanExecute(parameter))
                await _action(parameter);
        }
        public bool CanExecute(T parameter)
        {
            return _predicate == null || _predicate(parameter);
        }

        // ----- Implement ICommand
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }
        async void ICommand.Execute(object parameter)
        {
            await ExecuteAsync((T)parameter);
        }

        // ----- Events
        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
