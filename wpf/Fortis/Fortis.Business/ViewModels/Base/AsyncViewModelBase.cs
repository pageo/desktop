﻿using System;
using System.Threading.Tasks;

namespace Fortis.Business.ViewModels.Base
{
    public abstract class AsyncViewModelBase : ValidationViewModel
    {
        // ----- Properties
        public bool IsLoading
        {
            get { return GetNotifiableProperty<bool>(); }
            protected set { SetNotifiableProperty(value); }
        }

        // ----- Public methods
        public async Task RunAsync(Action action)
        {
            try
            {
                IsLoading = true;
                await Task.Run(action);
            }
            finally
            {
                IsLoading = false;
            }
        }
        public async Task<T> RunAsync<T>(Func<T> action)
        {
            try
            {
                IsLoading = true;
                return await Task.Run(action);
            }
            finally
            {
                IsLoading = false;
            }
        }

        // ----- Abstract methods
        public abstract Task Initialize();
        public abstract void Clean();
    }
}
