﻿using System.Threading.Tasks;

namespace Fortis.Business.ViewModels.Base
{
    public interface IAsyncCommand
    {
        Task ExecuteAsync();
        bool CanExecute();
    }
    public interface IAsyncCommand<T>
    {
        Task ExecuteAsync(T parameter);
        bool CanExecute(T parameter);
    }
}
