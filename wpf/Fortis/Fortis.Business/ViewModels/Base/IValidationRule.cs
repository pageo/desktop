﻿namespace Fortis.Business.ViewModels.Base
{
    public interface IValidationRule
    {
        bool IsValid();
        string ErrorMessage { get; }
    }
}
