﻿using System;
using System.Threading;

namespace Fortis.Business.ViewModels.Base
{
    public class ThreadSafeNotifiableObject : NotifiableObject
    {
        // ----- Fields
        private readonly SynchronizationContext _context;

        // ----- Constructors
        public ThreadSafeNotifiableObject()
        {
            _context = SynchronizationContext.Current;
        }

        // ----- Protected methods
        protected override void OnPropertyChanged(string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException(nameof(propertyName));

            if (_context == null)
                base.OnPropertyChanged(propertyName);
            else
                _context.Send(state => { base.OnPropertyChanged(propertyName); }, null);
        }
    }
}
