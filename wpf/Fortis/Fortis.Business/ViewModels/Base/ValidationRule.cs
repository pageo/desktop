﻿using System;

namespace Fortis.Business.ViewModels.Base
{
    public class ValidationRule : IValidationRule
    {
        // ----- Fields
        private readonly Func<bool> _predicate;

        // ----- Properties
        public string ErrorMessage { get; set; }

        // ----- Constructors
        public ValidationRule(Func<bool> predicate, string errorMessage)
        {
            _predicate = predicate;
            ErrorMessage = errorMessage;
        }

        // ----- Public methods
        public bool IsValid()
        {
            return !_predicate();
        }
    }
}
