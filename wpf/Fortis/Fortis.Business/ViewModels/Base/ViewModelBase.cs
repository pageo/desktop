﻿namespace Fortis.Business.ViewModels.Base
{
    public abstract class ViewModelBase : ValidationViewModel
    {
        // ----- Abstract methods
        public abstract void Initialize();
        public abstract void Clean();
    }
}
