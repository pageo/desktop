﻿using System;
using System.Collections.Generic;
using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.ViewModels.Events;
using Fortis.Common.Records;

namespace Fortis.Business.ViewModels
{
    public class CardViewModel : DockAnchorable
    {
        // ----- Fields
        private readonly IBus _bus;
        private static Collection _assetCollection;

        // ----- Properties
        public IEnumerable<RecordChange> CurrentRecord
        {
            get => GetNotifiableProperty<IEnumerable<RecordChange>>();
            private set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        public CardViewModel(
            IBus bus,
            DockManager dockManager) : base(dockManager)
        {
            _bus = bus;
            Title = "Details";
            ContentId = "95fb85f0-063f-4be2-8aa8-a350d04b3912";
            ToolTip = "Détails de l'élément courant";
            IconSource = new Uri("/Images/detail.png", UriKind.RelativeOrAbsolute);

            _bus.Register<WorkingFolderLoaded>(this, OnWorkingFolderLoaded);
        }
        // ----- Destructors
        ~CardViewModel()
        {
            _bus.Unregister(this);
        }

        // ----- Callbacks
        private void OnWorkingFolderLoaded(WorkingFolderLoaded @event)
        {
            _assetCollection = @event.AssetsCollection;
            _assetCollection.Change += CollectionOnChange;
        }

        private void CollectionOnChange(object sender, CollectionChangeEventArgs args)
        {
            switch (args.Action) {
                case CollectionChangeAction.CurrentRecordChanged:
                    // TODO : Afficher les informations de l'élément courant
                    CurrentRecord = args.Record.Changes;
                    break;
            }
        }
    }
}
