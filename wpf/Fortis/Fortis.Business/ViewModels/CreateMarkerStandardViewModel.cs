﻿using Fortis.Business.Base;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;
using Fortis.Common.Records;

namespace Fortis.Business.ViewModels
{
    public sealed class CreateMarkerStandardViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;

        // ----- Properties
        public Collection CurrentCollection { get; set; }
        public bool IsVisible
        {
            get => GetNotifiableProperty<bool>();
            set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        public CreateMarkerStandardViewModel(IBus bus)
        {
            _bus = bus;

            Initialize();
        }

        // ----- Destructors
        ~CreateMarkerStandardViewModel()
        {
            Clean();
        }

        // ----- Override methods
        public override void Initialize()
        {
            _bus.Register<WorkingFolderLoaded>(this, OnWorkingFolderLoaded);
            _bus.Register<WorkingFolderUnLoaded>(this, OnWorkingFolderUnLoaded);
        }
        public override void Clean()
        {
            _bus.Unregister(this);
        }

        // ----- Callbacks
        private void OnWorkingFolderLoaded(WorkingFolderLoaded @event)
        {
            CurrentCollection = @event.AssetsCollection;
        }
        private void OnWorkingFolderUnLoaded(WorkingFolderUnLoaded @event)
        {
            CurrentCollection = null;
        }

        // ----- Public methods
        public void Show()
        {
            IsVisible = true;
        }
    }
}
