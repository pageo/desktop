﻿using System;
using System.Threading.Tasks;
using Fortis.Business.Base;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Services;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;
using Fortis.Business.ViewModels.Exceptions;
using Fortis.Business.ViewModels.Messages;

namespace Fortis.Business.ViewModels
{
    public class CreateNewWorkingFolderViewModel : AsyncViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IWorkingFolderRepository _workingFolderRepository;
        private readonly IMessageBoxService _messageBoxService;


        // ----- Properties
        public string Name
        {
            get => GetNotifiableProperty<string>();
            set => SetNotifiableProperty(value);
        }
        public DateTime? EffectiveDate
        {
            get => GetNotifiableProperty<DateTime?>();
            set => SetNotifiableProperty(value);
        }    

        // ----- Commands
        public IAsyncCommand CreateAndLoadWorkingFolderCommand { get; }

        // ----- Constructors
        public CreateNewWorkingFolderViewModel(IBus bus,
            IWorkingFolderRepository workingFolderRepository,
            IMessageBoxService messageBoxService)
        {
            _bus = bus;
            _workingFolderRepository = workingFolderRepository;
            _messageBoxService = messageBoxService;

            CreateAndLoadWorkingFolderCommand = new AsyncRelayCommand(CreateAndLoadWorkingFolderAsync, CanCreateAndLoadWorkingFolder);
        }

        // ----- Override methods
        public override Task Initialize()
        {
            return Task.Delay(0);
        }
        protected override void ConfigureValidationRules(ValidationRuleBuilder builder)
        {
            builder.Add(() => string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name), "Veuillez choisir un nom pour le dossier.");
        }
        public override void Clean()
        {
            Name = null;

            ClearValidationErrors();
        }

        // ----- Implementation commands
        private bool CanCreateAndLoadWorkingFolder()
        {
            return AreAllValidationRulesValid();
        }
        private async Task CreateAndLoadWorkingFolderAsync()
        {
            try
            {
                _bus.Send(new CloseWorkingFolderCommand());
                _bus.Send(new WorkingFolderLoading());

                var @event = await RunAsync(() => {
                    var workingFolderId = _workingFolderRepository.Create(Name);
                    var workingFolder = _workingFolderRepository.Load(workingFolderId);
                    return new WorkingFolderLoaded(null, workingFolder);
                });

                _bus.Send(@event);
            }
            catch (WorkingFolderWithSameNameAlreadyExists) {
                _messageBoxService.ShowInformation($"Un dossier de travail avec le nom \"{Name}\" existe déjà. {Environment.NewLine}Veuillez saisir un autre nom.");
            }
        }
    }
}
