﻿namespace Fortis.Business.ViewModels.Events
{
    public class MapPositionUpdated
    {
        // ----- Properties
        public double X { get; }
        public double Y { get; }

        // ----- Constructors
        public MapPositionUpdated(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
