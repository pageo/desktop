﻿using System.Collections.Generic;

namespace Fortis.Business.ViewModels.Events
{
    public class WorkingFolderDeleted
    {
        // ----- Properties
        public IEnumerable<int> WorkingFoldersIds { get; set; }

        // ----- Constructors
        public WorkingFolderDeleted(IEnumerable<int> workingFoldersIds)
        {
            WorkingFoldersIds = workingFoldersIds;
        }
    }
}
