﻿using Fortis.Business.Models;
using Fortis.Common;
using Fortis.Common.Records;

namespace Fortis.Business.ViewModels.Events
{
    public class WorkingFolderLoaded
    {
        // ----- Properties
        public WorkingFolder WorkingFolder { get; }
        public Collection AssetsCollection { get; }

        // ----- Constructors
        public WorkingFolderLoaded(Collection assetsCollection, WorkingFolder workingFolder)
        {
            AssetsCollection = assetsCollection;
            WorkingFolder = workingFolder;
        }
    }
}
