﻿using System;

namespace Fortis.Business.ViewModels.Exceptions
{
    public class WorkingFolderWithSameNameAlreadyExists : Exception { }
}
