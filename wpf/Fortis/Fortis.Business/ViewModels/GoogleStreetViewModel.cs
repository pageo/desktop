﻿using System;
using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.ViewModels.Events;

namespace Fortis.Business.ViewModels
{
    public class GoogleStreetViewModel : DockAnchorable
    {
        // ----- Properties
        public Position Position
        {
            get => GetNotifiableProperty<Position>();
            set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        public GoogleStreetViewModel(
            IBus bus,
            DockManager dockManager) : base(dockManager)
        {
            Title = "Google Street";
            ContentId = "fb285790-efb3-4999-b4b1-76aff44f1376";
            ToolTip = "Google Street View";
            IconSource = new Uri("/Images/street_view.png", UriKind.RelativeOrAbsolute);

            Position = new Position(48.9174225, 2.2832445);

            bus.Register<MapPositionUpdated>(this, OnMapPositionUpdated);
        }

        // ----- Callbacks
        private void OnMapPositionUpdated(MapPositionUpdated @event)
        {
            Position.X = @event.X;
            Position.Y = @event.Y;
            // ReSharper disable once ExplicitCallerInfoArgument
            OnPropertyChanged(nameof(Position));
        }
    }
}