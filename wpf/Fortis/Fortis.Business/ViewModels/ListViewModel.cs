﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.ViewModels.Events;
using Fortis.Common.Records;

namespace Fortis.Business.ViewModels
{
    public class ListViewModel : DockDocument
    {
        // ----- Constants
        private const string XFieldName = "X";
        private const string YFieldName = "Y";
        private const string CodeFieldName = "Code";

        // ----- Fields
        private readonly IBus _bus;
        private readonly MapViewModel _mapViewModel;
        private Collection _currentCollection;

        // ----- Properties
        public IEnumerable<Asset> Assets
        {
            get => GetNotifiableProperty<IEnumerable<Asset>>();
            private set => SetNotifiableProperty(value);
        }
        public Asset SelectedAsset
        {
            get => GetNotifiableProperty<Asset>();
            set {
                if (SetNotifiableProperty(value))
                    UpdateCurrentRecord(value);
            }
        }

        // ----- Constructors
        public ListViewModel(
            IBus bus, 
            MapViewModel mapViewModel,
            DockManager dockManager) : base(dockManager)
        {
            _bus = bus;
            _mapViewModel = mapViewModel;

            Title = "Liste";
            ContentId = "78a46dbc-9d8f-4d2f-99e3-b70d54884d47";
            ToolTip = "Liste des éléments";
            IconSource = new Uri("/Images/list.png", UriKind.RelativeOrAbsolute);

            _bus.Register<WorkingFolderLoaded>(this, OnWorkingFolderLoaded);
            _bus.Register<WorkingFolderUnLoaded>(this, OnWorkingFolderUnloaded);
        }

        // ----- Destructors
        ~ListViewModel()
        {
            _bus.Unregister(this);
        }

        // ----- Callbacks
        private void OnWorkingFolderLoaded(WorkingFolderLoaded @event)
        {
            Assets = @event.WorkingFolder?.Assets;
            _currentCollection = @event.AssetsCollection;
            _currentCollection.Change += CollectionOnChange;
        }
        private void OnWorkingFolderUnloaded(WorkingFolderUnLoaded @event)
        {
            Assets = null;
            _currentCollection.Change -= CollectionOnChange;
            _currentCollection = null;
        }
        private void CollectionOnChange(object sender, CollectionChangeEventArgs e)
        {
            switch (e.Action) {
                case CollectionChangeAction.CurrentRecordChanged:
                    var currentX = e.Record.GetValue<double?>(XFieldName);
                    var currentY = e.Record.GetValue<double?>(YFieldName);
                    if (currentX == null || currentY == null) return;
                    _mapViewModel.CurrentPosition = new Position(currentX.Value, currentY.Value);
                    break;
            }
        }

        // ----- Internal logics
        private void UpdateCurrentRecord(Asset asset)
        {
            _currentCollection?
                .Records
                .FirstOrDefault(x => x.GetValue<string>(CodeFieldName) == asset?.Code)?
                .SetCurrent();
        }
    }
}