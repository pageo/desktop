﻿using System.Windows.Input;
using Fortis.Business.Base;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;

namespace Fortis.Business.ViewModels
{
    public sealed class LoadViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;

        // ----- Properties
        public bool? IsVisible
        {
            get => GetNotifiableProperty<bool?>();
            set => SetNotifiableProperty(value);
        }

        // ----- Commands
        public ICommand ShowCommand { get; }

        // ----- Constructors
        public LoadViewModel(IBus bus)
        {
            _bus = bus;

            ShowCommand = new RelayCommand(Show);

            Initialize();
        }

        // ----- Destructors
        ~LoadViewModel()
        {
            Clean();
        }

        // ----- Override methods
        public override void Initialize()
        {
            _bus.Register<WorkingFolderLoaded>(this, OnWorkingFolderLoaded);
        }

        public override void Clean()
        {
            _bus.Unregister(this);
        }

        // ----- Callbacks
        private void OnWorkingFolderLoaded(WorkingFolderLoaded @event)
        {
            IsVisible = false;
        }

        // ----- Internal logics
        private void Show()
        {
            IsVisible = true;
        }
    }
}
