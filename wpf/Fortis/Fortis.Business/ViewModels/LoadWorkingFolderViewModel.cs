﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fortis.Business.Base;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Models;
using Fortis.Business.Services;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;
using Fortis.Business.ViewModels.Messages;

namespace Fortis.Business.ViewModels
{
    public sealed class LoadWorkingFolderViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IWorkingFolderRepository _workingFolderRepository;
        private readonly ICollectionFactory _collectionFactory;

        // ----- Properties
        public IEnumerable<WorkingFolderListItem> ExistingWorkingFolders
        {
            get => GetNotifiableProperty<IEnumerable<WorkingFolderListItem>>();
            private set => SetNotifiableProperty(value);
        }
        public WorkingFolderListItem SelectedExistingWorkingFolder
        {
            get => GetNotifiableProperty<WorkingFolderListItem>();
            set => SetNotifiableProperty(value);
        }
        public bool IsLoading
        {
            get => GetNotifiableProperty<bool>();
            private set => SetNotifiableProperty(value);
        }

        // ----- Commands
        public IAsyncCommand LoadWorkingFolderCommand { get; }
        public IAsyncCommand RefreshWorkingFoldersCommand { get; }

        // ----- Constructors
        public LoadWorkingFolderViewModel(
            IBus bus,
            IWorkingFolderRepository workingFolderRepository,
            ICollectionFactory collectionFactory)
        {
            _bus = bus;
            _workingFolderRepository = workingFolderRepository;
            _collectionFactory = collectionFactory;

            LoadWorkingFolderCommand = new AsyncRelayCommand(LoadWorkingFolder, CanLoadWorkingFolder);
            RefreshWorkingFoldersCommand = new AsyncRelayCommand(RefreshWorkingFolders);

            ExistingWorkingFolders = Enumerable.Empty<WorkingFolderListItem>();

            Initialize();
        }

        // ----- Destructors
        ~LoadWorkingFolderViewModel()
        {
            Clean();
        }

        // ----- Public methods
        public override void Initialize()
        {
            _bus.RegisterAsync<WorkingFolderUpdated>(this, OnWorkingFolderUpdated);
        }
        public override void Clean()
        {
            ExistingWorkingFolders = Enumerable.Empty<WorkingFolderListItem>();
            SelectedExistingWorkingFolder = null;
            _bus.Unregister(this);
        }

        // ----- Command implementations
        private bool CanLoadWorkingFolder()
        {
            return SelectedExistingWorkingFolder != null;
        }
        private async Task LoadWorkingFolder()
        {
            _bus.Send(new CloseWorkingFolderCommand());
            _bus.Send(new WorkingFolderLoading());

            var @event = await RunAsync(() => {
                var selectedExistingWorkingFolder = SelectedExistingWorkingFolder;
                var workingFolder = _workingFolderRepository.Load(selectedExistingWorkingFolder.Id);
                var assetsCollection = _collectionFactory.CreateAssetCollection(selectedExistingWorkingFolder.Name, workingFolder.Assets);
                return new WorkingFolderLoaded(assetsCollection, workingFolder);
            });

            _bus.Send(@event);
        }
        private async Task RefreshWorkingFolders()
        {
            ExistingWorkingFolders = await RunAsync(() => _workingFolderRepository.GetList().ToList());
        }

        // ----- Callbacks
        private async Task OnWorkingFolderUpdated(WorkingFolderUpdated @event)
        {
            await RefreshWorkingFoldersCommand.ExecuteAsync();
        }

        // ----- Utils
        private async Task<T> RunAsync<T>(Func<T> func)
        {
            try {
                IsLoading = true;
                return await Task.Run(func);
            }
            finally {
                IsLoading = false;
            }
        }
    }
}