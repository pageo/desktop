﻿using Fortis.Business.ViewModels.Base;
using Microsoft.Practices.ServiceLocation;

namespace Fortis.Business.ViewModels
{
    public class MainMenuViewModel : NotifiableObject
    {
        // ------ Properties
        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public LoadViewModel Load => ServiceLocator.Current.GetInstance<LoadViewModel>();
        public SaveViewModel Save => ServiceLocator.Current.GetInstance<SaveViewModel>();
        public WhatIsNewViewModel WhatIsNew => ServiceLocator.Current.GetInstance<WhatIsNewViewModel>();
        public MapViewModel Map => ServiceLocator.Current.GetInstance<MapViewModel>();
        public ListViewModel List => ServiceLocator.Current.GetInstance<ListViewModel>();
        public CardViewModel Card => ServiceLocator.Current.GetInstance<CardViewModel>();
        public GoogleStreetViewModel Street => ServiceLocator.Current.GetInstance<GoogleStreetViewModel>();
        public MarkerViewModel Marker => ServiceLocator.Current.GetInstance<MarkerViewModel>();
    }
}
