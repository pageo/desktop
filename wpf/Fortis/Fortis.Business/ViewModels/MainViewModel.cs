﻿using System.Reflection;
using System.Windows.Input;
using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.Services;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;
using Fortis.Business.ViewModels.Messages;
using Fortis.Common.Records;

namespace Fortis.Business.ViewModels
{
    public sealed class MainViewModel : ViewModelBase
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IMessageBoxService _messageBoxService;
        private readonly CollectionManager _collectionManager;
        private WorkingFolder _workingFolder;

        // ----- Properties
        public string WorkingFolderInfoTitle
        {
            get => GetNotifiableProperty<string>();
            private set => SetNotifiableProperty(value);
        }
        public SaveViewModel Save { get; }
        public LoadViewModel Load { get; }
        public DockManager Manager { get; }

        // ----- Commands
        public ICommand ExitCommand { get; }

        // ----- Constructors
        public MainViewModel(
            IBus bus, 
            IMessageBoxService messageBoxService,
            CollectionManager collectionManager,
            DockManager dockManager,
            LoadViewModel loadViewModel,
            SaveViewModel saveViewModel)
        {
            _bus = bus;
            _messageBoxService = messageBoxService;
            _collectionManager = collectionManager;

            Manager = dockManager;
            Load = loadViewModel;
            Save = saveViewModel;

            ExitCommand = new RelayCommand(Exit);

            UpdateWorkingFolderInformation();

            Initialize();
        }

        // ----- Destructors
        ~MainViewModel()
        {
            Clean();
        }

        // ----- Override methods
        public override void Initialize()
        {
            _bus.Register<WorkingFolderLoaded>(this, OnWorkingFolderLoaded);
            _bus.Register<CloseWorkingFolderCommand>(this, OnCloseWorkingFolderCommand);
        }
        public override void Clean()
        {
            _bus.Unregister(this);
        }

        // ----- Callbacks
        private void OnWorkingFolderLoaded(WorkingFolderLoaded @event)
        {
            _workingFolder = @event.WorkingFolder;
            _collectionManager.AddCollection(@event.AssetsCollection);

            UpdateWorkingFolderInformation();
        }
        private void OnCloseWorkingFolderCommand(CloseWorkingFolderCommand @event)
        {
            if (CanCloseWorkingFolder())
                CloseWorkingFolder();
        }

        // ----- Implementation commands
        private void Exit()
        {
            _bus.Send(new ExitMessage());
        }
        private bool CanCloseWorkingFolder()
        {
            return _workingFolder != null;
        }
        private void CloseWorkingFolder()
        {
            if (Save.SaveCommand.CanExecute(null)) {
                var saveAnswer =
                    _messageBoxService.AskQuestion(
                        @"Vous avez des modifications en cours sur votre dossier. Voulez-vous les enregistrer avant de le fermer ?");
                if (saveAnswer == UserAnswer.Yes)
                    Save.SaveCommand.Execute(null);
            }
            _workingFolder = null;
            _bus.Send(new WorkingFolderUnLoaded());
        }

        // ----- Internal logics
        private void UpdateWorkingFolderInformation()
        {
            WorkingFolderInfoTitle = BuildTitle();
        }
        private string BuildTitle()
        {
            var title = string.Empty;
            if (_workingFolder != null) {
                title = $"{_workingFolder.Name} - ";
                title += $"{_workingFolder.CreationDate:d} - ";
            }
            title += $"{Assembly.GetEntryAssembly()?.GetName().Name} - v{Assembly.GetExecutingAssembly().GetName().Version}";
            return title;
        }
    }
}