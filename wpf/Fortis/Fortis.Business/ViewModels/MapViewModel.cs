﻿using System;
using Fortis.Business.Models;

namespace Fortis.Business.ViewModels
{
    public class MapViewModel : DockDocument
    {
        // ----- Properties
        public Position CurrentPosition
        {
            get => GetNotifiableProperty<Position>();
            set => SetNotifiableProperty(value);
        }

        // ----- Constructors
        public MapViewModel(DockManager dockManager) : base(dockManager)
        {
            Title = "Map";
            ContentId = "b7056c81-a02c-4d06-8f38-b5b9a07a7d2e";
            IconSource = new Uri("/Images/map.png", UriKind.RelativeOrAbsolute);
        }
    }
}