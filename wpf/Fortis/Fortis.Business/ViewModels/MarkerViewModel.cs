﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.ViewModels.Base;
using Fortis.Business.ViewModels.Events;
using Fortis.Common.Markers;
using Fortis.Common.Records;

namespace Fortis.Business.ViewModels
{
    public class MarkerViewModel : DockAnchorable
    {
        // ----- Fields
        private readonly IBus _bus;

        private readonly CreateMarkerStandardViewModel _createMarkerStandardViewModel;
        private Collection _collection;

        // ----- Properties
        public IEnumerable<Marker> Markers
        {
            get => GetNotifiableProperty<IEnumerable<Marker>>();
            set => SetNotifiableProperty(value);
        }

        // ----- Commands
        public ICommand ShowCreateMarkerCommand { get; }

        // ----- Constructors
        public MarkerViewModel(
            IBus bus, 
            DockManager dockManager,
            CreateMarkerStandardViewModel createMarkerStandardViewModel) : base(dockManager)
        {
            _bus = bus;
            _createMarkerStandardViewModel = createMarkerStandardViewModel;

            Title = "Marqueurs";
            ContentId = "30d47e4c-b240-44f1-b72f-79b8ad87a834";
            ToolTip = "La liste des marqueurs";
            IconSource = new Uri("/Images/marker.png", UriKind.RelativeOrAbsolute);

            ShowCreateMarkerCommand = new RelayCommand(ShowCreateMarker);

            Markers = Enumerable.Empty<Marker>();

            _bus.Register<WorkingFolderLoaded>(this, OnWorkingFolderLoaded);
            _bus.Register<WorkingFolderUnLoaded>(this, OnWorkingFolderUnLoaded);
        }

        private void ShowCreateMarker()
        {
            _createMarkerStandardViewModel.Show();
        }

        // ----- Desstructors
        ~MarkerViewModel()
        {
            _bus.Unregister(this);
        }

        // ----- Callbacks
        private void OnWorkingFolderLoaded(WorkingFolderLoaded @event)
        {
            _collection = @event.AssetsCollection;

            Markers = _collection.GetMarkers<Marker>();
        }
        private void OnWorkingFolderUnLoaded(WorkingFolderUnLoaded @event)
        {
            _collection = null;
        }
    }
}
