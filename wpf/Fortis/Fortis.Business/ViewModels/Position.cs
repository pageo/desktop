using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.ViewModels
{
    public class Position : ThreadSafeNotifiableObject
    {
        // ----- Properties
        public double X { get; set; }
        public double Y { get; set; }

        // ----- Constructors
        public Position(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}