﻿using System.Windows.Input;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.ViewModels
{
    public class SaveViewModel : NotifiableObject
    {
        // ----- Commands
        public ICommand SaveCommand { get; }

        // ----- Constructors
        public SaveViewModel()
        {
            SaveCommand = new RelayCommand(Save, CanSave);
        }

        // ----- Implementation commands
        private bool CanSave()
        {
            return false;
        }
        private void Save()
        {
        }
    }
}
