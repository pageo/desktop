﻿using System.Windows.Input;
using Fortis.Business.ViewModels.Base;

namespace Fortis.Business.ViewModels
{
    public class WhatIsNewViewModel : NotifiableObject
    {
        // ----- Properties
        public bool? ShowWhatIsNew
        {
            get => GetNotifiableProperty<bool?>();
            set => SetNotifiableProperty(value);
        }

        // ----- Commands
        public ICommand WhatIsNewCommand { get; }

        // ----- Constructors
        public WhatIsNewViewModel()
        {
            WhatIsNewCommand = new RelayCommand(WhatIsNew);
        }

        // ----- Implementation commands
        private void WhatIsNew()
        {
            ShowWhatIsNew = true;
        }
    }
}
