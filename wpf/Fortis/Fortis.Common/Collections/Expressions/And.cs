﻿using System;
using System.Linq.Expressions;
using Fortis.Common.Records;

namespace Fortis.Common.Collections.Expressions
{
    public class And : IRecordLogicalExpression
    {
        // ----- Fields
        private readonly IRecordLogicalExpression _leftExpression;
        private readonly IRecordLogicalExpression _rightExpression;


        // ----- Constructors
        public And(IRecordLogicalExpression leftExpression, IRecordLogicalExpression rightExpression)
        {
            _leftExpression = leftExpression ?? throw new ArgumentNullException(nameof(leftExpression));
            _rightExpression = rightExpression ?? throw new ArgumentNullException(nameof(rightExpression));
        }

        // ----- Public methods
        public Expression<Func<Record, bool>> ToLinqExpression()
        {
            var left = _leftExpression.ToLinqExpression();
            var right = _rightExpression.ToLinqExpression();

            return left.And(right);
        }

        // ----- Properties
        public string ToStringExpression() => $"({_leftExpression.ToStringExpression()} {LogicalOperators.AND} {_rightExpression.ToStringExpression()})";
    }
}