﻿namespace Fortis.Common.Collections.Expressions
{
    public static class ConditionValues
    {
        // ----- Constants
        public const string Null = "NULL";
        public const string NullOrEmpty = "NULL_OR_EMPTY";
        public const string Empty = "EMPTY";
        public const string NotNull = "NOT_NULL";
        public const string NotNullAndNotEmpty = "NOT_NULL_AND_NOT_EMPTY";
        public const string NotEmpty = "NOT_EMPTY";
    }
}
