﻿using System.Collections.Generic;

namespace Fortis.Common.Collections.Expressions
{
    public static class ConditionalOperators
    {
        // ----- Constants
        public new const string Equals = "=";
        public const string Different = "<>";
        public const string GreaterThan = ">";
        public const string GreaterOrEqualThan = ">=";
        public const string LowerThan = "<";
        public const string LowerOrEqualThan = "<=";

        public const string Contains = "CONTIENT";
        public const string NotContains = "NE_CONTIENT_PAS";
        public const string StartsWith = "COMMENCE_PAR";
        public const string EndsWith = "FINIT_PAR";
        public const string In = "DANS";
        public const string NotIn = "PAS_DANS";

        // ----- Public methods
        public static IEnumerable<string> All()
        {
            return new[] {
                Equals,
                Different,
                GreaterThan,
                GreaterOrEqualThan,
                LowerThan,
                LowerOrEqualThan,
                Contains,
                NotContains,
                StartsWith,
                EndsWith
            };
        }
    }
}