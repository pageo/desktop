﻿using System;
using System.Linq.Expressions;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Collections.Expressions
{
    public class DifferentFrom<T> : IRecordLogicalExpression
    {
        // ----- Fields
        private readonly string _recordPropertyName;
        private readonly T _value;

        // ----- Constructors
        public DifferentFrom(string recordPropertyName, T value)
        {
            _recordPropertyName = recordPropertyName ?? throw new ArgumentNullException(nameof(recordPropertyName));
            _value = value;
        }

        // ----- Public methods
        public Expression<Func<Record, bool>> ToLinqExpression()
        {
            if (typeof (T) == typeof (string) && Equals(_value, string.Empty))
                return record => !string.IsNullOrEmpty(record.GetValue<string>(_recordPropertyName));
            return record => !Equals(record.GetValue(_recordPropertyName), _value);
        }

        // ----- Properties
        public string ToStringExpression() => $"[{_recordPropertyName}] {ConditionalOperators.Different} {_value.ToNormalizedString()}";
    }
}
