﻿using System;
using System.Linq.Expressions;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Collections.Expressions
{
    public class GreaterOrEqualThan<T> : IRecordLogicalExpression where T : IComparable<T>
    {
        // ------ Fields
        private readonly string _fieldName;
        private readonly T _value;

        // ----- Constructors
        public GreaterOrEqualThan(string fieldName, T value)
        {
            _fieldName = fieldName;
            _value = value;
        }

        // ----- Properties
        public Expression<Func<Record, bool>> ToLinqExpression() => record => record.GetValue(_fieldName) != null && _value.CompareTo(record.GetValue<T>(_fieldName)) <= 0;
        public string ToStringExpression() => $"[{_fieldName}] {ConditionalOperators.GreaterOrEqualThan} {_value.ToNormalizedString()}";
    }
}
