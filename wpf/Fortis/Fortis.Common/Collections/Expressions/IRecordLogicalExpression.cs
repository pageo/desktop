﻿using System;
using System.Linq.Expressions;
using Fortis.Common.Records;

namespace Fortis.Common.Collections.Expressions
{
    public interface IRecordLogicalExpression
    {
        Expression<Func<Record, bool>> ToLinqExpression();
        string ToStringExpression();
    }
}
