﻿namespace Fortis.Common.Collections.Expressions
{
    public interface IRecordLogicalExpressionParser
    {
        IRecordLogicalExpression Parse(string expression);
    }
}