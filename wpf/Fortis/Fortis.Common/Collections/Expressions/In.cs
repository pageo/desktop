﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Collections.Expressions
{
    public class In<T> : IRecordLogicalExpression
    {
        // ----- Fields
        private readonly string _recordPropertyName;
        private readonly T[] _values;

        // ----- Constructors
        public In(string recordPropertyName, IEnumerable<T> values)
        {
            _recordPropertyName = recordPropertyName ?? throw new ArgumentNullException(nameof(recordPropertyName));
            _values = values.ToArray();
        }

        // ----- Public methods
        public Expression<Func<Record, bool>> ToLinqExpression()
        {
            if (typeof(T) != typeof(string) || !_values.Any(x => Equals(x, string.Empty)))
                return record => _values.Any(x => Equals(x, record.GetValue(_recordPropertyName)));
            var stringValues = _values.OfType<string>();
            return record => stringValues.Contains(record.GetValue(_recordPropertyName)) ||
                             record.GetValue(_recordPropertyName) == null;
        }

        // ----- Properties
        public string ToStringExpression() => $"[{_recordPropertyName}] {ConditionalOperators.In} {{{_values.ToNormalizedString()}}}";
    }
}
