﻿namespace Fortis.Common.Collections.Expressions
{
    public static class LogicalOperators
    {
        // ----- Constants
        public const string OR = "OU";
        public const string AND = "ET";

        // ----- Public methods
        public static string[] All()
        {
            return new[] {
                OR,
                AND
            };
        }
    }
}