﻿using System;
using System.Linq.Expressions;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Collections.Expressions
{
    public class NotContains : IRecordLogicalExpression
    {
        // ----- Fields
        private readonly string _fieldName;
        private readonly string _value;

        // ----- Constructors
        public NotContains(string fieldName, string value)
        {
            _fieldName = fieldName ?? throw new ArgumentNullException(nameof(fieldName));
            _value = value ?? throw new ArgumentNullException(nameof(value));
        }

        // ------ Properties
        public Expression<Func<Record, bool>> ToLinqExpression() => record => (record.GetValue<string>(_fieldName) ?? string.Empty).ToLower().Contains(_value.ToLower()) == false;
        public string ToStringExpression() => $"[{_fieldName}] {ConditionalOperators.NotContains} {_value.ToNormalizedString()}";
    }
}