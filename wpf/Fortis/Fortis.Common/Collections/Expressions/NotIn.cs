﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Collections.Expressions
{
    public class NotIn<T> : IRecordLogicalExpression
    {
        // ----- FIelds
        private readonly string _recordPropertyName;
        private readonly T[] _values;

        // ----- Constructors
        public NotIn(string recordPropertyName, T[] values)
        {
            _recordPropertyName = recordPropertyName ?? throw new ArgumentNullException(nameof(recordPropertyName));
            _values = values;
        }

        // ----- Public methods
        public Expression<Func<Record, bool>> ToLinqExpression()
        {
            if (typeof (T) == typeof (string) && _values.Any(x => Equals(x, string.Empty))) {
                var stringValues = _values.OfType<string>();
                return record => !stringValues.Contains(record.GetValue(_recordPropertyName)) && record.GetValue(_recordPropertyName) != null;
            }
            return record => !_values.Any(x => Equals(x, record.GetValue(_recordPropertyName)));
        }

        // ----- Properties
        public string ToStringExpression() => $"[{_recordPropertyName}] {ConditionalOperators.NotIn} {{{_values.ToNormalizedString()}}}";
    }
}