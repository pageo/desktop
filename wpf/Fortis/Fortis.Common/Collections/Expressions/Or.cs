using System;
using System.Linq.Expressions;
using Fortis.Common.Records;

namespace Fortis.Common.Collections.Expressions
{
    public class Or : IRecordLogicalExpression
    {
        // ----- FIelds
        private readonly IRecordLogicalExpression _leftExpression;
        private readonly IRecordLogicalExpression _rightExpression;

        // ----- Constructors
        public Or(IRecordLogicalExpression leftExpression, IRecordLogicalExpression rightExpression)
        {
            _leftExpression = leftExpression;
            _rightExpression = rightExpression;
        }

        // ----- Public methods
        public Expression<Func<Record, bool>> ToLinqExpression()
        {
            var left = _leftExpression.ToLinqExpression();
            var right = _rightExpression.ToLinqExpression();
            return left.Or(right);
        }

        // ----- Properties
        public string ToStringExpression() => $"({_leftExpression.ToStringExpression()} {LogicalOperators.OR} {_rightExpression.ToStringExpression()})";
    }
}