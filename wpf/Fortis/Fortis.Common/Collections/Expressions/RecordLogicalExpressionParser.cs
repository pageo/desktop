﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Collections.Expressions
{
    public class RecordLogicalExpressionParser : IRecordLogicalExpressionParser
    {
        // ----- Properties
        public Collection Collection { get; set; }

        // ----- Constructors
        public RecordLogicalExpressionParser(Collection collection)
        {
            Collection = collection;
        }

        // ----- Public methods
        public IRecordLogicalExpression Parse(string expression)
        {
            var words = SplitIntoWords(expression);
            var elements = JoinWords(words);

            if (CheckValidation(elements) == false)
                throw new Exception("L'expression saisie n'est pas valide : le nombre de parenthèses n'est pas correct.");

            var nodes = BuildReversePolishNotation(elements);
            return GetExpression(nodes);
        }

        public static string[] GetElements(string expression)
        {
            var words = SplitIntoWords(expression);
            return JoinWords(words).ToArray();
        }

        public static void SplitCondition(string condition, out string field, out string op, out string[] values)
        {
            field = null;
            op = null;
            values = null;
            var words = SplitIntoWords(condition);
            if (words.Count != 3)
                return;
            field = RemoveBrackets(words[0]);
            op = words[1];
            var condValues = words[2];
            if (condValues.IsSurroundedBy("{", "}")) {
                values = condValues
                    .Substring(1, condValues.Length - 2)
                    .Split(new[] { Separators.ValueSeparator }, StringSplitOptions.None)
                    .Select(RemoveQuotationMarks)
                    .ToArray();
            }
            else
                values = new string[] { RemoveQuotationMarks(condValues) };
        }

        // ----- Internal logics
        private static List<string> SplitIntoWords(string expression)
        {
            var chars = expression.ToCharArray();
            var words = new List<string>();
            var word = new StringBuilder();
            var stringValue = false;
            var fieldName = false;
            var inValues = false;
            foreach (var c in chars) {
                switch (c) {
                    case '(':
                        if (!stringValue)
                            words.Add("(");
                        else
                            word.Append(c);
                        break;
                    case ')':
                        if (!stringValue)  {
                            if (word.Length != 0) {
                                words.Add(word.ToString());
                                word.Clear();
                            }
                            words.Add(")");
                        }
                        else
                            word.Append(c);
                        break;
                    case '"':
                        stringValue = !stringValue;
                        word.Append(c);
                        break;
                    case '[':
                        fieldName = true;
                        word.Append(c);
                        break;
                    case ']':
                        word.Append(c);
                        words.Add(word.ToString());
                        word.Clear();
                        fieldName = false;
                        break;
                    case '{':
                        inValues = true;
                        word.Append(c);
                        break;
                    case '}':
                        word.Append(c);
                        words.Add(word.ToString());
                        word.Clear();
                        inValues = false;
                        break;
                    case ' ':
                        if (stringValue || fieldName)
                            word.Append(c);
                        else if (!inValues && word.Length != 0) {
                            words.Add(word.ToString());
                            word.Clear();
                        }
                        break;
                    default:
                        word.Append(c);
                        break;
                }
            }
            if (word.Length != 0)
                words.Add(word.ToString());
            return words;
        }
        private static List<string> JoinWords(IEnumerable<string> words)
        {
            var conditions = new List<string>();
            var cond = new StringBuilder();
            foreach (var s in words) {
                if (s == "(")
                    conditions.Add(s);
                else if (s == ")") {
                    if (cond.Length != 0) {
                        conditions.Add(cond.ToString());
                        cond.Clear();
                    }
                    conditions.Add(s);
                }
                else if (LogicalOperators.All().Contains(s)) {
                    if (cond.Length != 0) {
                        conditions.Add(cond.ToString());
                        cond.Clear();
                    }
                    conditions.Add(s);
                }
                else                 {
                    if (cond.Length > 0)
                        cond.Append(" ");
                    cond.Append(s);
                }
            }
            if (cond.Length != 0)
                conditions.Add(cond.ToString());
            return conditions;
        }
        private static List<string> BuildReversePolishNotation(List<string> conditions)
        {
            var results = new List<string>();
            var indexOperator = GetIndexOperatorHighestLevel(conditions);
            if (indexOperator != -1) {
                var leftConditions = conditions.GetRange(0, indexOperator);
                var rightConditions = conditions.GetRange(indexOperator + 1, conditions.Count - (indexOperator + 1));

                var reversePolishNotationLeft = BuildReversePolishNotation(Simplify(leftConditions));
                var reversePolishNotationRight = BuildReversePolishNotation(Simplify(rightConditions));

                results.Add(conditions[indexOperator]);
                results.AddRange(reversePolishNotationLeft);
                results.AddRange(reversePolishNotationRight);
            }
            else
                results.AddRange(Simplify(conditions));

            return results;
        }
        private static List<string> Simplify(List<string> conditions)
        {
            while (conditions.Count > 0) {
                if (conditions[0] == "(")
                    conditions.RemoveAt(0);
                else
                    break;
            }

            while (conditions.Count > 0) {
                if (conditions[conditions.Count - 1] == ")")
                    conditions.RemoveAt(conditions.Count - 1);
                else
                    break;
            }
            return conditions;
        }
        private static List<string> GetSubTree(IEnumerable<string> tree)
        {
            var subtree = new List<string>();
            foreach (var node in tree)             {
                if (LogicalOperators.All().Contains(node)) 
                    subtree.Add(node);
                else {
                    subtree.Add(node);
                    if (subtree.Count % 2 == 1)
                        break;
                }
            }
            return subtree;
        }
        private static bool CheckValidation(IReadOnlyList<string> conditions)
        {
            if (conditions.Count(x => x.Contains("(")) != conditions.Count(x => x.Contains(")")))
                return false;
            var level = 0;
            foreach (var condition in conditions)
            {
                if (condition == "(")
                    level++;
                else if (condition == ")")
                {
                    level--;
                    if (level < 0)
                        return false;
                }
                else if (level < 0)
                    return false;
            }
            return true;
        }
        private static int GetIndexOperatorHighestLevel(IReadOnlyList<string> conditions)
        {
            var operatorWithLevel = new Dictionary<int, int>();
            var level = 0;
            for (var i = 0; i < conditions.Count; i++)
            {
                var condition = conditions[i];
                if (condition == "(")
                    level++;
                else if (condition == ")")
                    level--;
                else if (LogicalOperators.All().Contains(condition))
                    operatorWithLevel.Add(i, level);
            }
            if (operatorWithLevel.Any() == false)
                return -1;
            return operatorWithLevel.OrderBy(x => x.Value).First().Key;
        }

        // ----- RecordLogicalExpressions
        private IRecordLogicalExpression GetExpression(IList<string> tree)
        {
            foreach (var node in tree) {
                switch (node) {
                    case LogicalOperators.AND: {
                        var leftTree = GetSubTree(tree.Skip(1).ToList());
                        var rightTree = tree.Skip(leftTree.Count + 1).ToList();
                        return new And(GetExpression(leftTree), GetExpression(rightTree));
                    }
                    case LogicalOperators.OR: {
                        var leftTree = GetSubTree(tree.Skip(1).ToList());
                        var rightTree = tree.Skip(leftTree.Count + 1).ToList();
                        return new Or(GetExpression(leftTree), GetExpression(rightTree));
                    }
                    default:
                        return GetConditionalExpression(node);
                }
            }
            return null;
        }
        private IRecordLogicalExpression GetConditionalExpression(string condition)
        {
            var words = SplitIntoWords(condition);
            var fieldName = RemoveBrackets(words[0]);
            var conditionalOperator = words[1];
            var value = words[2];

            if (value.IsSurroundedBy("{", "}")) {
                var values = value
                    .Substring(1, value.Length - 2)
                    .Split(new[] { Separators.ValueSeparator }, StringSplitOptions.None);

                return GetMultipleValuesExpression(fieldName, conditionalOperator, values);
            }
            return GetSingleValueExpression(fieldName, conditionalOperator, value);
        }
        private IRecordLogicalExpression GetSingleValueExpression(string fieldName, string conditionalOperator, string value)
        {
            var stringValue = RemoveQuotationMarks(value);

            switch (conditionalOperator) {
                case ConditionalOperators.Equals:
                    return GetSingleValueExpression(fieldName, typeof(Equals<>), stringValue);

                case ConditionalOperators.Different:
                    return GetSingleValueExpression(fieldName, typeof(DifferentFrom<>), stringValue);

                case ConditionalOperators.GreaterThan:
                    return GetSingleNotNullValueExpression(fieldName, typeof(GreaterThan<>), stringValue);

                case ConditionalOperators.LowerThan:
                    return GetSingleNotNullValueExpression(fieldName, typeof(LowerThan<>), stringValue);

                case ConditionalOperators.GreaterOrEqualThan:
                    return GetSingleNotNullValueExpression(fieldName, typeof(GreaterOrEqualThan<>), stringValue);

                case ConditionalOperators.LowerOrEqualThan:
                    return GetSingleNotNullValueExpression(fieldName, typeof(LowerOrEqualThan<>), stringValue);

                case ConditionalOperators.Contains:
                    return new Contains(fieldName, stringValue);

                case ConditionalOperators.NotContains:
                    return new NotContains(fieldName, stringValue);

                case ConditionalOperators.StartsWith:
                    return new StartsWith(fieldName, stringValue);

                case ConditionalOperators.EndsWith:
                    return new EndsWith(fieldName, stringValue);

                default:
                    return null;
            }
        }
        private IRecordLogicalExpression GetMultipleValuesExpression(string fieldName, string conditionalOperator, IEnumerable<string> values)
        {
            var stringValues = values
                .Select(RemoveQuotationMarks)
                .ToArray();

            switch (conditionalOperator)
            {
                case ConditionalOperators.In:
                    return GetMultipleValuesExpression(fieldName, typeof(In<>), stringValues);

                case ConditionalOperators.NotIn:
                    return GetMultipleValuesExpression(fieldName, typeof(NotIn<>), stringValues);

                default:
                    return null;
            }
        }
        private IRecordLogicalExpression GetSingleNotNullValueExpression(string fieldName, Type expressionType, string stringValue)
        {
            var type = Collection.GetField(fieldName).Type;
            var underlyingType = Nullable.GetUnderlyingType(type);
            if (underlyingType != null)
                type = underlyingType;
            var typeConverter = System.ComponentModel.TypeDescriptor.GetConverter(type);
            var value = typeConverter.ConvertFromString(stringValue);
            var genericExpressionType = expressionType.MakeGenericType(type);
            return (IRecordLogicalExpression)Activator.CreateInstance(genericExpressionType, fieldName, value);
        }
        private IRecordLogicalExpression GetSingleValueExpression(string fieldName, Type expressionType, string stringValue)
        {
            var field = Collection.GetField(fieldName);
            var typeConverter = System.ComponentModel.TypeDescriptor.GetConverter(field.Type);
            var value = typeConverter.ConvertFromString(stringValue);
            var genericExpressionType = expressionType.MakeGenericType(field.Type);
            return (IRecordLogicalExpression)Activator.CreateInstance(genericExpressionType, fieldName, value);
        }
        private IRecordLogicalExpression GetMultipleValuesExpression(string fieldName, Type expressionType, IEnumerable<string> stringValues)
        {
            var type = Collection.GetField(fieldName).Type;
            var typeConverter = System.ComponentModel.TypeDescriptor.GetConverter(type);
            var values = stringValues.Select(x => typeConverter.ConvertFromString(x)).ToArray().ChangeType(type);
            var genericExpressionType = expressionType.MakeGenericType(type);
            return (IRecordLogicalExpression)Activator.CreateInstance(genericExpressionType, fieldName, values);
        }

        // ----- Utils
        private static string RemoveQuotationMarks(string value)
        {
            if (value.IsSurroundedBy("\""))
                return value.Substring(1, value.Length - 2);
            return value;
        }
        private static string RemoveBrackets(string value)
        {
            if (value.IsSurroundedBy("[", "]"))
                return value.Substring(1, value.Length - 2);
            return value;
        }

        // ----- Internal classes
        private class ExpressionElement
        {
            public string Element { get; set; }
            public int Level { get; set; }
        }
    }

}
