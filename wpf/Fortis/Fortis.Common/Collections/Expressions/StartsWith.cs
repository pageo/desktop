﻿using System;
using System.Linq.Expressions;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Collections.Expressions
{
    public class StartsWith : IRecordLogicalExpression
    {
        // ----- Fields
        private readonly string _fieldName;
        private readonly string _value;

        // ----- Constructors
        public StartsWith(string fieldName, string value)
        {
            _fieldName = fieldName ?? throw new ArgumentNullException(nameof(fieldName));
            _value = value ?? throw new ArgumentNullException(nameof(value));
        }

        // ----- Properties
        public Expression<Func<Record, bool>> ToLinqExpression() => record => (record.GetValue<string>(_fieldName) ?? string.Empty).ToLower().StartsWith(_value.ToLower());
        public string ToStringExpression() => $"[{_fieldName}] {ConditionalOperators.StartsWith} {_value.ToNormalizedString()}";
    }
}
