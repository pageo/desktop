﻿using System.Windows.Controls;
using System.Windows.Input;
using Fortis.Common.Records;

namespace Fortis.Common.Controls
{
    public partial class MarkerComboBoxCtrl : UserControl
    {
        // ----- Properties
        public Collection Collection { get; set; }
        public ComboBox ComboBox => MarkerComboBox;
        public string Value
        {
            get => MarkerComboBox.Text;
            set
            {
                if (value != null)
                    MarkerComboBox.SelectedIndex = MarkerComboBox.Items.IndexOf(value);
            }
        }

        // ----- Constructors
        public MarkerComboBoxCtrl()
        {
            InitializeComponent();
        }

        // ----- Public methods
        public void FillList()
        {
            MarkerComboBox.Items.Clear();
            if (Collection == null)
                return;
            for (var i = 0; i < Collection.MarkersCount; i++) {
                var marker = Collection.GetMarker(i);
                if (marker != null)
                    MarkerComboBox.Items.Add(marker.Name);
            }
        }

        // ----- Callbacks
        private void MarkerComboBox_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            FillList();
        }
    }
}
