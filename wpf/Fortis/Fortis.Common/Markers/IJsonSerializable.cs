﻿namespace Fortis.Common.Markers
{
    public interface IJsonSerializable
    {
        object Metadata();
    }
}