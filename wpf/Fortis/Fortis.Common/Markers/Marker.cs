﻿using System.Collections.Generic;
using System.Drawing;
using Fortis.Common.Records;
using CollectionChangeAction = Fortis.Common.Records.CollectionChangeAction;
using CollectionChangeEventArgs = Fortis.Common.Records.CollectionChangeEventArgs;

namespace Fortis.Common.Markers
{
    public abstract class Marker
    {
        // ----- Fields
        private readonly SortedList<string, object> _properties = new SortedList<string, object>();

        // ----- Properties
        public string Name { get; private set; }
        public string Type { get; set; }
        public Marker ParentMarker { get; set; }
        public virtual string Description => Name;
        public virtual Collection Collection { get; protected set; }
        public int Count { get; private set; }
        public bool Visible { get; set; } = true;
        public int Zoom { get; set; } = 100;
        public Color Color { get; set; }
        public Color BackColor { get; set; } = Color.FromArgb(233, 229, 220);
        public FontStyle FontStyle { get; set; } = FontStyle.Regular;
        public int Priority { get; internal set; }
        public bool PreventEvents { get; set; } = false;
        public bool CurrentSelection { get; set; } = false;
        public override string ToString() => Description;

        // ----- Delegates
        public delegate void ChangeEventHandler(object sender, CollectionChangeEventArgs e);

        // ----- Events
        public event ChangeEventHandler Change;

        // ----- Constructors
        protected Marker(string name, string type)
        {
            Name = name;
            Type = type;
        }

        // ----- Internal methods
        internal void SetCollection(Collection collection)
        {
            if (Collection != null)
                Collection.Change -= OnCollectionChange;
            Collection = collection;
            if (Collection != null)
                Collection.Change += OnCollectionChange;

            Count = 0;
        }
        internal void SetEvalState(Record record, bool marked)
        {
            var wasMarked = CheckRecord(record);
            var mask = record.EvalMask;
            mask[Priority] = marked;
            var isMarked = CheckRecord(record);
            if (!wasMarked && isMarked)
                Count++;
            else if (wasMarked && !isMarked)
                Count--;
        }
        internal void ResetEvalState()
        {
            var records = Collection?.GetRecords();
            if (records == null) return;
            foreach (var record in records)
                SetEvalState(record, false);
        }
        internal void ResetRecordManualState(Record record, bool raiseEvent = true)
        {
            var wasMarked = CheckRecord(record);
            var maskEx = record.ExcludeMask;
            var maskIn = record.IncludeMask;
            maskEx[Priority] = false;
            maskIn[Priority] = false;
            var isMarked = CheckRecord(record);
            if (!wasMarked && isMarked) {
                Count++;
                if (raiseEvent)
                    RaiseChange(this, record, CollectionChangeAction.MarkerEvaluated);
            }
            else if (wasMarked && !isMarked) {
                Count--;
                if (raiseEvent)
                    RaiseChange(this, record, CollectionChangeAction.MarkerEvaluated);
            }
        }

        // ----- Public methods
        public void Rename(string newName)
        {
            Name = newName;
            RaiseAttributeChange(this);
        }
        public virtual void RaiseChange(object sender, CollectionChangeAction action)
        {
            if (!PreventEvents)
                Change?.Invoke(sender, new CollectionChangeEventArgs(this, action));
        }
        public virtual void RaiseChange(object sender, Record record, CollectionChangeAction action)
        {
            if (!PreventEvents)
                Change?.Invoke(sender, new CollectionChangeEventArgs(this, record, action));
        }
        public void RaiseAttributeChange(object sender)
        {
            if (!PreventEvents)
                Change?.Invoke(sender, new CollectionChangeEventArgs(this, CollectionChangeAction.MarkerAttributeChanged));
        }
        public void ChangeManualState(Record record, bool raiseEvent = true)
        {
            var wasMarked = CheckRecord(record);

            var mask = record.EvalMask;
            var maskEx = record.ExcludeMask;
            var maskIn = record.IncludeMask;

            if (mask[Priority])
                maskEx[Priority] = !maskEx[Priority];
            else
                maskIn[Priority] = !maskIn[Priority];
            var isMarked = CheckRecord(record);
            if (!wasMarked && isMarked) {
                Count++;
                if (raiseEvent)
                    RaiseChange(this, record, CollectionChangeAction.MarkerEvaluated);
            }
            else if (wasMarked && !isMarked) {
                Count--;
                if (raiseEvent)
                    RaiseChange(this, record, CollectionChangeAction.MarkerEvaluated);
            }
        }
        public void ManualInclude(Record record, bool raiseEvent = true)
        {
            var evalMask = record.EvalMask;
            var includeMask = record.IncludeMask;
            var excludeMask = record.ExcludeMask;

            if (includeMask[Priority])
                return;

            if (evalMask[Priority] && !excludeMask[Priority])
                return;

            if (excludeMask[Priority])
                excludeMask[Priority] = false;
            else
                includeMask[Priority] = true;
            Count++;
            if (raiseEvent)
                RaiseChange(this, record, CollectionChangeAction.MarkerEvaluated);
        }
        public void ManualExclude(Record record, bool raiseEvent = true)
        {
            var evalMask = record.EvalMask;
            var excludeMask = record.ExcludeMask;
            var includeMask = record.IncludeMask;

            if (excludeMask[Priority])
                return;

            if (!evalMask[Priority] && !includeMask[Priority])
                return;

            if (includeMask[Priority])
                includeMask[Priority] = false;
            else
                excludeMask[Priority] = true;
            Count--;
            if (raiseEvent)
                RaiseChange(this, record, CollectionChangeAction.MarkerEvaluated);
        }
        public void CancelManualInclusion(Record record, bool raiseEvent = true)
        {
            var includeMask = record.IncludeMask;
            if (includeMask[Priority] == false)
                return;
            includeMask[Priority] = false;
            Count--;
            if (raiseEvent)
                RaiseChange(this, record, CollectionChangeAction.MarkerEvaluated);
        }
        public void CancelManualExclusion(Record record, bool raiseEvent = true)
        {
            var excludeMask = record.ExcludeMask;
            if (excludeMask[Priority] == false)
                return;
            excludeMask[Priority] = false;
            Count++;
            if (raiseEvent)
                RaiseChange(this, record, CollectionChangeAction.MarkerEvaluated);
        }
        public void ResetManualState(bool raiseEvent = true)
        {
            var records = Collection?.GetRecords();
            if (records == null) return;
            foreach (Record record in records)
                ResetRecordManualState(record, raiseEvent);
        }
        public bool CheckRecord(Record record)
        {
            return CheckRecord(record, out bool manualInclusion, out bool manualExclusion);
        }
        public bool CheckRecord(Record record, out bool manualInclusion, out bool manualExclusion)
        {
            manualInclusion = false;
            manualExclusion = false;
            var mask = record.IncludeMask;
            if (mask[Priority] == true) {
                manualInclusion = true;
                return true;
            }
            mask = record.ExcludeMask;
            if (mask[Priority]) {
                manualExclusion = true;
                return false;
            }
            mask = record.EvalMask;
            return mask[Priority];
        }
        public void CheckRecordState(Record record, out bool eval, out bool manualInclusion, out bool manualExclusion)
        {
            var mask = record.EvalMask;
            eval = mask[Priority];

            var includeMask = record.IncludeMask;
            manualInclusion = includeMask[Priority];

            var excludeMask = record.ExcludeMask;
            manualExclusion = excludeMask[Priority];
        }
        public int GetRecordStateErrorCount()
        {
            var errCount = 0;
            var records = Collection?.GetRecords();
            if (records == null) return errCount;
            foreach (var record in records) {
                CheckRecordState(record, out bool eval, out bool manualInclusion, out bool manualExclusion);
                if (eval && manualInclusion)
                    errCount++;
                else if (!eval && manualExclusion)
                    errCount++;
            }
            return errCount;
        }
        public void SetProperty(string property, object value)
        {
            _properties[property] = value;
        }
        public object GetProperty(string property)
        {
            return _properties[property];
        }
        public virtual void OnCollectionChange(object sender, CollectionChangeEventArgs e)
        {
            if (e.Action == CollectionChangeAction.MarkerDeleted) {
                if (e.Marker != null && e.Marker == ParentMarker) {
                    ParentMarker = null;
                    Eval();
                }
            }
            else if (e.Action == CollectionChangeAction.MarkerEvaluated) {
                if (e.Marker != null && e.Marker == ParentMarker)
                    Eval();
            }
        }

        // ----- Abstract methods
        public abstract void Eval();
    }
}