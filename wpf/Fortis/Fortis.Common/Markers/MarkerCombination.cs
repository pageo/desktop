﻿using System.Text;
using Fortis.Common.Records;

namespace Fortis.Common.Markers
{
    public class MarkerCombination : Marker
    {
        // ----- Properties
        public string Expression { get; set; }
        public override string Description => Expression;

        // ----- Constructors
        public MarkerCombination(string name)
            : base(name, "Combinaison")
        {
        }

        // ----- Public methods
        public override void Eval()
        {
            ResetEvalState();
            if (Collection == null)
                return;
            if (string.IsNullOrEmpty(Expression))
                return;
            var records = Collection.GetRecords();
            foreach (var record in records) {
                var exp = new StringBuilder(Expression);
                EvalRecord(record, exp);
                if (exp.ToString().Equals("1"))
                    SetEvalState(record, true);
            }
            RaiseChange(this, CollectionChangeAction.MarkerEvaluated);
        }
        public override void OnCollectionChange(object sender, CollectionChangeEventArgs e)
        {
            if (e.Action == CollectionChangeAction.MarkerDeleted) {
                if (e.Marker != null && !string.IsNullOrEmpty(Expression)) {
                    if (Expression.Contains("(" + e.Marker.Name + ")")) {
                        Expression = null;
                        Eval();
                    }
                }
            }
            else if (e.Action == CollectionChangeAction.MarkerEvaluated) {
                if (e.Marker != null && !string.IsNullOrEmpty(Expression)) {
                    if (Expression.Contains("(" + e.Marker.Name + ")"))
                        Eval();
                }
            }
        }

        // ----- Internal logics
        private void EvalRecord(Record record, StringBuilder exp)
        {
            var s = exp.ToString();
            var end = s.IndexOf(')');
            if (end <= 0) return;

            var start = s.LastIndexOf('(', end - 1, end);
            if (start == -1 || end <= start + 1) return;

            var cond = s.Substring(start + 1, end - start - 1);
            var newcond = "0";
            cond.Trim();
            Marker marker;
            if ((marker = Collection.GetMarker(cond)) != null) {
                if (marker.CheckRecord(record))
                    newcond = "1";
            }
            else {
                var words = cond.Split(' ');
                if (words.Length == 2 && words[0].Equals("non")) {
                    if (words[1].Equals("0"))
                        newcond = "1";
                }
                else if (words.Length == 3 && words[1].Equals("et")) {
                    if (words[0].Equals("1") && words[2].Equals("1"))
                        newcond = "1";
                }
                else if (words.Length == 3 && words[1].Equals("ou")) {
                    if (words[0].Equals("1") || words[2].Equals("1"))
                        newcond = "1";
                }
            }
            exp.Remove(start, end - start + 1);
            exp.Insert(start, newcond);
            EvalRecord(record, exp);
        }
    }
}
