﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Markers
{
    // ----- Enums
    public enum MarkerDateType { DateInPeriod = 0, DateNotInPeriod = 1, PeriodInPeriod = 2, PeriodNotInPeriod = 3, PeriodContainsDate = 4, PeriodNotContainsDate = 5, PeriodContainsPeriod = 6, PeriodNotContainsPeriod = 7 };
    public enum MarkerDateOp { IncludedIn = 0, NotIncludedIn = 1, Contains = 2, NotContains = 3, Equals = 4, Different = 5 };

    public class DateMarkerParams
    {
        // ----- Properties
        public MarkerDateOp DateOp = 0;
        public bool CheckSimpleDate = true;
        public DateTime Date = DateTime.Now;
        public DateTime StartDate = DateTime.MinValue;
        public DateTime EndDate = DateTime.MaxValue;
        public string DateField = null;
        public string StartField = null;
        public string EndField = null;

        // ----- Public methods
        public override string ToString()
        {
            var dictionary = new Dictionary<MarkerDateOp, string> {
                [MarkerDateOp.Equals] = "=",
                [MarkerDateOp.Different] = "!=",
                [MarkerDateOp.Contains] = "IN",
                [MarkerDateOp.NotContains] = "NOT IN",
                [MarkerDateOp.IncludedIn] = "IN",
                [MarkerDateOp.NotIncludedIn] = "NOT IN",
            };

            var left = string.Empty;
            var right = string.Empty;

            if (string.IsNullOrEmpty(DateField) == false) {
                left = DateField;
                if (DateOp == MarkerDateOp.Equals || DateOp == MarkerDateOp.Different)
                    right = Date.ToString("dd/MM/yyyy").SurroundWith("'");
                if (DateOp == MarkerDateOp.Contains || DateOp == MarkerDateOp.NotContains)
                    right = FormatInterval(StartDate, EndDate);
            }
            else {
                right = FormatInterval(StartField, EndField);
                if (DateOp == MarkerDateOp.IncludedIn || DateOp == MarkerDateOp.NotIncludedIn) {
                    if (Date.Date != DateTime.Now.Date)
                        left = Date.ToString("dd/MM/yyyy").SurroundWith("'");
                    else
                        left = FormatInterval(StartDate, EndDate);
                }
            }
            if (string.IsNullOrEmpty(left) || string.IsNullOrEmpty(right))
                return " - ";
            return left + dictionary[DateOp].SurroundWith(" ") + right;
        }

        // ----- Internal logics
        private static string FormatInterval(DateTime startDate, DateTime endDate)
        {
            return FormatInterval(startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));
        }
        private static string FormatInterval(string start, string end)
        {
            return (start + " , " + end).SurroundWith("[", "]");
        }
    }

   
    public class MarkerDate : Marker
    {
        // ----- Properties
        public DateMarkerParams Params { get; set; } = null;
        public override string Description => Params?.ToString();

        // ----- Fields
        public static readonly string[] DateOps = {"Incluse dans", "Non incluse dans", "Contient", "Ne contient pas", "Egal", "Différent"};

        // ----- Constructors
        public MarkerDate(string name) : base(name, "Date")
        {
        }

        // ----- Internal logics
        private static bool DateInPeriod(DateTime date, object startVal, object endVal)
        {
            if (startVal is DateTime) {
                if (date < (DateTime) startVal)
                    return false;
            }
            else if (startVal is String) {
                var start = DateTime.ParseExact((string) startVal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (date < start)
                    return false;
            }
            else
                return false;
            if (endVal is DateTime) {
                if (date > (DateTime) endVal)
                    return false;
            }
            else if (endVal is String) {
                var end = DateTime.ParseExact((string) endVal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (date > end)
                    return false;
            }
            else if (endVal is DBNull)
                return true;
            else
                return false;
            return true;
        }
        private static bool PeriodContainsDate(DateTime startDate, DateTime endDate, object date)
        {
            if (date is DateTime) {
                if ((DateTime) date < startDate)
                    return false;
                if ((DateTime) date > endDate)
                    return false;
            }
            else if (date is String) {
                var t = DateTime.ParseExact((string) date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (t < startDate)
                    return false;
                if (t > endDate)
                    return false;
            }
            else
                return false;
            return true;
        }
        private static bool PeriodInPeriod(DateTime startDate, DateTime endDate, object startVal, object endVal)
        {
            if (startVal is DateTime) {
                if (startDate < (DateTime) startVal)
                    return false;
            }
            else if (startVal is String) {
                var start = DateTime.ParseExact((string) startVal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (startDate < start)
                    return false;
            }
            else
                return false;
            if (endVal is DateTime) {
                if (endDate > (DateTime) endVal)
                    return false;
            }
            else if (endVal is String) {
                var end = DateTime.ParseExact((string) endVal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (endDate > end)
                    return false;
            }
            else if (endVal is DBNull)
                return true;
            else
                return false;
            return true;
        }
        private bool PeriodContainsPeriod(DateTime startDate, DateTime endDate, Object startVal, Object endVal)
        {
            if (startVal is DateTime) {
                if (startDate > (DateTime) startVal)
                    return false;
            }
            else if (startVal is String) {
                var start = DateTime.ParseExact((string) startVal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (startDate > start)
                    return false;
            }
            else
                return false;
            if (endVal is DateTime) {
                if (endDate < (DateTime) endVal)
                    return false;
            }
            else if (endVal is String) {
                var end = DateTime.ParseExact((string) endVal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (endDate < end)
                    return false;
            }
            else
                return false;
            return true;
        }
        private static bool PeriodEqualsPeriod(DateTime startDate, DateTime endDate, object startVal, object endVal)
        {
            if (startVal is DateTime) {
                if (startDate != (DateTime) startVal)
                    return false;
            }
            else if (startVal is String) {
                var start = DateTime.ParseExact((string) startVal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (startDate != start)
                    return false;
            }
            else
                return false;
            if (endVal is DateTime) {
                if (endDate != (DateTime) endVal)
                    return false;
            }
            else if (endVal is String) {
                var end = DateTime.ParseExact((string) endVal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (endDate != end)
                    return false;
            }
            else
                return false;
            return true;
        }
        private bool EvalRecord(Record record)
        {
            try {
                object startVal, endVal, dateVal;
                switch (Params.DateOp) {
                    case (int) MarkerDateOp.IncludedIn:
                        if (string.IsNullOrEmpty(Params.StartField) || string.IsNullOrEmpty(Params.EndField))
                            return false;
                        startVal = record.GetValue(Params.StartField);
                        endVal = record.GetValue(Params.EndField);
                        if (Params.CheckSimpleDate)
                            return DateInPeriod(Params.Date, startVal, endVal);
                        else
                            return PeriodInPeriod(Params.StartDate, Params.EndDate, startVal, endVal);

                    case MarkerDateOp.NotIncludedIn:
                        if (string.IsNullOrEmpty(Params.StartField) || string.IsNullOrEmpty(Params.EndField))
                            return false;
                        startVal = record.GetValue(Params.StartField);
                        endVal = record.GetValue(Params.EndField);
                        if (Params.CheckSimpleDate)
                            return !DateInPeriod(Params.Date, startVal, endVal);
                        else
                            return !PeriodInPeriod(Params.StartDate, Params.EndDate, startVal, endVal);

                    case MarkerDateOp.Contains:
                        if (!string.IsNullOrEmpty(Params.StartField) && !string.IsNullOrEmpty(Params.EndField)) {
                            startVal = record.GetValue(Params.StartField);
                            endVal = record.GetValue(Params.EndField);
                            return PeriodInPeriod(Params.StartDate, Params.EndDate, startVal, endVal);
                        }
                        else if (!String.IsNullOrEmpty(Params.DateField)) {
                            dateVal = record.GetValue(Params.DateField);
                            return PeriodContainsDate(Params.StartDate, Params.EndDate, dateVal);
                        }
                        else
                            return false;

                    case MarkerDateOp.NotContains:
                        if (!string.IsNullOrEmpty(Params.StartField) && !string.IsNullOrEmpty(Params.EndField)) {
                            startVal = record.GetValue(Params.StartField);
                            endVal = record.GetValue(Params.EndField);
                            return !PeriodInPeriod(Params.StartDate, Params.EndDate, startVal, endVal);
                        }
                        else if (!string.IsNullOrEmpty(Params.DateField)) {
                            dateVal = record.GetValue(Params.DateField);
                            return !PeriodContainsDate(Params.StartDate, Params.EndDate, dateVal);
                        }
                        else
                            return false;

                    case MarkerDateOp.Equals:
                        if (Params.CheckSimpleDate) {
                            if (string.IsNullOrEmpty(Params.DateField))
                                return false;
                            dateVal = record.GetValue(Params.DateField);
                            if (dateVal is DateTime)
                                return dateVal.Equals(Params.Date);
                            if (dateVal is String) {
                                var date = DateTime.ParseExact(dateVal as string, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                return date.Equals(Params.Date);
                            }
                            return false;
                        }
                        else {
                            if ((String.IsNullOrEmpty(Params.StartField)) || string.IsNullOrEmpty(Params.EndField))
                                return false;
                            startVal = record.GetValue(Params.StartField);
                            endVal = record.GetValue(Params.EndField);
                            return PeriodEqualsPeriod(Params.StartDate, Params.EndDate, startVal, endVal);
                        }

                    case MarkerDateOp.Different:
                        if (Params.CheckSimpleDate) {
                            if (string.IsNullOrEmpty(Params.DateField))
                                return false;
                            dateVal = record.GetValue(Params.DateField);
                            if (dateVal is DateTime)
                                return !dateVal.Equals(Params.Date);
                            if (dateVal is String) {
                                var date = DateTime.ParseExact(dateVal as string, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                return !date.Equals(Params.Date);
                            }
                            return false;
                        }
                        else {
                            if (string.IsNullOrEmpty(Params.StartField) || string.IsNullOrEmpty(Params.EndField))
                                return false;
                            startVal = record.GetValue(Params.StartField);
                            endVal = record.GetValue(Params.EndField);
                            return !PeriodEqualsPeriod(Params.StartDate, Params.EndDate, startVal, endVal);
                        }

                    default:
                        return false;
                }
            }
            catch {
                return false;
            }
        }

        // ----- Public methods
        public override void Eval()
        {
            ResetEvalState();
            if (Collection == null)
                return;
            var records = Collection.GetRecords();
            foreach (var record in records) {
                if (ParentMarker != null && !ParentMarker.CheckRecord(record))
                    continue;
                var ok = EvalRecord(record);
                if (ok)
                    SetEvalState(record, true);
            }
            RaiseChange(this, CollectionChangeAction.MarkerEvaluated);
        }
    }
}
