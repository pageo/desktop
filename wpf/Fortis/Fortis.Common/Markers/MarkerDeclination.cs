﻿namespace Fortis.Common.Markers
{
    public class MarkerDeclination : MarkerStandard
    {
        // ----- Properties
        public string DeclinationName { get; set; }

        // ----- Constructors
        public MarkerDeclination(string name) : base(name)
        {
            Type = "Déclinaison";
        }
    }
}
