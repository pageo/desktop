﻿using System.Linq;
using Fortis.Common.Records;

namespace Fortis.Common.Markers
{
    public class MarkerDistinct : Marker, IJsonSerializable
    {
        // ----- Properties
        public string DistinctFieldName { get; set; } = null;
        public override string Description => "Distinct " + DistinctFieldName;

        // ----- Constructors
        public MarkerDistinct(string name)
            : base(name, "Distinct")
        {
        }

        // ----- Public methods
        public override void Eval()
        {
            object prevVal = null;
            ResetEvalState();
            if (Collection == null)
                return;
            if (string.IsNullOrEmpty(DistinctFieldName))
                return;
            var records = Collection.GetRecords().OrderBy(x => x.GetValue(DistinctFieldName));
            foreach (var record in records) {
                if ((ParentMarker != null) && (!ParentMarker.CheckRecord(record)))
                    continue;
                var val = record.GetValue(DistinctFieldName);
                if (Equals(val, prevVal)) continue;
                SetEvalState(record, true);
                prevVal = val;
            }
            RaiseChange(this, CollectionChangeAction.MarkerEvaluated);
        }

        // ----- IJsonSerializable
        object IJsonSerializable.Metadata()
        {
            return new {
                Name,
                DistinctFieldName,
                Color,
                BackColor
            };
        }
    }
}
