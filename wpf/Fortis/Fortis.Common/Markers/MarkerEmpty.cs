﻿using Fortis.Common.Records;

namespace Fortis.Common.Markers
{
    public class MarkerEmpty : Marker
    {
        // ----- Properties
        public override string Description { get; } = "Vide";

        // ----- Constructors
        public MarkerEmpty(string name)
            : base(name, "Vide")
        {
        }

        // ----- Public methods
        public override void Eval()
        {
            ResetEvalState();
            RaiseChange(this, CollectionChangeAction.MarkerEvaluated);
        }
    }
}
