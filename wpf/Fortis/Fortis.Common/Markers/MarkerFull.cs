﻿using Fortis.Common.Records;

namespace Fortis.Common.Markers
{
    public class MarkerFull : Marker
    {
        // ----- Properties
        public override string Description { get; } = "Plein";

        // ----- Constructors
        public MarkerFull(string name)
            : base(name, "Plein")
        {
        }

        // ----- Public methods
        public override void Eval()
        {
            if (Collection == null) return;
            var records = Collection.GetRecords();
            foreach (var record in records) {
                if ( ParentMarker != null && !ParentMarker.CheckRecord(record) )
                    SetEvalState(record, false);
                else
                    SetEvalState(record, true);
            }
            RaiseChange(this, CollectionChangeAction.MarkerEvaluated);
        }
    }
}
