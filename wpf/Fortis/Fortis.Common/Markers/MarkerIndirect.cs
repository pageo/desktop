﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fortis.Common.Records;
using Fortis.Common.Utils;

namespace Fortis.Common.Markers
{
    public class IndirectMarkerParams
    {
        // ----- Fields
        public Collection ExternalCollection = null;
        public Marker ExternalMarker;
        public MarkerIndirect.Operator Operator = MarkerIndirect.Operator.Equal;
        public string InternalField = null;
        public string ExternalField = null;

        // ----- Public methods
        public override string ToString()
        {
            if (ExternalCollection == null)
                return " - ";

            var dictionary = new Dictionary<MarkerIndirect.Operator, string> {
                [MarkerIndirect.Operator.Equal] = "=",
                [MarkerIndirect.Operator.Substring] = "Sous-chaîne de",
                [MarkerIndirect.Operator.Different] = "<>",
                [MarkerIndirect.Operator.Contains] = "Contient"
            };

            var left = InternalField;
            var op = dictionary[Operator].SurroundWith(" ");
            var right = ExternalMarker != null ? string.Join(".", ExternalCollection.Name, ExternalMarker.Name, ExternalField).SurroundWith("{", "}") : string.Join(".", ExternalCollection.Name, ExternalField).SurroundWith("{", "}");
            return left + op + right;
        }
    }

    public class MarkerIndirect : Marker
    {
        // ----- Fields
        private readonly SortedSet<string> _externalValues = new SortedSet<string>();
        private IndirectMarkerParams _params;

        // ----- Properties
        public IndirectMarkerParams Params
        {
            get => _params;
            set {
                if (_params?.ExternalCollection != null)
                    _params.ExternalCollection.Change -= OnExternalCollectionChange;
                _params = value;
                if (_params?.ExternalCollection != null)
                    _params.ExternalCollection.Change += OnExternalCollectionChange;
            }
        }

        // ----- Enums
        public enum Operator
        {
            Equal = 1,
            Different = 2,
            Substring = 3,
            Contains = 4
        };

        // ----- Properties
        public override string Description => Params?.ToString();

        // ----- Constructors
        public MarkerIndirect(string name): base(name, "Indirect") {}

        // ----- Public methods
        public override void Eval()
        {
            ResetEvalState();
            if (Collection == null)
                return;
            if (_params == null)
                return;

            InitializeExternalValues();

            foreach (var record in GetAllInternalRecords()) {
                if (EvalRecord(record))
                    SetEvalState(record, true);
            }
            RaiseChange(this, CollectionChangeAction.MarkerEvaluated);
        }
        public Record GetExternalRecord(Record internalRecord)
        {
            return GetAllExternalRecords().FirstOrDefault(externalRecord => AreLinked(internalRecord, externalRecord));
        }
        public IDictionary<Record, Record> GetExternalRecords(IReadOnlyCollection<Record> internalRecords)
        {
            var results = new Dictionary<Record, Record>();

            foreach (var externalRecord in GetAllExternalRecords()) {
                foreach (var internalRecord in internalRecords) {
                    if (AreLinked(internalRecord, externalRecord) && !results.ContainsKey(internalRecord)) {
                        results.Add(internalRecord, externalRecord);
                    }
                }
            }
            return results;
        }

        // ----- Callbacks
        private void OnExternalCollectionChange(Object sender, CollectionChangeEventArgs e)
        {
            if (e.Action == CollectionChangeAction.MarkerDeleted) {
                if (_params?.ExternalMarker != null && e.Marker == _params.ExternalMarker) {
                    _params.ExternalMarker = null;
                    Eval();
                }
            }
            else if (e.Action == CollectionChangeAction.MarkerEvaluated) {
                if (_params?.ExternalMarker != null && e.Marker == _params.ExternalMarker) {
                    Eval();
                }
            }
        }

        // ----- Internal logics
        private bool AreLinked(Record internalRecord, Record externalRecord)
        {
            var internalValue = Normalize(internalRecord.GetValue(Params.InternalField));
            var externalValue = Normalize(externalRecord.GetValue(Params.ExternalField));

            switch (Params.Operator) {
                case Operator.Equal:
                    return Equals(externalValue, internalValue);

                case Operator.Contains:
                    return internalValue.Contains(externalValue);

                case Operator.Substring:
                    return externalValue.Contains(internalValue);

                case Operator.Different:
                    return false;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        private IEnumerable<Record> GetAllExternalRecords()
        {
            var records = Params.ExternalCollection.GetRecords();
            if (Params.ExternalMarker != null)
                records = records.Where(x => Params.ExternalMarker.CheckRecord(x));
            return records;
        }
        private IEnumerable<Record> GetAllInternalRecords()
        {
            var records = Collection.GetRecords();
            if (ParentMarker != null)
                records = records.Where(x => ParentMarker.CheckRecord(x));
             return records;
        }
        private void InitializeExternalValues()
        {
            _externalValues.Clear();
            if (_params?.ExternalCollection == null) return;
            foreach (var record in GetAllExternalRecords())
                _externalValues.Add(Normalize(record.GetValue(_params.ExternalField)));
        }
        private bool EvalRecord(Record record)
        {
            var val = Normalize(record.GetValue(_params.InternalField));
            switch (_params.Operator) {
                case Operator.Equal:
                    return _externalValues.Contains(val);

                case Operator.Different:
                    return !_externalValues.Contains(val);

                case Operator.Contains:
                    return _externalValues.Any(s => val.Contains(s));

                case Operator.Substring:
                    return _externalValues.Any(s => s.Contains(val));

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        private static string Normalize(object obj)
        {
            return obj?.ToString().ToLower() ?? string.Empty;
        }
    }
}