﻿using System.Linq;
using Fortis.Common.Collections.Expressions;
using Fortis.Common.Records;

namespace Fortis.Common.Markers
{
    public class MarkerStandard : Marker, IJsonSerializable
    {
        // ----- Enums
        public enum ExpressionTypes { Basic, Advanced };

        // ----- Fields
        private string _expression;

        // ----- Properties
        public string Expression
        {
            get => _expression;
            set {
                _expression = value;
                TryToCreateLogicalExpression();
            }
        }
        public IRecordLogicalExpression LogicalExpression { get; private set; }
        public override string Description => Expression;
        public override Collection Collection
        {
            get => base.Collection;
            protected set {
                base.Collection = value;
                TryToCreateLogicalExpression();
            }
        }
        public ExpressionTypes ExpressionType { get; set; } = ExpressionTypes.Basic;

        // ----- Constructors
        public MarkerStandard(string name)
            : base(name, "Standard")
        {
        }

        public override void Eval()
        {
            if (Collection == null) return;

            ResetEvalState();

            var predicate = LogicalExpression.ToLinqExpression().Compile();
            var records = Collection.GetRecords().Where(predicate);
            foreach (var record in records) {
                if ((ParentMarker != null) && (!ParentMarker.CheckRecord(record)))
                    continue;
                SetEvalState(record, true);
            }
            RaiseChange(this, CollectionChangeAction.MarkerEvaluated);
        }

        // ----- Internal logics
        private void TryToCreateLogicalExpression()
        {
            if (Collection == null || string.IsNullOrEmpty(Expression)) return;
            var parser = new RecordLogicalExpressionParser(Collection);
            LogicalExpression = parser.Parse(Expression);
        }

        // ----- IJsonSerializable
        object IJsonSerializable.Metadata()
        {
            return new {
                Name,
                Expression,
                ExpressionType,
                Color,
                BackColor
            };
        }
    }
}
