﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Fortis.Common.Markers;

namespace Fortis.Common.Records
{
    public class Collection
    {
        // ----- Constants
        public const string DefaultMarkerName = "Tout";

        // ----- Fields
        private Record _currentRecord;
        private readonly IDictionary<string, Field> _fields = new Dictionary<string, Field>();
        private readonly SortedList<string, Marker> _markers = new SortedList<string, Marker>();
        private readonly Color[] _markerForeColors = {
            Color.FromArgb(46, 95, 167), Color.FromArgb(192, 0, 0), Color.FromArgb(0, 192, 0), Color.FromArgb(192, 0, 255),
            Color.FromArgb(255, 198, 0), Color.FromArgb(128, 79, 30), Color.FromArgb(255, 86, 0), Color.FromArgb(30, 111, 30),
            Color.FromArgb(79, 30, 79), Color.FromArgb(128, 30, 45), Color.FromArgb(128, 128, 0), Color.FromArgb(79, 79, 79),
            Color.FromArgb(28, 23, 119), Color.FromArgb(110, 244, 153), Color.FromArgb(76, 195, 241), Color.FromArgb(254, 143, 113),
            Color.FromArgb(111, 161, 176)
        };
        private readonly Random _random = new Random();

        // ----- Properties
        public IList<Record> Records { get; } = new List<Record>();
        public string Name { get; }
        public string Type { get; }
        public bool Modified { get; private set; }
        public int MarkersCount => _markers.Count;
        public IEnumerable<T> GetMarkers<T>() => _markers.Values.OfType<T>().ToArray();
        public Marker GetSelectionMarker() => _markers.Values.ToArray().FirstOrDefault(mrk => mrk.CurrentSelection);
        public IEnumerable<Record> GetRecords() => Records.Where(x => x.State != RecordState.Deleted);
        public Record CurrentRecord
        {
            get => _currentRecord;
            set => SetCurrentRecord(null, value);
        }

        // ----- Events
        public delegate void ChangeEventHandler(object sender, CollectionChangeEventArgs e);
        public event ChangeEventHandler Change;

        // ----- Enums
        public enum MarkerPriority { High, Low }

        // ----- Constructors
        public Collection(string name, string type)
        {
            Name = name;
            Type = type;
            Init();
        }

        // ----- Public methods
        public virtual void OnChange(object sender, CollectionChangeAction action)
        {
            Change?.Invoke(this, new CollectionChangeEventArgs(this, action));
        }
        public virtual void OnChange(object sender, Record record, string fieldname, CollectionChangeAction action)
        {
            Change?.Invoke(sender, new CollectionChangeEventArgs(record, fieldname, action));
        }
        public virtual void OnChange(object sender, Marker marker, CollectionChangeAction action)
        {
            Change?.Invoke(sender, new CollectionChangeEventArgs(marker, action));
        }
        public Record AddRecord(bool raiseEvent = false)
        {
            var record = new Record(this);
            record.SetAdded();
            Records.Add(record);

            SetCollectionAsModified();

            if (raiseEvent)
                OnChange(this, record, null, CollectionChangeAction.RecordAdded);

            return record;
        }
        public void DeleteRecord(Record record, bool raiseEvent = false)
        {
            if (_currentRecord == record)
                _currentRecord = null;

            record.Delete();

            Modified = true;

            if (raiseEvent)
                OnChange(this, CollectionChangeAction.RecordDeleted);
        }
        public void SetCurrentRecord(object sender, Record record)
        {
            if (record != null && record.Collection != this)
                throw new Exception("Cannot set current record to a record from another collection!");
            _currentRecord = record;
            OnChange(sender, record, null, CollectionChangeAction.CurrentRecordChanged);
        }
        public Record GetRecord(int index)
        {
            return GetRecords().ElementAt(index);
        }     
        public IEnumerable<Record> GetRecords(string markerName)
        {
            var marker = GetMarker(markerName);
            var records = GetRecords();
            return marker == null ? records : records.Where(rec => marker.CheckRecord(rec));
        }
        public IEnumerable<Record> GetChangedRecords() => Records.Where(x => x.State == RecordState.Added || x.State == RecordState.Modified);
        public IEnumerable<Record> GetDeletedRecords() => Records.Where(x => x.State == RecordState.Deleted);
        public IEnumerable<Record> GetSortedRecords(params string[] sortFields)
        {
            var records = GetRecords();
            if (sortFields == null || sortFields.Length <= 0)
                return records;
            var orderedRecords = records.OrderBy(x => x.GetValue(sortFields.First()));
            foreach (var sortField in sortFields.Skip(1))
                orderedRecords = orderedRecords.ThenBy(x => x.GetValue(sortField));
            return orderedRecords;
        }
        public void AddField(string fieldName, Type fieldType)
        {
            AddField(new Field(_fields.Count, fieldName, fieldType));
            SetCollectionAsModified();
        }
        public void RemoveField(string fieldName)
        {
            var field = GetField(fieldName);

            _fields.Remove(fieldName);

            foreach (var record in Records)
                record.RemoveField(field);
        }
        public Field GetField(string fieldName)
        {
            Field field;
            return _fields.TryGetValue(fieldName, out field) ? field : null;
        }
        public bool FieldExists(string fieldName)
        {
            return GetField(fieldName) != null;
        }
        public IEnumerable<Field> GetFields() => _fields.Values;
        public void SetCollectionAsModified()
        {
            Modified = true;
        }
        public void Clear(bool raiseEvent = false)
        {
            Records.Clear();
            _currentRecord = null;

            SetCollectionAsModified();

            if (raiseEvent)
                OnChange(this, CollectionChangeAction.CollectionCleared);
        }
        public bool CheckMarkerName(string name)
        {
            return _markers.IndexOfKey(name) == -1;
        }
        public void RenameMarker(Marker marker, string newName)
        {
            if (marker == null) throw new ArgumentNullException(nameof(marker));
            if (string.IsNullOrEmpty(newName))
                throw (new ArgumentNullException(nameof(newName)));
            if (marker.Name == newName)
                return;
            if (CheckMarkerName(newName)) {
                var oldName = marker.Name;
                _markers.Remove(oldName);
                marker.Rename(newName);
                _markers.Add(newName, marker);
                foreach (var m in _markers.Values.OfType<MarkerCombination>())
                    m.Expression = m.Expression?.Replace("(" + oldName + ")", "(" + newName + ")");
            }
            else
                throw (new Exception("A Marker named " + newName + " already exists in the collection!"));
        }
        public void AddMarker(Marker marker, MarkerPriority priority = MarkerPriority.High)
        {
            if (marker == null) throw new ArgumentNullException(nameof(marker));
            if (_markers.IndexOfKey(marker.Name) != -1)
                throw (new Exception("A Marker named " + marker.Name + " already exists in the collection!"));
            _markers.Add(marker.Name, marker);
            marker.SetCollection(this);

            if (priority == MarkerPriority.High) {
                marker.Priority = _markers.Count - 1;

                foreach (var record in Records) {
                    record.EvalMask.Add(false);
                    record.IncludeMask.Add(false);
                    record.ExcludeMask.Add(false);
                }
            }
            else {
                foreach (var existingMarker in _markers.Values) {
                    if (existingMarker.Priority > 0)
                        existingMarker.Priority++;
                }

                marker.Priority = Math.Min(_markers.Count - 1, 1);

                foreach (var record in Records) {
                    record.EvalMask.Insert(marker.Priority, false);
                    record.IncludeMask.Insert(marker.Priority, false);
                    record.ExcludeMask.Insert(marker.Priority, false);
                }
            }
            marker.Color = (marker.Color == default(Color)) ? GetFreeColor() : marker.Color;
            marker.Change += OnMarkerChange;
            OnChange(this, marker, CollectionChangeAction.MarkerAdded);
        }
        public void DeleteMarker(Marker marker, bool raisenEvent = true)
        {
            if (marker == null) throw new ArgumentNullException(nameof(marker));

            marker.Change -= OnMarkerChange;
            _markers.Remove(marker.Name);
            marker.SetCollection(null);

            RemoveBitMarkerMasks(marker.Priority);

            foreach (var mrk in _markers.Values)
                if (mrk.Priority > marker.Priority)
                    mrk.Priority--;

            if (raisenEvent) {
                OnChange(this, marker, CollectionChangeAction.MarkerDeleted);
                if (marker == GetSelectionMarker())
                    OnChange(this, CollectionChangeAction.SelectionMarkerChanged);
            }
        }
        public void DeleteAllMarkers()
        {
            var markers = _markers.Values.ToArray();
            foreach (var marker in markers)
                DeleteMarker(marker, false);
        }
        public Marker GetMarker(string name)
        {
            try {
                return _markers[name];
            }
            catch (KeyNotFoundException ex) {
                //Logger.LogDebug(ex);
                return null;
            }
        }
        public List<Marker> GetMarkers(string type)
        {
            var markers = new List<Marker>();
            foreach (var marker in _markers.Values) {
                if (marker.Type.Equals(type))
                    markers.Add(marker);
            }
            return markers;
        }
        public Marker GetDefaultMarker() => GetMarker(DefaultMarkerName);
        public bool ContainsMarker(string name)
        {
            return _markers.ContainsKey(name);
        }
        public Marker GetMarker(int index)
        {
            try {
                return _markers.Values[index];
            }
            catch (ArgumentOutOfRangeException ex) {
                //Logger.LogDebug(ex);
                return null;
            }
        }
        public void SetSelectionMarker(Marker marker)
        {
            if (ContainsMarker(marker.Name) == false)
                throw new Exception($"Unable to select the marker '{marker.Name}' : it is not present in the collection.");
            foreach (var mrk in _markers.Values)
                mrk.CurrentSelection = false;
            marker.CurrentSelection = true;
            if (marker.Priority != MarkersCount - 1)
                ChangeMarkerPriority(marker, MarkersCount - 1);
            OnChange(this, CollectionChangeAction.SelectionMarkerChanged);
        }
        public void ChangeMarkerPriority(Marker marker, int priority)
        {
            if (marker.Priority == priority) return;
            if (GetMarkers<Marker>().All(x => x.Priority != priority)) return;
            MoveMarkerMasks(marker.Priority, priority);

            if (priority > marker.Priority) {
                foreach (var mrk in _markers.Values) {
                    if (mrk.Priority > marker.Priority && mrk.Priority <= priority)
                        mrk.Priority = mrk.Priority - 1;
                }
            }
            else if (marker.Priority > priority) {
                foreach (var mrk in _markers.Values) {
                    if (mrk.Priority >= priority && mrk.Priority < marker.Priority)
                        mrk.Priority = mrk.Priority + 1;
                }
            }

            marker.Priority = priority;
            OnChange(this, CollectionChangeAction.MarkersAttributesChanged);
        }
        public void EvalAllMarkers()
        {
            for (var i = 0; i < MarkersCount; i++)
                _markers.Values[i].Eval();
        }
        public Marker GetFavouriteMarker(Record record)
        {
            Marker favouriteMarker = null;
            foreach (var marker in _markers.Values.ToArray())
                if ((favouriteMarker == null || marker.Priority > favouriteMarker.Priority) && marker.CheckRecord(record))
                    favouriteMarker = marker;
            return favouriteMarker;
        }
        public void ViewSelectionOnly(bool selectionOnly)
        {
            var selMarker = GetSelectionMarker();
            if (selMarker == null)
                return;
            for (var i = MarkersCount - 1; i >= 0; i--) {
                var mrk = _markers.Values[i];
                if (!selectionOnly)
                    mrk.Visible = true;
                else if (mrk == selMarker)
                    mrk.Visible = true;
                else
                    mrk.Visible = false;
            }
            OnChange(this, CollectionChangeAction.MarkersAttributesChanged);
        }
        public Marker[] GetMarkersSortedByPriority()
        {
            var markers = new Marker[_markers.Count];
            foreach (var marker in _markers.Values)
                markers[marker.Priority] = marker;
            return markers;
        }

        // ----- Override methods
        public override string ToString()
        {
            return Name ?? base.ToString();
        }

        // ----- Internal logics
        private void Init()
        {
            InitMarker();
            OnChange(this, CollectionChangeAction.CollectionCreated);
        }
        private void InitMarker()
        {
            var marker = new MarkerFull(DefaultMarkerName) {
                Color = Color.DodgerBlue
            };
            AddMarker(marker, MarkerPriority.Low);
            marker.Eval();
        }
        private void AddField(Field field)
        {
            _fields.Add(field.Name, field);
            foreach (var record in Records)
                record.AddField(field);
        }
        private Color GetFreeColor()
        {
            var usedColors = new HashSet<Color>();
            foreach (var marker in _markers.Values)
                usedColors.Add(marker.Color);
            foreach (var color in _markerForeColors)
                if (!usedColors.Contains(color))
                    return color;
            return GetRandomColor();
        }
        private Color GetRandomColor()
        {
            return Color.FromArgb(_random.Next(256), _random.Next(256), _random.Next(256));
        }
        private void MoveMarkerMasks(int fromIndex, int toIndex)
        {
            foreach (var record in Records) {
                foreach (var mask in new[] { record.EvalMask, record.IncludeMask, record.ExcludeMask }) {
                    var value = mask[fromIndex];
                    mask.RemoveAt(fromIndex);
                    mask.Insert(toIndex, value);
                }
            }
        }
        private void RemoveBitMarkerMasks(int index)
        {
            foreach (var record in Records)
                foreach (var mask in new[] { record.EvalMask, record.IncludeMask, record.ExcludeMask })
                    mask.RemoveAt(index);
        }
        private void OnMarkerChange(object sender, CollectionChangeEventArgs e)
        {
            OnChange(this, e.Marker, e.Action);
        }
    }
}
