﻿namespace Fortis.Common.Records
{
    public enum CollectionChangeAction
    {
        None,

        RecordModified,
        RecordAdded,
        RecordDeleted,
        CurrentRecordChanged,
        RecordLocationChanged,

        CollectionCleared,
        CollectionCreated,
        CollectionDeleted,
        CollectionAdded,

        MarkerCreated,
        MarkerAdded,
        MarkerDeleted,
        MarkerEvaluated,
        MarkerAttributeChanged,
        MarkersAttributesChanged,
        SelectionMarkerChanged
    };
}