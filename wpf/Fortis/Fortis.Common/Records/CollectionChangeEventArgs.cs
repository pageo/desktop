﻿using System;
using Fortis.Common.Markers;
using GMap.NET.WindowsPresentation;

namespace Fortis.Common.Records
{
    public class CollectionChangeEventArgs : EventArgs
    {
        // ----- Properties
        public Marker Marker { get; }
        public Record Record { get; }
        public string PropertyName { get; }
        public Collection Collection { get; }
        public CollectionChangeAction Action { get; }

        // ----- Public methods
        public CollectionChangeEventArgs(CollectionChangeAction action)
        {
            Action = action;
        }
        public CollectionChangeEventArgs(Collection collection, CollectionChangeAction action)
        {
            Collection = collection;
            Action = action;
        }
        public CollectionChangeEventArgs(Record record, string propertyName, CollectionChangeAction action)
        {
            Record = record;
            PropertyName = propertyName;
            Action = action;
        }
        public CollectionChangeEventArgs(Marker marker, CollectionChangeAction action)
        {
            Marker = marker;
            Action = action;
        }
        public CollectionChangeEventArgs(Marker marker, Record record, CollectionChangeAction action)
        {
            Marker = marker;
            Record = record;
            Action = action;
        }
    }
}
