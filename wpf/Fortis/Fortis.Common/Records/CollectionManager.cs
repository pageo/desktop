﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Fortis.Common.Records
{
    public class CollectionManager
    {
        // ----- Fields
        private readonly IList<Collection> _collections;

        // ----- Events

        public delegate void CollectionChangeEventHandler(object sender, CollectionChangeEventArgs e);
        public event CollectionChangeEventHandler CollectionChange;

        // ----- Constructors
        public CollectionManager()
        {
            _collections = new List<Collection>();
        }

        // ----- Public methods
        public void CheckCollectionName(ref string Name)
        {
            if (string.IsNullOrEmpty(Name))
                Name = "Collection";
            var colName = Name;
            var n = 1;
            while (GetCollection(colName) != null)
                colName = Name + (++n);
            Name = colName;
        }
        public void AddCollection(Collection collection)
        {
            if (collection == null)
                return;
            if (GetCollection(collection.Name) != null)
                throw (new Exception($"A collection named {collection.Name} already exists!"));
            _collections.Add(collection);
            collection.Change += OnCollectionChange;
            RaiseCollectionChange(collection, new CollectionChangeEventArgs(CollectionChangeAction.CollectionAdded));
        }
        public void DeleteCollection(Collection collection)
        {
            _collections.Remove(collection);
            collection.Clear();
            RaiseCollectionChange(collection, new CollectionChangeEventArgs(CollectionChangeAction.CollectionDeleted));
            collection.Change -= OnCollectionChange;
        }
        public void DeleteAllCollections()
        {
            var collections = _collections.ToArray();
            foreach (var collection in collections)
                DeleteCollection(collection);
        }
        public Collection GetCollection(string name)
        {
            return string.IsNullOrEmpty(name) ? null : _collections.FirstOrDefault(collection => name.Equals(collection.Name, StringComparison.InvariantCultureIgnoreCase));
        }
        public IList<Collection> GetCollections(string type = null)
        {
            return _collections.Where(collection => string.IsNullOrEmpty(type) || collection.Type.Equals(type)).ToList();
        }

        // ----- Internal logics
        protected virtual void RaiseCollectionChange(object sender, CollectionChangeEventArgs e)
        {
            CollectionChange?.Invoke(sender, e);
        }
        private void OnCollectionChange(object sender, CollectionChangeEventArgs e)
        {
            RaiseCollectionChange(sender, e);
        }
    }
}