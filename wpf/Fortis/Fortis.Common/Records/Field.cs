﻿using System;
using System.Collections.Generic;
using Fortis.Common.Collections.Expressions;

namespace Fortis.Common.Records
{
    public class Field
    {
        // ----- Properties
        public int Index { get; }
        public string Name { get; }
        public Type Type { get; }

        // ----- Constructors
        public Field(int index, string name, Type type)
        {
            Index = index;
            Name = name;
            Type = type;
        }

        // ----- Public methods
        public IEnumerable<string> GetAvailableConditionalOperators()
        {
            var operators = new List<string> {ConditionalOperators.Equals, ConditionalOperators.Different};
            if (Type == typeof(string)) {
                operators.Add(ConditionalOperators.Contains);
                operators.Add(ConditionalOperators.NotContains);
                operators.Add(ConditionalOperators.StartsWith);
                operators.Add(ConditionalOperators.EndsWith);
            }
            else if (IsComparable() && NotExcluded()) {
                operators.Add(ConditionalOperators.GreaterThan);
                operators.Add(ConditionalOperators.GreaterOrEqualThan);
                operators.Add(ConditionalOperators.LowerThan);
                operators.Add(ConditionalOperators.LowerOrEqualThan);
            }
            return operators;
        }
        private bool NotExcluded()
        {
            var underlyingType = Nullable.GetUnderlyingType(Type);
            return (underlyingType ?? Type) != typeof(bool);
        }
        private bool IsComparable()
        {
            var underlyingType = Nullable.GetUnderlyingType(Type);
            if (underlyingType != null)
                return underlyingType.GetInterface(typeof(IComparable).Name) != null;
            return Type.GetInterface(typeof(IComparable).Name) != null;
        }
    }
}