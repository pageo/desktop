﻿using System;

namespace Fortis.Common.Records
{
    public class FieldNotFoundException : Exception
    {
        // ----- Constructors
        public FieldNotFoundException(string fieldName) : base(BuildMessage(fieldName))
        {
        }

        // ----- Public methods
        private static string BuildMessage(string fieldName)
        {
            return $"Le champ '{fieldName}' n'existe pas.";
        }
    }
}