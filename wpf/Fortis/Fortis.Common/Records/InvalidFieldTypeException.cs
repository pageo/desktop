﻿using System;

namespace Fortis.Common.Records
{
    public class InvalidFieldTypeException : Exception
    {
        // ----- Constructors
        public InvalidFieldTypeException(string fieldName, Type fieldType, object value) : base(BuildMessage(fieldName, fieldType, value))
        {
        }

        // ----- Public methods
        private static string BuildMessage(string fieldName, Type expectedFieldType, object value)
        {
            return $"Impossible de mettre la valeur '{value}' de type '{value.GetType()}' dans le champ '{fieldName}' de type '{expectedFieldType}'.";
        }
    }
}