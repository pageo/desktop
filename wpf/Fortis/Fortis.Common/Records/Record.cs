﻿using System;
using System.Collections.Generic;
using Fortis.Common.Markers;

namespace Fortis.Common.Records
{
    public class Record
    {
        // ----- Fields
        private readonly List<object> _values = new List<object>();
        internal List<bool> EvalMask { get; private set; } = new List<bool>();
        internal List<bool> ExcludeMask { get; private set; } = new List<bool>();
        internal List<bool> IncludeMask { get; private set; } = new List<bool>();

        // ----- Properties
        public RecordState State { get; private set; }
        public IList<RecordChange> Changes { get; set; } = new List<RecordChange>();
        public Collection Collection { get; }

        // ----- Public methods
        public Record(Collection collection)
        {
            Collection = collection ?? throw new ArgumentNullException(nameof(collection));

            foreach (var field in Collection.GetFields())
                AddField(field);

            foreach (var marker in Collection.GetMarkers<Marker>()) {
                EvalMask.Add(false);
                ExcludeMask.Add(false);
                IncludeMask.Add(false);
            }
        }
        public object GetValue(string fieldName)
        {
            var field = Collection.GetField(fieldName);
            if (field == null)
                throw new FieldNotFoundException(fieldName);
            return _values.Count > field.Index ? _values[field.Index] : null;
        }
        public T GetValue<T>(string fieldName)
        {
            var value = GetValue(fieldName);
            if (value != null && value is T == false)
                throw new InvalidFieldTypeException(fieldName, typeof(T), value);
            if (value == null && default(T) != null)
                throw new Exception($"null n'est pas une valeur affectable à {typeof(T)}.");
            return (T)value;
        }
        public void SetValue(string fieldName, object newValue, bool raiseEvent = false)
        {
            var field = Collection.GetField(fieldName);
            if (field == null)
                throw new FieldNotFoundException(fieldName);
            if (newValue != null && field.Type.IsInstanceOfType(newValue) == false)
                throw new InvalidFieldTypeException(fieldName, field.Type, newValue);
            if (_values.Count <= field.Index)
                throw new IndexOutOfRangeException("bad field");

            var oldValue = _values[field.Index];
            if (oldValue == newValue) return;
            _values[field.Index] = newValue;

            Changes.Add(new RecordChange(fieldName, oldValue, newValue));
            SetRecordAsModified();
            Collection.SetCollectionAsModified();

            if (raiseEvent)
                Collection.OnChange(this, this, fieldName, CollectionChangeAction.RecordModified);
        }
        public void SetValues(IEnumerable<RecordValue> values)
        {
            foreach (var value in values)
                SetValue(value.PropertyName, value.Value);
        }
        public void Delete()
        {
            State = RecordState.Deleted;
        }
        public void SetAdded()
        {
            State = RecordState.Added;
        }
        public void AcceptChanges()
        {
            if (State == RecordState.Deleted)
                return;

            Changes.Clear();
            State = RecordState.Unchanged;
        }
        public void AddField(Field field)
        {
            if (field == null) throw new ArgumentNullException(nameof(field));
            _values.Insert(field.Index, field.Type.GetDefaultValue());
        }
        public void RemoveField(Field field)
        {
            if (field == null) throw new ArgumentNullException(nameof(field));
            _values.RemoveAt(field.Index);
        }
        public void SetCurrent()
        {
            Collection.CurrentRecord = this;
        }

        // ----- Internal logics
        private void SetRecordAsModified()
        {
            if (State == RecordState.Unchanged)
                State = RecordState.Modified;
        }
    }
}