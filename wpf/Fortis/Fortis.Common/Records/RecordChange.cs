﻿namespace Fortis.Common.Records
{
    public class RecordChange
    {
        // ----- Fields
        public string FieldName { get; }
        public object OldValue { get; }
        public object NewValue { get; }

        // ----- Constructors
        public RecordChange(string fieldName, object oldValue, object newValue)
        {
            FieldName = fieldName;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}