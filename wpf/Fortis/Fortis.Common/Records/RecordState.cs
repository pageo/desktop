﻿namespace Fortis.Common.Records
{
    public enum RecordState
    {
        Unchanged = 0,
        Added = 1,
        Modified = 2,
        Deleted = 3
    }
}