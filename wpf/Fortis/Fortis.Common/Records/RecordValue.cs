﻿namespace Fortis.Common.Records
{
    public class RecordValue
    {
        // ----- Properties
        public string PropertyName { get; }
        public object Value { get; }

        // ----- Constructors
        public RecordValue(string propertyName, object value)
        {
            PropertyName = propertyName;
            Value = value;
        }
    }
}
