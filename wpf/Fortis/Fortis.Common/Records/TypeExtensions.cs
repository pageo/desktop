﻿using System;

namespace Fortis.Common.Records
{
    public static class TypeExtensions
    {
        // ----- Public methods
        public static bool IsReferenceType(this Type type)
        {
            return !type.IsValueType || Nullable.GetUnderlyingType(type) != null;
        }
        public static object GetDefaultValue(this Type type)
        {
            return type.IsReferenceType() == false ? Activator.CreateInstance(type) : null;
        }
        public static Type ToNullable(this Type sourceType)
        {
            if (sourceType == null)
                throw new ArgumentNullException(nameof(sourceType));
            if (sourceType == typeof(void))
                throw new Exception("Void n'est pas un type nullable !");
            return IsReferenceType(sourceType) ? sourceType : typeof(Nullable<>).MakeGenericType(sourceType);
        }
    }
}