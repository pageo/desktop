﻿using System;
using System.Linq;

namespace Fortis.Common.Utils
{
    public static class ArrayExtensions
    {
        // ----- Public methods
        public static Array ChangeType(this object[] values, Type type)
        {
            var array = Array.CreateInstance(type, values.Length);
            for (var index = 0; index < values.Length; index++)
                array.SetValue(values.ElementAt(index), index);
            return array;
        }
    }
}
