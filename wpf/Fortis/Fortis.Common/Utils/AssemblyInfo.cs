﻿using System.Linq;
using System.Reflection;

namespace Fortis.Common.Utils
{
    public class AssemblyInfo
    {
        // ----- Public methods
        public static string GetCompanyName()
        {
            var attribute = GetAssemblyAttributeOfExecutingAssembly<AssemblyCompanyAttribute>();
            return attribute?.Company ?? "Fortis";
        }
        public static string GetApplicationName()
        {
            var attribute = GetAssemblyAttributeOfExecutingAssembly<AssemblyProductAttribute>();
            return attribute?.Product;
        }
        private static T GetAssemblyAttributeOfExecutingAssembly<T>() where T : class
        {
            var assembly = Assembly.GetEntryAssembly();
            return assembly?.GetCustomAttributes(typeof(T), true).Select(x => (T)x).FirstOrDefault();
        }
    }
}
