﻿using System;
using System.Collections;
using System.Linq;
using Fortis.Common.Collections.Expressions;
using Fortis.Common.Records;

namespace Fortis.Common.Utils
{
    public static class ObjectExtensions
    {
        // ----- Public methods
        public static string ToNormalizedString(this object value)
        {
            if (value == null)
                return "null";
            if (value is string)
                return $"\"{value}\"";
            if (value is IEnumerable) {
                var result = ((IEnumerable)value).Cast<object>();
                return string.Join(Separators.ValueSeparator, result.Select(x => x.ToNormalizedString()));
            }

            if (value is DateTime) {
                var dateValue = ((DateTime)value);
                if (dateValue == dateValue.Date)
                    return $"\"{dateValue:d}\"";
                return $"\"{dateValue}\"";
            }
            return value.ToString();
        }
        public static object To(this object value, Type conversionType)
        {
            var u = Nullable.GetUnderlyingType(conversionType);
            if (u != null)
                return value == null ? conversionType.GetDefaultValue() : Convert.ChangeType(value, u);
            return Convert.ChangeType(value, conversionType);
        }
    }
}
