﻿namespace Fortis.Common.Utils
{
    public static class StringExtensions
    {
        // ----- Public methods
        public static string SurroundWith(this string text, string start, string end = null)
        {
            end = end ?? start;
            return start + text + end;
        }
        public static bool IsSurroundedBy(this string value, string start, string end)
        {
            return value != null && value.Length >= 2 && value.StartsWith(start) && value.EndsWith(end);
        }
        public static bool IsSurroundedBy(this string value, string separator)
        {
            return value.IsSurroundedBy(separator, separator);
        }
    }
}
