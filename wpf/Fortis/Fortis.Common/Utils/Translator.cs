﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using NLog;

namespace Fortis.Common.Utils
{
    public class Translator
    {
        // ----- Fields
        private static Translator _instance;
        private static readonly object Lock = new object();
        private readonly Hashtable _table = new Hashtable();
        private string _dictionary;
        private bool _modified;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // ----- Constructors
        private Translator()
        {
            try {
                ReadConf();
                LoadDictionary();
            }
            catch (Exception e) {
                Logger.Error(e);
            }
        }

        // ----- Destructors
        ~Translator()
        {
            try {
                if (_modified)
                    SaveDictionary();
            }
            catch (Exception e) {
                Logger.Error(e);
            }
        }

        // ----- Internal logics
        private void ReadConf()
        {
            var dictionary = Properties.Settings.Default.Dictionary;
            if (!Path.IsPathRooted(dictionary))
                dictionary = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\" + dictionary;
            if (File.Exists(dictionary))
                _dictionary = dictionary;
        }
        private void LoadDictionary()
        {
            XmlReader reader = null;
            try {
                if (string.IsNullOrEmpty(_dictionary)) return;
                var settings = new XmlReaderSettings {
                    IgnoreWhitespace = true,
                    IgnoreComments = true
                };
                reader = XmlReader.Create(_dictionary, settings);
                reader.MoveToContent();
                while (reader.Read()) {
                    switch (reader.NodeType) {
                        case XmlNodeType.Element:
                            if (reader.Name.ToLower() == "string") {
                                var key = reader.GetAttribute("key");
                                var val = reader.GetAttribute("value");
                                if ((key != null) && (val != null))
                                    _table.Add(key, val);
                            }
                            break;
                    }
                }
            }
            catch (Exception e) {
                Logger.Error(e);
            }
            finally {
                reader?.Close();
            }
        }
        private void SaveDictionary()
        {
            try {
                if (string.IsNullOrEmpty(_dictionary)) return;
                var settings = new XmlWriterSettings {
                    Indent = true,
                    Encoding = Encoding.UTF8
                };
                using (var writer = XmlWriter.Create(_dictionary, settings)) {
                    writer.WriteStartElement("translation");
                    foreach (DictionaryEntry de in _table) {
                        writer.WriteStartElement("string");
                        writer.WriteAttributeString("key", de.Key.ToString());
                        writer.WriteAttributeString("value", de.Value.ToString());
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
            }
            catch (Exception e) {
                Logger.Error(e);
            }
        }
        private static Translator GetInstance()
        {
            lock (Lock) {
                return _instance ?? (_instance = new Translator());
            }
        }
        public static string Translate(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            var translation = str;
            try {
                var translator = GetInstance();
                lock (Lock) {
                    if (!translator._table.ContainsKey(str)) {
                        translator._table.Add(str, str);
                        translator._modified = true;
                    }
                    else
                        translation = translator._table[str].ToString();
                }
            }
            catch (Exception e) {
                Logger.Error(e);
            }
            return translation;
        }
    }
}
