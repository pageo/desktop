﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Windows;
using Fortis.Core.Modules.Shell.Views;
using Prism.Mef;

namespace Fortis.Core
{
    public class AppBootstrapper : MefBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Container.GetExportedValue<ShellView>();
        }

        protected override void ConfigureAggregateCatalog()
        {
            base.ConfigureAggregateCatalog();

            AggregateCatalog.Catalogs.Add(
                new AssemblyCatalog(typeof(AppBootstrapper).Assembly));

            AggregateCatalog.Catalogs.Add(
                new DirectoryCatalog(AppDomain.CurrentDomain.BaseDirectory));
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow.Show();
        }
    }
}
