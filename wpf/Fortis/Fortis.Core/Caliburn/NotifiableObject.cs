﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Prism.Mvvm;

namespace Fortis.Core.Caliburn
{
    public class NotifiableObject : BindableBase
    {
        // ----- Fields
        private readonly IDictionary<string, object> _notifiableProperties = new Dictionary<string, object>();

        // ----- Protected methods
        protected T GetNotifiableProperty<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException(nameof(propertyName));

            object propertyValue;
            if (_notifiableProperties.TryGetValue(propertyName, out propertyValue))
                return (T)propertyValue;
            return default(T);
        }
        protected bool SetNotifiableProperty<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException(nameof(propertyName));

            var oldValue = GetNotifiableProperty<T>(propertyName);
            if (Equals(oldValue, newValue))
                return false;

            _notifiableProperties[propertyName] = newValue;
            RaisePropertyChanged(propertyName);
            return true;
        }
    }
}
