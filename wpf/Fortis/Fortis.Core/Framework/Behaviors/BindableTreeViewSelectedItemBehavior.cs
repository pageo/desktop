﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace Fortis.Core.Framework.Behaviors
{
    public class BindableTreeViewSelectedItemBehavior : Behavior<TreeView>
    {
        // ----- Properties
        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }
        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            "SelectedItem", typeof(object), typeof(BindableTreeViewSelectedItemBehavior), 
            new UIPropertyMetadata(null, OnSelectedItemChanged));

        private static void OnSelectedItemChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Action<TreeViewItem> selectTreeViewItem = tvi2 =>
            {
                if (tvi2 != null) {
                    tvi2.IsSelected = true;
                    tvi2.Focus();
                }
            };

            var tvi = e.NewValue as TreeViewItem;

            if (tvi == null) {
                var tree = ((BindableTreeViewSelectedItemBehavior) sender).AssociatedObject;
                if (tree == null)
                    return;

                if (!tree.IsLoaded) {
                    RoutedEventHandler handler = null;
                    handler = (sender2, e2) => {
                        tvi = GetTreeViewItem(tree, e.NewValue);
                        selectTreeViewItem(tvi);
                        tree.Loaded -= handler;
                    };
                    tree.Loaded += handler;
                    
                    return;
                }
                tvi = GetTreeViewItem(tree, e.NewValue);
            }

            selectTreeViewItem(tvi);
        }

        // ----- Internal methods
        private void OnTreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            SelectedItem = e.NewValue;
        }
        private static TreeViewItem GetTreeViewItem(ItemsControl container, object item)
        {
            if (container != null) {
                if (container.DataContext == item)
                    return container as TreeViewItem;

                if (container is TreeViewItem && !((TreeViewItem) container).IsExpanded)
                    container.SetValue(TreeViewItem.IsExpandedProperty, true);

                container.ApplyTemplate();
                var itemsPresenter =
                    (ItemsPresenter) container.Template.FindName("ItemsHost", container);
                if (itemsPresenter != null)
                    itemsPresenter.ApplyTemplate();
                else {
                    itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    if (itemsPresenter == null)
                        container.UpdateLayout();
                }

                for (int i = 0, count = container.Items.Count; i < count; i++) {
                    var subContainer = (TreeViewItem) container.ItemContainerGenerator.
                                                          ContainerFromIndex(i);
                    if (subContainer == null)
                        continue;

                    subContainer.BringIntoView();

                    var resultContainer = GetTreeViewItem(subContainer, item);
                    if (resultContainer != null)
                        return resultContainer;
                }
            }
            return null;
        }
        private static T FindVisualChild<T>(Visual visual) where T : Visual
        {
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(visual); i++) {
                var child = (Visual) VisualTreeHelper.GetChild(visual, i);
                if (child != null) {
                    var correctlyTyped = child as T;
                    if (correctlyTyped != null)
                        return correctlyTyped;

                    T descendent = FindVisualChild<T>(child);
                    if (descendent != null)
                        return descendent;
                }
            }
            return null;
        }

        // ----- Protected methods
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.SelectedItemChanged += OnTreeViewSelectedItemChanged;
        }
        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (AssociatedObject != null)
            {
                AssociatedObject.SelectedItemChanged -= OnTreeViewSelectedItemChanged;
            }
        }
    }
}