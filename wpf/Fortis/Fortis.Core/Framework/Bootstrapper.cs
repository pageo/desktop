﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.ReflectionModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using Caliburn.Micro;
using Gu.Localization;

namespace Fortis.Core.Framework
{
    public class Bootstrapper<TRootModel> : BootstrapperBase
    {
        // ----- Fields
        protected CompositionContainer CompositionContainer;

        // ----- Properties
        public IList<Assembly> PriorityAssemblies => SelectAssemblies().ToList();

        // ----- Constructors
        public Bootstrapper()
        {
            PreInitialize();
            Initialize();
        }

        // ----- Protected methods
        protected override void Configure()
        {
            var directoryCatalog = new DirectoryCatalog(@"./");
            AssemblySource.Instance.AddRange(
                directoryCatalog.Parts
                    .Select(part => ReflectionModelServices.GetPartType(part).Value.Assembly)
                    .Where(assembly => !AssemblySource.Instance.Contains(assembly)));
            var priorityCatalog = new AggregateCatalog(PriorityAssemblies.Select(x => new AssemblyCatalog(x)));
            var priorityProvider = new CatalogExportProvider(priorityCatalog);
            var mainCatalog = new AggregateCatalog(
                AssemblySource.Instance
                    .Where(assembly => !PriorityAssemblies.Contains(assembly))
                    .Select(x => new AssemblyCatalog(x)));
            var mainProvider = new CatalogExportProvider(mainCatalog);

            CompositionContainer = new CompositionContainer(priorityProvider, mainProvider);
            priorityProvider.SourceProvider = CompositionContainer;
            mainProvider.SourceProvider = CompositionContainer;

            var batch = new CompositionBatch();

            BindServices(batch);
            batch.AddExportedValue(mainCatalog);

            CompositionContainer.Compose(batch);
        }
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            base.OnStartup(sender, e);
            DisplayRootViewFor<TRootModel>();
        }
        protected virtual void BindServices(CompositionBatch batch)
        {
            batch.AddExportedValue<IWindowManager>(new WindowManager());
            batch.AddExportedValue<IEventAggregator>(new EventAggregator());
            batch.AddExportedValue(CompositionContainer);
        }
        protected override object GetInstance(Type serviceType, string key)
        {
            var contract = string.IsNullOrEmpty(key) ? AttributedModelServices.GetContractName(serviceType) : key;
            var exports = CompositionContainer.GetExports<object>(contract).ToList();
            if (exports.Any())
                return exports.First().Value;
            throw new Exception($"Could not locate any instances of contract {contract}.");
        }
        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return CompositionContainer.GetExportedValues<object>(AttributedModelServices.GetContractName(serviceType));
        }
        protected override void BuildUp(object instance)
        {
            CompositionContainer.SatisfyImportsOnce(instance);
        }
        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            return new[] { Assembly.GetEntryAssembly() };
        }

        // ----- Internal logics
        private static void PreInitialize()
        {
            var code = Properties.Settings.Default.LanguageCode;
            if (!string.IsNullOrWhiteSpace(code)) {
                var culture = CultureInfo.GetCultureInfo(code);
                Translator.Culture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }
    }
}
