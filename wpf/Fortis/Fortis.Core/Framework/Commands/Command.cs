﻿using System;
using Fortis.Core.Caliburn;

namespace Fortis.Core.Framework.Commands
{
    public class Command : NotifiableObject
    {
        // ----- Properties
        public CommandDefinitionBase CommandDefinition { get; }
        public bool Visible
        {
            get { return GetNotifiableProperty<bool>(); }
            set { SetNotifiableProperty(value); }
        }
        public bool Enabled
        {
            get { return GetNotifiableProperty<bool>(); }
            set { SetNotifiableProperty(value); }
        }
        public bool Checked
        {
            get { return GetNotifiableProperty<bool>(); }
            set { SetNotifiableProperty(value); }
        }
        public string Text
        {
            get { return GetNotifiableProperty<string>(); }
            set { SetNotifiableProperty(value); }
        }
        public string ToolTip
        {
            get { return GetNotifiableProperty<string>(); }
            set { SetNotifiableProperty(value); }
        }
        public Uri IconSource
        {
            get { return GetNotifiableProperty<Uri>(); }
            set { SetNotifiableProperty(value); }
        }
        public object Tag { get; set; }

        // ----- Constructors
        public Command(CommandDefinitionBase commandDefinition)
        {
            CommandDefinition = commandDefinition;
            Text = commandDefinition.Text;
            ToolTip = commandDefinition.ToolTip;
            IconSource = commandDefinition.IconSource;

            Visible = true;
            Enabled = true;
        }
    }
}