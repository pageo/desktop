﻿using System;

namespace Fortis.Core.Framework.Commands
{
    public abstract class CommandDefinition : CommandDefinitionBase
    {
        // ----- Properties
        public override Uri IconSource => null;
        public sealed override bool IsList => false;
    }
}