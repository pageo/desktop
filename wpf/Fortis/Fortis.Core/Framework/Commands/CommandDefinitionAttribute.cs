﻿using System;
using System.ComponentModel.Composition;

namespace Fortis.Core.Framework.Commands
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class)]
    public class CommandDefinitionAttribute : ExportAttribute
    {
        // ----- Constructors
        public CommandDefinitionAttribute()
            : base(typeof(CommandDefinitionBase))
        {
            
        }
    }
}