﻿using System;

namespace Fortis.Core.Framework.Commands
{
    public abstract class CommandDefinitionBase
    {
        // ----- Properties
        public abstract string Name { get; }
        public abstract string Text { get; }
        public abstract string ToolTip { get; }
        public abstract Uri IconSource { get; }
        public abstract bool IsList { get; }
    }
}