﻿using System;
using System.Windows.Input;
using Caliburn.Micro;

namespace Fortis.Core.Framework.Commands
{
    public abstract class CommandKeyboardShortcut
    {
        // ----- Fields
        private readonly Func<CommandDefinitionBase> _commandDefinition;

        // ----- Properties
        public CommandDefinitionBase CommandDefinition => _commandDefinition();
        public KeyGesture KeyGesture { get; }
        public int SortOrder { get; }

        // ----- Constructors
        protected CommandKeyboardShortcut(KeyGesture keyGesture, int sortOrder, Func<CommandDefinitionBase> commandDefinition)
        {
            _commandDefinition = commandDefinition;
            KeyGesture = keyGesture;
            SortOrder = sortOrder;
        }
    }

    public class CommandKeyboardShortcut<TCommandDefinition> : CommandKeyboardShortcut
        where TCommandDefinition : CommandDefinition
    {
        // ----- Constructors
        public CommandKeyboardShortcut(KeyGesture keyGesture, int sortOrder = 5)
            : base(keyGesture, sortOrder, () => IoC.Get<ICommandService>().GetCommandDefinition(typeof(TCommandDefinition)))
        {
            
        }
    }
}