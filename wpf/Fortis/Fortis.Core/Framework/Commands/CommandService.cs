﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace Fortis.Core.Framework.Commands
{
    [Export(typeof(ICommandService))]
    public class CommandService : ICommandService
    {
        // ----- Fields
        private readonly IDictionary<Type, CommandDefinitionBase> _commandDefinitionsLookup;
        private readonly IDictionary<CommandDefinitionBase, Command> _commands;
        private readonly IDictionary<Command, TargetableCommand> _targetableCommands;

        // ----- Imports
#pragma warning disable 649
        [ImportMany]
        private IEnumerable<CommandDefinitionBase> _commandDefinitions;
#pragma warning restore 649

        // ----- Constructors
        public CommandService()
        {
            _commandDefinitionsLookup = new Dictionary<Type, CommandDefinitionBase>();
            _commands = new Dictionary<CommandDefinitionBase, Command>();
            _targetableCommands = new Dictionary<Command, TargetableCommand>();
        }

        // ----- Public methods
        public CommandDefinitionBase GetCommandDefinition(Type commandDefinitionType)
        {
            CommandDefinitionBase commandDefinition;
            if (!_commandDefinitionsLookup.TryGetValue(commandDefinitionType, out commandDefinition))
                commandDefinition = _commandDefinitionsLookup[commandDefinitionType] =
                    _commandDefinitions.First(x => x.GetType() == commandDefinitionType);
            return commandDefinition;
        }
        public Command GetCommand(CommandDefinitionBase commandDefinition)
        {
            Command command;
            if (!_commands.TryGetValue(commandDefinition, out command))
                command = _commands[commandDefinition] = new Command(commandDefinition);
            return command;
        }
        public TargetableCommand GetTargetableCommand(Command command)
        {
            TargetableCommand targetableCommand;
            if (!_targetableCommands.TryGetValue(command, out targetableCommand))
                targetableCommand = _targetableCommands[command] = new TargetableCommand(command);
            return targetableCommand;
        }
    }
}