﻿namespace Fortis.Core.Framework.Commands
{
    public class ExcludeCommandKeyboardShortcut
    {
        // ----- Properties
        public CommandKeyboardShortcut KeyboardShortcut { get; }

        // ----- Constructors
        public ExcludeCommandKeyboardShortcut(CommandKeyboardShortcut keyboardShortcut)
        {
            KeyboardShortcut = keyboardShortcut;
        }
    }
}