﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fortis.Core.Framework.Commands
{
    public interface ICommandHandler<TCommandDefinition> : ICommandHandler
        where TCommandDefinition : CommandDefinition
    {
        void Update(Command command);
        Task Run(Command command);
    }

    public interface ICommandListHandler<TCommandDefinition> : ICommandHandler
        where TCommandDefinition : CommandListDefinition
    {
        void Populate(Command command, List<Command> commands);
        Task Run(Command command);
    }

    public interface ICommandHandler
    {
        
    }

    public interface ICommandListHandler : ICommandHandler
    {
        
    }

    public abstract class CommandHandlerBase<TCommandDefinition> : ICommandHandler<TCommandDefinition>
        where TCommandDefinition : CommandDefinition
    {
        // ----- Public methods
        public virtual void Update(Command command)
        {
            
        }

        // ----- Abstract methods
        public abstract Task Run(Command command);
    }
}