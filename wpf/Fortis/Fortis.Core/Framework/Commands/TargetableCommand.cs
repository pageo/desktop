﻿using System;
using System.Windows.Input;
using Caliburn.Micro;

namespace Fortis.Core.Framework.Commands
{
    public class TargetableCommand : ICommand
    {
        // ----- Fields
        private readonly Command _command;
        private readonly ICommandRouter _commandRouter;

        // ----- Constructors
        public TargetableCommand(Command command)
        {
            _command = command;
            _commandRouter = IoC.Get<ICommandRouter>();
        }

        // ----- Public methods
        public bool CanExecute(object parameter)
        {
            var commandHandler = _commandRouter.GetCommandHandler(_command.CommandDefinition);
            if (commandHandler == null)
                return false;
            commandHandler.Update(_command);
            return _command.Enabled;
        }
        public async void Execute(object parameter)
        {
            var commandHandler = _commandRouter.GetCommandHandler(_command.CommandDefinition);
            if (commandHandler == null)
                return;

            await commandHandler.Run(_command);
        }
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}