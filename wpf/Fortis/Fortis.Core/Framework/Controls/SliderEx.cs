﻿using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Fortis.Core.Framework.Controls
{
    public class SliderEx : Slider
    {
        // ----- Events
        [Category("Behavior")]
        public event DragStartedEventHandler ThumbDragStarted;
        [Category("Behavior")]
        public event DragCompletedEventHandler ThumbDragCompleted;

        // ----- Override methods
        protected override void OnThumbDragStarted(DragStartedEventArgs e)
        {
            ThumbDragStarted?.Invoke(this, e);

            base.OnThumbDragStarted(e);
        }
        protected override void OnThumbDragCompleted(DragCompletedEventArgs e)
        {
            ThumbDragCompleted?.Invoke(this, e);

            base.OnThumbDragCompleted(e);
        }
    }
}
