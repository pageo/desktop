﻿using System.Windows.Data;
using Xceed.Wpf.AvalonDock.Converters;

namespace Fortis.Core.Framework
{
    public static class Converters
    {
        // ----- Fields
        public static readonly IValueConverter NullToDoNothingConverter = new NullToDoNothingConverter();
    }
}