using Fortis.Core.Modules.UndoRedo;

namespace Fortis.Core.Framework
{
	public interface IDocument : ILayoutItem
	{
        IUndoRedoManager UndoRedoManager { get; }
	}
}