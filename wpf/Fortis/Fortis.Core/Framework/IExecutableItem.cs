﻿namespace Fortis.Core.Framework
{
    internal interface IExecutableItem
    {
        void RaiseCanExecuteChanged();
    }
}