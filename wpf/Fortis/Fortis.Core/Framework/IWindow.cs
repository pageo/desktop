using Caliburn.Micro;

namespace Fortis.Core.Framework
{
    public interface IWindow : IActivate, IDeactivate, INotifyPropertyChangedEx
    {
        
    }
}