﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Input;
using Fortis.Core.Caliburn;

namespace Fortis.Core.Framework
{
	public abstract class LayoutItemBase : NotifiableObject, ILayoutItem
	{
        // ----- Commands
        public abstract ICommand CloseCommand { get; }

        [Browsable(false)]
		public Guid Id { get; } = Guid.NewGuid();
	    [Browsable(false)]
		public string ContentId => Id.ToString();
	    [Browsable(false)]
		public virtual Uri IconSource => null;
        [Browsable(false)]
		public bool IsSelected
		{
			get { return GetNotifiableProperty<bool>(); }
			set { SetNotifiableProperty(value); }
		}
        [Browsable(false)]
        public virtual bool ShouldReopenOnStart => false;
	    public virtual void LoadState(BinaryReader reader)
		{
		}
		public virtual void SaveState(BinaryWriter writer)
		{
		}
	}
}