﻿using System;
using System.Windows.Input;
using Caliburn.Micro;
using Fortis.Core.Framework.Commands;

namespace Fortis.Core.Framework.Menus
{
    public class CommandMenuItemDefinition<TCommandDefinition> : MenuItemDefinition
        where TCommandDefinition : CommandDefinitionBase
    {
        // ----- Fields
        private readonly CommandDefinitionBase _commandDefinition;

        // ----- Properties
        public override string Text => _commandDefinition.Text;
        public override Uri IconSource => _commandDefinition.IconSource;
        public override KeyGesture KeyGesture { get; }
        public override CommandDefinitionBase CommandDefinition => _commandDefinition;

        // ----- Constructors
        public CommandMenuItemDefinition(MenuItemGroupDefinition group, int sortOrder)
            : base(group, sortOrder)
        {
            _commandDefinition = IoC.Get<ICommandService>().GetCommandDefinition(typeof(TCommandDefinition));
            KeyGesture = IoC.Get<ICommandKeyGestureService>().GetPrimaryKeyGesture(_commandDefinition);
        }
    }
}