﻿namespace Fortis.Core.Framework.Menus
{
    public class ExcludeMenuDefinition
    {
        // ----- Properties
        public MenuDefinition MenuDefinitionToExclude { get; }

        // ----- Constructors
        public ExcludeMenuDefinition(MenuDefinition menuDefinition)
        {
            MenuDefinitionToExclude = menuDefinition;
        }
    }
}
