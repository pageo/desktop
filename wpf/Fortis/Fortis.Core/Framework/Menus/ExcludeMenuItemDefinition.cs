﻿namespace Fortis.Core.Framework.Menus
{
    public class ExcludeMenuItemDefinition
    {
        // ----- Properties
        public MenuItemDefinition MenuItemDefinitionToExclude { get; }

        // ----- Constructors
        public ExcludeMenuItemDefinition(MenuItemDefinition menuItemDefinition)
        {
            MenuItemDefinitionToExclude = menuItemDefinition;
        }
    }
}
