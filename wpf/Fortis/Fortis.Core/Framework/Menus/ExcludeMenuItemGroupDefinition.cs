﻿namespace Fortis.Core.Framework.Menus
{
    public class ExcludeMenuItemGroupDefinition
    {
        // ----- Properties
        public MenuItemGroupDefinition MenuItemGroupDefinitionToExclude { get; }

        // ----- Constructors
        public ExcludeMenuItemGroupDefinition(MenuItemGroupDefinition menuItemGroupDefinition)
        {
            MenuItemGroupDefinitionToExclude = menuItemGroupDefinition;
        }
    }
}
