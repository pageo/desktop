﻿using System;
using System.Windows.Input;
using Fortis.Core.Framework.Commands;

namespace Fortis.Core.Framework.Menus
{
    public class MenuDefinition : MenuDefinitionBase
    {
        // ----- Properties
        public MenuBarDefinition MenuBar { get; }
        public override int SortOrder { get; }
        public override string Text { get; }
        public override Uri IconSource => null;
        public override KeyGesture KeyGesture => null;
        public override CommandDefinitionBase CommandDefinition => null;

        // ----- Constructors
        public MenuDefinition(MenuBarDefinition menuBar, int sortOrder, string text)
        {
            MenuBar = menuBar;
            SortOrder = sortOrder;
            Text = text;
        }
    }
}