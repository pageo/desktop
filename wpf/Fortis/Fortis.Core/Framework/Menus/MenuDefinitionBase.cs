﻿using System;
using System.Windows.Input;
using Fortis.Core.Framework.Commands;

namespace Fortis.Core.Framework.Menus
{
    public abstract class MenuDefinitionBase
    {
        // ----- Properties
        public abstract int SortOrder { get; }
        public abstract string Text { get; }
        public abstract Uri IconSource { get; }
        public abstract KeyGesture KeyGesture { get; }
        public abstract CommandDefinitionBase CommandDefinition { get; }
    }
}