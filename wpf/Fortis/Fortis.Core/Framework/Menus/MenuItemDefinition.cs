﻿namespace Fortis.Core.Framework.Menus
{
    public abstract class MenuItemDefinition : MenuDefinitionBase
    {
        // ----- Properties
        public MenuItemGroupDefinition Group { get; }
        public override int SortOrder { get; }

        // ----- Constructors
        protected MenuItemDefinition(MenuItemGroupDefinition group, int sortOrder)
        {
            Group = group;
            SortOrder = sortOrder;
        }
    }
}