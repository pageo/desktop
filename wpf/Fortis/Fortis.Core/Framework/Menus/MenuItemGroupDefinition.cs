﻿namespace Fortis.Core.Framework.Menus
{
    public class MenuItemGroupDefinition
    {
        // ----- Properties
        public MenuDefinitionBase Parent { get; }
        public int SortOrder { get; }

        // ----- Constuctors
        public MenuItemGroupDefinition(MenuDefinitionBase parent, int sortOrder)
        {
            Parent = parent;
            SortOrder = sortOrder;
        }
    }
}