﻿using System;
using System.Windows.Input;
using Fortis.Core.Framework.Commands;

namespace Fortis.Core.Framework.Menus
{
    public class TextMenuItemDefinition : MenuItemDefinition
    {
        // ----- Properties
        public override string Text { get; }
        public override Uri IconSource { get; }
        public override KeyGesture KeyGesture => null;
        public override CommandDefinitionBase CommandDefinition => null;

        // ----- Constructors
        public TextMenuItemDefinition(MenuItemGroupDefinition group, int sortOrder, string text, Uri iconSource = null)
            : base(group, sortOrder)
        {
            Text = text;
            IconSource = iconSource;
        }
    }
}