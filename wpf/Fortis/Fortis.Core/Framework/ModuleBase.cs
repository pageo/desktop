﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using Fortis.Core.Framework.Services;
using Fortis.Core.Modules.MainMenu;
using Fortis.Core.Modules.ToolBars;

namespace Fortis.Core.Framework
{
	public abstract class ModuleBase : IModule
	{
#pragma warning disable 649
        [Import]
        private IMainWindow _mainWindow;

        [Import]
        private IShell _shell;
#pragma warning restore 649

        protected IMainWindow MainWindow => _mainWindow;

	    protected IShell Shell => _shell;

	    protected IMenu MainMenu => _shell.MainMenu;

	    protected IToolBars ToolBars => _shell.ToolBars;

	    public virtual IEnumerable<ResourceDictionary> GlobalResourceDictionaries
        {
            get { yield break; }
        }

        public virtual IEnumerable<IDocument> DefaultDocuments
        {
            get { yield break; }
        }

	    public virtual IEnumerable<Type> DefaultTools
	    {
            get { yield break; }
	    }

        public virtual void PreInitialize()
        {
            
        }

		public virtual void Initialize()
		{
		    
		}

        public virtual void PostInitialize()
        {

        }
	}
}