﻿using System;
using Caliburn.Micro;

namespace Fortis.Core.Framework.Results
{
    public class LambdaResult : IResult
    {
        // ----- FIelds
        private readonly Action<CoroutineExecutionContext> _lambda;

        // ----- Constructors
        public LambdaResult(Action<CoroutineExecutionContext> lambda)
        {
            _lambda = lambda;
        }

        // ----- Public methods
        public void Execute(CoroutineExecutionContext context)
        {
            _lambda(context);

            var completedHandler = Completed;
            completedHandler?.Invoke(this, new ResultCompletionEventArgs());
        }

        // ----- Events
        public event EventHandler<ResultCompletionEventArgs> Completed;
    }
}