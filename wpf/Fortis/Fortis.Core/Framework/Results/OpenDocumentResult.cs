﻿using System;
using System.ComponentModel.Composition;
using Caliburn.Micro;
using Fortis.Core.Framework.Services;
using Fortis.Core.Modules.Shell.Commands;

namespace Fortis.Core.Framework.Results
{
	public class OpenDocumentResult : OpenResultBase<IDocument>
	{
        // ----- Fields
		private readonly IDocument _editor;
		private readonly Type _editorType;
		private readonly string _path;

#pragma warning disable 649
        [Import]
		private IShell _shell;
#pragma warning restore 649

        // ----- Constructors
        public OpenDocumentResult(IDocument editor)
		{
			_editor = editor;
		}
		public OpenDocumentResult(string path)
		{
			_path = path;
		}
		public OpenDocumentResult(Type editorType)
		{
			_editorType = editorType;
		}

        // ----- Override methods
		public override void Execute(CoroutineExecutionContext context)
		{
			var editor = _editor ??
				(string.IsNullOrEmpty(_path)
					? (IDocument)IoC.GetInstance(_editorType, null)
					:  GetEditor(_path));

			if (editor == null)
			{
				OnCompleted(null, true);
				return;
			}

		    SetData?.Invoke(editor);

		    OnConfigure?.Invoke(editor);

		    editor.Deactivated += (s, e) =>
			{
				if (!e.WasClosed)
					return;

			    OnShutDown?.Invoke(editor);
			};

			_shell.OpenDocument(editor);

			OnCompleted(null, false);
		}

        // ----- Internal logics
		private static IDocument GetEditor(string path)
		{
		    return OpenFileCommandHandler.GetEditor(path).Result;
		}
	}
}