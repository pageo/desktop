﻿using System;
using Caliburn.Micro;

namespace Fortis.Core.Framework.Results
{
	public abstract class OpenResultBase<TTarget> : IOpenResult<TTarget>
	{
        // ----- Fields
		protected Action<TTarget> SetData;
		protected Action<TTarget> OnConfigure;
		protected Action<TTarget> OnShutDown;

        // ----- Properties
		Action<TTarget> IOpenResult<TTarget>.OnConfigure
		{
			get { return OnConfigure; }
			set { OnConfigure = value; }
		}
		Action<TTarget> IOpenResult<TTarget>.OnShutDown
		{
			get { return OnShutDown; }
			set { OnShutDown = value; }
		}

        // ----- Protected methods
		protected virtual void OnCompleted(Exception exception, bool wasCancelled)
		{
		    Completed?.Invoke(this, new ResultCompletionEventArgs { Error = exception, WasCancelled = wasCancelled});
		}

        // ----- Abstract methods
		public abstract void Execute(CoroutineExecutionContext context);

        // ----- Events
		public event EventHandler<ResultCompletionEventArgs> Completed;
	}
}