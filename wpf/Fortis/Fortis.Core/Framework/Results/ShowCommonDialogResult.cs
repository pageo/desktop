﻿using System;
using Caliburn.Micro;
using Microsoft.Win32;

namespace Fortis.Core.Framework.Results
{
	public class ShowCommonDialogResult : IResult
	{
        // ----- Events
		public event EventHandler<ResultCompletionEventArgs> Completed;

        // ----- Fields
		private readonly CommonDialog _commonDialog;

        // ----- Constructors
		public ShowCommonDialogResult(CommonDialog commonDialog)
		{
			_commonDialog = commonDialog;
		}

        // ----- Public methods
		public void Execute(CoroutineExecutionContext context)
		{
			var result = _commonDialog.ShowDialog().GetValueOrDefault(false);
			Completed(this, new ResultCompletionEventArgs { WasCancelled = !result });
		}
	}
}