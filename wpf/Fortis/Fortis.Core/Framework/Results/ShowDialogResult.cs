﻿using System;
using System.ComponentModel.Composition;
using Caliburn.Micro;

namespace Fortis.Core.Framework.Results
{
    public class ShowDialogResult<TWindow> : OpenResultBase<TWindow>
        where TWindow : IWindow
    {
        // ----- Fields
        private readonly Func<TWindow> _windowLocator = () => IoC.Get<TWindow>();

        // ----- Constructors
        public ShowDialogResult()
        {
        }
        public ShowDialogResult(TWindow window)
        {
            _windowLocator = () => window;
        }

        [Import]
        public IWindowManager WindowManager { get; set; }

        // ----- Override methods
        public override void Execute(CoroutineExecutionContext context)
        {
            var window = _windowLocator();

            SetData?.Invoke(window);

            OnConfigure?.Invoke(window);

            var result = WindowManager.ShowDialog(window).GetValueOrDefault();

            OnShutDown?.Invoke(window);

            OnCompleted(null, !result);
        }
    }
}