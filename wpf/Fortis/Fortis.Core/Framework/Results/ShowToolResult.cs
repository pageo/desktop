﻿using System;
using System.ComponentModel.Composition;
using Caliburn.Micro;
using Fortis.Core.Framework.Services;

namespace Fortis.Core.Framework.Results
{
	public class ShowToolResult<TTool> : OpenResultBase<TTool>
		where TTool : ITool
	{
        // ----- Fields
		private readonly Func<TTool> _toolLocator = () => IoC.Get<TTool>();

#pragma warning disable 649
        [Import]
		private IShell _shell;
#pragma warning restore 649

        // ----- Constructors
        public ShowToolResult()
		{
			
		}
		public ShowToolResult(TTool tool)
		{
			_toolLocator = () => tool;
		}

        // ----- Override methods
		public override void Execute(CoroutineExecutionContext context)
		{
			var tool = _toolLocator();

		    SetData?.Invoke(tool);

		    OnConfigure?.Invoke(tool);

		    tool.Deactivated += (s, e) =>
			{
				if (!e.WasClosed)
					return;

			    OnShutDown?.Invoke(tool);

			    OnCompleted(null, false);
			};

			_shell.ShowTool(tool);
		}
	}
}