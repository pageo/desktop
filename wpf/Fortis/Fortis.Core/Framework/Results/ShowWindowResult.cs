﻿using System;
using System.ComponentModel.Composition;
using Caliburn.Micro;

namespace Fortis.Core.Framework.Results
{
    public class ShowWindowResult<TWindow> : OpenResultBase<TWindow>
        where TWindow : IWindow
    {
        // ----- Fields
        private readonly Func<TWindow> _windowLocator = () => IoC.Get<TWindow>();

        [Import]
        public IWindowManager WindowManager { get; set; }

        // ----- Constructors
        public ShowWindowResult()
        {
            
        }
        public ShowWindowResult(TWindow window)
        {
            _windowLocator = () => window;
        }

        // ----- Override methods
        public override void Execute(CoroutineExecutionContext context)
        {
            var window = _windowLocator();

            SetData?.Invoke(window);

            OnConfigure?.Invoke(window);

            window.Deactivated += (s, e) =>
            {
                if (!e.WasClosed)
                    return;

                OnShutDown?.Invoke(window);

                OnCompleted(null, false);
            };

            WindowManager.ShowWindow(window);
        }
    }
}