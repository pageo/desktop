﻿namespace Fortis.Core.Framework.Services
{
    public class EditorFileType
    {
        // ----- Properties
        public string Name { get; set; }
        public string FileExtension { get; set; }

        // ----- Constructors
        public EditorFileType(string name, string fileExtension)
        {
            Name = name;
            FileExtension = fileExtension;
        }
        public EditorFileType()
        {
            
        }
    }
}