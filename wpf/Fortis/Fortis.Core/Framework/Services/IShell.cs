﻿using System;
using System.Collections.Generic;
using Fortis.Core.Modules.MainMenu;
using Fortis.Core.Modules.StatusBar;
using Fortis.Core.Modules.ToolBars;

namespace Fortis.Core.Framework.Services
{
    public interface IShell
    {
        event EventHandler ActiveDocumentChanging;
        event EventHandler ActiveDocumentChanged;

        bool ShowFloatingWindowsInTaskbar { get; set; }
        
		IMenu MainMenu { get; }
        IToolBars ToolBars { get; }
		IStatusBar StatusBar { get; }

        // TODO: Rename this to ActiveItem.
        ILayoutItem ActiveLayoutItem { get; set; }

        // TODO: Rename this to SelectedDocument.
		IDocument ActiveItem { get; }

        IEnumerable<IDocument> Documents { get; }
        IEnumerable<ITool> Tools { get; }

        void ShowTool<TTool>() where TTool : ITool;
		void ShowTool(ITool model);

		void OpenDocument(IDocument model);
		void CloseDocument(IDocument document);

		void Close();
	}
}