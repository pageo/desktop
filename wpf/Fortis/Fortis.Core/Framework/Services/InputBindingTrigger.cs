﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace Fortis.Core.Framework.Services
{
	public class InputBindingTrigger : TriggerBase<FrameworkElement>, ICommand
	{
        // ----- Properties
		public static readonly DependencyProperty InputBindingProperty =
			DependencyProperty.Register("InputBinding", typeof(InputBinding), 
			typeof(InputBindingTrigger), new UIPropertyMetadata(null));
		public InputBinding InputBinding
		{
			get { return (InputBinding)GetValue(InputBindingProperty); }
			set { SetValue(InputBindingProperty, value); }
		}

        // ----- Override methods
		protected override void OnAttached()
		{
			if (InputBinding != null) {
				InputBinding.Command = this;
				AssociatedObject.InputBindings.Add(InputBinding);
			}
			base.OnAttached();
		}

		// ----- Commands
		public bool CanExecute(object parameter)
		{
			return true;
		}

        // ----- Events
		public event EventHandler CanExecuteChanged = delegate { };

        // ----- Public methods
		public void Execute(object parameter)
		{
			InvokeActions(parameter);
		}
	}
}