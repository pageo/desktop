﻿namespace Fortis.Core.Framework.Services
{
	public enum PaneLocation
	{
		Left,
		Right,
		Bottom
	}
}