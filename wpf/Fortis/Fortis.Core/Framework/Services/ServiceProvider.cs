﻿using System;
using System.ComponentModel.Composition;
using Caliburn.Micro;

namespace Fortis.Core.Framework.Services
{
    [Export(typeof(IServiceProvider))]
    public class ServiceProvider : IServiceProvider
    {
        // ----- Public methods
        public object GetService(Type serviceType)
        {
            return IoC.GetInstance(serviceType, null);
        }
    }
}