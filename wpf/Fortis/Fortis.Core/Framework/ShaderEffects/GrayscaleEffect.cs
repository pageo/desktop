﻿using System.Windows;
using System.Windows.Media;

namespace Fortis.Core.Framework.ShaderEffects
{
    public class GrayscaleEffect : ShaderEffectBase<GrayscaleEffect>
    {
        // ----- Fields
        public static readonly DependencyProperty InputProperty = RegisterPixelShaderSamplerProperty("Input", typeof(GrayscaleEffect), 0);

        // ----- Properties
        public Brush Input
        {
            get { return (Brush) GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        // ----- Constructors
        public GrayscaleEffect()
        {
            UpdateShaderValue(InputProperty);
        }
    }
}