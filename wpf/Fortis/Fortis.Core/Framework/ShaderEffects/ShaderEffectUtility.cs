﻿using System;
using System.Windows.Media.Effects;

namespace Fortis.Core.Framework.ShaderEffects
{
    internal static class ShaderEffectUtility
    {
        // ----- Public methods
        public static PixelShader GetPixelShader(string name)
        {
            return new PixelShader
            {
                UriSource = new Uri(@"pack://application:,,,/Fortis.Core;component/Framework/ShaderEffects/" + name + ".ps")
            };
        }
    }
}