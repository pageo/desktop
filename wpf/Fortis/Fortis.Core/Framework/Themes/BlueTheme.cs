﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace Fortis.Core.Framework.Themes
{
    [Export(typeof(ITheme))]
    public class BlueTheme : ITheme
    {
        // ----- Properties
        public virtual string Name => Properties.Resources.ThemeBlueName;
        public virtual IEnumerable<Uri> ApplicationResources
        {
            get
            {
                yield return new Uri("pack://application:,,,/Xceed.Wpf.AvalonDock.Themes.VS2013;component/BlueTheme.xaml");
                yield return new Uri("pack://application:,,,/Fortis.Core;component/Themes/VS2013/BlueTheme.xaml");
            }
        }
        public virtual IEnumerable<Uri> MainWindowResources
        {
            get { yield break; }
        }
    }
}