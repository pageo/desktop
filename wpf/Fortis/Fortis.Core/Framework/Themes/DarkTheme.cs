﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace Fortis.Core.Framework.Themes
{
    [Export(typeof(ITheme))]
    public class DarkTheme : ITheme
    {
        // ----- Properties
        public virtual string Name => Properties.Resources.ThemeDarkName;
        public virtual IEnumerable<Uri> ApplicationResources
        {
            get
            {
                yield return new Uri("pack://application:,,,/Xceed.Wpf.AvalonDock.Themes.VS2013;component/DarkTheme.xaml");
                yield return new Uri("pack://application:,,,/Fortis.Core;component/Themes/VS2013/DarkTheme.xaml");
            }
        }
        public virtual IEnumerable<Uri> MainWindowResources
        {
            get { yield break; }
        }
    }
}