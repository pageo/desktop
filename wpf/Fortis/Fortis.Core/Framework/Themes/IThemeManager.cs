﻿using System;
using System.Collections.Generic;

namespace Fortis.Core.Framework.Themes
{
    public interface IThemeManager
    {
        event EventHandler CurrentThemeChanged;

        List<ITheme> Themes { get; }
        ITheme CurrentTheme { get; }

        bool SetCurrentTheme(string name);
    }
}