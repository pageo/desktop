﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace Fortis.Core.Framework.Themes
{
    [Export(typeof(ITheme))]
    public class LightTheme : ITheme
    {
        // ----- Properties
        public virtual string Name => Properties.Resources.ThemeLightName;
        public virtual IEnumerable<Uri> ApplicationResources
        {
            get
            {
                yield return new Uri("pack://application:,,,/Xceed.Wpf.AvalonDock.Themes.VS2013;component/LightTheme.xaml");
                yield return new Uri("pack://application:,,,/Fortis.Core;component/Themes/VS2013/LightTheme.xaml");
            }
        }
        public virtual IEnumerable<Uri> MainWindowResources
        {
            get { yield break; }
        }
    }
}