﻿using System.Threading.Tasks;

namespace Fortis.Core.Framework.Threading
{
    public class TaskUtility
    {
        // ----- Public methods
        public static readonly Task Completed = Task.FromResult(true);
    }
}