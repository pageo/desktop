using System.Windows.Input;
using Caliburn.Micro;
using Fortis.Core.Framework.Services;
using Fortis.Core.Framework.ToolBars;
using Fortis.Core.Modules.ToolBars;
using Fortis.Core.Modules.ToolBars.Models;

namespace Fortis.Core.Framework
{
	public abstract class Tool : LayoutItemBase, ITool
	{
        // ----- Fields
		private ICommand _closeCommand;
        private bool _isVisible;
        private IToolBar _toolBar;

        // ----- Commands
        public override ICommand CloseCommand
		{
			get { return _closeCommand ?? (_closeCommand = new RelayCommand(p => IsVisible = false, p => true)); }
		}

        // ----- Properties
	    public abstract PaneLocation PreferredLocation { get; }
	    public virtual double PreferredWidth => 200;
	    public virtual double PreferredHeight => 200;
		public bool IsVisible
		{
			get { return _isVisible; }
			set
			{
				_isVisible = value;
				NotifyOfPropertyChange(() => IsVisible);
			}
		}
        public ToolBarDefinition ToolBarDefinition
        {
            get { return GetNotifiableProperty<ToolBarDefinition>(); }
            protected set { SetNotifiableProperty(value); }
        }
        public IToolBar ToolBar
        {
            get
            {
                if (_toolBar != null)
                    return _toolBar;

                if (ToolBarDefinition == null)
                    return null;

                var toolBarBuilder = IoC.Get<IToolBarBuilder>();
                _toolBar = new ToolBarModel();
                toolBarBuilder.BuildToolBar(ToolBarDefinition, _toolBar);
                return _toolBar;
            }
        }
        public override bool ShouldReopenOnStart => true;

        // ----- Constructors
	    protected Tool()
		{
			IsVisible = true;
		}
	}
}