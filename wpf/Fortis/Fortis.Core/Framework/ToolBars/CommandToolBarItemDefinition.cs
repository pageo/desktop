﻿using System;
using System.Windows.Input;
using Caliburn.Micro;
using Fortis.Core.Framework.Commands;

namespace Fortis.Core.Framework.ToolBars
{
    public class CommandToolBarItemDefinition<TCommandDefinition> : ToolBarItemDefinition
        where TCommandDefinition : CommandDefinitionBase
    {
        // ----- Fields
        private readonly CommandDefinitionBase _commandDefinition;

        // ----- Properties
        public override string Text => _commandDefinition.ToolTip;
        public override Uri IconSource => _commandDefinition.IconSource;
        public override KeyGesture KeyGesture { get; }
        public override CommandDefinitionBase CommandDefinition => _commandDefinition;

        // ----- Constructors
        public CommandToolBarItemDefinition(ToolBarItemGroupDefinition group, int sortOrder, ToolBarItemDisplay display = ToolBarItemDisplay.IconOnly)
            : base(group, sortOrder, display)
        {
            _commandDefinition = IoC.Get<ICommandService>().GetCommandDefinition(typeof(TCommandDefinition));
            KeyGesture = IoC.Get<ICommandKeyGestureService>().GetPrimaryKeyGesture(_commandDefinition);
        }
    }
}