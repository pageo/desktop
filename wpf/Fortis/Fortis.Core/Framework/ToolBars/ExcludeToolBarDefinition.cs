﻿namespace Fortis.Core.Framework.ToolBars
{
    public class ExcludeToolBarDefinition
    {
        // ----- Properties
        public ToolBarDefinition ToolBarDefinitionToExclude { get; }

        // ----- Constructors
        public ExcludeToolBarDefinition(ToolBarDefinition toolBarDefinition)
        {
            ToolBarDefinitionToExclude = toolBarDefinition;
        }
    }
}