﻿namespace Fortis.Core.Framework.ToolBars
{
    public class ExcludeToolBarItemDefinition
    {
        // ----- Properties
        public ToolBarItemDefinition ToolBarItemDefinitionToExclude { get; }

        // ----- Constructors
        public ExcludeToolBarItemDefinition(ToolBarItemDefinition ToolBarItemDefinition)
        {
            ToolBarItemDefinitionToExclude = ToolBarItemDefinition;
        }
    }
}