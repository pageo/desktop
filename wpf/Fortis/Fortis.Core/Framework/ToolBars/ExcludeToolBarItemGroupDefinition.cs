namespace Fortis.Core.Framework.ToolBars
{
    public class ExcludeToolBarItemGroupDefinition
    {
        // ----- Properties
        public ToolBarItemGroupDefinition ToolBarItemGroupDefinitionToExclude { get; }

        // ----- Constructors
        public ExcludeToolBarItemGroupDefinition(ToolBarItemGroupDefinition toolBarItemGroupDefinition)
        {
            ToolBarItemGroupDefinitionToExclude = toolBarItemGroupDefinition;
        }
    }
}