﻿namespace Fortis.Core.Framework.ToolBars
{
    public class ToolBarDefinition
    {
        // ----- Properties
        public int SortOrder { get; }
        public string Name { get; }

        // ----- Constructors
        public ToolBarDefinition(int sortOrder, string name)
        {
            SortOrder = sortOrder;
            Name = name;
        }
    }
}