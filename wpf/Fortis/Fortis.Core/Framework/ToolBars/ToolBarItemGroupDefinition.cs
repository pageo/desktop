﻿namespace Fortis.Core.Framework.ToolBars
{
    public class ToolBarItemGroupDefinition
    {
        // ----- Properties
        public ToolBarDefinition ToolBar { get; }
        public int SortOrder { get; }

        // ----- Constructors
        public ToolBarItemGroupDefinition(ToolBarDefinition toolBar, int sortOrder)
        {
            ToolBar = toolBar;
            SortOrder = sortOrder;
        }
    }
}