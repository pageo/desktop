﻿using System.Windows;
using System.Windows.Controls;
using Fortis.Core.Framework.Controls;
using Fortis.Core.Modules.MainMenu.Models;

namespace Fortis.Core.Modules.MainMenu.Controls
{
    public class MenuItemEx : MenuItem
	{
        // ----- Fields
		private object _currentItem;

        // ----- Override methods
		protected override bool IsItemItsOwnContainerOverride(object item)
		{
			_currentItem = item;
			return base.IsItemItsOwnContainerOverride(item);
		}

        // ----- Override methods
		protected override DependencyObject GetContainerForItemOverride()
		{
			return GetContainer(this, _currentItem);
		}
		internal static DependencyObject GetContainer(FrameworkElement frameworkElement, object item)
		{
		    if (item is MenuItemSeparator)
		        return new Separator();

		    const string styleKey = "MenuItem";

		    var result = new MenuItemEx();
            result.SetResourceReference(DynamicStyle.BaseStyleProperty, typeof(MenuItem));
		    result.SetResourceReference(DynamicStyle.DerivedStyleProperty, styleKey);
		    return result;
		}
	}
}