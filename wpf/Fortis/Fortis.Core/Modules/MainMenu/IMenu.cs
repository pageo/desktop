﻿using Caliburn.Micro;
using Fortis.Core.Modules.MainMenu.Models;

namespace Fortis.Core.Modules.MainMenu
{
	public interface IMenu : IObservableCollection<MenuItemBase>
	{
		
	}
}