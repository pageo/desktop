using Fortis.Core.Framework.Menus;
using Fortis.Core.Modules.MainMenu.Models;

namespace Fortis.Core.Modules.MainMenu
{
    public interface IMenuBuilder
    {
        void BuildMenuBar(MenuBarDefinition menuBarDefinition, MenuModel result);
    }
}