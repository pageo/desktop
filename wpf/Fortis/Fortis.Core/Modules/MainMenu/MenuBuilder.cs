﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Framework.Menus;
using Fortis.Core.Modules.MainMenu.Models;

namespace Fortis.Core.Modules.MainMenu
{
    [Export(typeof(IMenuBuilder))]
    public class MenuBuilder : IMenuBuilder
    {
        // ----- Fields
        private readonly ICommandService _commandService;
        private readonly IEnumerable<MenuBarDefinition> _menuBars;
        private readonly IEnumerable<MenuDefinition> _menus;
        private readonly IEnumerable<MenuItemGroupDefinition> _menuItemGroups;
        private readonly IEnumerable<MenuItemDefinition> _menuItems;
        private readonly IEnumerable<MenuDefinition> _excludeMenus;
        private readonly IEnumerable<MenuItemGroupDefinition> _excludeMenuItemGroups;
        private readonly IEnumerable<MenuItemDefinition> _excludeMenuItems;

        // ----- Constructors
        [ImportingConstructor]
        public MenuBuilder(
            ICommandService commandService,
            [ImportMany] IEnumerable<MenuBarDefinition> menuBars,
            [ImportMany] IEnumerable<MenuDefinition> menus,
            [ImportMany] IEnumerable<MenuItemGroupDefinition> menuItemGroups,
            [ImportMany] IEnumerable<MenuItemDefinition> menuItems,
            [ImportMany] IEnumerable<ExcludeMenuDefinition> excludeMenus,
            [ImportMany] IEnumerable<ExcludeMenuItemGroupDefinition> excludeMenuItemGroups,
            [ImportMany] IEnumerable<ExcludeMenuItemDefinition> excludeMenuItems)
        {
            _commandService = commandService;
            _menuBars = menuBars;
            _menus = menus;
            _menuItemGroups = menuItemGroups;
            _menuItems = menuItems;
            _excludeMenus = excludeMenus.Select(x => x.MenuDefinitionToExclude).ToArray();
            _excludeMenuItemGroups = excludeMenuItemGroups.Select(x => x.MenuItemGroupDefinitionToExclude).ToArray();
            _excludeMenuItems = excludeMenuItems.Select(x => x.MenuItemDefinitionToExclude).ToArray();
        }

        // ----- Public methods
        public void BuildMenuBar(MenuBarDefinition menuBarDefinition, MenuModel result)
        {
            var menus = _menus
                .Where(x => x.MenuBar == menuBarDefinition)
                .Where(x => !_excludeMenus.Contains(x))
                .OrderBy(x => x.SortOrder);

            foreach (var menu in menus)
            {
                var menuModel = new TextMenuItem(menu);
                AddGroupsRecursive(menu, menuModel);
                if (menuModel.Children.Any())
                    result.Add(menuModel);
            }
        }

        // ----- Internal logicsm
        private void AddGroupsRecursive(MenuDefinitionBase menu, StandardMenuItem menuModel)
        {
            var groups = _menuItemGroups
                .Where(x => x.Parent == menu)
                .Where(x => !_excludeMenuItemGroups.Contains(x))
                .OrderBy(x => x.SortOrder)
                .ToList();

            for (var i = 0; i < groups.Count; i++) {
                var group = groups[i];
                var menuItems = _menuItems
                    .Where(x => x.Group == group)
                    .Where(x => !_excludeMenuItems.Contains(x))
                    .OrderBy(x => x.SortOrder);

                foreach (var menuItem in menuItems) {
                    var menuItemModel = (menuItem.CommandDefinition != null)
                        ? new CommandMenuItem(_commandService.GetCommand(menuItem.CommandDefinition), menuModel)
                        : (StandardMenuItem)new TextMenuItem(menuItem);
                    AddGroupsRecursive(menuItem, menuItemModel);
                    menuModel.Add(menuItemModel);
                }

                if (i < groups.Count - 1 && menuItems.Any())
                    menuModel.Add(new MenuItemSeparator());
            }
        }
    }
}