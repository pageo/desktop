﻿using System.Collections;
using System.Collections.Generic;
using Caliburn.Micro;
using Fortis.Core.Caliburn;

namespace Fortis.Core.Modules.MainMenu.Models
{
	public class MenuItemBase : NotifiableObject, IEnumerable<MenuItemBase>
	{
        // ----- Properties
        public static MenuItemBase Separator => new MenuItemSeparator();
		public IObservableCollection<MenuItemBase> Children { get; }

		// ----- Constructors
		protected MenuItemBase()
		{
			Children = new BindableCollection<MenuItemBase>();
		}

        // ----- Public methods
		public void Add(params MenuItemBase[] menuItems)
		{
			menuItems.Apply(Children.Add);
		}
		public IEnumerator<MenuItemBase> GetEnumerator()
		{
			return Children.GetEnumerator();
		}
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}