﻿using System;
using System.Globalization;
using System.Windows.Input;
using Fortis.Core.Framework.Menus;

namespace Fortis.Core.Modules.MainMenu.Models
{
    public class TextMenuItem : StandardMenuItem
	{
        // ----- Fields
	    private readonly MenuDefinitionBase _menuDefinition;

        // ----- Properties
        public override string Text => _menuDefinition.Text;
	    public override Uri IconSource => _menuDefinition.IconSource;
	    public override string InputGestureText => _menuDefinition.KeyGesture == null
	        ? string.Empty
	        : _menuDefinition.KeyGesture.GetDisplayStringForCulture(CultureInfo.CurrentUICulture);
	    public override ICommand Command => null;
	    public override bool IsChecked => false;
	    public override bool IsVisible => true;

        // ----- Constructors
	    public TextMenuItem(MenuDefinitionBase menuDefinition)
        {
            _menuDefinition = menuDefinition;
        }
	}
}