﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Caliburn.Micro;
using Fortis.Core.Caliburn;
using Fortis.Core.Framework.Themes;
using Fortis.Core.Modules.Settings;

namespace Fortis.Core.Modules.MainMenu.ViewModels
{
    [Export(typeof (ISettingsEditor))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MainMenuSettingsViewModel : NotifiableObject, ISettingsEditor
    {
        // ----- Fields
        private readonly IThemeManager _themeManager;
        private static readonly List<string> AvailableLanguages = new List<string> {
            string.Empty,
            "en",
            "de",
            "ru",
            "zh-Hans",
            "ko",
        };

        // ----- Constructors
        [ImportingConstructor]
        public MainMenuSettingsViewModel(IThemeManager themeManager)
        {
            _themeManager = themeManager;
            SelectedTheme = themeManager.CurrentTheme;
            AutoHideMainMenu = Properties.Settings.Default.AutoHideMainMenu;
            SelectedLanguage = Properties.Settings.Default.LanguageCode;
        }

        // ------ Properties
        public IEnumerable<ITheme> Themes => _themeManager.Themes;
        public ITheme SelectedTheme
        {
            get { return GetNotifiableProperty<ITheme>(); }
            set { SetNotifiableProperty(value); }
        }
        public IEnumerable<string> Languages => AvailableLanguages;
        public string SelectedLanguage
        {
            get { return GetNotifiableProperty<string>(); }
            set { SetNotifiableProperty(value); }
        }
        public bool AutoHideMainMenu
        {
            get { return GetNotifiableProperty<bool>(); }
            set { SetNotifiableProperty(value); }
        }
        public string SettingsPageName => Properties.Resources.SettingsPageGeneral;
        public string SettingsPagePath => Properties.Resources.SettingsPathEnvironment;

        // ----- Public methods
        public void ApplyChanges()
        {
            Properties.Settings.Default.ThemeName = SelectedTheme.GetType().Name;
            Properties.Settings.Default.AutoHideMainMenu = AutoHideMainMenu;
            Properties.Settings.Default.LanguageCode = SelectedLanguage;
            Properties.Settings.Default.Save();
        }
    }
}