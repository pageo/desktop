﻿using System.Windows.Controls;

namespace Fortis.Core.Modules.MainMenu.Views
{
    /// <summary>
    /// Interaction logic for MainMenuView.xaml
    /// </summary>
    public partial class MainMenuView : UserControl
    {
        public MainMenuView()
        {
            InitializeComponent();
        }
    }
}
