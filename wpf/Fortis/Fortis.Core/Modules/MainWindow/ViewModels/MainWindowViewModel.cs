﻿using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Media;
using Fortis.Core.Caliburn;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Framework.Services;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.MainWindow.ViewModels
{
    [Export(typeof(IMainWindow))]
    public class MainWindowViewModel : NotifiableObject, IMainWindow, IPartImportsSatisfiedNotification
    {
#pragma warning disable 649
        [Import]
        private IShell _shell;
        [Import]
        private IResourceManager _resourceManager;
        [Import]
        private ICommandKeyGestureService _commandKeyGestureService;
#pragma warning restore 649

        // ----- Properties
        public WindowState WindowState
        {
            get { return GetNotifiableProperty<WindowState>(); }
            set { SetNotifiableProperty(value); }
        }
        public double Width
        {
            get { return GetNotifiableProperty<double>(); }
            set { SetNotifiableProperty(value); }
        }
        public double Height
        {
            get { return GetNotifiableProperty<double>(); }
            set { SetNotifiableProperty(value); }
        }
        public string Title
        {
            get { return GetNotifiableProperty<string>(); }
            set { SetNotifiableProperty(value); }
        }
        public ImageSource Icon
        {
            get { return GetNotifiableProperty<ImageSource>(); }
            set { SetNotifiableProperty(value); }
        }
        public IShell Shell => _shell;

        // ----- Constructors
        public MainWindowViewModel()
        {
            WindowState = WindowState.Normal;
            Width = 1000.0;
            Height = 800.0;
            Title = Resources.MainWindowDefaultTitle;
        }

        // ----- Public methods
        void IPartImportsSatisfiedNotification.OnImportsSatisfied()
        {
            if (Icon == null)
                Icon = _resourceManager.GetBitmap("Resources/Icons/Fortis.ico");
            //ActivateItem(_shell);
        }

        // ----- Override methods
        //protected override void OnViewLoaded(object view)
        //{
        //    _commandKeyGestureService.BindKeyGestures((UIElement) view);
        //    base.OnViewLoaded(view);
        //}
    }
}