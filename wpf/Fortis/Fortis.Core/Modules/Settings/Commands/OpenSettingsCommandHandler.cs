﻿using System.ComponentModel.Composition;
using System.Threading.Tasks;
using Caliburn.Micro;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Framework.Threading;
using Fortis.Core.Modules.Settings.ViewModels;

namespace Fortis.Core.Modules.Settings.Commands
{
    [CommandHandler]
    public class OpenSettingsCommandHandler : CommandHandlerBase<OpenSettingsCommandDefinition>
    {
        private readonly IWindowManager _windowManager;

        [ImportingConstructor]
        public OpenSettingsCommandHandler(IWindowManager windowManager)
        {
            _windowManager = windowManager;
        }

        public override Task Run(Command command)
        {
            _windowManager.ShowDialog(IoC.Get<SettingsViewModel>());
            return TaskUtility.Completed;
        }
    }
}