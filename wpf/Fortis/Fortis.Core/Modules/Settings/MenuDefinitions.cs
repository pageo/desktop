﻿using System.ComponentModel.Composition;
using Fortis.Core.Framework.Menus;
using Fortis.Core.Modules.Settings.Commands;

namespace Fortis.Core.Modules.Settings
{
    public static class MenuDefinitions
    {
        [Export]
        public static MenuItemDefinition OpenSettingsMenuItem = new CommandMenuItemDefinition<OpenSettingsCommandDefinition>(
            MainMenu.MenuDefinitions.ToolsOptionsMenuGroup, 0);
    }
}