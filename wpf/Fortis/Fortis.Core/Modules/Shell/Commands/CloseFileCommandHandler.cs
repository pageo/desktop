using System.ComponentModel.Composition;
using System.Threading.Tasks;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Framework.Services;
using Fortis.Core.Framework.Threading;

namespace Fortis.Core.Modules.Shell.Commands
{
    [CommandHandler]
    public class CloseFileCommandHandler : CommandHandlerBase<CloseFileCommandDefinition>
    {
        private readonly IShell _shell;

        [ImportingConstructor]
        public CloseFileCommandHandler(IShell shell)
        {
            _shell = shell;
        }

        public override void Update(Command command)
        {
            command.Enabled = _shell.ActiveItem != null;
            base.Update(command);
        }

        public override Task Run(Command command)
        {
            _shell.CloseDocument(_shell.ActiveItem);
            return TaskUtility.Completed;
        }
    }
}