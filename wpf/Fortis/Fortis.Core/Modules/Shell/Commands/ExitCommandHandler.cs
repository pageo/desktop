﻿using System.ComponentModel.Composition;
using System.Threading.Tasks;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Framework.Services;
using Fortis.Core.Framework.Threading;

namespace Fortis.Core.Modules.Shell.Commands
{
    [CommandHandler]
    public class ExitCommandHandler : CommandHandlerBase<ExitCommandDefinition>
    {
        private readonly IShell _shell;

        [ImportingConstructor]
        public ExitCommandHandler(IShell shell)
        {
            _shell = shell;
        }

        public override Task Run(Command command)
        {
            _shell.Close();
            return TaskUtility.Completed;
        }
    }
}