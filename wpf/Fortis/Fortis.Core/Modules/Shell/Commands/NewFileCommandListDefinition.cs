﻿using Fortis.Core.Framework.Commands;

namespace Fortis.Core.Modules.Shell.Commands
{
    [CommandDefinition]
    public class NewFileCommandListDefinition : CommandListDefinition
    {
        public const string CommandName = "File.NewFile";

        public override string Name => CommandName;
    }
}