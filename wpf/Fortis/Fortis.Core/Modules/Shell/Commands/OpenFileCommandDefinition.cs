﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.Shell.Commands
{
    [CommandDefinition]
    public class OpenFileCommandDefinition : CommandDefinition
    {
        public const string CommandName = "File.OpenFile";

        public override string Name => CommandName;

        public override string Text => Resources.FileOpenCommandText;

        public override string ToolTip => Resources.FileOpenCommandToolTip;

        public override Uri IconSource => new Uri("pack://application:,,,/Fortis.Core;component/Resources/Icons/Open.png");

        [Export]
        public static CommandKeyboardShortcut KeyGesture = new CommandKeyboardShortcut<OpenFileCommandDefinition>(new KeyGesture(Key.O, ModifierKeys.Control));
    }
}