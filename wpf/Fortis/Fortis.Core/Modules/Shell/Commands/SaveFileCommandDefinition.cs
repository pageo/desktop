﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.Shell.Commands
{
    [CommandDefinition]
    public class SaveFileCommandDefinition : CommandDefinition
    {
        public const string CommandName = "File.SaveFile";

        public override string Name => CommandName;

        public override string Text => Resources.FileSaveCommandText;

        public override string ToolTip => Resources.FileSaveCommandToolTip;

        public override Uri IconSource => new Uri("pack://application:,,,/Fortis.Core;component/Resources/Icons/Save.png");

        [Export]
        public static CommandKeyboardShortcut KeyGesture = new CommandKeyboardShortcut<SaveFileCommandDefinition>(new KeyGesture(Key.S, ModifierKeys.Control));
    }
}