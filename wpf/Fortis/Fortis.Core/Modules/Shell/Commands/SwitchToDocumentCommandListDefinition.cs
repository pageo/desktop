﻿using Fortis.Core.Framework.Commands;

namespace Fortis.Core.Modules.Shell.Commands
{
    [CommandDefinition]
    public class SwitchToDocumentCommandListDefinition : CommandListDefinition
    {
        public const string CommandName = "Window.SwitchToDocument";

        public override string Name => CommandName;
    }
}