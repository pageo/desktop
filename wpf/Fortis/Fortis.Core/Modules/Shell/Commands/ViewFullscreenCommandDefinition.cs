﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.Shell.Commands
{
    [CommandDefinition]
    public class ViewFullScreenCommandDefinition : CommandDefinition
    {
        public const string CommandName = "View.FullScreen";

        public override string Name => CommandName;

        public override string Text => Resources.ViewFullScreenCommandText;

        public override string ToolTip => Resources.ViewFullScreenCommandToolTip;

        public override Uri IconSource => new Uri("pack://application:,,,/Fortis.Core;component/Resources/Icons/FullScreen.png");

        [Export]
        public static CommandKeyboardShortcut KeyGesture = new CommandKeyboardShortcut<ViewFullScreenCommandDefinition>(new KeyGesture(Key.Enter, ModifierKeys.Shift | ModifierKeys.Alt));
    }
}