using Fortis.Core.Framework.Services;
using Fortis.Core.Modules.Shell.Views;

namespace Fortis.Core.Modules.Shell.Services
{
    public interface ILayoutItemStatePersister
    {
        bool SaveState(IShell shell, IShellView shellView, string fileName);
        bool LoadState(IShell shell, IShellView shellView, string fileName);
    }
}