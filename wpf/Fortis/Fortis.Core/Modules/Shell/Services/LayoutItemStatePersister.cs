﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using Caliburn.Micro;
using Fortis.Core.Framework;
using Fortis.Core.Framework.Services;
using Fortis.Core.Modules.Shell.Views;

namespace Fortis.Core.Modules.Shell.Services
{
    [Export(typeof(ILayoutItemStatePersister))]
    public class LayoutItemStatePersister : ILayoutItemStatePersister
    {
        // ----- Fields
        private static readonly Type LayoutBaseType = typeof(ILayoutItem);

        // ----- Public methods
        public bool SaveState(IShell shell, IShellView shellView, string fileName)
        {
            try
            {
                using (var writer = new BinaryWriter(new FileStream(fileName, FileMode.Create, FileAccess.Write))) {
                    var itemStates = shell.Documents.Concat(shell.Tools.Cast<ILayoutItem>());

                    var itemCount = 0;
                    writer.Write(itemCount);

                    foreach (var item in itemStates) {
                        if (!item.ShouldReopenOnStart)
                            continue;

                        var itemType = item.GetType();
                        var exportAttributes = itemType
                                .GetCustomAttributes(typeof(ExportAttribute), false)
                                .Cast<ExportAttribute>().ToList();

                        var exportTypes = new List<Type>();
                        var foundExportContract = false;
                        foreach (var att in exportAttributes) {
                            var type = att.ContractType;
                            if (LayoutBaseType.IsAssignableFrom(type)) {
                                exportTypes.Add(type);
                                foundExportContract = true;
                                continue;
                            }

                            type = GetTypeFromContractNameAsILayoutItem(att);
                            if (LayoutBaseType.IsAssignableFrom(type)) {
                                exportTypes.Add(type);
                                foundExportContract = true;
                            }
                        }
                        if (!foundExportContract && LayoutBaseType.IsAssignableFrom(itemType))
                            exportTypes.Add(itemType);

                        var firstExport = exportTypes.FirstOrDefault();
                        if (firstExport == null)
                            throw new InvalidOperationException(
                                $"A ViewModel that participates in LayoutItem.ShouldReopenOnStart must be decorated with an ExportAttribute who's ContractType that inherits from ILayoutItem, infringing type is {itemType}.");
                        if (exportTypes.Count > 1)
                            throw new InvalidOperationException(
                                $"A ViewModel that participates in LayoutItem.ShouldReopenOnStart can't be decorated with more than one ExportAttribute which inherits from ILayoutItem. infringing type is {itemType}.");

                        var selectedTypeName = firstExport.AssemblyQualifiedName;
                        if (string.IsNullOrEmpty(selectedTypeName))
                            throw new InvalidOperationException(
                                $"Could not retrieve the assembly qualified type name for {firstExport}, most likely because the type is generic.");

                        writer.Write(selectedTypeName);
                        writer.Write(item.ContentId);

                        var stateSizePosition = writer.BaseStream.Position;

                        writer.Write(0L);

                        long stateSize;

                        try {
                            var stateStartPosition = writer.BaseStream.Position;
                            item.SaveState(writer);
                            stateSize = writer.BaseStream.Position - stateStartPosition;
                        }
                        catch {
                            stateSize = 0;
                        }

                        writer.BaseStream.Seek(stateSizePosition, SeekOrigin.Begin);
                        writer.Write(stateSize);

                        if (stateSize > 0)
                            writer.BaseStream.Seek(0, SeekOrigin.End);

                        itemCount++;
                    }

                    writer.BaseStream.Seek(0, SeekOrigin.Begin);
                    writer.Write(itemCount);
                    writer.BaseStream.Seek(0, SeekOrigin.End);

                    shellView.SaveLayout(writer.BaseStream);
                }
            }
            catch {
                return false;
            }

            return true;
        }
        public bool LoadState(IShell shell, IShellView shellView, string fileName)
        {
            var layoutItems = new Dictionary<string, ILayoutItem>();
            if (!File.Exists(fileName))
                return false;

            try
            {
                using (var reader = new BinaryReader(new FileStream(fileName, FileMode.Open, FileAccess.Read))) {
                    var count = reader.ReadInt32();

                    for (var i = 0; i < count; i++) {
                        var typeName = reader.ReadString();
                        var contentId = reader.ReadString();
                        var stateEndPosition = reader.ReadInt64();
                        stateEndPosition += reader.BaseStream.Position;

                        var contentType = Type.GetType(typeName);
                        var skipStateData = true;

                        if (contentType != null) {
                            var contentInstance = IoC.GetInstance(contentType, null) as ILayoutItem;

                            if (contentInstance != null) {
                                layoutItems.Add(contentId, contentInstance);

                                try {
                                    contentInstance.LoadState(reader);
                                    skipStateData = false;
                                }
                                catch {
                                    skipStateData = true;
                                }
                            }
                        }
                        if (skipStateData)
                            reader.BaseStream.Seek(stateEndPosition, SeekOrigin.Begin);
                    }
                    shellView.LoadLayout(reader.BaseStream, shell.ShowTool, shell.OpenDocument, layoutItems);
                }
            }
            catch {
                return false;
            }
            return true;
        }

        // ----- Internal logics
        private static Type GetTypeFromContractNameAsILayoutItem(ExportAttribute attribute)
        {
            string typeName;
            if ((typeName = attribute.ContractName) == null)
                return null;

            var type = Type.GetType(typeName);
            return typeof(ILayoutItem).IsAssignableFrom(type) ? type : null;
        }
    }
}