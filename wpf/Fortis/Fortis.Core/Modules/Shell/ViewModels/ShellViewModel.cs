﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows;
using Fortis.Core.Caliburn;
using Fortis.Core.Framework;
using Fortis.Core.Framework.Services;
using Fortis.Core.Framework.Themes;
using Fortis.Core.Modules.MainMenu;
using Fortis.Core.Modules.Shell.Services;
using Fortis.Core.Modules.Shell.Views;
using Fortis.Core.Modules.StatusBar;
using Fortis.Core.Modules.ToolBars;
using Prism.Regions;

namespace Fortis.Core.Modules.Shell.ViewModels
{
    [Export(typeof(IShell))]
    public class ShellViewModel : NotifiableObject, IShell
    {
        // ----- Fields
        private bool _activateItemGuard;
        private readonly IRegion _region;

        // ----- Events
        public event EventHandler ActiveDocumentChanging;
        public event EventHandler ActiveDocumentChanged;

#pragma warning disable 649
		[ImportMany(typeof(IModule))]
		private IEnumerable<IModule> _modules;

        [Import]
	    private IThemeManager _themeManager;

        [Import]
        private IMenu _mainMenu;

        [Import]
        private IToolBars _toolBars;

        [Import]
        private IStatusBar _statusBar;

        [Import]
        private ILayoutItemStatePersister _layoutItemStatePersister;
#pragma warning restore 649

        // ----- Properties
        private IShellView _shellView;
	    private bool _closing;
        public IMenu MainMenu => _mainMenu;
        public IToolBars ToolBars => _toolBars;
        public IStatusBar StatusBar => _statusBar;
	    public ILayoutItem ActiveLayoutItem
	    {
	        get { return GetNotifiableProperty<ILayoutItem>(); }
	        set { SetNotifiableProperty(value); }
	    }
        private readonly IEnumerable<ITool> _tools;
        public IEnumerable<ITool> Tools => _tools;
        public IEnumerable<IDocument> Documents => Items;
        public bool ShowFloatingWindowsInTaskbar
        {
            get { return GetNotifiableProperty<bool>(); }
            set
            {
                SetNotifiableProperty(value);
                _shellView?.UpdateFloatingWindows();
            }
        }
	    public virtual string StateFile => @".\ApplicationState.bin";
        public bool HasPersistedState => File.Exists(StateFile);
        public IDocument ActiveItem => _region?.ActiveViews.FirstOrDefault() as IDocument;

        // ----- Constructors
        public ShellViewModel(IRegionManager regionManager)
        {
            _tools = Enumerable.Empty<ITool>();
            _region = regionManager.Regions["ContentRegion"];
        }

        // ----- Override methods
        protected void OnViewLoaded(object view)
	    {
            foreach (var module in _modules)
                foreach (var globalResourceDictionary in module.GlobalResourceDictionaries)
                    Application.Current.Resources.MergedDictionaries.Add(globalResourceDictionary);

	        foreach (var module in _modules)
	            module.PreInitialize();
	        foreach (var module in _modules)
	            module.Initialize();

            if (_themeManager.CurrentTheme == null) {
                if (!_themeManager.SetCurrentTheme(Properties.Settings.Default.ThemeName)) {
                    Properties.Settings.Default.ThemeName = (string)Properties.Settings.Default.Properties["ThemeName"]?.DefaultValue;
                    Properties.Settings.Default.Save();
                    if (!_themeManager.SetCurrentTheme(Properties.Settings.Default.ThemeName))
                        throw new InvalidOperationException("unable to load application theme");
                }
            }

            _shellView = (IShellView)view;
            if (!_layoutItemStatePersister.LoadState(this, _shellView, StateFile)) {
                foreach (var defaultDocument in _modules.SelectMany(x => x.DefaultDocuments))
                    OpenDocument(defaultDocument);
                foreach (var defaultTool in _modules.SelectMany(x => x.DefaultTools))
                    ShowTool((ITool)IoC.GetInstance(defaultTool, null));
            }

            foreach (var module in _modules)
                module.PostInitialize();

            _region.Add(_shellView);
        }
        public void ActivateItem(IDocument item)
        {
            if (_closing || _activateItemGuard)
                return;

            _activateItemGuard = true;

            try {
                RaiseActiveDocumentChanging();

                _region.Activate(item);

                RaiseActiveDocumentChanged();
            }
            finally {
                _activateItemGuard = false;
            }
        }

        // ----- Public methods
        public void ShowTool<TTool>()
            where TTool : ITool
	    {
	        ShowTool(IoC.Get<TTool>());
	    }
	    public void ShowTool(ITool model)
		{
		    if (Tools.Contains(model))
		        model.IsVisible = true;
		    else
		        Tools.Add(model);
		    model.IsSelected = true;
	        ActiveLayoutItem = model;
		}
		public void OpenDocument(IDocument model)
		{
			ActivateItem(model);
		}
		public void CloseDocument(IDocument document)
		{
			DeactivateItem(document, true);
		}
        public void Close()
        {
            Application.Current.MainWindow.Close();
        }

        // ----- Internal logics
	    private void RaiseActiveDocumentChanging()
	    {
            var handler = ActiveDocumentChanging;
	        handler?.Invoke(this, EventArgs.Empty);
	    }
	    private void RaiseActiveDocumentChanged()
	    {
            var handler = ActiveDocumentChanged;
	        handler?.Invoke(this, EventArgs.Empty);
	    }

        // ----- Protected methods
        protected void OnActivationProcessed(IDocument item, bool success)
        {
            if (!ReferenceEquals(ActiveLayoutItem, item))
                ActiveLayoutItem = item;
        }
	    public void DeactivateItem(IDocument item, bool close)
	    {
	        RaiseActiveDocumentChanging();

	        _region.Deactivate(item);

            RaiseActiveDocumentChanged();
	    }
	    protected void OnDeactivate(bool close)
        {
            _closing = true;

            _layoutItemStatePersister.SaveState(this, _shellView, StateFile);
        }
    }
}