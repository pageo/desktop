﻿using System;
using System.Collections.Generic;
using System.IO;
using Fortis.Core.Framework;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace Fortis.Core.Modules.Shell.Views
{
    internal static class LayoutUtility
    {
        // ----- Public methods
        public static void SaveLayout(DockingManager manager, Stream stream)
        {
            var layoutSerializer = new XmlLayoutSerializer(manager);
            layoutSerializer.Serialize(stream);
        }
        public static void LoadLayout(DockingManager manager, Stream stream, Action<IDocument> addDocumentCallback,
                                      Action<ITool> addToolCallback, IDictionary<string, ILayoutItem> items)
        {
            var layoutSerializer = new XmlLayoutSerializer(manager);
            layoutSerializer.LayoutSerializationCallback += (s, e) => {
                    ILayoutItem item;
                    if (items.TryGetValue(e.Model.ContentId, out item)) {
                        e.Content = item;

                        var tool = item as ITool;
                        var anchorable = e.Model as LayoutAnchorable;

                        var document = item as IDocument;
                        var layoutDocument = e.Model as LayoutDocument;

                        if (tool != null && anchorable != null) {
                            addToolCallback(tool);
                            tool.IsVisible = anchorable.IsVisible;
                            if (anchorable.IsActive)
                                tool.Activate();
                            tool.IsSelected = e.Model.IsSelected;
                            return;
                        }

                        if (document != null && layoutDocument != null) {
                            addDocumentCallback(document);
                            layoutDocument.GetType().GetProperty("IsLastFocusedDocument").SetValue(layoutDocument, false, null);
                            document.IsSelected = layoutDocument.IsSelected;
                            return;
                        }
                    }
                    e.Cancel = true;
                };

            try {
                layoutSerializer.Deserialize(stream);
            }
            catch
            {
                // ignored
            }
        }
    }
}