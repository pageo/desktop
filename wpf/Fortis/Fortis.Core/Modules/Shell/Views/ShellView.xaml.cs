﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Fortis.Core.Framework;
using Fortis.Core.Modules.Shell.ViewModels;

namespace Fortis.Core.Modules.Shell.Views
{
	public partial class ShellView : IShellView
	{
        // ----- Constructors
	    public ShellView()
		{
			InitializeComponent();
		}

        // ----- Public methods
	    public void LoadLayout(Stream stream, Action<ITool> addToolCallback, Action<IDocument> addDocumentCallback,
                               IDictionary<string, ILayoutItem> itemsState)
	    {
            LayoutUtility.LoadLayout(Manager, stream, addDocumentCallback, addToolCallback, itemsState);
	    }
        public void SaveLayout(Stream stream)
        {
            LayoutUtility.SaveLayout(Manager, stream);
        }
        public void UpdateFloatingWindows()
        {
            var mainWindow = Window.GetWindow(this);
            var mainWindowIcon = mainWindow?.Icon;
            var showFloatingWindowsInTaskbar = ((ShellViewModel)DataContext).ShowFloatingWindowsInTaskbar;
            foreach (var window in Manager.FloatingWindows)
            {
                window.Icon = mainWindowIcon;
                window.ShowInTaskbar = showFloatingWindowsInTaskbar;
            }
        }

        // ----- Private methods
        private void OnManagerLayoutUpdated(object sender, EventArgs e)
	    {
	        UpdateFloatingWindows();
	    }
	}
}
