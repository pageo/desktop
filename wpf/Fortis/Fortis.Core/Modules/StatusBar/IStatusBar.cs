﻿using System.Windows;
using Caliburn.Micro;
using Fortis.Core.Modules.StatusBar.ViewModels;

namespace Fortis.Core.Modules.StatusBar
{
	public interface IStatusBar
	{
        IObservableCollection<StatusBarItemViewModel> Items { get; }

	    void AddItem(string message, GridLength width);
	}
}