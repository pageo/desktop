﻿using System.Windows;
using Caliburn.Micro;
using Fortis.Core.Caliburn;

namespace Fortis.Core.Modules.StatusBar.ViewModels
{
    public class StatusBarItemViewModel : NotifiableObject
    {
        // ----- Properties
        public int Index
        {
            get { return GetNotifiableProperty<int>(); }
            set { SetNotifiableProperty(value); }
        }
        public string Message
        {
            get { return GetNotifiableProperty<string>(); }
            set { SetNotifiableProperty(value); }
        }
        public GridLength Width { get; }

        // ----- Constructors
        public StatusBarItemViewModel(string message, GridLength width)
        {
            Message = message;
            Width = width;
        }
    }
}