﻿using System.ComponentModel.Composition;
using System.Windows;
using Caliburn.Micro;
using Fortis.Core.Caliburn;

namespace Fortis.Core.Modules.StatusBar.ViewModels
{
	[Export(typeof(IStatusBar))]
	public class StatusBarViewModel : NotifiableObject, IStatusBar
	{
        // ----- Fields
        private readonly StatusBarItemCollection _items;

        // ----- Properties
	    public IObservableCollection<StatusBarItemViewModel> Items => _items;

        // ----- Constructors
	    public StatusBarViewModel()
        {
            _items = new StatusBarItemCollection();
        }

        // ----- Public methods
	    public void AddItem(string message, GridLength width)
	    {
	        Items.Add(new StatusBarItemViewModel(message, width));
	    }

        // ----- Internal logics
	    private class StatusBarItemCollection : BindableCollection<StatusBarItemViewModel>
        {
            // ----- Override methods
            protected override void InsertItemBase(int index, StatusBarItemViewModel item)
            {
                item.Index = index;
                base.InsertItemBase(index, item);
            }
            protected override void SetItemBase(int index, StatusBarItemViewModel item)
            {
                item.Index = index;
                base.SetItemBase(index, item);
            }
        }
	}
}