﻿using Caliburn.Micro;
using Fortis.Core.Modules.ToolBars.Models;

namespace Fortis.Core.Modules.ToolBars
{
    public interface IToolBar : IObservableCollection<ToolBarItemBase>
    {
        
    }
}