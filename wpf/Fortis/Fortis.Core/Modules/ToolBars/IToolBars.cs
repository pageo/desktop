﻿using System.Collections.Generic;
namespace Fortis.Core.Modules.ToolBars
{
    public interface IToolBars
    {
        IEnumerable<IToolBar> Items {get;}
        bool Visible { get; set; }
    }
}