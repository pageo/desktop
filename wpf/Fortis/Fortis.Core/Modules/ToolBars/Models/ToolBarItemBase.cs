﻿using Caliburn.Micro;
using Fortis.Core.Caliburn;

namespace Fortis.Core.Modules.ToolBars.Models
{
	public class ToolBarItemBase : NotifiableObject
	{
        // ----- Properties
		public static ToolBarItemBase Separator => new ToolBarItemSeparator();
	    public virtual string Name => "-";
	}
}