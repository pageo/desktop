﻿using System.ComponentModel.Composition;
using Fortis.Core.Framework.ToolBars;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.ToolBars
{
    internal static class ToolBarDefinitions
    {
        [Export]
        public static ToolBarDefinition StandardToolBar = new ToolBarDefinition(0, Resources.ToolBarStandard);
    }
}