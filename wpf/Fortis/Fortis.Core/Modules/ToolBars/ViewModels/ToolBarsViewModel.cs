﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Fortis.Core.Caliburn;
using Fortis.Core.Modules.ToolBars.Controls;
using Fortis.Core.Modules.ToolBars.Views;

namespace Fortis.Core.Modules.ToolBars.ViewModels
{
    [Export(typeof(IToolBars))]
    public class ToolBarsViewModel : NotifiableObject, IToolBars
    {
        // ----- Fields
        private readonly IEnumerable<IToolBar> _items;
        private readonly IToolBarBuilder _toolBarBuilder;

        // ----- Properties
        public IEnumerable<IToolBar> Items => _items;
        public bool Visible
        {
            get { return GetNotifiableProperty<bool>(); }
            set { SetNotifiableProperty(value); }
        }

        // ----- Constructors
        [ImportingConstructor]
        public ToolBarsViewModel(IToolBarBuilder toolBarBuilder)
        {
            _toolBarBuilder = toolBarBuilder;
            _items = Enumerable.Empty<IToolBar>();
        }

        // ----- Override methods
        protected void OnViewLoaded(object view)
        {
            _toolBarBuilder.BuildToolBars(this);

            foreach (var toolBar in Items)
                ((IToolBarsView) view).ToolBarTray.ToolBars.Add(new MainToolBar
                {
                    ItemsSource = toolBar
                });

            base.OnViewLoaded(view);
        }
    }
}