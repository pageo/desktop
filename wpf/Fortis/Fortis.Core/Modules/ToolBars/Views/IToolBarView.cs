﻿using System.Windows.Controls;

namespace Fortis.Core.Modules.ToolBars.Views
{
    public interface IToolBarsView
    {
        ToolBarTray ToolBarTray { get; }
    }
}