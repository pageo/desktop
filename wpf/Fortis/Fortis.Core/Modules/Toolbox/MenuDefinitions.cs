﻿using System.ComponentModel.Composition;
using Fortis.Core.Framework.Menus;
using Fortis.Core.Modules.Toolbox.Commands;

namespace Fortis.Core.Modules.Toolbox
{
    public static class MenuDefinitions
    {
        [Export]
        public static MenuItemDefinition ViewToolboxMenuItem = new CommandMenuItemDefinition<ViewToolboxCommandDefinition>(
            MainMenu.MenuDefinitions.ViewToolsMenuGroup, 4);
    }
}