﻿using System;
using System.Collections.Generic;
using Fortis.Core.Modules.Toolbox.Models;

namespace Fortis.Core.Modules.Toolbox.Services
{
    public interface IToolboxService
    {
        IEnumerable<ToolboxItem> GetToolboxItems(Type documentType);
    }
}