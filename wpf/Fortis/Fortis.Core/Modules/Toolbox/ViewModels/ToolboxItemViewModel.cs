﻿using System;
using Fortis.Core.Modules.Toolbox.Models;

namespace Fortis.Core.Modules.Toolbox.ViewModels
{
    public class ToolboxItemViewModel
    {
        public ToolboxItem Model { get; }

        public string Name => Model.Name;

        public virtual string Category => Model.Category;

        public virtual Uri IconSource => Model.IconSource;

        public ToolboxItemViewModel(ToolboxItem model)
        {
            Model = model;
        }
    }
}