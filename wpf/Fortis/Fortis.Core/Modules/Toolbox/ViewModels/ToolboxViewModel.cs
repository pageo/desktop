﻿using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Caliburn.Micro;
using Fortis.Core.Framework;
using Fortis.Core.Framework.Services;
using Fortis.Core.Modules.Toolbox.Services;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.Toolbox.ViewModels
{
    [Export(typeof(IToolbox))]
    public class ToolboxViewModel : Tool, IToolbox
    {
        private readonly IToolboxService _toolboxService;

        public override PaneLocation PreferredLocation => PaneLocation.Left;

        private readonly BindableCollection<ToolboxItemViewModel> _items;
        public IObservableCollection<ToolboxItemViewModel> Items => _items;

        [ImportingConstructor]
        public ToolboxViewModel(IShell shell, IToolboxService toolboxService)
        {
            DisplayName = Resources.ToolboxDisplayName;

            _items = new BindableCollection<ToolboxItemViewModel>();

            var groupedItems = CollectionViewSource.GetDefaultView(_items);
            groupedItems.GroupDescriptions.Add(new PropertyGroupDescription(Resources.ToolboxCategory));

            _toolboxService = toolboxService;

            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                return;

            shell.ActiveDocumentChanged += (sender, e) => RefreshToolboxItems(shell);
            RefreshToolboxItems(shell);
        }

        private void RefreshToolboxItems(IShell shell)
        {
            _items.Clear();

            if (shell.ActiveItem == null) 
                return;

            _items.AddRange(_toolboxService.GetToolboxItems(shell.ActiveItem.GetType())
                .Select(x => new ToolboxItemViewModel(x)));
        }
    }
}