﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.UndoRedo.Commands
{
    [CommandDefinition]
    public class RedoCommandDefinition : CommandDefinition
    {
        public const string CommandName = "Edit.Redo";

        public override string Name => CommandName;

        public override string Text => Resources.EditRedoCommandText;

        public override string ToolTip => Resources.EditRedoCommandToolTip;

        public override Uri IconSource => new Uri("pack://application:,,,/Fortis.Core;component/Resources/Icons/Redo.png");

        [Export]
        public static CommandKeyboardShortcut KeyGesture = new CommandKeyboardShortcut<RedoCommandDefinition>(new KeyGesture(Key.Y, ModifierKeys.Control));
    }
}