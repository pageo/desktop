﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.UndoRedo.Commands
{
    [CommandDefinition]
    public class UndoCommandDefinition : CommandDefinition
    {
        public const string CommandName = "Edit.Undo";

        public override string Name => CommandName;

        public override string Text => Resources.EditUndoCommandText;

        public override string ToolTip => Resources.EditUndoCommandToolTip;

        public override Uri IconSource => new Uri("pack://application:,,,/Fortis.Core;component/Resources/Icons/Undo.png");

        [Export]
        public static CommandKeyboardShortcut KeyGesture = new CommandKeyboardShortcut<UndoCommandDefinition>(new KeyGesture(Key.Z, ModifierKeys.Control));
    }
}