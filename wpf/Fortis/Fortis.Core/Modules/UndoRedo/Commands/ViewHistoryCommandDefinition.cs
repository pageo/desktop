﻿using Fortis.Core.Framework.Commands;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.UndoRedo.Commands
{
    [CommandDefinition]
    public class ViewHistoryCommandDefinition : CommandDefinition
    {
        public const string CommandName = "View.History";

        public override string Name => CommandName;

        public override string Text => Resources.ViewHistoryCommandText;

        public override string ToolTip => Resources.ViewHistoryCommandToolTip;
    }
}