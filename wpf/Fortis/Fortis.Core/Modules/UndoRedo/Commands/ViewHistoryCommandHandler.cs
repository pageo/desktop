﻿using System.ComponentModel.Composition;
using System.Threading.Tasks;
using Fortis.Core.Framework.Commands;
using Fortis.Core.Framework.Services;
using Fortis.Core.Framework.Threading;

namespace Fortis.Core.Modules.UndoRedo.Commands
{
    [CommandHandler]
    public class ViewHistoryCommandHandler : CommandHandlerBase<ViewHistoryCommandDefinition>
    {
        private readonly IShell _shell;

        [ImportingConstructor]
        public ViewHistoryCommandHandler(IShell shell)
        {
            _shell = shell;
        }

        public override Task Run(Command command)
        {
            _shell.ShowTool<IHistoryTool>();
            return TaskUtility.Completed;
        }
    }
}