﻿using Fortis.Core.Framework;

namespace Fortis.Core.Modules.UndoRedo
{
    public interface IHistoryTool : ITool
    {
        IUndoRedoManager UndoRedoManager { get; set; }
    }
}