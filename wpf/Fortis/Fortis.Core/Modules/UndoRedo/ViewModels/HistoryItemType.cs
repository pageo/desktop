﻿namespace Fortis.Core.Modules.UndoRedo.ViewModels
{
    public enum HistoryItemType
    {
        InitialState,
        Undo,
        Current,
        Redo
    }
}