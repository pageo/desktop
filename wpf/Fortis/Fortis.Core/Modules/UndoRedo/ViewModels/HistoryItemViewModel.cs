﻿using Caliburn.Micro;
using Fortis.Core.Caliburn;

namespace Fortis.Core.Modules.UndoRedo.ViewModels
{
    public class HistoryItemViewModel : NotifiableObject
    {
        // ----- Fields
        private readonly string _name;

        // ----- Properties
        public IUndoableAction Action { get; }
        public string Name => _name ?? Action.Name;
        public HistoryItemType ItemType { get; }

        // ----- Constructors
        public HistoryItemViewModel(IUndoableAction action, HistoryItemType itemType)
        {
            Action = action;
            ItemType = itemType;
        }
        public HistoryItemViewModel(string name, HistoryItemType itemType)
        {
            _name = name;
            ItemType = itemType;
        }
    }
}