﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using Caliburn.Micro;
using Fortis.Core.Framework;
using Fortis.Core.Framework.Services;
using Fortis.Core.Properties;

namespace Fortis.Core.Modules.UndoRedo.ViewModels
{
    [Export(typeof(IHistoryTool))]
    public class HistoryViewModel : Tool, IHistoryTool
    {
        // ----- Fields
        private readonly BindableCollection<HistoryItemViewModel> _historyItems;
        private bool _internallyTriggeredChange;
        private IUndoRedoManager _undoRedoManager;

        // ----- Properties
        public override PaneLocation PreferredLocation => PaneLocation.Right;
        public IUndoRedoManager UndoRedoManager
        {
            get { return _undoRedoManager; }
            set
            {
                if (_undoRedoManager != null) {
                    _undoRedoManager.UndoStack.CollectionChanged -= OnUndoRedoStackChanged;
                    _undoRedoManager.RedoStack.CollectionChanged -= OnUndoRedoStackChanged;
                }

                _undoRedoManager = value;
                if (_undoRedoManager != null) {
                    _undoRedoManager.UndoStack.CollectionChanged += OnUndoRedoStackChanged;
                    _undoRedoManager.RedoStack.CollectionChanged += OnUndoRedoStackChanged;
                }
                RefreshHistory();
            }
        }
        public int SelectedIndex
        {
            get { return GetNotifiableProperty<int>(); }
            set
            {
                SetNotifiableProperty(value);
                TriggerInternalHistoryChange(() => UndoOrRedoToInternal(HistoryItems[value - 1]));
            }
        }
        public IObservableCollection<HistoryItemViewModel> HistoryItems => _historyItems;

        // ----- Constructors
        [ImportingConstructor]
        public HistoryViewModel(IShell shell)
        {
            DisplayName = Resources.HistoryDisplayName;
            _historyItems = new BindableCollection<HistoryItemViewModel>();

            if (shell == null)
                return;

            shell.ActiveDocumentChanged += (sender, e) => {
                UndoRedoManager = shell.ActiveItem?.UndoRedoManager;
            };
            if (shell.ActiveItem != null)
                UndoRedoManager = shell.ActiveItem.UndoRedoManager;
        }

        // ----- Public methods
        public void UndoOrRedoTo(HistoryItemViewModel item)
        {
            TriggerInternalHistoryChange(() => UndoOrRedoToInternal(item));
            UpdateSelectedIndexOnly(_historyItems.IndexOf(_historyItems.First(x => x.Action == item.Action)) + 1);
        }

        // ----- Internal logics
        private void OnUndoRedoStackChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RefreshHistory();
        }
        private void RefreshHistory()
        {
            _historyItems.Clear();
            if (_undoRedoManager != null) {
                _historyItems.Add(new HistoryItemViewModel(Resources.HistoryInitialState,
                    (_undoRedoManager.UndoStack.Any() ? HistoryItemType.InitialState : HistoryItemType.Current)));
                for (var i = 0; i < _undoRedoManager.UndoStack.Count; i++)
                    _historyItems.Add(new HistoryItemViewModel(_undoRedoManager.UndoStack[i],
                        (i == _undoRedoManager.UndoStack.Count - 1) ? HistoryItemType.Current : HistoryItemType.Undo));
                for (var i = _undoRedoManager.RedoStack.Count - 1; i >= 0; i--)
                    _historyItems.Add(new HistoryItemViewModel(
                        _undoRedoManager.RedoStack[i],
                        HistoryItemType.Redo));
            }

            if (!_internallyTriggeredChange)
                UpdateSelectedIndexOnly(_historyItems.Count);
        }
        private void TriggerInternalHistoryChange(System.Action callback)
        {
            _internallyTriggeredChange = true;
            callback();
            _internallyTriggeredChange = false;
        }
        private void UpdateSelectedIndexOnly(int selectedIndex)
        {
            SelectedIndex = selectedIndex;
            NotifyOfPropertyChange(() => SelectedIndex);
        }
        private void UndoOrRedoToInternal(HistoryItemViewModel item)
        {
            switch (item.ItemType)
            {
                case HistoryItemType.InitialState:
                    _undoRedoManager.UndoAll();
                    break;
                case HistoryItemType.Undo:
                    _undoRedoManager.UndoTo(item.Action);
                    break;
                case HistoryItemType.Current:
                    break;
                case HistoryItemType.Redo:
                    _undoRedoManager.RedoTo(item.Action);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}