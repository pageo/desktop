﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Markup;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Fortis Core")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("Retail")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Fortis")]
[assembly: AssemblyCopyright("Copyright ©2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("35ea041d-cf35-45ed-9675-7e199505612b")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0.0")]

[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]

[assembly: XmlnsDefinition("http://schemas.timjones.tw/gemini", "Fortis.Core")]
[assembly: XmlnsDefinition("http://schemas.timjones.tw/gemini", "Fortis.Core.Framework.Controls")]