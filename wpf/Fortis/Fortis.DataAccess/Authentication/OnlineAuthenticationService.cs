﻿using Fortis.Business.Infrastructure.Authentication;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.DataAccess.Repositories.Base;

namespace Fortis.DataAccess.Authentication
{
    public class OnlineAuthenticationService : IAuthenticationService
    {
        // ----- Fields
        private readonly IUserRepository _userRepository;
        private readonly ApiToken _apiToken;

        // ----- Constructors
        public OnlineAuthenticationService(IUserRepository userRepository, ApiToken apiToken)
        {
            _userRepository = userRepository;
            _apiToken = apiToken;
        }

        // ----- Public methods
        public bool ConnectUser()
        {
            // $"{Environment.UserDomainName}\\{Environment.UserName}"
            _apiToken.Value = _userRepository.Authenticate("opage4", "patrick");
            return string.IsNullOrEmpty(_apiToken.Value) == false;
        }
    }
}
