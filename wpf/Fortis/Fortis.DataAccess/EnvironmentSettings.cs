﻿namespace Fortis.DataAccess
{
    public class EnvironmentSettings : JsonAppSettings<EnvironmentSettings>
    {
        // ----- Properties
        public string FortisApiServiceUrl { get; set; }
        public string GoogleMapsApiKey { get; set; }
    }
}
