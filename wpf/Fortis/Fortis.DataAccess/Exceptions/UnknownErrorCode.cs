﻿using System;
using System.Net;

namespace Fortis.DataAccess.Exceptions
{
    public class UnknownErrorCode : Exception
    {
        // ----- Constantes
        private const string BaseMessage = "Un code d'erreur non géré a été retourné par le serveur fortis : {0}. {1}";

        // ----- Properties
        public HttpStatusCode Code { get; set; }

        // ----- Constructors
        public UnknownErrorCode(HttpStatusCode code, string message, Exception innerException)
            : base(string.Format(BaseMessage, (int)code, message), innerException)
        {
            Code = code;
        }
    }
}
