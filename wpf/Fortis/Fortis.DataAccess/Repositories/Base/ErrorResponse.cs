namespace Fortis.DataAccess.Repositories.Base
{
    public class ErrorResponse
    {
        // ----- Properties
        public string Message { get; set; }
    }
}