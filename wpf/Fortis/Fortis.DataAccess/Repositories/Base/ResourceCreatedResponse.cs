namespace Fortis.DataAccess.Repositories.Base
{
    public class ResourceCreatedResponse
    {
        // ----- Properties
        public int Id { get; set; }
    }
}