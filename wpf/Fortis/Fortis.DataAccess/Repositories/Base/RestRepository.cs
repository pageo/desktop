﻿using System;
using System.Net;
using System.Threading.Tasks;
using Fortis.DataAccess.Exceptions;
using Newtonsoft.Json;
using RestSharp;

namespace Fortis.DataAccess.Repositories.Base
{
    public abstract class RestRepository
    {
        // ----- Fields
        private readonly ApiToken _apiToken;
        private readonly RestClient _client;

        // ----- Constructors
        protected RestRepository(EnvironmentSettings settings, ApiToken apiToken)
        {
            _apiToken = apiToken;
            _client = new RestClient(settings.FortisApiServiceUrl);
        }

        // ----- Protected methods
        protected T Get<T>(RestRequest request)
        {
            ConfigureAuthenticableRequest(request);
            request.Method = Method.GET;
            return Execute<T>(request);
        }
        protected async Task<T> GetAsync<T>(RestRequest request)
        {
            ConfigureAuthenticableRequest(request);
            request.Method = Method.GET;
            return await ExecuteAsync<T>(request);
        }
        protected ResourceCreatedResponse Post(RestRequest request)
        {
            ConfigureAuthenticableRequest(request);
            request.Method = Method.POST;

            var response = _client.Execute(request);
            if (response.StatusCode == HttpStatusCode.Created)
                return Deserialize<ResourceCreatedResponse>(response.Content);
            if (response.StatusCode == HttpStatusCode.BadRequest) {
                var error = Deserialize<ErrorResponse>(response.Content);
                throw new Exception(error.Message);
            }
            throw ErrorFallback(response);
        }
        protected void Put(IRestRequest request)
        {
            ConfigureAuthenticableRequest(request);
            request.Method = Method.PUT;

            var response = _client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw ErrorFallback(response);
        }
        protected void Delete(IRestRequest request)
        {
            ConfigureAuthenticableRequest(request);
            request.Method = Method.DELETE;

            var response = _client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw ErrorFallback(response);
        }
        protected T AnonymousPost<T>(IRestRequest request)
        {
            ConfigureAnonymousRequest(request);
            request.Method = Method.POST;
            return Execute<T>(request);
        }

        // ----- Internal logics
        private T Execute<T>(IRestRequest request)
        {
            var response = _client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
                return Deserialize<T>(response.Content);
            throw ErrorFallback(response);
        }
        private async Task<T> ExecuteAsync<T>(IRestRequest request)
        {
            var response = await _client.ExecuteTaskAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
                return Deserialize<T>(response.Content);
            throw ErrorFallback(response);
        }

        // ----- Utils
        private void ConfigureAuthenticableRequest(IRestRequest request)
        {
            ConfigureAnonymousRequest(request);
            request.AddHeader("Token", _apiToken.Value);
        }
        private static void ConfigureAnonymousRequest(IRestRequest request)
        {
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Accept-Encoding", "gzip");
        }
        private static Exception ErrorFallback(IRestResponse response)
        {
            var errorMessage = GetErrorMessage(response);
            if (response.StatusCode == HttpStatusCode.InternalServerError)
                return new InternalServerError(errorMessage, response.ErrorException);
            if (response.StatusCode == HttpStatusCode.BadRequest)
                return new Exception("Requête invalide. " + errorMessage);
            return new UnknownErrorCode(response.StatusCode, errorMessage, response.ErrorException);
        }
        private static string GetErrorMessage(IRestResponse response)
        {
            if (string.IsNullOrEmpty(response.Content))
                return response.ErrorMessage;
            if (response.ContentType == "application/json")
                return Deserialize<ErrorResponse>(response.Content).Message;
            return response.StatusDescription;
        }
        private static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}