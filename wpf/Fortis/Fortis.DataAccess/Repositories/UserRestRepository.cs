﻿using Fortis.Business.Infrastructure.Repositories;
using Fortis.DataAccess.Repositories.Base;
using RestSharp;

namespace Fortis.DataAccess.Repositories
{
    public class UserRestRepository : RestRepository, IUserRepository
    {
        // ----- Constructors
        public UserRestRepository(EnvironmentSettings settings, ApiToken apiToken) : base(settings, apiToken) { }

        // ----- Public methods
        public string Authenticate(string login, string password)
        {
            var request = new RestRequest("authenticate");
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new {
                LoginOrEmail = login,
                Password = password
            });
            return AnonymousPost<string>(request);
        }
        public string Register(string fullName, string login, string password)
        {
            var request = new RestRequest("register");
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new {
                FullName = fullName,
                LoginOrEmail = login,
                Password = password
            });
            return AnonymousPost<string>(request);
        }
    }
}
