﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Models;
using Fortis.Business.ViewModels.Exceptions;
using Fortis.DataAccess.Exceptions;
using Fortis.DataAccess.Repositories.Base;
using RestSharp;

namespace Fortis.DataAccess.Repositories
{
    public class WorkingFolderRestRepository : RestRepository, IWorkingFolderRepository
    {
        // ----- Constructors
        public WorkingFolderRestRepository(EnvironmentSettings settings, ApiToken apiToken) : base(settings, apiToken) { }

        // ----- Public methods
        public IEnumerable<WorkingFolderListItem> GetList()
        {
            var request = new RestRequest("workingfolders");
            return Get<IEnumerable<WorkingFolderListItem>>(request);
        }
        public int Create(string name)
        {
            try
            {
                var request = new RestRequest("workingfolders") {
                    RequestFormat = DataFormat.Json
                };
                request.AddBody(new {
                    name
                });
                return Post(request).Id;
            }
            catch (UnknownErrorCode ex) {
                if (ex.Code == HttpStatusCode.Conflict)
                    throw new WorkingFolderWithSameNameAlreadyExists();
                throw;
            }
        }
        public WorkingFolder Load(int workingFolderId)
        {
            try
            {
                var loadDetailsTask = Task.Factory.StartNew(() => GetWorkingFolderDetails(workingFolderId));
                var loadAssetsTask = Task.Factory.StartNew(() => GetAssetsOfWorkingFolder(workingFolderId));

                Task.WaitAll(loadDetailsTask, loadAssetsTask);
                return new WorkingFolder(loadDetailsTask.Result, loadAssetsTask.Result);
            }
            catch (AggregateException ex) {
                throw ex.InnerExceptions.First();
            }
        }
        public void Delete(int workingFolderId)
        {
            var request = new RestRequest($"workingFolders/{workingFolderId}") {
                RequestFormat = DataFormat.Json
            };
            Delete(request);
        }
        public void Save(int workingFolderId)
        {
            var request = new RestRequest($"workingFolders/{workingFolderId}") {
                RequestFormat = DataFormat.Json
            };
            Put(request);
        }

        // ----- Internal logics
        private WorkingFolderDetails GetWorkingFolderDetails(int workingFolderId)
        {
            var request = new RestRequest($"workingfolders/{workingFolderId}");
            return Get<WorkingFolderDetails>(request);
        }
        private IEnumerable<Asset> GetAssetsOfWorkingFolder(int workingFolderId)
        {
            const int take = 20000;
            var skip = 0;
            int lastLoadedCount;

            var assets = new List<Asset>();

            do
            {
                var pageOfAssets = GetAssetsOfWorkingFolder(workingFolderId, skip, take);
                assets.AddRange(pageOfAssets);
                lastLoadedCount = pageOfAssets.Count;
                skip += lastLoadedCount;
            }
            while (lastLoadedCount == take);

            return assets;
        }
        private IReadOnlyCollection<Asset> GetAssetsOfWorkingFolder(int workingFolderId, int skip, int take)
        {
            var request = new RestRequest($"workingfolders/{workingFolderId}/assets");
            request.AddParameter("skip", skip);
            request.AddParameter("take", take);
            return Get<List<Asset>>(request);
        }
    }
}
