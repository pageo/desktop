﻿using System.Linq;
using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using Fortis.Common.Records;
using Fortis.Tests.Utils;
using NFluent;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class CardViewModelTests
    {
        // ----- Fields
        private readonly CardViewModel _viewModel;
        private readonly Collection _collection;
        private readonly Bus _bus;

        // ----- Constructors
        public CardViewModelTests()
        {
            _bus = new Bus();

            var dockManager = Substitute.For<DockManager>();
            _viewModel = new CardViewModel(_bus, dockManager);

            _collection = new CollectionBuilder()
                .WithField<string>("Code")
                .WithField<string>("Nom de commune")
                .WithField<string>("Nom de région")
                .WithField<double?>("X")
                .WithField<double?>("Y")
                .Build();
        }

        // ----- Tests
        [Fact]
        public void update_current_record_when_record_is_selected_and_working_folder_loaded()
        {
            // Arranges
            _collection.AddRecord()
                .With("Code", "1234")
                .With("Nom de commune", "BEZIERS")
                .With("Nom de région", "OCCITANIE")
                .With("X", 43.3481064)
                .With("Y", 3.199201);

            // Acts
            _bus.Send(new WorkingFolderLoaded(_collection, null));
            var record = _collection.Records.Last();
            record.SetCurrent();

            // Asserts
            Check.That(_viewModel.CurrentRecord).IsEqualTo(record.Changes);
        }
    }
}
