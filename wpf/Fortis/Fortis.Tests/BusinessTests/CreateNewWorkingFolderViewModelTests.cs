﻿using System;
using System.Threading.Tasks;
using Fortis.Business.Base;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Models;
using Fortis.Business.Services;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using Fortis.Business.ViewModels.Exceptions;
using Fortis.Tests.Utils;
using NFluent;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class CreateNewWorkingFolderViewModelTests
    {
        // ----- Constants
        private const int SOME_WORKING_FOLDER_ID = 99;

        // ----- Fields
        private readonly IWorkingFolderRepository _workingFolderRepository;
        private readonly IMessageBoxService _messageBoxService;
        private WorkingFolder SOME_WORKING_FOLDER;
        private readonly CreateNewWorkingFolderViewModel _viewModel;
        private readonly IBus _bus;

        // ----- Constructors
        public CreateNewWorkingFolderViewModelTests()
        {
            _bus = Substitute.For<IBus>();

            _workingFolderRepository = Substitute.For<IWorkingFolderRepository>();
            _workingFolderRepository.Create(Arg.Any<string>()).Returns(1);
            _workingFolderRepository.Load(Arg.Any<int>()).Returns(SOME_WORKING_FOLDER);

            _messageBoxService = Substitute.For<IMessageBoxService>();

            _viewModel = new CreateNewWorkingFolderViewModel(_bus, _workingFolderRepository, _messageBoxService);
            _viewModel.Initialize();
        }

        // ----- Tests
        [Fact]
        public void have_all_empty_fields_when_initialized()
        {
            // Asserts
            Check.That(_viewModel.Name).IsNull();
            Check.That(_viewModel.EffectiveDate).IsNull();
        }

        [Fact]
        public void name_is_mandatory_field_for_working_folder_creation()
        {
            // Acts
            _viewModel.Name = null;
            _viewModel.EffectiveDate = DateTime.Now;

            // Asserts
            Check.That(_viewModel.CreateAndLoadWorkingFolderCommand).IsDisabled();
        }

        [Fact]
        public void effective_date_is_optional_field_for_working_folder_creation()
        {
            // Acts
            _viewModel.Name = "my working folder";
            _viewModel.EffectiveDate = null;

            // Asserts
            Check.That(_viewModel.CreateAndLoadWorkingFolderCommand.CanExecute()).IsTrue();
        }

        [Fact]
        public async Task request_working_folder_repository_when_executing_create_command()
        {
            // Arrange
            _viewModel.Name = "my working folder";

            // Act
            await _viewModel.CreateAndLoadWorkingFolderCommand.ExecuteAsync();

            // Assert
            _workingFolderRepository.Received(1).Create("my working folder");
        }

        [Fact]
        public async Task send_collection_loaded_event_to_bus_when_creation_succeeds()
        {
            // Arrange
            _viewModel.Name = "my working folder";

            // Act
            await _viewModel.CreateAndLoadWorkingFolderCommand.ExecuteAsync();

            // Assert
            _bus.Received(1).Send(Arg.Any<WorkingFolderLoaded>());
        }

        [Fact]
        public async Task load_created_working_folder()
        {
            // Data
            _workingFolderRepository.Create(Arg.Any<string>()).Returns(SOME_WORKING_FOLDER_ID);

            // Arrange
            _viewModel.Name = "my working folder";

            // Act
            await _viewModel.CreateAndLoadWorkingFolderCommand.ExecuteAsync();

            // Assert
            _workingFolderRepository.Received(1).Load(SOME_WORKING_FOLDER_ID);
        }

        [Fact]
        public async Task raise_exception_when_working_folder_already_exists()
        {
            // Data
            _workingFolderRepository.Create(Arg.Any<string>()).Throws(new WorkingFolderWithSameNameAlreadyExists());

            // Arrange
            _viewModel.Name = "my existing working folder";

            // Act
            await _viewModel.CreateAndLoadWorkingFolderCommand.ExecuteAsync();

            // Assert
            _messageBoxService.Received(1).ShowInformation($"Un dossier de travail avec le nom \"my existing working folder\" existe déjà. {Environment.NewLine}Veuillez saisir un autre nom.");
        }
    }
}
