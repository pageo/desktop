﻿using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using Fortis.Tests.Utils;
using NFluent;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class GoogleStreetViewModelTests
    {
        // ----- FIelds
        private readonly Bus _bus;
        private readonly GoogleStreetViewModel _viewModel;

        // ----- Constructors
        public GoogleStreetViewModelTests()
        {
            _bus = new Bus();
            var dockManager = Substitute.For<DockManager>();

            _viewModel = new GoogleStreetViewModel(_bus, dockManager);
        }

        // ----- Tests
        [Fact]
        public void update_position_when_map_position_updated()
        {
            // Acts
            _bus.Send(new MapPositionUpdated(43.3481064, 3.199201));

            // Asserts
            Check.That(new {
                X = 43.3481064,
                Y = 3.199201
            }).HasPropertiesWithSameValues(_viewModel.Position);
        }
    }
}
