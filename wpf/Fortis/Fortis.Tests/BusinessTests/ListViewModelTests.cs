﻿using System.Collections.Generic;
using System.Linq;
using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using Fortis.Common.Records;
using Fortis.Tests.Utils;
using NFluent;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class ListViewModelTests
    {
        // ----- FIelds
        private readonly Bus _bus;
        private readonly MapViewModel _mapViewModel;
        private readonly Collection _collection;
        private readonly ListViewModel _viewModel;

        // ----- Constructors
        public ListViewModelTests()
        {
            _bus = new Bus();
            var dockManager = Substitute.For<DockManager>();
            _mapViewModel = new MapViewModel(dockManager);

            _viewModel = new ListViewModel(_bus, _mapViewModel, dockManager);

            _collection = new CollectionBuilder()
                .WithField<string>("Code")
                .WithField<string>("Nom de commune")
                .WithField<string>("Nom de région")
                .WithField<double?>("X")
                .WithField<double?>("Y")
                .Build();
        }

        // ----- Tests
        [Fact]
        public void update_current_position()
        {
            // Arranges
            _collection.AddRecord()
                .With("Code", "1234")
                .With("Nom de commune", "BEZIERS")
                .With("Nom de région", "OCCITANIE")
                .With("X", 43.3481064)
                .With("Y", 3.199201);

            var workingFolder = new WorkingFolderBuilder()
                .WithId(1)
                .Build();

            // Acts
            _bus.Send(new WorkingFolderLoaded(_collection, workingFolder));
            var record = _collection.Records.Last();
            record.SetCurrent();

            // Asserts
            Check.That(new
            {
                X = 43.3481064,
                Y = 3.199201
            }).HasPropertiesWithSameValues(_mapViewModel.CurrentPosition);
        }

        [Fact]
        public void update_current_position_on_asset_selected()
        {
            // Arranges
            _collection.AddRecord()
                .With("Code", "1234")
                .With("Nom de commune", "BEZIERS")
                .With("Nom de région", "OCCITANIE")
                .With("X", 43.3481064)
                .With("Y", 3.199201);

            var workingFolder = new WorkingFolderBuilder().
                WithId(1).
                WithAssets(new List<Asset> { new Asset{ Code = "1234", TownName = "BEZIERS" , RegionName = "OCCITANIE", X = 43.3481064 , Y = 3.199201 } })
                .Build();

            // Acts
            _bus.Send(new WorkingFolderLoaded(_collection, workingFolder));
            _viewModel.SelectedAsset = _viewModel.Assets.Last();

            // Asserts
            Check.That(_viewModel.SelectedAsset.Code).IsEqualTo(_collection.CurrentRecord.GetValue<string>("Code"));
        }
    }
}
