﻿using Fortis.Business.Base;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using NFluent;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class LoadViewModelTests
    {
        // ----- Fields
        private readonly Bus _bus;
        private readonly LoadViewModel _viewModel;

        // ----- Constructors
        public LoadViewModelTests()
        {
            _bus = new Bus();

            _viewModel = new LoadViewModel(_bus);
            _viewModel.Initialize();
        }

        // ----- Tests
        [Fact]
        public void change_visible_on_show()
        {
            // Acts
            _viewModel.ShowCommand.Execute(null);

            // Asserts
            Check.That(_viewModel.IsVisible).IsEqualTo(true);
        }

        [Fact]
        public void change_to_invisible_on_working_folder_loaded()
        {
            // Acts
            _bus.Send(new WorkingFolderLoaded(null, null));

            // Assets
            Check.That(_viewModel.IsVisible).IsEqualTo(false);
        }
    }
}
