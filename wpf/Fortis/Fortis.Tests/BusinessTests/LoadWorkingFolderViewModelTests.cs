﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fortis.Business.Base;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Models;
using Fortis.Business.Services;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using NFluent;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class LoadWorkingFolderViewModelTests
    {
        // ----- Fields
        private readonly Bus _bus;
        private readonly LoadWorkingFolderViewModel _viewModel;
        private IEnumerable<WorkingFolderListItem> _workingFoldersListItems = Enumerable.Empty<WorkingFolderListItem>();

        // ----- Constructors
        public LoadWorkingFolderViewModelTests()
        {
            _bus = new Bus();

            var workingFolderRepository = Substitute.For<IWorkingFolderRepository>();
            workingFolderRepository.GetList().Returns(info =>_workingFoldersListItems);

            var collectionFactory = Substitute.For<ICollectionFactory>();


            _viewModel = new LoadWorkingFolderViewModel(_bus, workingFolderRepository, collectionFactory);
        }

        // ----- Tests
        [Fact]
        public async Task refresh_working_folders_list_on_event_workingFolderUpdated()
        {
            // Data
            _workingFoldersListItems = new[] {
                new WorkingFolderListItem { Id = 1, Name = "#1", CreationDate = DateTime.Now, IsOwner = true },
                new WorkingFolderListItem { Id = 2, Name = "#2", CreationDate = DateTime.Now, IsOwner = false }
            };

            // Acts
            await _bus.SendAsync(new WorkingFolderUpdated());

            // Asserts
            Check.That(_viewModel.ExistingWorkingFolders).HasSize(2);
        }
    }
}
