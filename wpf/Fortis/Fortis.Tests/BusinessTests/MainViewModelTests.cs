﻿using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.Services;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using Fortis.Common.Records;
using Fortis.Tests.Utils;
using NFluent;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class MainViewModelTests
    {
        // ----- Fields
        private readonly Bus _bus;
        private readonly MainViewModel _viewModel;

        // ----- Constructors
        public MainViewModelTests()
        {
            _bus = new Bus();

            var messageBoxService = Substitute.For<IMessageBoxService>();
            var collectionManager = Substitute.For<CollectionManager>();
            var dockManager = Substitute.For<DockManager>();
            var loadViewModel = new LoadViewModel(_bus);
            var saveViewModel = new SaveViewModel();

            _viewModel = new MainViewModel(_bus, messageBoxService, collectionManager, dockManager, loadViewModel, saveViewModel);
        }

        // ----- Tests
        [Fact]
        public void update_working_folder_info_title_on_working_folder_loaded()
        {
            // Arranges
            var workingFolder = new WorkingFolderBuilder().WithName("test").Build();

            // Acts
            _bus.Send(new WorkingFolderLoaded(null, workingFolder));

            // Asserts
            Check.That(_viewModel.WorkingFolderInfoTitle).Contains("test");
        }
    }
}
