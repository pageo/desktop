﻿using System.Linq;
using Fortis.Business.Base;
using Fortis.Business.Models;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using Fortis.Tests.Utils;
using NFluent;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.BusinessTests
{
    public class MarkerViewModelTests
    {
        // ----- Fields
        private readonly Bus _bus;
        private readonly MarkerViewModel _viewModel;

        // ----- Constructors
        public MarkerViewModelTests()
        {
            _bus = new Bus();
            var dockManager = Substitute.For<DockManager>();
            var createMarkerStandardViewModel = new CreateMarkerStandardViewModel(_bus);

            _viewModel = new MarkerViewModel(_bus, dockManager, createMarkerStandardViewModel);
        }

        // ----- Tests
        [Fact]
        public void init_markers_with_default_markers_on_working_folder_loaded()
        {
            // Arranges
            var collection = new CollectionBuilder().Build();

            // Acts
            _bus.Send(new WorkingFolderLoaded(collection, null));

            // Asserts
            Check.That(_viewModel.Markers).HasSize(1);
            Check.That(_viewModel.Markers.LastOrDefault()?.Name).IsEqualTo("Tout");
        }
    }
}
