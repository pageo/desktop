﻿using System.Windows.Input;
using Fortis.Business.ViewModels.Base;
using NFluent;
using NFluent.Extensibility;

namespace Fortis.Tests.Utils
{
    public static class CheckCommandExtensions
    {
        // ----- Public methods
        public static void IsDisabled(this ICheck<ICommand> check)
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);
            var command = checker.Value;
            Check.That(command.CanExecute(null)).IsFalse();
        }
        public static void IsEnabled(this ICheck<ICommand> check)
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);
            var command = checker.Value;
            Check.That(command.CanExecute(null)).IsTrue();
        }
        public static void IsDisabled(this ICheck<IAsyncCommand> check)
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);
            var command = checker.Value;
            Check.That(command.CanExecute()).IsFalse();
        }
        public static void IsEnabled(this ICheck<IAsyncCommand> check)
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);
            var command = checker.Value;
            Check.That(command.CanExecute()).IsTrue();
        }
        public static void HasPropertiesWithSameValues(this ICheck<object> check, object expected)
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);
            var source = checker.Value;

            var properties = source.GetType().GetProperties();
            foreach (var propertyInfo in properties) {
                var sourceValue = propertyInfo.GetValue(source);
                var propertyInfoTarget = expected.GetType().GetProperty(propertyInfo.Name);
                if (propertyInfoTarget == null)
                    throw new FluentCheckException($"Missing property '{propertyInfo.Name}'.");
                var expectedValue = propertyInfoTarget.GetValue(expected);
                Check.That(sourceValue).IsEqualTo(expectedValue);
            }
        }
    }
}