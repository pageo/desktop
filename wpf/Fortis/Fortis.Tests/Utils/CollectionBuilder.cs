﻿using System;
using System.Collections.Generic;
using Fortis.Common.Records;

namespace Fortis.Tests.Utils
{
    internal class CollectionBuilder
    {
        // ----- FIelds
        private readonly IList<Tuple<string, Type>> _fields = new List<Tuple<string, Type>>();
        private readonly IList<IEnumerable<RecordValue>> _recordValues = new List<IEnumerable<RecordValue>>();

        // ----- Constructors
        public CollectionBuilder WithField<T>(string fieldName)
        {
            _fields.Add(new Tuple<string, Type>(fieldName, typeof(T)));
            return this;
        }

        // ----- Public methods
        public CollectionBuilder WithRecord(RecordValue[] recordValues)
        {
            _recordValues.Add(recordValues);
            return this;
        }
        public Collection Build()
        {
            var collection = new Collection("test", "PAT");
            foreach (var field in _fields)
                collection.AddField(field.Item1, field.Item2);
            foreach (var recordValues in _recordValues)
                collection.AddRecord().SetValues(recordValues);
            return collection;
        }
    }
}
