﻿using Fortis.Common.Records;

namespace Fortis.Tests.Utils
{
    public static class RecordExtensions
    {
        // ----- Public methods
        public static Record With(this Record record, string fieldName, object value)
        {
            record.SetValue(fieldName, value);
            return record;
        }
    }
}
