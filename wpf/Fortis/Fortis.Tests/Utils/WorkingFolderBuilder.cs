﻿using System.Collections.Generic;
using Fortis.Business.Models;

namespace Fortis.Tests.Utils
{
    internal class WorkingFolderBuilder
    {
        // ----- Fields
        private readonly WorkingFolderDetails _details = new WorkingFolderDetails();
        private IList<Asset> _assets = new List<Asset>();

        // ----- Public methods
        public WorkingFolderBuilder WithId(int id)
        {
            _details.Id = id;
            return this;
        }
        public WorkingFolderBuilder WithName(string name)
        {
            _details.Name = name;
            return this;
        }
        public WorkingFolderBuilder WithAssets(IList<Asset> assets)
        {
            _assets = assets;
            return this;
        }
        public WorkingFolder Build()
        {
            return new WorkingFolder(_details, _assets);
        }
    }
}
