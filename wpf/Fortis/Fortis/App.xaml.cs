﻿using System.Windows;

namespace Fortis.WPF
{
    public partial class App : Application
    {
        // ----- Override methods
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            new AppBootstrapper().Run();
        }
    }
}
