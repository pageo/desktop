﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using AutoMapper;
using Fortis.Business.Base;
using Fortis.Business.Infrastructure.Authentication;
using Fortis.Business.Infrastructure.Repositories;
using Fortis.Business.Models;
using Fortis.Business.Services;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Base;
using Fortis.Common.Records;
using Fortis.DataAccess;
using Fortis.DataAccess.Authentication;
using Fortis.DataAccess.Repositories;
using Fortis.DataAccess.Repositories.Base;
using Fortis.WPF.Constants;
using Fortis.WPF.Controls.Dock.Behavior;
using Fortis.WPF.Dialogs;
using Fortis.WPF.Logging;
using Fortis.WPF.Utils.Themes;
using Fortis.WPF.Views;
using Prism.Logging;
using Prism.Mvvm;
using Prism.StructureMap;

namespace Fortis.WPF
{
    public class AppBootstrapper : StructureMapBootstrapper
    {
        // ------ Constructors
        public AppBootstrapper()
        {
            CommandManager.Configure(new WpfNotifyApplicationStateChanged());
        }

        // ----- Protected methods
        protected override DependencyObject CreateShell()
        {
            Container.GetInstance<Authenticator>().Authenticate();

            return Container.GetInstance<MainView>();
        }
        protected override void InitializeShell()
        {
            Application.Current.MainWindow.Show();

            base.InitializeShell();
        }
        protected override void ConfigureContainer()
        {
            // Mappings
            Mapper.Initialize(configuration => configuration.CreateMap<Record, Asset>());

            Container.Configure(configuration =>
            {
                // UI
                configuration.ForSingletonOf<MainView>();
                configuration.ForSingletonOf<DockManager>();
                configuration.ForSingletonOf<AvalonDockLayout>();
                configuration.ForSingletonOf<IMessageBoxService>().Use<WinformMessageBoxService>();
                configuration.For<Application>().Use(Application.Current);
                configuration.Forward<Application, DispatcherObject>();

                // UI Themes
                configuration.For<ITheme>().Use<BlueTheme>().Named(ThemeNames.BlueTheme);
                configuration.For<ITheme>().Use<DarkTheme>().Named(ThemeNames.DarkTheme);
                configuration.For<ITheme>().Use<LightTheme>().Named(ThemeNames.LightTheme);
                configuration.ForSingletonOf<IThemeManager>().Use<ThemeManager>();

                // ViewModels
                configuration.ForSingletonOf<MainViewModel>();
                configuration.ForSingletonOf<MainMenuViewModel>();
                configuration.ForSingletonOf<WhatIsNewViewModel>();
                configuration.ForSingletonOf<LoadViewModel>();
                configuration.ForSingletonOf<LoadWorkingFolderViewModel>();
                configuration.ForSingletonOf<CreateNewWorkingFolderView>();
                configuration.ForSingletonOf<CreateNewWorkingFolderViewModel>();
                configuration.ForSingletonOf<SaveViewModel>();
                configuration.ForSingletonOf<ListViewModel>();
                configuration.ForSingletonOf<CardViewModel>();
                configuration.ForSingletonOf<MapViewModel>();
                configuration.ForSingletonOf<GoogleStreetViewModel>();
                configuration.ForSingletonOf<MarkerViewModel>();
                configuration.ForSingletonOf<CreateMarkerStandardViewModel>();

                // Business
                configuration.ForSingletonOf<IBus>().Use<Bus>();
                configuration.ForSingletonOf<IClock>().Use<Clock>();
                configuration.For<ICollectionFactory>().Use<CollectionFactory>();
                configuration.For<IColumnTranslationService>().Use<ColumnAttributeTranslationService>();
                configuration.For<IRecordFeeder>().Use<ReflectionRecordFeeder>();
                configuration.ForSingletonOf<CollectionManager>();
                configuration.ForSingletonOf<Authenticator>();
                configuration.ForSingletonOf<ApiToken>();

                // Services
                configuration.ForSingletonOf<EnvironmentSettings>()
                    .Use(info => EnvironmentSettings.Load(
                        Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Fortis.WPF.exe.json")));
                configuration.ForSingletonOf<IAuthenticationService>().Use<OnlineAuthenticationService>();
                configuration.For<IMapper>().Use(Mapper.Instance);

                // Repositories
                configuration.For<IUserRepository>().Use<UserRestRepository>();
                configuration.For<IWorkingFolderRepository>().Use<WorkingFolderRestRepository>();
            });

            base.ConfigureContainer();
        }
        protected override ILoggerFacade CreateLogger()
        {
            return new NLogLogger();
        }
        protected override void ConfigureViewModelLocator()
        {
            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver(viewType => {
                var suffix = viewType.Name.EndsWith("View") ? "Model" : "ViewModel";
                var viewModelTypeName = $"Fortis.Business.ViewModels.{viewType.Name}{suffix}, Fortis.Business";
                return Type.GetType(viewModelTypeName, true);
            });

            ViewModelLocationProvider.SetDefaultViewModelFactory(t => Container.GetInstance(t));

            base.ConfigureViewModelLocator();
        }
    }
}