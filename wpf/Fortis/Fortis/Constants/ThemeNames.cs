﻿namespace Fortis.WPF.Constants
{
    public static class ThemeNames
    {
        public static string BlueTheme = "BlueTheme";
        public static string DarkTheme = "DarkTheme";
        public static string LightTheme = "LightTheme";
    }
}
