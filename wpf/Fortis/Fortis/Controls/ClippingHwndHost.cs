﻿using Fortis.WPF.Win32;
using System.Collections;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

namespace Fortis.WPF.Controls
{
    public class ClippingHwndHost : HwndHost
    {
        // ----- Fields
        private HwndSource _source;

        // ----- Properties
        public static readonly DependencyProperty ContentProperty = DependencyProperty.Register(
            "Content", typeof(Visual), typeof(ClippingHwndHost),
            new PropertyMetadata(OnContentChanged));
        public Visual Content
        {
            get => (Visual) GetValue(ContentProperty);
            set => SetValue(ContentProperty, value);
        }
        protected override IEnumerator LogicalChildren
        {
            get {
                if (Content != null)
                    yield return Content;
            }
        }

        // ----- Internal logics
        private static void OnContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var hwndHost = (ClippingHwndHost)d;

            if (e.OldValue != null) {
                if (hwndHost._source != null)
                    hwndHost._source.RootVisual = null;
                hwndHost.RemoveLogicalChild(e.OldValue);
            }

            if (e.NewValue != null) {
                hwndHost.AddLogicalChild(e.NewValue);
                if (hwndHost._source != null)
                    hwndHost._source.RootVisual = (Visual)e.NewValue;
            }
        }

        // ----- Override methods
        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            var param = new HwndSourceParameters("FortisClippingHwndHost", (int) Width, (int) Height) {
                ParentWindow = hwndParent.Handle,
                WindowStyle = NativeMethods.WS_VISIBLE | NativeMethods.WS_CHILD,
            };

            _source = new HwndSource(param) {
                RootVisual = Content
            };

            return new HandleRef(null, _source.Handle);
        }
        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            _source.Dispose();
            _source = null;
        }
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            UpdateWindowPos();
            base.OnRenderSizeChanged(sizeInfo);
        }
    }
}