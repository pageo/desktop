﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Fortis.Business.Models;
using Fortis.Business.ViewModels.Base;
using Fortis.Common.Utils;
using Microsoft.Practices.ServiceLocation;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace Fortis.WPF.Controls.Dock.Behavior
{
    public class AvalonDockLayout
    {
        public const string DefaultLayoutFileName = "last-layout.xml";

        // ----- Fields
        private static readonly DockManager DockManager = ServiceLocator.Current.GetInstance<DockManager>();

        // ----- Properties
        public string DirLayout
        {
            get {
                var dirPath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    AssemblyInfo.GetCompanyName(),
                    AssemblyInfo.GetApplicationName(),
                    @"Layout\");
                if (Directory.Exists(dirPath) == false)
                    Directory.CreateDirectory(dirPath);
                return dirPath;
            }
        }
        public string CurrentLayout { get; set; } = DefaultLayoutFileName;

        // ----- Commands
        public ICommand LoadLayoutCommand => new RelayCommand<DockingManager>(LoadDockingManagerLayout);
        public ICommand SaveLayoutCommand => new RelayCommand<string>(SaveDockingManagerLayout);

        // ----- Internal logics
        private void LoadDockingManagerLayout(DockingManager dockingManager)
        {
            var layoutFileName = Path.Combine(DirLayout, CurrentLayout ?? DefaultLayoutFileName);
            if (File.Exists(layoutFileName) == false)
                return;

            var layoutSerializer = new XmlLayoutSerializer(dockingManager);
            layoutSerializer.LayoutSerializationCallback += (s, args) => {
                if (args.Model.ContentId == null) {
                    args.Cancel = true;
                    return;
                }
                ReloadContentOnStartUp(args);
            };

            // Reset Layout before loading
            ResetLayout(dockingManager);

            layoutSerializer.Deserialize(layoutFileName);
        }
        private void SaveDockingManagerLayout(string xmlLayout)
        {
            // TODO La sauvegarde du layout ne fonctionne plus
            if (xmlLayout == null) return;
            var fileName = Path.Combine(DirLayout, CurrentLayout ?? DefaultLayoutFileName);
            File.WriteAllText(fileName, xmlLayout, Encoding.UTF8);
        }
        private static void ReloadContentOnStartUp(LayoutSerializationCallbackEventArgs args)
        {
            var sId = args.Model.ContentId;
            if (string.IsNullOrWhiteSpace(sId)) {
                args.Cancel = true;
                return;
            }
            args.Content = ReloadDocument(args.Model.ContentId);
            if (args.Content == null)
                args.Cancel = true;
        }
        private static object ReloadDocument(string contentId)
        {
            if (string.IsNullOrWhiteSpace(contentId)) return null;
            var ret = Open(contentId);
            if (ret != null) ret.IsVisible = true;
            return ret;
        }
        private static void ResetLayout(DockingManager dockingManager)
        {
            // TODO: Not working => this raise System.NullReferenceException was unhandled by user code
            //var dockManager = ServiceLocator.Current.GetInstance<DockManager>();
            //dockManager.Documents.Clear();
            //dockManager.Anchorables.Clear(); // <- Here

            // HACK : https://xceed.com/forums/topic/avalondock-nullreferenceexception-when-restoring-state-w-dynamic-anchorables/
            var layoutContents = GatherLayoutContent(dockingManager.Layout).ToArray();
            // Remove the views by force
            foreach (var x in layoutContents) {
                dockingManager
                    .GetType()
                    .GetMethods(BindingFlags.Instance | BindingFlags.NonPublic)
                    .First(m => m.Name.Equals("RemoveViewFromLogicalChild"))
                    .Invoke(dockingManager, new object[] { x });
            }

            // Close all float windows
            foreach (var window in dockingManager.FloatingWindows.ToList())
                window.Close();
        }

        // ----- Utils
        public static DockItem Open(string contentId)
        {
            return DockManager.Documents.OfType<DockItem>().Union(DockManager.Anchorables).FirstOrDefault(fm => fm.ContentId == contentId);
        }
        private static IEnumerable<LayoutContent> GatherLayoutContent(ILayoutElement le)
        {
            if (le is LayoutContent)
                yield return (LayoutContent)le;
            IEnumerable<ILayoutElement> children = new ILayoutElement[0];
            if (le is LayoutRoot)
                children = ((LayoutRoot)le).Children;
            else if (le is LayoutPanel)
                children = ((LayoutPanel)le).Children;
            else if (le is LayoutDocumentPaneGroup)
                children = ((LayoutDocumentPaneGroup)le).Children;
            else if (le is LayoutAnchorablePane)
                children = ((LayoutAnchorablePane)le).Children;
            else if (le is LayoutDocumentPane)
                children = ((LayoutDocumentPane)le).Children;
            foreach (var child in children)
            foreach (var x in GatherLayoutContent(child))
                yield return x;
        }
    }
}