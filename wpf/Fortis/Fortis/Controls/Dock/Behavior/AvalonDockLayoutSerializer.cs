﻿using System.IO;
using System.Windows;
using System.Windows.Input;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace Fortis.WPF.Controls.Dock.Behavior
{
    public static class AvalonDockLayoutSerializer
    {
        // ----- Fields
        private static readonly DependencyProperty LoadLayoutCommandProperty =
            DependencyProperty.RegisterAttached("LoadLayoutCommand",
            typeof(ICommand),
            typeof(AvalonDockLayoutSerializer),
            new PropertyMetadata(null, OnLoadLayoutCommandChanged));
        private static readonly DependencyProperty SaveLayoutCommandProperty =
            DependencyProperty.RegisterAttached("SaveLayoutCommand",
            typeof(ICommand),
            typeof(AvalonDockLayoutSerializer),
            new PropertyMetadata(null, OnSaveLayoutCommandChanged));

        // ----- Commands
        public static ICommand GetLoadLayoutCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(LoadLayoutCommandProperty);
        }
        public static void SetLoadLayoutCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(LoadLayoutCommandProperty, value);
        }
        public static ICommand GetSaveLayoutCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(SaveLayoutCommandProperty);
        }
        public static void SetSaveLayoutCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(SaveLayoutCommandProperty, value);
        }

        // ----- Callbacks
        private static void OnLoadLayoutCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var frameworkElement = d as FrameworkElement;
            if (frameworkElement == null)
                return;
            frameworkElement.Loaded -= OnFrameworkElement_Loaded;

            var command = e.NewValue as ICommand;
            if (command != null)
                frameworkElement.Loaded += OnFrameworkElement_Loaded;
        }
        private static void OnFrameworkElement_Loaded(object sender, RoutedEventArgs e)
        {
            var frameworkElement = sender as FrameworkElement;
            if (frameworkElement == null)
                return;

            var loadLayoutCommand = GetLoadLayoutCommand(frameworkElement);
            if (loadLayoutCommand == null)
                return;

            if (loadLayoutCommand is RoutedCommand)
                (loadLayoutCommand as RoutedCommand).Execute(frameworkElement, frameworkElement);
            else
                loadLayoutCommand.Execute(frameworkElement);
        }
        private static void OnSaveLayoutCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var frameworkElement = d as FrameworkElement;
            if (frameworkElement == null)
                return;
            frameworkElement.Unloaded -= OnFrameworkElement_Saveed;

            var command = e.NewValue as ICommand;
            if (command != null)
                frameworkElement.Unloaded += OnFrameworkElement_Saveed;
        }
        private static void OnFrameworkElement_Saveed(object sender, RoutedEventArgs e)
        {
            var frameworkElement = sender as DockingManager;
            if (frameworkElement == null)
                return;

            var saveLayoutCommand = GetSaveLayoutCommand(frameworkElement);
            if (saveLayoutCommand == null)
                return;

            string xmlLayoutString;
            using (var fs = new StringWriter()) {
                var xmlLayout = new XmlLayoutSerializer(frameworkElement);
                xmlLayout.Serialize(fs);
                xmlLayoutString = fs.ToString();
            }

            if (saveLayoutCommand is RoutedCommand)
                (saveLayoutCommand as RoutedCommand).Execute(xmlLayoutString, frameworkElement);
            else
                saveLayoutCommand.Execute(xmlLayoutString);
        }
    }
}
