﻿using System;
using System.Linq;
using System.Reflection;
using Fortis.Business.Models;
using Xceed.Wpf.AvalonDock.Layout;

namespace Fortis.WPF.Controls.Dock.Behavior
{
    public class LayoutUpdateStrategy : ILayoutUpdateStrategy
    {
        // ----- Public methods
        public bool BeforeInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableToShow, ILayoutContainer destinationContainer)
        {
            return BeforeInsertContent(layout, anchorableToShow);
        }
        public void AfterInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableShown) { }
        public bool BeforeInsertDocument(LayoutRoot layout, LayoutDocument anchorableToShow, ILayoutContainer destinationContainer)
        {
            return BeforeInsertContent(layout, anchorableToShow);
        }
        public void AfterInsertDocument(LayoutRoot layout, LayoutDocument anchorableShown) { }

        // ----- Internal logics
        private static bool BeforeInsertContent(ILayoutElement layout, LayoutContent anchorableToShow)
        {
            var viewModel = (DockItem)anchorableToShow.Content;
            var layoutContent = layout.Descendents().OfType<LayoutContent>().FirstOrDefault(x => x.ContentId == viewModel.ContentId);
            if (layoutContent == null)
                return false;
            layoutContent.Content = anchorableToShow.Content;
            var propertyInfo = layoutContent.GetType().GetProperty("PreviousContainer", BindingFlags.NonPublic | BindingFlags.Instance);
            if (propertyInfo == null) return true;
            var layoutContainer = propertyInfo.GetValue(layoutContent, null) as ILayoutContainer;
            if (layoutContainer is LayoutAnchorablePane)
                (layoutContainer as LayoutAnchorablePane).Children.Add(layoutContent as LayoutAnchorable);
            else if (layoutContainer is LayoutDocumentPane)
                (layoutContainer as LayoutDocumentPane).Children.Add(layoutContent);
            else
                throw new NotSupportedException();
            return true;
        }
    }
}
