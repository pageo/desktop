﻿using System.Windows;
using System.Windows.Controls;
using Fortis.Business.Models;

namespace Fortis.WPF.Controls.Dock.Behavior
{
    public class PanesStyleSelector : StyleSelector
    {
        // ----- Properties
        public Style DocumentStyle { get; set; }
        public Style AnchorableStyle { get; set; }

        // ----- Override methods
        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is DockDocument)
                return DocumentStyle;

            if (item is DockAnchorable)
                return AnchorableStyle;

            return base.SelectStyle(item, container);
        }
    }
}
