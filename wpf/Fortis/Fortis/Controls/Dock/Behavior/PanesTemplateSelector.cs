﻿using System.Windows;
using System.Windows.Controls;
using Fortis.Business.Models;

namespace Fortis.WPF.Controls.Dock.Behavior
{
    public class PanesTemplateSelector : DataTemplateSelector
    {
        // ----- Properties
        public DataTemplate AnchorableTemplate { get; set; }
        public DataTemplate DocumentTemplate { get; set; }

        // ----- Override methods
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is DockDocument)
                return DocumentTemplate;

            if (item is DockAnchorable)
                return AnchorableTemplate;

            return base.SelectTemplate(item, container);
        }
    }
}
