﻿using System.Windows;
using System.Windows.Controls;
using Fortis.Business.Models;

namespace Fortis.WPF.Controls.Dock
{
    public abstract class DockAnchorableView : UserControl
    {
        // ----- Constructors
        protected DockAnchorableView()
        {
            DataContextChanged += DockAnchorableView_DataContextChanged;
        }

        // ----- UI Callbacks
        private void DockAnchorableView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ((DockAnchorable)DataContext).View = this;
        }
    }
}
