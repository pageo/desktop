﻿using System.Windows;
using System.Windows.Controls;
using Fortis.Business.Models;

namespace Fortis.WPF.Controls.Dock
{
    public abstract class DockDocumentView : UserControl
    {
        // ----- Constructors
        protected DockDocumentView()
        {
            DataContextChanged += DockDocumentView_DataContextChanged;
        }

        // ----- UI Callbacks
        private void DockDocumentView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ((DockDocument)DataContext).View = this;
        }
    }
}
