﻿using System.Windows;
using System.Windows.Controls;

namespace Fortis.WPF.Controls
{
    public class ExpanderEx : Expander
    {
        // ----- Constructors
        static ExpanderEx()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ExpanderEx),
                new FrameworkPropertyMetadata(typeof(ExpanderEx)));
        } 
    }
}