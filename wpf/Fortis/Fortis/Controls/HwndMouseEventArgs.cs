﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Fortis.WPF.Controls
{
    public class HwndMouseEventArgs : EventArgs
    {
        // ----- Properties
        public MouseButtonState LeftButton { get; }
        public MouseButtonState RightButton { get; }
        public MouseButtonState MiddleButton { get; }
        public MouseButtonState X1Button { get; }
        public MouseButtonState X2Button { get; }
        public MouseButton? DoubleClickButton { get; }
        public int WheelDelta { get; }
        public int HorizontalWheelDelta { get; }
        public Point ScreenPosition { get; }
        public Point GetPosition(UIElement relativeTo)
        {
            return relativeTo.PointFromScreen(ScreenPosition);
        }

        // ----- Constructors
        public HwndMouseEventArgs(HwndMouseState state)
        {
            LeftButton = state.LeftButton;
            RightButton = state.RightButton;
            MiddleButton = state.MiddleButton;
            X1Button = state.X1Button;
            X2Button = state.X2Button;
            ScreenPosition = state.ScreenPosition;
        }
        public HwndMouseEventArgs(HwndMouseState state, int mouseWheelDelta, int mouseHWheelDelta)
            : this(state)
        {
            WheelDelta = mouseWheelDelta;
            HorizontalWheelDelta = mouseHWheelDelta;
        }
        public HwndMouseEventArgs(HwndMouseState state, MouseButton doubleClickButton)
            : this(state)
        {
            DoubleClickButton = doubleClickButton;
        }
    }
}
