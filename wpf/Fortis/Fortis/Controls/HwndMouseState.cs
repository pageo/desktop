﻿using System.Windows;
using System.Windows.Input;

namespace Fortis.WPF.Controls
{
    public class HwndMouseState
    {
        // ----- Fields
        public MouseButtonState LeftButton;
        public MouseButtonState RightButton;
        public MouseButtonState MiddleButton;
        public MouseButtonState X1Button;
        public MouseButtonState X2Button;
        public Point ScreenPosition;
    }
}
