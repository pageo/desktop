﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsPresentation;

namespace Fortis.WPF.Controls
{
    public class MapControl : GMapControl
    {
        // ----- Constructors
        public MapControl()
        {
            // config map  
            Manager.Mode = AccessMode.ServerOnly;
            MapProvider = GoogleMapProvider.Instance;

            // Zoom  
            MinZoom = 0;
            MaxZoom = 24;
            Zoom = 12;
        }
    }
}
