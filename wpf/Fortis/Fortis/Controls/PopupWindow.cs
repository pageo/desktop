﻿using System.ComponentModel;
using System.Windows;
using MahApps.Metro.Controls;

namespace Fortis.WPF.Controls
{
    public class PopupWindow : MetroWindow
    {
        // ----- Constructors
        public PopupWindow()
        {
            Topmost = true;
            ShowInTaskbar = false;
        }

        // ----- Override methods
        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }
    }
}
