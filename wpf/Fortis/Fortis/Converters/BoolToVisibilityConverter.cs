﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Fortis.WPF.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        // ----- Properties
        public bool TriggerValue { get; set; } = false;
        public bool IsHidden { get; set; } = false;

        // ----- Internal logics
        private object GetVisibility(object value)
        {
            if (!(value is bool))
                return DependencyProperty.UnsetValue;
            var objValue = (bool)value;
            if (objValue && TriggerValue && IsHidden || (!objValue && !TriggerValue && IsHidden))
                return Visibility.Hidden;
            if (objValue && TriggerValue && !IsHidden || !objValue && !TriggerValue && !IsHidden)
                return Visibility.Collapsed;
            return Visibility.Visible;
        }

        // ----- Public methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return GetVisibility(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Visibility))
                return null;
            var objValue = (Visibility)value;
            if (objValue == Visibility.Hidden)
                return TriggerValue && IsHidden;
            if (objValue == Visibility.Collapsed)
                return TriggerValue && !IsHidden;
            return true;
        }
    }
}
