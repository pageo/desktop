﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using MColor = System.Windows.Media.Color;
using DColor = System.Drawing.Color;

namespace Fortis.WPF.Converters
{
    public class ColorToMediaColorConverter : IValueConverter
    {
        // ----- Public methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DColor))
                return DependencyProperty.UnsetValue;
            var color = (DColor)value;
            return MColor.FromArgb(color.A, color.R, color.G, color.B);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is MColor))
                return DColor.Empty;
            var color = (MColor)value;
            return DColor.FromArgb(color.A, color.R, color.G, color.B);
        }
    }
}
