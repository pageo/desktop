﻿using System.Windows;
using Fortis.Business.Services;

namespace Fortis.WPF.Dialogs
{
    public class WinformMessageBoxService : IMessageBoxService
    {
        // ----- Public methods
        public void ShowInformation(string message)
        {
            MessageBox.Show(message, @"Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        public UserAnswer AskQuestion(string question)
        {
            var dialogResult = MessageBox.Show(question, @"Question", MessageBoxButton.YesNo, MessageBoxImage.Question);
            return dialogResult == MessageBoxResult.Yes ? UserAnswer.Yes : UserAnswer.No;
        }
    }
}