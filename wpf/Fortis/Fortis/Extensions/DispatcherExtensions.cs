﻿using System;
using System.Windows.Threading;

namespace Fortis.WPF.Extensions
{
    public static class DispatcherExtensions
    {
        // ----- Public methods
        public static void InvokeIfRequired(this Dispatcher dispatcher,
            Action action, DispatcherPriority priority)
        {
            if (!dispatcher.CheckAccess())
                dispatcher.Invoke(priority, action);
            else
                action();
        }
        public static void InvokeIfRequired(this Dispatcher dispatcher, Action action)
        {
            if (!dispatcher.CheckAccess())
                dispatcher.Invoke(DispatcherPriority.Normal, action);
            else
                action();
        }
        public static void InvokeInBackgroundIfRequired(
            this Dispatcher dispatcher,
            Action action)
        {
            if (!dispatcher.CheckAccess())
                dispatcher.Invoke(DispatcherPriority.Background, action);
            else
                action();
        }
        public static void InvokeAsynchronouslyInBackground(
            this Dispatcher dispatcher, Action action)
        {
            if (dispatcher != null)
                dispatcher.BeginInvoke(DispatcherPriority.Background, action);
            else
                action();
        }
    }
}

