﻿using NLog;
using Prism.Logging;

namespace Fortis.WPF.Logging
{
    public class NLogLogger : ILoggerFacade
    {
        // ----- Fields
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        // ----- Public methods
        public void Log(string message, Category category, Priority priority)
        {
            switch (category){
                case Category.Debug:
                    _logger.Debug(message);
                    break;
                case Category.Exception:
                    _logger.Error(message);
                    break;
                case Category.Info:
                    _logger.Info(message);
                    break;
                case Category.Warn:
                    _logger.Warn(message);
                    break;
                default:
                    _logger.Info(message);
                    break;
            }
        }
    }
}
