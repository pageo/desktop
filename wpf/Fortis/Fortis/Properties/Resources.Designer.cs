﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fortis.WPF.Properties {
    using System;
    
    
    /// <summary>
    ///   Une classe de ressource fortement typée destinée, entre autres, à la consultation des chaînes localisées.
    /// </summary>
    // Cette classe a été générée automatiquement par la classe StronglyTypedResourceBuilder
    // à l'aide d'un outil, tel que ResGen ou Visual Studio.
    // Pour ajouter ou supprimer un membre, modifiez votre fichier .ResX, puis réexécutez ResGen
    // avec l'option /str ou régénérez votre projet VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Retourne l'instance ResourceManager mise en cache utilisée par cette classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Fortis.WPF.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Remplace la propriété CurrentUICulture du thread actuel pour toutes
        ///   les recherches de ressources à l'aide de cette classe de ressource fortement typée.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-16&quot;?&gt;
        ///&lt;LayoutRoot xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot;&gt;
        ///  &lt;RootPanel Orientation=&quot;Horizontal&quot;&gt;
        ///    &lt;LayoutPanel Orientation=&quot;Horizontal&quot;&gt;
        ///      &lt;LayoutPanel Orientation=&quot;Horizontal&quot;&gt;
        ///        &lt;LayoutDocumentPaneGroup Orientation=&quot;Vertical&quot;&gt;
        ///          &lt;LayoutDocumentPane Id=&quot;8fa8b2df-df07-45e6-a0b2-fb93c6357a45&quot; DockHeight=&quot;1.08212960222089*&quot;&gt;
        ///            &lt;LayoutDocument Title=&quot;Map&quot; IsSelected=&quot;True&quot; Content [le reste de la chaîne a été tronqué]&quot;;.
        /// </summary>
        internal static string custom_layout {
            get {
                return ResourceManager.GetString("custom_layout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-16&quot;?&gt;
        ///&lt;LayoutRoot xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot;&gt;
        ///  &lt;RootPanel Orientation=&quot;Horizontal&quot;&gt;
        ///    &lt;LayoutPanel Orientation=&quot;Horizontal&quot;&gt;
        ///      &lt;LayoutDocumentPaneGroup Orientation=&quot;Vertical&quot;&gt;
        ///        &lt;LayoutDocumentPane Id=&quot;7c9889d2-ad50-487c-a738-936b309dec2c&quot; DockHeight=&quot;0.40187908600043*&quot;&gt;
        ///          &lt;LayoutDocument Title=&quot;Liste&quot; IsSelected=&quot;True&quot; IsLastFocusedDocument=&quot;True&quot; ContentId=&quot;78a46dbc-9d8f-4d2 [le reste de la chaîne a été tronqué]&quot;;.
        /// </summary>
        internal static string positioning_layout {
            get {
                return ResourceManager.GetString("positioning_layout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à &lt;!DOCTYPE html&gt;
        ///&lt;html&gt;
        ///&lt;head&gt;
        ///    &lt;title&gt;Custom Street View panorama tiles&lt;/title&gt;
        ///    &lt;meta name=&quot;viewport&quot; content=&quot;initial-scale=1.0&quot;&gt;
        ///    &lt;meta charset=&quot;utf-8&quot;&gt;
        ///    &lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE&quot;/&gt;
        ///    &lt;style&gt;
        ///        html, body {
        ///            height: 100%;
        ///            margin: 0;
        ///            padding: 0;
        ///        }
        ///
        ///        #street-view { height: 100%; }
        ///    &lt;/style&gt;
        ///&lt;/head&gt;
        ///&lt;body&gt;
        ///&lt;div id=&quot;street-view&quot;&gt;&lt;/div&gt;
        ///&lt;script&gt;
        ///
        ///    var panorama;
        ///
        ///// StreetViewPanoramaData of [le reste de la chaîne a été tronqué]&quot;;.
        /// </summary>
        internal static string streetview {
            get {
                return ResourceManager.GetString("streetview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à Blue.
        /// </summary>
        internal static string ThemeBlueName {
            get {
                return ResourceManager.GetString("ThemeBlueName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à Dark.
        /// </summary>
        internal static string ThemeDarkName {
            get {
                return ResourceManager.GetString("ThemeDarkName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Recherche une chaîne localisée semblable à Light.
        /// </summary>
        internal static string ThemeLightName {
            get {
                return ResourceManager.GetString("ThemeLightName", resourceCulture);
            }
        }
    }
}
