﻿using System;
using System.Windows.Media.Effects;

namespace Fortis.WPF.ShaderEffects
{
    public class ShaderEffectBase<T> : ShaderEffect, IDisposable where T : ShaderEffectBase<T>
    {
        // ----- Fields
        [ThreadStatic]
        private static PixelShader _shader;
        private static PixelShader Shader => _shader ?? (_shader = ShaderEffectUtility.GetPixelShader(typeof(T).Name));

        // ----- Constructors
        protected ShaderEffectBase()
        {
            PixelShader = Shader;
        }

        // ----- Public methods
        void IDisposable.Dispose()
        {
            PixelShader = null;
        }
    }
}