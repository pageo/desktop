﻿using System;
using System.Windows.Media.Effects;

namespace Fortis.WPF.ShaderEffects
{
    internal static class ShaderEffectUtility
    {
        // ----- Properties
        public static PixelShader GetPixelShader(string name)
        {
            return new PixelShader {
                UriSource = new Uri(@"pack://application:,,,/Fortis.WPF;component/ShaderEffects/" + name + ".ps")
            };
        }
    }
}