﻿using System;
using System.Collections.Generic;
using Fortis.WPF.Properties;

namespace Fortis.WPF.Utils.Themes
{
    public class BlueTheme : ITheme
    {
        // ----- Properties
        public virtual string Name => Resources.ThemeBlueName;
        public virtual IEnumerable<Uri> ApplicationResources
        {
            get {
                yield return new Uri("pack://application:,,,/Xceed.Wpf.AvalonDock.Themes.VS2013;component/BlueTheme.xaml");
                yield return new Uri("pack://application:,,,/Fortis.WPF;component/Themes/VS2013/BlueTheme.xaml");
            }
        }
        public virtual IEnumerable<Uri> MainWindowResources
        {
            get { yield break; }
        }
    }
}