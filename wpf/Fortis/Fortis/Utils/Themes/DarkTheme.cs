﻿using System;
using System.Collections.Generic;
using Fortis.WPF.Properties;

namespace Fortis.WPF.Utils.Themes
{
    public class DarkTheme : ITheme
    {
        // ----- Properties
        public virtual string Name => Resources.ThemeDarkName;
        public virtual IEnumerable<Uri> ApplicationResources
        {
            get {
                yield return new Uri("pack://application:,,,/Xceed.Wpf.AvalonDock.Themes.VS2013;component/DarkTheme.xaml");
                yield return new Uri("pack://application:,,,/Fortis.WPF;component/Themes/VS2013/DarkTheme.xaml");
            }
        }
        public virtual IEnumerable<Uri> MainWindowResources
        {
            get { yield break; }
        }
    }
}