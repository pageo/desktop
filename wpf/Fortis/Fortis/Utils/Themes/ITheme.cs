﻿using System;
using System.Collections.Generic;

namespace Fortis.WPF.Utils.Themes
{
    public interface ITheme
    {
        string Name { get; }
        IEnumerable<Uri> ApplicationResources { get; }
        IEnumerable<Uri> MainWindowResources { get; }
    }
}