﻿using System;
using System.Collections.Generic;

namespace Fortis.WPF.Utils.Themes
{
    public interface IThemeManager
    {
        event EventHandler CurrentThemeChanged;

        IEnumerable<ITheme> Themes { get; }
        ITheme CurrentTheme { get; }

        bool SetCurrentTheme(string name);
    }
}