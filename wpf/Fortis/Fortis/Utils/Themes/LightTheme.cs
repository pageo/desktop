﻿using System;
using System.Collections.Generic;
using Fortis.WPF.Properties;

namespace Fortis.WPF.Utils.Themes
{
    public class LightTheme : ITheme
    {
        // ----- Properties
        public virtual string Name => Resources.ThemeLightName;
        public virtual IEnumerable<Uri> ApplicationResources
        {
            get {
                yield return new Uri("pack://application:,,,/Xceed.Wpf.AvalonDock.Themes.VS2013;component/LightTheme.xaml");
                yield return new Uri("pack://application:,,,/Fortis.WPF;component/Themes/VS2013/LightTheme.xaml");
            }
        }
        public virtual IEnumerable<Uri> MainWindowResources
        {
            get { yield break; }
        }
    }
}