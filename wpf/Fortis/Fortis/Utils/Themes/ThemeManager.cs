﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Fortis.WPF.Properties;
using Fortis.WPF.Services;

namespace Fortis.WPF.Utils.Themes
{
    public class ThemeManager : IThemeManager
    {
        // ----- Events
        public event EventHandler CurrentThemeChanged;

        // ----- Fields
        private readonly SettingsPropertyChangedEventManager<Settings> _settingsEventManager =
            new SettingsPropertyChangedEventManager<Settings>(Settings.Default);
        private ResourceDictionary _applicationResourceDictionary;

        // ----- Properties
        public IEnumerable<ITheme> Themes { get; }
        public ITheme CurrentTheme { get; private set; }

        // ----- Constructors
        public ThemeManager(IEnumerable<ITheme> themes)
        {
            Themes = themes;
            _settingsEventManager.AddListener(s => s.ThemeName, value => SetCurrentTheme(value));
        }

        // ----- Public methods
        public bool SetCurrentTheme(string name)
        {
            var theme = Themes.FirstOrDefault(x => x.GetType().Name == name);
            if (theme == null)
                return false;

            var mainWindow = Application.Current.MainWindow;
            if (mainWindow == null)
                return false;

            CurrentTheme = theme;

            if (_applicationResourceDictionary == null) {
                _applicationResourceDictionary = new ResourceDictionary();
                Application.Current.Resources.MergedDictionaries.Add(_applicationResourceDictionary);
            }
            _applicationResourceDictionary.BeginInit();
            _applicationResourceDictionary.MergedDictionaries.Clear();

            var windowResourceDictionary = mainWindow.Resources.MergedDictionaries[0];
            windowResourceDictionary.BeginInit();
            windowResourceDictionary.MergedDictionaries.Clear();

            foreach (var uri in theme.ApplicationResources)
                _applicationResourceDictionary.MergedDictionaries.Add(new ResourceDictionary { Source = uri });

            foreach (var uri in theme.MainWindowResources)
                windowResourceDictionary.MergedDictionaries.Add(new ResourceDictionary { Source = uri });

            _applicationResourceDictionary.EndInit();
            windowResourceDictionary.EndInit();

            RaiseCurrentThemeChanged(EventArgs.Empty);

            return true;
        }

        // ----- Internal logics
        private void RaiseCurrentThemeChanged(EventArgs args)
        {
            CurrentThemeChanged?.Invoke(this, args);
        }
    }
}