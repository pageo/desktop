﻿using System.Windows.Controls;

namespace Fortis.WPF.Views
{
    /// <summary>
    /// Logique d'interaction pour BottomBar.xaml
    /// </summary>
    public partial class BottomBar : UserControl
    {
        public BottomBar()
        {
            InitializeComponent();
        }
    }
}
