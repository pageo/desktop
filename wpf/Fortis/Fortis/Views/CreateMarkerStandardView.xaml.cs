﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Fortis.Common.Collections.Expressions;
using Fortis.Common.Markers;
using Fortis.Common.Records;
using Fortis.Common.Utils;
using Fortis.WPF.Controls;
using MahApps.Metro.Controls;

namespace Fortis.WPF.Views
{
    public partial class CreateMarkerStandardView : PopupWindow
    {
        // ----- Fields
        private Collection _collection;
        private MarkerStandard _editedMarker;

        // ----- Constructors
        public CreateMarkerStandardView()
        {
            InitializeComponent();
        }

        // ----- Callbacks
        private void ListBoxFields_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TextBoxValue.Text = "";
            if (ListBoxFields.SelectedIndex == -1) return;
            var field = (Field)ListBoxFields.SelectedItem;
            FillOperatorValues(field);
            FillListBoxValue(field);
        }
        private void ListBoxValue_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TextBoxValue.Text = string.Join(Separators.ValueSeparator, ListBoxValue.SelectedItems.OfType<string>());
        }
        private void BtnAdd_OnClick(object sender, RoutedEventArgs e)
        {
            if (ListBoxFields.SelectedIndex == -1)
                MessageBox.Show(Translator.Translate("Veuillez choisir un champ!"));
            else if (ListBoxOperators.SelectedIndex == -1)
                MessageBox.Show(Translator.Translate("Veuillez choisir un opérateur!"));
            else if (ListBoxValue.SelectedIndex == -1 && TextBoxValue.SelectionLength == 0)
                MessageBox.Show(Translator.Translate("Veuillez choisir une ou plusieurs valeurs!"));
            else
            {
                var field = (Field)ListBoxFields.SelectedItem;
                var fieldName = field.Name;
                var op = ListBoxOperators.SelectedItem.ToString();
                var values = TextBoxValue.Text.Split(new[] { Separators.ValueSeparator }, StringSplitOptions.None);
                if (op == ConditionalOperators.Equals && values.Length > 1)
                    op = ConditionalOperators.In;
                else if (op == ConditionalOperators.Different && (values.Length > 1))
                    op = ConditionalOperators.NotIn;
                if (_collection != null) {
                    for (var i = 0; i < values.Length; i++) {
                        if (values[i] == "" || field.Type == typeof(string) || field.Type == typeof(DateTime))
                            values[i] = "\"" + values[i] + "\"";
                    }
                }
                var val = string.Join(Separators.ValueSeparator, values);
                string expression;
                if (op == ConditionalOperators.In || op == ConditionalOperators.NotIn)
                    expression = "[" + fieldName + "] " + op + " {" + val + "}";
                else
                    expression = "[" + fieldName + "] " + op + " " + val;

                var index = ListBoxConditions.SelectedIndex;
                if (index != -1) {
                    ListBoxConditions.Items[index] = expression;
                    ListBoxConditions.SelectedIndex = -1;
                }
                else
                    ListBoxConditions.Items.Add(expression);
            }
        }
        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            if (TextBoxName.Text == "") {
                MessageBox.Show(Translator.Translate("Le nom du marqueur n'est pas renseigné!"));
                DialogResult = null;
            }
            else if (ListBoxConditions.Items.Count == 0) {
                MessageBox.Show(Translator.Translate("Aucune condition n'a été précisée!"));
                DialogResult = null;
            }
            else if (_collection?.GetMarkers<Marker>().Where(x => x != _editedMarker).Any(x => x.Name == TextBoxName.Text) == true) {
                MessageBox.Show(Translator.Translate("Un marqueur nommé ainsi existe déja pour cette collection!"));
                DialogResult = null;
            }
        }
        private void ListBoxConditions_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = ListBoxConditions.SelectedIndex;
            if (index == -1)
                BtnDelete.IsEnabled = false;
            else {
                BtnDelete.IsEnabled = true;
                var condition = ListBoxConditions.Items[index].ToString();
                string op;
                string field;
                RecordLogicalExpressionParser.SplitCondition(condition, out field, out op, out string[] values);
                if (field != null) {
                    for (var i = 0; i < ListBoxFields.Items.Count; i++) {
                        if (((Field)ListBoxFields.Items[i]).Name == field) {
                            ListBoxFields.SelectedIndex = i;
                            break;
                        }
                    }
                }
                if (op != null) {
                    if (op == ConditionalOperators.In)
                        op = ConditionalOperators.Equals;
                    else if (op == ConditionalOperators.NotIn)
                        op = ConditionalOperators.Different;
                    ListBoxOperators.SelectedIndex = ListBoxOperators.Items.IndexOf(op);
                }
                if (values != null) {
                    ListBoxValue.SelectedItems.Clear();
                    foreach (var value in values) {
                        var i = ListBoxValue.Items.IndexOf(value);
                        if (i != -1)
                            ListBoxValue.SelectedItems.Add(ListBoxValue.Items[i]);
                    }
                }
            }
        }
        private void BtnDelete_OnClick(object sender, RoutedEventArgs e)
        {
            var index = ListBoxConditions.SelectedIndex;
            if (index != -1)
                ListBoxConditions.Items.RemoveAt(index);
        }
        private void TextBoxField_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var item = ListBoxFields.Items.OfType<Field>().OrderBy(x => x.Index).FirstOrDefault(x => x.Name.ToLower().Contains(TextBoxField.Text.ToLower()));
            if (item != null)
                ListBoxFields.SelectedItem = item;
        }
        private void TextBoxField_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Return) return;
            var item = ListBoxFields.SelectedItem as Field;
            Field nextItem = null;
            if (item != null)
                nextItem = ListBoxFields.Items.OfType<Field>().OrderBy(x => x.Index).FirstOrDefault(x => x.Index > item.Index && x.Name.ToLower().Contains(TextBoxField.Text.ToLower()));
            if (nextItem == null)
                nextItem = ListBoxFields.Items.OfType<Field>().OrderBy(x => x.Index).FirstOrDefault(x => x.Name.ToLower().Contains(TextBoxField.Text.ToLower()));
            if (nextItem != null)
                ListBoxFields.SelectedItem = nextItem;
        }

        // ----- Public methods
        public bool? ShowDialogForEdition(MarkerStandard marker)
        {
            if (marker == null) throw new ArgumentNullException(nameof(marker));

            _collection = marker.Collection;
            _editedMarker = marker;

            ClearForm();
            InitFormUsingCollection(marker.Collection);

            if (marker.ParentMarker != null)
                MarkerComboBox.Value = marker.ParentMarker.Name;

            if (marker.Expression != null) {
                var expr = marker.Expression;
                if (expr != null && expr.Length >= 2 && expr.StartsWith("(") && expr.EndsWith(")"))
                    expr = expr.Substring(1, expr.Length - 2);
                var conds = RecordLogicalExpressionParser.GetElements(expr).Where(e => e != LogicalOperators.AND);
                conds.ToList().ForEach(x => ListBoxConditions.Items.Add(x.ToArray()));
            }

            var result = ShowDialog();
            if (result != true) return result;
            if (marker.Name != TextBoxName.Name)
                marker.Collection.RenameMarker(marker, TextBoxName.Text);
            marker.Expression = string.Join($" {LogicalOperators.AND} ", ListBoxConditions.Items.OfType<string>());
            marker.ParentMarker = _collection.GetMarker(MarkerComboBox.Value);
            marker.Eval();
            return true;
        }
        public MarkerStandard ShowDialogForCreation(Collection collection, string markerName = null, string parentName = null)
        {
            _collection = collection ?? throw new ArgumentNullException(nameof(collection));
            _editedMarker = null;

            ClearForm();
            InitFormUsingCollection(collection);

            if (string.IsNullOrEmpty(markerName) == false)
                TextBoxName.Text = markerName;

            if (string.IsNullOrEmpty(parentName) == false)
                MarkerComboBox.Value = parentName;

            var result = ShowDialog();
            if (result != true) return null;
            var marker = new MarkerStandard(TextBoxName.Text) {
                Expression = string.Join($" {LogicalOperators.AND} ", ListBoxConditions.Items.OfType<string>()),
                ParentMarker = _collection.GetMarker(MarkerComboBox.Value)
            };
            collection.AddMarker(marker);
            marker.Eval();
            return marker;
        }

        // ----- Internal logics
        private void InitFormUsingCollection(Collection collection)
        {
            collection.GetFields().ToList().ForEach(x => ListBoxFields.Items.Add(x));

            MarkerComboBox.Collection = collection;
            MarkerComboBox.FillList();
        }
        private void ClearForm()
        {
            ListBoxFields.Items.Clear();
            ListBoxConditions.Items.Clear();
            MarkerComboBox.Value = null;
            TextBoxName.Text = string.Empty;
        }
        private void FillOperatorValues(Field field)
        {
            ListBoxOperators.Items.Clear();
            field.GetAvailableConditionalOperators().ToList().ForEach(x => ListBoxOperators.Items.Add(x));
            if (ListBoxOperators.Items.Count > 0)
                ListBoxOperators.SelectedIndex = 0;
        }
        private void FillListBoxValue(Field field)
        {
            ListBoxValue.Items.Clear();
            if (_collection == null || field == null)
                return;
            if (field.Type == typeof(DateTime) || Nullable.GetUnderlyingType(field.Type) == typeof(DateTime))
            {
                var values = new SortedSet<DateTime>();
                var nullOccurs = false;
                foreach (var record in _collection.GetRecords()) {
                    var val = record.GetValue<DateTime?>(field.Name);
                    if (val != null)
                        values.Add((DateTime)val);
                    else 
                        nullOccurs = true;
                }
                if (nullOccurs)
                    ListBoxValue.Items.Add(string.Empty);
                values.ToList().ForEach(x => ListBoxValue.Items.Add(x.ToString()));
            }
            else {
                var values = new SortedSet<string>();
                foreach (var record in _collection.GetRecords()) {
                    var s = record.GetValue(field.Name)?.ToString() ?? string.Empty;
                    values.Add(s);
                }
                values.ToList().ForEach(x => ListBoxValue.Items.Add(x));
            }
        }
    }
}
