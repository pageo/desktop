﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Controls;
using System.Windows.Navigation;
using Fortis.Business.ViewModels;
using Fortis.DataAccess;
using Fortis.WPF.Controls.Dock;
using Microsoft.Practices.ServiceLocation;

namespace Fortis.WPF.Views
{
    public partial class GoogleStreetView : DockAnchorableView
    {
        // ----- Fields
        private readonly string _googleMapsApiKey = ServiceLocator.Current.GetInstance<EnvironmentSettings>().GoogleMapsApiKey;

        // ----- Constructors
        public GoogleStreetView()
        {
            InitializeComponent();

            var viewModel = (GoogleStreetViewModel)DataContext;
            viewModel.PropertyChanged += ViewModelOnPropertyChanged;

            WebBrowser.Navigated += WebBrowser_Navigated;
        }

        // ----- ViewModel
        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            UpdatePosition(((GoogleStreetViewModel)sender).Position);
        }

        // ----- WebBrowser
        private void WebBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            SetSilent(WebBrowser, true);
        }

        // ----- Internal logics
        private void UpdatePosition(Position position)
        {
            WebBrowser.NavigateToString(Properties.Resources.streetview
                .Replace("API_KEY", _googleMapsApiKey)
                .Replace("lat: -33.867386, lng: 151.195767", FormattableString.Invariant($"lat: {position?.X}, lng: {position?.Y}")));
        }
        public static void SetSilent(WebBrowser browser, bool silent)
        {
            if (browser == null)
                throw new ArgumentNullException(nameof(browser));

            var sp = browser.Document as IOleServiceProvider;
            if (sp == null) return;
            var iidIWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");
            var iidIWebBrowser2 = new Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E");

            sp.QueryService(ref iidIWebBrowserApp, ref iidIWebBrowser2, out object webBrowser);
            webBrowser?.GetType().InvokeMember("Silent", BindingFlags.Instance | BindingFlags.Public | BindingFlags.PutDispProperty, null, webBrowser, new object[] { silent });
        }
        [ComImport, Guid("6D5140C1-7436-11CE-8034-00AA006009FA"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        private interface IOleServiceProvider
        {
            [PreserveSig]
            int QueryService([In] ref Guid guidService, [In] ref Guid riid, [MarshalAs(UnmanagedType.IDispatch)] out object ppvObject);
        }
    }
}
