﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using Fortis.Business.Services;
using Fortis.WPF.Controls.Dock;

namespace Fortis.WPF.Views
{
    public partial class ListView : DockDocumentView
    {
        // ----- Constructors
        public ListView()
        {
            InitializeComponent();
        }

        // ----- Internal logics
        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            var labelName = (e.PropertyDescriptor as PropertyDescriptor)?
                .Attributes.OfType<LabelAttribute>()
                .FirstOrDefault()?
                .Name;
            if (!string.IsNullOrEmpty(labelName))
                e.Column.Header = labelName;
        }
    }
}