﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using Fortis.Business.ViewModels;

namespace Fortis.WPF.Views
{
    public partial class LoadWorkingFolderView : UserControl
    {
        // ----- Constructors
        public LoadWorkingFolderView()
        {
            InitializeComponent();
        }

        // ----- DataGrid
        private async void DataGrid_OnDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var loadWorkingFolderViewModel = DataContext as LoadWorkingFolderViewModel;
            if (loadWorkingFolderViewModel != null)
                await loadWorkingFolderViewModel.LoadWorkingFolderCommand.ExecuteAsync();
        }
        private async void DataGrid_OnInitialized(object sender, EventArgs e)
        {
            var loadWorkingFolderViewModel = DataContext as LoadWorkingFolderViewModel;
            if (loadWorkingFolderViewModel != null)
                await loadWorkingFolderViewModel.RefreshWorkingFoldersCommand.ExecuteAsync();
        }
    }
}