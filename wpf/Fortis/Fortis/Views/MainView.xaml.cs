﻿using System;
using System.ComponentModel;
using System.Windows;
using Fortis.Business.Base;
using Fortis.Business.ViewModels.Messages;
using Fortis.WPF.Controls.Dock.Behavior;
using Fortis.WPF.Utils.Themes;
using MahApps.Metro.Controls;
using Microsoft.Practices.ServiceLocation;
using System.IO;
using System.Windows.Input;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace Fortis.WPF.Views
{
    public partial class MainView : MetroWindow
    {
        // ----- Constants
        private const string CustomLayout = "custom-layout.xml";
        private const string PositioningLayout = "positioning-layout.xml";

        // ----- Views
        private LoadView _loadView;
        private WhatIsNewView _whatIsNewView;
        private readonly MapView _mapView = new MapView();
        private readonly CardView _cardView = new CardView();
        private readonly GoogleStreetView _googleStreetView = new GoogleStreetView();
        private readonly ListView _listView = new ListView();
        private readonly MarkerView _markerView = new MarkerView();

        // ----- Fields
        private readonly AvalonDockLayout _dockLayout;
        private string _currentLayout;

        // ----- Contructors
        public MainView(
            IBus bus,
            AvalonDockLayout dockLayout)
        {
            _dockLayout = dockLayout;
            InitializeComponent();

            Application.Current.MainWindow = this;

            bus.Register<ExitMessage>(this, OnExit);

            Loaded += MainView_Loaded;
            KeyDown += MainView_KeyDown; ;
        }

        // ----- Internal logics
        private void OnExit(ExitMessage message)
        {
            Close();
        }

        // ----- Override methods
        protected override void OnClosing(CancelEventArgs e)
        {
            var answer = MessageBox.Show(@"Êtes-vous sûr de vouloir quitter Fortis ?", @"Quitter Fortis", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (answer == MessageBoxResult.No)
                e.Cancel = true;
            base.OnClosing(e);
        }

        // ----- UI Callbacks
        private void MainView_Loaded(object sender, RoutedEventArgs e)
        {
            _loadView = new LoadView { Owner = this };
            _whatIsNewView = new WhatIsNewView { Owner = this };
            
            InitTheme();

            ConfigureLayouts();
        }
        private void MainView_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key) {
                case Key.F8:
                    SetLayout(CustomLayout);
                    break;
                case Key.F9:
                    SetLayout(PositioningLayout);
                    break;
            }
        }

        // ----- Internal logics
        private static void InitTheme()
        {
            var themeManager = ServiceLocator.Current.GetInstance<IThemeManager>();
            if (themeManager.CurrentTheme != null || themeManager.SetCurrentTheme(Properties.Settings.Default.ThemeName)) return;

            Properties.Settings.Default.ThemeName = (string)Properties.Settings.Default.Properties["ThemeName"]?.DefaultValue;
            Properties.Settings.Default.Save();
            if (!themeManager.SetCurrentTheme(Properties.Settings.Default.ThemeName))
                throw new InvalidOperationException("unable to load application theme");
        }
        private void ConfigureLayouts()
        {
            MainMenu.CustomLayoutMenu.IsCheckable = true;
            MainMenu.PositionningLayoutMenu.IsCheckable = true;

            MainMenu.CustomLayoutMenu.Click += (sender, args) => { SetLayout(CustomLayout); };
            MainMenu.PositionningLayoutMenu.Click += (sender, args) => { SetLayout(PositioningLayout); };
            MainMenu.ResetCurrentLayoutMenu.Click += (sender, args) => { ReinitializeCurrentLayout(); };
        }
        private void SetLayout(string layoutFileName)
        {
            SaveCurrentLayout();

            _currentLayout = layoutFileName;
            var userLayoutPath = GetCurrentLayoutFilePath();
            if (!File.Exists(userLayoutPath)) {
                var originLayoutPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", layoutFileName);
                if (File.Exists(originLayoutPath))
                    File.Copy(originLayoutPath, userLayoutPath);
            }
            if (File.Exists(userLayoutPath))
                LoadLayout();

            MainMenu.CustomLayoutMenu.IsChecked = CustomLayout.Equals(layoutFileName);
            MainMenu.PositionningLayoutMenu.IsChecked = PositioningLayout.Equals(layoutFileName);
        }
        private void LoadLayout()
        {
            _dockLayout.CurrentLayout = _currentLayout;
            _dockLayout.LoadLayoutCommand.Execute(AvalonDockingManager);
        }
        private void SaveCurrentLayout()
        {
            if (string.IsNullOrEmpty(_currentLayout)) return;
            var layoutSerializer = new XmlLayoutSerializer(AvalonDockingManager);
            layoutSerializer.Serialize(GetCurrentLayoutFilePath());
        }
        private void ReinitializeCurrentLayout()
        {
            var userLayoutPath = GetCurrentLayoutFilePath();
            var originLayoutPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", _currentLayout);
            if (File.Exists(originLayoutPath))
                File.Copy(originLayoutPath, userLayoutPath, true);
            if (File.Exists(userLayoutPath))
                LoadLayout();
        }
        private string GetCurrentLayoutFilePath()
        {
            return Path.Combine(
                _dockLayout.DirLayout,
                _currentLayout);
        }
    }
}
