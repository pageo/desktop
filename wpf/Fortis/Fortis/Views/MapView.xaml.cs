﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Fortis.Business.Base;
using Fortis.Business.ViewModels;
using Fortis.Business.ViewModels.Events;
using Fortis.WPF.Controls.Dock;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsPresentation;
using Microsoft.Practices.ServiceLocation;
using Point = System.Windows.Point;

namespace Fortis.WPF.Views
{
    public partial class MapView : DockDocumentView
    {
        // ----- Constants
        private const string DefaultAddress = "77 rue du révérend père christian gilbert, Asnières-sur-Seine, France";

        // ----- Fields
        private readonly string[] _providers = {"OpenStreetMap", "BingMap", "BingSatelliteMap", "BingHybridMap", "GoogleMap", "GoogleSatelliteMap", "GoogleHybridMap",  "GoogleTerrainMap"};
        private readonly IBus _bus = ServiceLocator.Current.GetInstance<IBus>();
        private readonly GMapMarker _currentMarker;

        // ----- Constructors
        public MapView()
        {
            InitializeComponent();

            // providers map
            Providers.ItemsSource = GMapProviders.List.Where(x => _providers.Contains(x.Name));
            Providers.SelectedItem = GoogleMapProvider.Instance;

            // zoom  
            MainMap.MinZoom = 0;
            MainMap.MaxZoom = 18;
            MainMap.Zoom = 14;

            // setup zoom min/max
            Scale.Maximum = MainMap.MaxZoom * 100;
            Scale.Minimum = MainMap.MinZoom * 100;
            Scale.Value = MainMap.Zoom * 100;

            // config map
            MainMap.MapProvider = GMapProviders.GoogleMap;
            MainMap.SetPositionByKeywords(DefaultAddress);
            MainMap.DragButton = MouseButton.Right;
            MainMap.ShowCenter = false;
            MainMap.CanDragMap = true;

            // set current marker
            _currentMarker = CreateCurrentGMapMarkerOnMap();

            // maps
            MainMap.OnMapTypeChanged += MainMap_OnMapTypeChanged;
            MainMap.OnPositionChanged += MainMap_OnPositionChanged;
            MainMap.MouseMove += MainMap_MouseMove;
            MainMap.MouseLeftButtonDown += MainMap_MouseLeftButtonDown;

            // viewModel
            var viewModel = (MapViewModel) DataContext;
            viewModel.PropertyChanged += ViewModel_PropertyChanged;

            // tests
            var points = new List<PointLatLng> {
                new PointLatLng(48.866383, 2.323575),
                new PointLatLng(48.863868, 2.321554),
                new PointLatLng(48.861017, 2.330030),
                new PointLatLng(48.863727, 2.331918)
            };
            var polygon = new GMapPolygon(points);
            polygon.RegenerateShape(MainMap);
            MainMap.Markers.Add(polygon);
        }

        // ----- Destructors
        ~MapView() {
            _bus.Unregister(this);

            MainMap.OnMapTypeChanged -= MainMap_OnMapTypeChanged;
        }

        // ------ ViewModel
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var viewModel = sender as MapViewModel;
            if (viewModel?.CurrentPosition == null) return;
            if (e.PropertyName == nameof(viewModel.CurrentPosition)){
                var currentPosition = new PointLatLng(viewModel.CurrentPosition.X, viewModel.CurrentPosition.Y);
                MainMap.Position = _currentMarker.Position = currentPosition;
            }
        }

        // ------ Callbacks
        private void Providers_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainMap.MapProvider = Providers.SelectedItem as GMapProvider;
        }
        private void MainMap_OnMapTypeChanged(GMapProvider type)
        {
            Scale.Maximum = MainMap.MaxZoom * 100;
            Scale.Minimum = MainMap.MinZoom * 100;
        }
        private void MainMap_OnPositionChanged(PointLatLng point)
        {
            _bus.Send(new MapPositionUpdated(point.Lat, point.Lng));
        }
        private void Scale_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            MainMap.Zoom = Scale.Value / 100.0;
        }
        private void Address_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            e.Handled = true;
            var textValue = (sender as TextBox)?.Text;
            if (string.IsNullOrEmpty(textValue) == false) {
                MainMap.SetPositionByKeywords(textValue);
                _currentMarker.Position = MainMap.Position;
            }
        }
        private void MainMap_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var point = e.GetPosition(MainMap);
            var position = MainMap.FromLocalToLatLng((int) point.X, (int) point.Y);
            ((MapViewModel)DataContext).CurrentPosition = new Position(position.Lat, position.Lng);
        }
        private void MainMap_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed) return;
            var p = e.GetPosition(MainMap);
            _currentMarker.Position = MainMap.FromLocalToLatLng((int)p.X, (int)p.Y);
        }

        // ----- Internal logics
        private GMapMarker CreateCurrentGMapMarkerOnMap()
        {
            var marker = CreateMarker(MainMap.Position, "Images/default_marker_32.png");
            MainMap.Markers.Add(marker);
            if (MainMap.Markers.Count > 1)
                MainMap.ZoomAndCenterMarkers(null);
            return marker;
        }
        private static GMapMarker CreateMarker(PointLatLng position, string imagePath)
        {
            return new GMapMarker(position) {
                Offset = new Point(-15, -15),
                ZIndex = int.MaxValue,
                Shape = new Image {
                    ToolTip = position,
                    Source = new BitmapImage(
                        new Uri($"pack://application:,,,/Fortis.WPF;component/{imagePath}"))
                }
            };
        }
    }
}