﻿using Fortis.WPF.Controls.Dock;

namespace Fortis.WPF.Views
{
    public partial class MarkerView : DockAnchorableView
    {
        // ----- Views
        private CreateMarkerStandardView _createMarkerStandardView = new CreateMarkerStandardView();

        // ----- Constructors
        public MarkerView()
        {
            InitializeComponent();
        }
    }
}
