﻿using System;
using System.Windows.Interop;
using Fortis.Business.Base;

namespace Fortis.WPF
{
    public class WpfNotifyApplicationStateChanged : INotifyApplicationStateChanged
    {
        // ----- Events
        public event EventHandler Idle
        {
            add => ComponentDispatcher.ThreadIdle += value;
            remove => ComponentDispatcher.ThreadIdle -= value;
        }
    }
}
