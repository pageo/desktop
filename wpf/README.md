WORK-IN-PROGRESS

![Fortis WPF](Fortis/Fortis/Fortis.ico)

# Fortis WPF

[Fortis WPF](http://olivierpage.fr) is a personal project to learn create WPF apps.

## Acknowledgements

It uses the following open source libraries :

* [StructureMap](https://github.com/structuremap/structuremap) - A Dependency Injection/Inversion of Control tool for .NET .

## Contributing

Please fork this repository and contribute back.

>**NOTE:** Any contributions, large or small, major features, bug fixes, additional
language translations, unit/integration tests are welcomed and appreciated
but will be thoroughly reviewed and discussed.

##Developed By
 * [Olivier Page](https://github.com/opage) - <opage4@gmail.com>

##License
```
The MIT License (MIT)

Copyright (c) 2015 Olivier Page

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
